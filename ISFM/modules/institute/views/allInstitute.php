<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('all_inst'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('inst'); ?>                        
                    </li>
                    <li>
                        <?php echo lang('all_inst'); ?>                        
                    </li>
                   
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN All account list-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('inst_list'); ?> 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th><?php echo lang('tax_id'); ?></th>
                                    <th><?php echo lang('header_sname'); ?></th>
                                    <th><?php echo lang('header_sid'); ?></th>
                                    <th><?php echo lang('header_schoolemail'); ?></th>
                                    <th><?php echo lang('header_sgrname'); ?></th>
                                    <th><?php echo lang('header_city'); ?></th>
                                    <th><?php echo lang('header_pin'); ?></th>
                                    <th><?php echo lang('header_contact'); ?></th>
                                    <th><?php echo lang('header_admin'); ?></th>
                                    <th><?php echo lang('header_adminNo'); ?></th>
                                    <th><?php echo lang('header_description'); ?></th>
                                    <th><?php echo lang('header_registrationdt'); ?></th>
                                    <th><?php echo lang('header_foundyear'); ?></th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($school_info as $row) { ?>
                                    <tr style="">
                                        
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td><?php echo $row['school_name'];?></td>
                                        <td><?php echo $row['school_id'];?></td>
                                        <td><?php echo $row['school_email'];?></td>
                                        <td><?php echo $row['school_grpname'];?></td>
                                        <td><?php echo $row['sub_location'];?></td>
                                        <td><?php echo $row['city'];?></td>
                                        <td><?php echo $row['pincode'];?></td>
                                        <td><?php echo $row['admin_name'];?></td>
                                        <td><?php echo $row['admin_num'];?></td>
                                        <td><?php echo $row['description'];?></td>
                                        <td><?php echo $row['reg_date'];?></td>
                                        <td><?php echo $row['foundation_year'];?></td>

                                    </tr>
                                <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- END CONTENT -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL script -->
<script type="text/javascript" src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/components-form-tools.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>

<script type="text/javascript">
    

    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>