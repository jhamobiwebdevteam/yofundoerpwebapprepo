<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>

<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        
        
        <div class="row">
            <?php ?>
            <div class="col-md-12">
                <div class="portlet box green row">
                    <div class="portlet-title">
                        <div class="caption col-md-10">
                            <?php echo $course_name;?>
                        </div>  
                    </div>
                    <div class="portlet-body">
                        <table class="table" id="sample_1">
                            <thead>
                                <tr>
                                    <th>Heading</th> <?php 
                                    for($i=0; $i< count($data['heading']); $i++) { ?>
                                    <th><?php echo $data['heading'][$i] .'('.$data['outof'][$i].')'; ?></th>
                                    <?php  } ?>
                                    <th>Attempted Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td> <?php echo $quiz_name; ?></td>
                                         <?php for($i=0; $i< count($data['obtained']); $i++) { ?>
                                            <td><?php echo $data['obtained'][$i]; ?> </td>
                                            <?php } ?>
                                        <td><?php echo $status; ?></td>
                                    </tr>   
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        

<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT -->


        
    </div>
</div>

<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
