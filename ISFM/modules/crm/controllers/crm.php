<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Crm extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('crmmodule');
        $this->load->model('common');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    //This function add lead in this function
    function addNewLead() { 
       /* if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }*/
        if ($this->input->post('submit', TRUE)) {
            $leadInfo = array(
                'fname' => $this->db->escape_like_str($this->input->post('first_name', TRUE)),
                'lname' => $this->db->escape_like_str($this->input->post('last_name', TRUE)),
                'mobile' => $this->db->escape_like_str($this->input->post('mobile', TRUE)),
                'email' => $this->db->escape_like_str($this->input->post('email', TRUE)),
                'lead_source' => $this->db->escape_like_str($this->input->post('lead_sourse', TRUE)),
                'class' => $this->db->escape_like_str($this->input->post('class', TRUE)),
                'location' => $this->db->escape_like_str($this->input->post('location', TRUE)),
                'lead_stage' => 1
            );
            if ($this->db->insert('lead', $leadInfo)) {
                $insert_id = $this->db->insert_id();
                $leadHis = array(
                    'lead_id' => $insert_id,
                    'lead_activity_id' => 1,
                    'stage_id' => 1,
                    'followup_remark' => "",
                    'lead_remark' => "",
                    'followup' => ""
                );
                $this->db->insert('lead_history', $leadHis);
               // $data['allAccount'] = $this->common->getAllData('account_title');
               $data['classTile'] = $this->common->getAllData('department');
               $data['leadSource'] = $this->common->getAllData('lead_source');
               $data['leadLocation'] = $this->common->getAllData('lead_location');
                $data['message'] = '<div class="alert alert-success alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
								<strong>Success ! </strong> Lead added successfully. 
							</div>';
                $this->load->view('temp/header');
                $this->load->view('addNewLead',$data);
                $this->load->view('temp/footer');
            }
        }else {
            $data['classTile'] = $this->common->getAllData('department');
            $data['leadSource'] = $this->common->getAllData('lead_source');
            $data['leadLocation'] = $this->common->getAllData('lead_location');
            $this->load->view('temp/header');
            $this->load->view('addNewLead',$data);
            $this->load->view('temp/footer');
        }
    }

    function editLead() {

        $id = $this->input->get('id');
        $update_lead_sourceId = $this->input->post('lead_sourse'); 
        $update_locationId = $this->input->post('location');
        //print_r($update_lead_sourceId);die();
        if ($this->input->post('submit', TRUE)) 
        {
        
            $leadInfo = array(
                'fname' => $this->db->escape_like_str($this->input->post('first_name', TRUE)),
                'lname' => $this->db->escape_like_str($this->input->post('last_name', TRUE)),
                'mobile' => $this->db->escape_like_str($this->input->post('mobile', TRUE)),
                'email' => $this->db->escape_like_str($this->input->post('email', TRUE)),
               'lead_source' => $this->db->escape_like_str($this->input->post('lead_sourse',TRUE)),
                'class_id' => $this->db->escape_like_str($this->input->post('class', TRUE)),
                'location' => $this->db->escape_like_str($this->input->post('location', TRUE))
            );

            
            $result = array();
            $beforeUpdateLead = $this->crmmodule->LeadsById($id);
            $get_leadSource_namedb=$this->crmmodule->LeadtogetSource($update_lead_sourceId);
            $get_locationNamedb = $this->crmmodule->LeadtogetLocation($update_locationId);
            //echo "<pre>";print_r($get_leadSource_namedb[0]['name']);die();
            if($get_leadSource_namedb[0]['name']!=$beforeUpdateLead[0]['lead_source'])
            {
                $result['lead_source']=$get_leadSource_namedb[0]['name'];
            }
            if($leadInfo['class_id']!=$beforeUpdateLead[0]['class_id'])
            {
                $result['class_id']=$leadInfo['class_id'];
            }
            if($get_locationNamedb[0]['name']!=$beforeUpdateLead[0]['location'])
            {
                $result['location']=$get_locationNamedb[0]['name'];
            }
            $json_data= json_encode($result);

            //echo $json_data;die();

            $leadHistoryData = array(
                                'lead_id' => $id,
                                'action_description' => $json_data,
                                'lead_activity_id'=>'1',
                                'stage_id'=>'5');

            $this->db->insert('lead_history', $leadHistoryData);

            $this->db->where('id', $id);
            $this->db->set('updated_at',date('Y-m-d H:i:s'));
            $this->db->update('lead', $leadInfo);
            $data['lead'] = $this->crmmodule->allLeads();
            $this->load->view('temp/header');
            $this->load->view('displayLeads', $data);
            $this->load->view('temp/footer');
        }else {
            $data['classTile'] = $this->common->getAllData('department');
            $data['leadSource'] = $this->common->getAllData('lead_source');
            $data['leadLocation'] = $this->common->getAllData('lead_location');
            $data['lead'] = $this->crmmodule->LeadsById($id);
            $this->load->view('temp/header');
            $this->load->view('editNewLead', $data);
            $this->load->view('temp/footer');
        }
    }

    
    function displayLeads() { 
        $data['lead'] = $this->crmmodule->allLeads();
        $this->load->view('temp/header');
        $this->load->view('displayLeads', $data);
        $this->load->view('temp/footer');
    }
    public function registeredStudentList()
    {     
        $data['student_info'] = $this->crmmodule->RegisteredStudents();
        $this->load->view('temp/header');
        $this->load->view('registeredStudentList', $data);
        $this->load->view('temp/footer');
    }
    
    public function registeredStudentListLms(){  
        $postData = '';
        $data['stud_info'] ='';
        $urldata = $this->config->item('api_url').'get_schools.php';
        $data['data'] = $this->common->schoolname_data($urldata,$postData);
        if ($this->input->post('submit', TRUE)){   
            $cids = $this->input->post('course_ids');  
            $urldata = $this->config->item('api_url').'get_enrolled_student_by_course.php'; 
            $postData = '{  "course_ids": "'.$cids.'"}';
            $res = $this->common->schoolname_data($urldata,$postData);
            // print_r($res['data']); die('---');
            $data['stud_info'] = $res['data'];
            $data['cname'] = $res['cname'];
            $this->load->view('temp/header');
            $this->load->view('registeredStudentListLms', $data);
            $this->load->view('temp/footer');
        }else{
            $data['cname'] = '';
            $this->load->view('temp/header');
            $this->load->view('registeredStudentListLms', $data);
            $this->load->view('temp/footer');
        }
    }
    public function getStudentsDetail()
    {        
        $get_StudentsInfo = $this->input->get('id');
        $data['stud_info'] = $this->crmmodule->studDatafromdb($get_StudentsInfo);
        $data['student_history'] = $this->crmmodule->getStudentsHistory($get_StudentsInfo);
        $this->load->view('temp/header');
        $this->load->view('displayStudInfo',$data);
        $this->load->view('temp/footer');
    }
    public function edit_data()
    {

        $class_id = $this->db->escape_like_str($this->input->post('class', TRUE));
        $update_id = $this->input->post('ed_id');
            
            if($this->input->post('update', TRUE)) 
            {
                $studentsInfo = array(
                    'academic_year' => $this->db->escape_like_str($this->input->post('academic_year', TRUE)),
                    'class_id' => $this->db->escape_like_str($class_id, TRUE),
                    'first_name'=>$this->db->escape_like_str($this->input->post('first_name', TRUE)),
                   
                    'middle_name'=> $this->db->escape_like_str($this->input->post('mid_name', TRUE)),
                    'last_name'=> $this->db->escape_like_str($this->input->post('last_name', TRUE)),
                    'dob' => $this->db->escape_like_str($this->input->post('dob', TRUE)),
                    /*'email' => $this->db->escape_like_str($this->input->post('email', TRUE)),*/
                    'nationality' => $this->db->escape_like_str($this->input->post('nationality', TRUE)),
                    'gender' => $this->db->escape_like_str($this->input->post('gender', TRUE)),
                    'caste' => $this->db->escape_like_str($this->input->post('caste_cat', TRUE)),
                    'religion' => $this->db->escape_like_str($this->input->post('religion', TRUE)),
                    /*'student_photo' => $uploadFileInfo['file_name'],*/
                    'birth_place' => $this->db->escape_like_str($this->input->post('birth_place', TRUE)),
                    'address' => $this->db->escape_like_str($this->input->post('address', TRUE)),
                    'pin_code' => $this->db->escape_like_str($this->input->post('pin_code', TRUE)),
                    'fathers_firstname' => $this->db->escape_like_str($this->input->post('fathers_firstname', TRUE)),
                    'fathers_midname' => $this->db->escape_like_str($this->input->post('fathers_midname', TRUE)),
                    'fathers_lastname' => $this->db->escape_like_str($this->input->post('fathers_lastname', TRUE)),
                    /*'fathers_mobile' => $this->db->escape_like_str($this->input->post('fathers_mob', TRUE)),*/
                    'fathers_occup' => $this->db->escape_like_str($this->input->post('fathers_occup', TRUE)),
                    'mothers_occup' => $this->db->escape_like_str($this->input->post('mothers_occup', TRUE)),
                    'mothers_firstname' => $this->db->escape_like_str($this->input->post('mothers_firstname', TRUE)),
                    'mothers_midname' => $this->db->escape_like_str($this->input->post('mothers_midname', TRUE)),
                    'mothers_lastname' => $this->db->escape_like_str($this->input->post('mothers_lastname', TRUE)),
                    'mothers_mobile' => $this->db->escape_like_str($this->input->post('mothers_mobile', TRUE)),
                    'mothers_qualification' => $this->db->escape_like_str($this->input->post('mothers_qualification', TRUE)),
                    'fathers_qualification' => $this->db->escape_like_str($this->input->post('fathers_qualification', TRUE)),
                    'mother_toung' => $this->db->escape_like_str($this->input->post('mother_toung', TRUE)),
                    'anuual_income' => $this->db->escape_like_str($this->input->post('anuual_income', TRUE)),
                    'isthr_medical_condition' => $this->db->escape_like_str($this->input->post('isthr_medical_condition', TRUE)),
                    'isthr_undergoing_treatment' => $this->db->escape_like_str($this->input->post('isthr_undergoing_treatment', TRUE)),
                    'is_child_taking_medication' =>  $this->db->escape_like_str($this->input->post('is_child_taking_medication', TRUE)),
                    'food_allergy' =>  $this->db->escape_like_str($this->input->post('food_allergy', TRUE)),
                    'medical_fitness_cert' => $this->db->escape_like_str($this->input->post('medical_fitness_cert', TRUE)),
                    'adoption_document' => $this->db->escape_like_str($this->input->post('adoption_document', TRUE)),
                    'passport_photos' => $this->db->escape_like_str($this->input->post('passport_photos', TRUE)),
                    'passport_pic_both_parent' =>  $this->db->escape_like_str($this->input->post('passport_pic_both_parent', TRUE)),
                    'original_birth_cert' => $this->db->escape_like_str($this->input->post('original_birth_cert', TRUE)),
                    'pan_card_copy_parent' => $this->db->escape_like_str($this->input->post('pan_card_copy_parent', TRUE)),
                    'adhar_copy_parent'=> $this->db->escape_like_str($this->input->post('adhar_copy_parent', TRUE)),
                    'caste_certificate' => $this->db->escape_like_str($this->input->post('caste_certificate', TRUE)),
                    'original_bonafied' => $this->db->escape_like_str($this->input->post('original_bonafied', TRUE)),
                    'original_tc' => $this->db->escape_like_str($this->input->post('original_tc', TRUE)),
                    'residence_proof' => $this->db->escape_like_str($this->input->post('residence_proof', TRUE)),
                    'if_foreign_nationals_copy' => $this->db->escape_like_str($this->input->post('if_foreign_nationals_copy', TRUE)),
                    'copy_report_card' => $this->db->escape_like_str($this->input->post('copy_report_card', TRUE)),
                    'updated_at' => $this->db->escape_like_str(date('Y-m-d H:i:s')),

                );
                
                $user = $this->ion_auth->user()->row(); $userId = $user->username;
                $username = $this->input->post('first_name') . ' ' . $this->input->post('last_name');

                $res=array();
                $beforeupdateVal = $this->crmmodule->studDatafromdb($update_id);
                //echo "<pre>";print_r($beforeupdateVal[0]['middle_name']);die();
                if($studentsInfo['middle_name']!=$beforeupdateVal[0]['middle_name'])
                {
                    $res['middle_name']=$studentsInfo['middle_name'];
                    
                }
                if($studentsInfo['nationality']!=$beforeupdateVal[0]['nationality'])
                {
                    $res['nationality']=$studentsInfo['nationality'];
                    
                }
                if($studentsInfo['caste']!=$beforeupdateVal[0]['caste'])
                {
                    $res['caste']=$studentsInfo['caste'];
                    
                }
                if($studentsInfo['religion']!=$beforeupdateVal[0]['religion'])
                {
                    $res['religion']=$studentsInfo['religion'];
                    
                }
                if($studentsInfo['birth_place']!=$beforeupdateVal[0]['birth_place'])
                {
                    $res['birth_place']=$studentsInfo['birth_place'];
                    
                }
                if($studentsInfo['address']!=$beforeupdateVal[0]['address'])
                {
                    $res['address']=$studentsInfo['address'];
                    
                }
                if($studentsInfo['pin_code']!=$beforeupdateVal[0]['pin_code'])
                {
                    $res['pin_code']=$studentsInfo['pin_code'];
                    
                }
                if($studentsInfo['fathers_firstname']!=$beforeupdateVal[0]['fathers_firstname'])
                {
                    $res['fathers_firstname']=$studentsInfo['fathers_firstname'];
                    
                }
                if($studentsInfo['fathers_midname']!=$beforeupdateVal[0]['fathers_midname'])
                {
                    $res['fathers_midname']=$studentsInfo['fathers_midname'];
                    
                }
                if($studentsInfo['fathers_lastname']!=$beforeupdateVal[0]['fathers_lastname'])
                {
                    $res['fathers_lastname']=$studentsInfo['fathers_lastname'];
                    
                }
                if($studentsInfo['fathers_occup']!=$beforeupdateVal[0]['fathers_occup'])
                {
                    $res['fathers_occup']=$studentsInfo['fathers_occup'];
                    
                }
                if($studentsInfo['mothers_occup']!=$beforeupdateVal[0]['mothers_occup'])
                {
                    $res['mothers_occup']=$studentsInfo['mothers_occup'];
                    
                }
                if($studentsInfo['mothers_firstname']!=$beforeupdateVal[0]['mothers_firstname'])
                {
                    $res['mothers_firstname']=$studentsInfo['mothers_firstname'];
                    
                }
                if($studentsInfo['mothers_midname']!=$beforeupdateVal[0]['mothers_midname'])
                {
                    $res['mothers_midname']=$studentsInfo['mothers_midname'];
                    
                }
                if($studentsInfo['mothers_lastname']!=$beforeupdateVal[0]['mothers_lastname'])
                {
                    $res['mothers_lastname']=$studentsInfo['mothers_lastname'];
                    
                }
                if($studentsInfo['mothers_mobile']!=$beforeupdateVal[0]['mothers_mobile'])
                {
                    $res['mothers_mobile']=$studentsInfo['mothers_mobile'];
                    
                }
                if($studentsInfo['mothers_qualification']!=$beforeupdateVal[0]['mothers_qualification'])
                {
                    $res['mothers_qualification']=$studentsInfo['mothers_qualification'];
                    
                }
                if($studentsInfo['fathers_qualification']!=$beforeupdateVal[0]['fathers_qualification'])
                {
                    $res['fathers_qualification']=$studentsInfo['fathers_qualification'];
                    
                }
                if($studentsInfo['mother_toung']!=$beforeupdateVal[0]['mother_toung'])
                {
                    $res['mother_toung']=$studentsInfo['mother_toung'];
                    
                }
                if($studentsInfo['anuual_income']!=$beforeupdateVal[0]['anuual_income'])
                {
                    $res['anuual_income']=$studentsInfo['anuual_income'];
                    
                }
                if($studentsInfo['isthr_medical_condition']!=$beforeupdateVal[0]['isthr_medical_condition'])
                {
                    $res['isthr_medical_condition']=$studentsInfo['isthr_medical_condition'];                  
                }
                if($studentsInfo['isthr_undergoing_treatment']!=$beforeupdateVal[0]['isthr_undergoing_treatment'])
                {
                    $res['isthr_undergoing_treatment']=$studentsInfo['isthr_undergoing_treatment'];                    
                }
                if($studentsInfo['is_child_taking_medication']!=$beforeupdateVal[0]['is_child_taking_medication'])
                {
                    $res['is_child_taking_medication']=$studentsInfo['is_child_taking_medication'];                    
                }
                if($studentsInfo['food_allergy']!=$beforeupdateVal[0]['food_allergy'])
                {
                    $res['food_allergy']=$studentsInfo['food_allergy'];                    
                }
                if($studentsInfo['medical_fitness_cert']!=$beforeupdateVal[0]['medical_fitness_cert'])
                {
                    $res['medical_fitness_cert']=$studentsInfo['medical_fitness_cert'];                    
                }
                if($studentsInfo['adoption_document']!=$beforeupdateVal[0]['adoption_document'])
                {
                    $res['adoption_document']=$studentsInfo['adoption_document'];                    
                }
                if($studentsInfo['passport_photos']!=$beforeupdateVal[0]['passport_photos'])
                {
                    $res['passport_photos']=$studentsInfo['passport_photos'];                    
                }
                if($studentsInfo['passport_pic_both_parent']!=$beforeupdateVal[0]['passport_pic_both_parent'])
                {
                    $res['passport_pic_both_parent']=$studentsInfo['passport_pic_both_parent'];                    
                }
                if($studentsInfo['original_birth_cert']!=$beforeupdateVal[0]['original_birth_cert'])
                {
                    $res['original_birth_cert']=$studentsInfo['original_birth_cert'];                    
                }
                if($studentsInfo['pan_card_copy_parent']!=$beforeupdateVal[0]['pan_card_copy_parent'])
                {
                    $res['pan_card_copy_parent']=$studentsInfo['pan_card_copy_parent'];   
                }
                if($studentsInfo['adhar_copy_parent']!=$beforeupdateVal[0]['adhar_copy_parent'])
                {
                    $res['adhar_copy_parent']=$studentsInfo['adhar_copy_parent'];
                }
                if($studentsInfo['caste_certificate']!=$beforeupdateVal[0]['caste_certificate'])
                {
                    $res['caste_certificate']=$studentsInfo['caste_certificate'];
                }
                if($studentsInfo['original_bonafied']!=$beforeupdateVal[0]['original_bonafied'])
                {
                    $res['original_bonafied']=$studentsInfo['original_bonafied'];
                }
                if($studentsInfo['original_tc']!=$beforeupdateVal[0]['original_tc'])
                {
                    $res['original_tc']=$studentsInfo['original_tc'];
                }
                if($studentsInfo['residence_proof']!=$beforeupdateVal[0]['residence_proof'])
                {
                    $res['residence_proof']=$studentsInfo['residence_proof'];
                }
                if($studentsInfo['if_foreign_nationals_copy']!=$beforeupdateVal[0]['if_foreign_nationals_copy'])
                {
                    $res['if_foreign_nationals_copy']=$studentsInfo['if_foreign_nationals_copy'];
                }
                if($studentsInfo['copy_report_card']!=$beforeupdateVal[0]['copy_report_card'])
                {
                    $res['copy_report_card']=$studentsInfo['copy_report_card'];
                }
                $json =  json_encode($res);

                $student_history = array(
                    'registration_id' => $this->db->escape_like_str($update_id, TRUE),
                    'students_name' => $this->db->escape_like_str($username, TRUE),
                    'action' => $this->db->escape_like_str('Students Info Updated', TRUE),
                    'by_whom'=> $this->db->escape_like_str($user->username, TRUE),
                    'description' => $json,
                );

                if ($this->db->insert('registration_history', $student_history))
                { 
                    $update_info = $this->crmmodule->StudentDetailUpadte($update_id,$studentsInfo);
                    if($update_info)
                    {
                        $data['message'] = '<div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                        <strong>Success ! </strong>  Record updated successfully. 
                        </div>';
                        redirect('crm/registeredStudent',$data);
                    }
                    else
                    {
                        $data['message'] = '<div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                        <strong>Failed ! </strong>Error occured while updateing. 
                        </div>';
                        redirect('crm/registeredStudent',$data);
                    }
                }
            }
            
    }
    public function StudeInfoDelete()
    {
        $sudentsId = $this->input->get('id');
       /* $userId = $this->input->get('uid');*/
        $this->db->delete('registration_student', array('id' => $sudentsId));
        /*$this->db->delete('users', array('id' => $userId));*/
        redirect('crm/registeredStudent', 'refresh');
    
    }
    public function approvedList()
    {
        $data['approvedStudent'] = $this->crmmodule->approvStudList();
        $this->load->view('temp/header');
        $this->load->view('approvedList',$data);
        $this->load->view('temp/footer');
    }
    public function update()
    {
        $status = $this->input->post('status');
        $stud_id = $this->input->post('id');

        /*$student_history = array(
                    'registration_id' => $this->db->escape_like_str($stud_id, TRUE),
                    'students_name' => $this->db->escape_like_str($username, TRUE),
                    'action' => $this->db->escape_like_str('Students status Updated', TRUE),
                    'by_whom'=> $this->db->escape_like_str($user->username, TRUE),
                    'description' =>$this->db->escape_like_str('Students status changed', TRUE),
                );*/
        $upd_status = $this->crmmodule->updateStudStatus($stud_id,$status);
        if($upd_status)
        {

            redirect('crm/registeredStudent');
        }
        else
        {
            redirect('crm/registeredStudent');
        }
    }
    public function checkEmail(){
        $value = $this->input->get('val');
        $query = $this->db->query("SELECT email FROM lead WHERE email='$value'");
        foreach($query->result_array() as $row){
            $email = $row['email'];
        }
        if(!empty($email)){           
            echo '<div class="row"><div class="alert alert-danger">
                    <strong>Notic: </strong> This email <b>'.$value.'</b> have in our database from past. One email account you can not use for two time.
                </div></div>';
        }
    }

    public function checkMobile(){
        $value = $this->input->get('val');
        if(strlen($value) != 10){
            echo '<div class="row"><div class="alert alert-danger">
            <strong>Notic: </strong> Please enter 10 digit phone number.
            </div></div>';
        }else{
            $query = $this->db->query("SELECT mobile FROM lead WHERE mobile='$value'");
            foreach($query->result_array() as $row){
                $mobile = $row['mobile'];
            }
            if(!empty($mobile)){           
                echo '<div class="row"><div class="alert alert-danger">
                        <strong>Notic: </strong> This phone number <b>'.$value.'</b> have in our database from past. One phone number you can not use for two time.
                    </div></div>';
            }
        }
    }
    
    public function checkName(){
        $value = $this->input->get('val');
        $tbl_name = $this->input->get('tbl_name');
        $query = $this->db->query("SELECT name FROM $tbl_name WHERE name='$value'");
        foreach($query->result_array() as $row){
            $name = $row['name'];
        }
        if(!empty($name)){           
            echo '<div class="row"><div class="alert alert-danger">
                    <strong>Notic: </strong> This name <b>'.$value.'</b> have in our database from past. One name you can not use for two time.
                </div></div>';
        }
        
    }

    //This function will delete master entry.
    public function deleteEntry() {
        $id = $this->input->get('id', TRUE);
        $tbl_name = $this->input->get('tbl_name', TRUE);
        $this->db->delete($tbl_name, array('id' => $id));
        $data['allRecords'] = $this->common->getAllData($tbl_name);
        $data['message'] = '<div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                            <strong>Success ! </strong>  Record deleted successfully. 
                    </div>';
        $this->load->view('temp/header');
        if($tbl_name === 'lead_source'){
            $this->load->view('addLeadSource',$data);
        }else if($tbl_name === 'lead_source'){
            $this->load->view('addLeadStage',$data);
        }else{
            $this->load->view('addLocation', $data);
        }
        $this->load->view('temp/footer');
    }

    public function leadDetails() {
        $id = $this->input->get('id');
        $stageId = $this->input->get('stage');
        $data['lead'] = $this->crmmodule->LeadsById($id);
        $data['leadHistory'] = $this->crmmodule->LeadHisById($id);
        $data['stages'] = $this->common->getAllData('lead_stage');
        $data['leadSource'] = $this->common->getAllData('lead_source');
        $data['leadLocation'] = $this->common->getAllData('lead_location');
        $data['classTile'] = $this->common->getAllData('department');
        $data['activity'] = $this->crmmodule->getDataExceptId(1,'lead_activity');
        $data['stages1'] = $this->crmmodule->getDataExceptId($stageId,'lead_stage');
         $this->load->view('temp/header');
        $this->load->view('leadDetails', $data);
        $this->load->view('temp/footer');
    }
    public function activity() {
        $lead_stage = $this->input->post('lead_stage', TRUE);
        $stageId = $this->input->post('stageId', TRUE);
        $lead_activity = $this->input->post('lead_activity', TRUE);
        $act_remark = $this->input->post('act_remark', TRUE);
        $fdate = $this->input->post('fdate', TRUE);
        $ftime = $this->input->post('ftime', TRUE);
        $leadId = $this->input->post('leadId', TRUE);
        $fremark = $this->input->post('fremark', TRUE);
        if($lead_stage == ''){            
            $lead_stage = $stageId;
        }
        $arr = array(
            'lead_stage' => $lead_stage
        );
        $this->db->where('id', $leadId);
        $this->db->update('lead', $arr);
        $leadHistory = array(
            'lead_id' => $leadId,
            'lead_activity_id' => $lead_activity,
            'lead_remark' => $act_remark,
            'stage_id' => $lead_stage,
            'followup' => date("Y-m-d H:i:s", strtotime($fdate)),
            'followup_remark' => $fremark
        );
       // die(print_r($leadHistory));
        if ($this->db->insert('lead_history', $leadHistory)) {
            echo "<script type='text/javascript'>alert('Lead Stage Updated successfully.');
            window.location.href='crm/displayLeads/';
            </script>";           
        }
    }

    public function syncStudents() {
        $data = urldecode($_REQUEST['studArr']);
        $studArr = json_decode($data);
        $cnt = count($studArr);  
        $sec = $studArr[0]->section_path;
        $secArr = explode('/', $sec);    
        $sem_id = end($secArr);
        for($i=0; $i<$cnt;$i++){
            $section_path = $studArr[$i]->section_path;
            $dept = $studArr[$i]->grades;
            $username = $studArr[$i]->username;
            $email = $studArr[$i]->email;
            $password = "Password01$";
            $phone =$studArr[$i]->phone2;
            $idArr = $this->common->getSingleField('id','users','email',$email);            
            if(empty($idArr)){ 
                $pathArr = explode('/',$section_path);
                $inst_lms_id = $pathArr[1];
                $dept_lms_id = $pathArr[2];
                // echo $email.'---';
                $cdata = $this->common->getTwoField('lms_id','idnumber','institute','lms_id',$inst_lms_id);
                $cidnumber = $cdata[0]->idnumber;
                $ddata = $this->common->getTwoField('lms_id','idnumber','department','lms_id',$dept_lms_id);
                $didnumber = $ddata[0]->idnumber;
                
                $last_id = $this->common->getLatestStudId();
                $studId = '';
                if($last_id != ''){ 
                    $n = substr($last_id, -3);
                    // $n = '003';
                    $x = substr($n, -0, 1);
                    $y = substr($n, -2, 1);
                    $z = substr($n, -1);
                    if(($x == 0) &&($y == 0)){
                        $z++;
                        if(strlen($z) == 2){
                            $n = '0'.$z;
                        }else{
                            $n = '00'.$z;
                        }
                    }else if(($x == 0) &&($y != 0)){
                        $n = substr($last_id, -2);
                        $n++;
                        if(strlen($n) == 3){
                            $n;
                        }else{
                            $n = '0'.$n;
                        }
                    }else{
                        $n++;
                    }
                    $studId = $cidnumber.$didnumber.$n;
                }else{
                    $studId = $cidnumber.$didnumber.'001';
                }
                // die($n.'---'.$x.'---'.$y.'---'.$z.'==='.$studId);

                //This array information's are sending to "user" table as a core information as a user this system
                $additional_data = array(
                    'first_name' => $studArr[$i]->firstname,
                    'last_name' => $studArr[$i]->lastname,
                    'phone' => $phone,
                    'profile_image' => '',
                );
                $group_ids = array('group_id' => 3);
                $class_title = $this->common->class_title($sem_id);
                if ($this->ion_auth->register($username, $password, $email, $additional_data, $group_ids)) {
                    $userid = $this->common->usersId();
                    //This array information's are sending to "student_info" table.
                    $studentsInfo = array(
                        'college_id' => $secArr[1],
                        'dept_id' => $secArr[2],
                        'year_id' => $secArr[3],
                        'sem_id' => $secArr[4],
                        'course' => '',
                        'user_id' => $userid,
                        'class_id' => $sem_id,
                        'first_name'=> $studArr[$i]->firstname,
                        'student_id' => $studId,
                        'last_name'=> $studArr[$i]->lastname,                        
                        'email' => $email,
                    );
                    if ($this->db->insert('registration_student', $studentsInfo)) {
                        $student_info_id = $this->db->insert_id();
                        $additionalData3 = array(
                            'year' => date('Y'),
                            'user_id' => $userid,
                            'roll_number' => '',
                            'student_id' => $studId,
                            'class_title' => $class_title,
                            'class_id' => $sem_id,
                            'section' => '',
                            'student_title' => $studArr[$i]->firstname.' '.$studArr[$i]->lastname,
                        );

                        if ($this->db->insert('class_students', $additionalData3)) {
                            $studentAmount = $this->common->classStudentAmount($sem_id);
                            $clas_info = array(
                                'student_amount' => $studentAmount
                            );
                            $this->db->where('lms_id', $sem_id);
                            if ($this->db->update('department', $clas_info)) {
                                $student_access = array(
                                    'user_id' => $this->db->escape_like_str($userid),
                                    'group_id' => $this->db->escape_like_str(3),
                                    'das_top_info' => $this->db->escape_like_str(0),
                                    'das_grab_chart' => $this->db->escape_like_str(0),
                                    'das_class_info' => $this->db->escape_like_str(0),
                                    'das_message' => $this->db->escape_like_str(1),
                                    'das_employ_attend' => $this->db->escape_like_str(0),
                                    'das_notice' => $this->db->escape_like_str(1),
                                    'das_calender' => $this->db->escape_like_str(1),
                                    'admission' => $this->db->escape_like_str(0),
                                    'all_student_info' => $this->db->escape_like_str(0),
                                    'stud_edit_delete' => $this->db->escape_like_str(0),
                                    'stu_own_info' => $this->db->escape_like_str(1),
                                    'teacher_info' => $this->db->escape_like_str(1),
                                    'add_teacher' => $this->db->escape_like_str(0),
                                    'teacher_details' => $this->db->escape_like_str(0),
                                    'teacher_edit_delete' => $this->db->escape_like_str(0),
                                    'all_parents_info' => $this->db->escape_like_str(0),
                                    'own_parents_info' => $this->db->escape_like_str(1),
                                    'make_parents_id' => $this->db->escape_like_str(0),
                                    'parents_edit_dlete' => $this->db->escape_like_str(0),
                                    'add_new_class' => $this->db->escape_like_str(0),
                                    'all_class_info' => $this->db->escape_like_str(0),
                                    'class_details' => $this->db->escape_like_str(0),
                                    'class_delete' => $this->db->escape_like_str(0),
                                    'class_promotion' => $this->db->escape_like_str(0),
                                    'assin_optio_sub' => $this->db->escape_like_str(0),
                                    'add_class_routine' => $this->db->escape_like_str(0),
                                    'own_class_routine' => $this->db->escape_like_str(1),
                                    'all_class_routine' => $this->db->escape_like_str(0),
                                    'rutin_edit_delete' => $this->db->escape_like_str(0),
                                    'attendance_preview' => $this->db->escape_like_str(0),
                                    'take_studence_atten' => $this->db->escape_like_str(0),
                                    'edit_student_atten' => $this->db->escape_like_str(0),
                                    'add_employee' => $this->db->escape_like_str(0),
                                    'employee_list' => $this->db->escape_like_str(0),
                                    'employ_attendance' => $this->db->escape_like_str(0),
                                    'empl_atte_view' => $this->db->escape_like_str(0),
                                    'add_subject' => $this->db->escape_like_str(0),
                                    'all_subject' => $this->db->escape_like_str(0),
                                    'make_suggestion' => $this->db->escape_like_str(0),
                                    'all_suggestion' => $this->db->escape_like_str(0),
                                    'own_suggestion' => $this->db->escape_like_str(1),
                                    'add_exam_gread' => $this->db->escape_like_str(0),
                                    'exam_gread' => $this->db->escape_like_str(0),
                                    'add_exam_routin' => $this->db->escape_like_str(0),
                                    'all_exam_routine' => $this->db->escape_like_str(0),
                                    'own_exam_routine' => $this->db->escape_like_str(1),
                                    'exam_attend_preview' => $this->db->escape_like_str(0),
                                    'approve_result' => $this->db->escape_like_str(0),
                                    'view_result' => $this->db->escape_like_str(1),
                                    'all_mark_sheet' => $this->db->escape_like_str(0),
                                    'own_mark_sheet' => $this->db->escape_like_str(1),
                                    'take_exam_attend' => $this->db->escape_like_str(0),
                                    'change_exam_attendance' => $this->db->escape_like_str(0),
                                    'make_result' => $this->db->escape_like_str(0),
                                    'add_category' => $this->db->escape_like_str(0),
                                    'all_category' => $this->db->escape_like_str(1),
                                    'edit_delete_category' => $this->db->escape_like_str(0),
                                    'add_books' => $this->db->escape_like_str(0),
                                    'all_books' => $this->db->escape_like_str(1),
                                    'edit_delete_books' => $this->db->escape_like_str(0),
                                    'add_library_mem' => $this->db->escape_like_str(0),
                                    'memb_list' => $this->db->escape_like_str(0),
                                    'issu_return' => $this->db->escape_like_str(0),
                                    'add_dormitories' => $this->db->escape_like_str(0),
                                    'add_set_dormi' => $this->db->escape_like_str(0),
                                    'set_member_bed' => $this->db->escape_like_str(0),
                                    'dormi_report' => $this->db->escape_like_str(1),
                                    'add_transport' => $this->db->escape_like_str(0),
                                    'all_transport' => $this->db->escape_like_str(1),
                                    'transport_edit_dele' => $this->db->escape_like_str(0),
                                    'add_account_title' => $this->db->escape_like_str(0),
                                    'edit_dele_acco' => $this->db->escape_like_str(0),
                                    'trensection' => $this->db->escape_like_str(0),
                                    'fee_collection' => $this->db->escape_like_str(0),
                                    'all_slips' => $this->db->escape_like_str(0),
                                    'own_slip' => $this->db->escape_like_str(1),
                                    'slip_edit_delete' => $this->db->escape_like_str(0),
                                    'pay_salary' => $this->db->escape_like_str(0),
                                    'creat_notice' => $this->db->escape_like_str(0),
                                    'send_message' => $this->db->escape_like_str(0),
                                    'vendor' => $this->db->escape_like_str(0),
                                    'delet_vendor' => $this->db->escape_like_str(0),
                                    'add_inv_cat' => $this->db->escape_like_str(0),
                                    'inve_item' => $this->db->escape_like_str(0),
                                    'delete_inve_ite' => $this->db->escape_like_str(0),
                                    'delete_inv_cat' => $this->db->escape_like_str(0),
                                    'inve_issu' => $this->db->escape_like_str(0),
                                    'delete_inven_issu' => $this->db->escape_like_str(0),
                                    'check_leav_appli' => $this->db->escape_like_str(0),
                                    'setting_manage_user' => $this->db->escape_like_str(0),
                                    'setting_accounts' => $this->db->escape_like_str(0),
                                    'other_setting' => $this->db->escape_like_str(0),
                                    'front_setings' => $this->db->escape_like_str(0),
                                );
                                if ($this->db->insert('role_based_access', $student_access)) {
                                    $to =  $email;  // User email pass here
                                    $subject = 'Welcome To Educational ERP';
                                    $from = 'sarvdip.pol@jhamobi.com'; // Pass here your mail id
    
                                    $emailContent = '<!DOCTYPE><html><head></head><body><table width="100%" style="border:1px solid #cccccc;margin: auto;border-spacing:0;"><tr><td style="background-color: #243854;padding-left:3%"><center><h1 style="color:#fff">COLLEGE MANAGEMENT</h1></center></td></tr>';
                                    $emailContent .='<tr><td style="height:20px"></td></tr>';
    
                                    $user_rand = rand();
                                    // $emailContent .= '<p class="form-control">
                                    // Thank you for Registration, Please <a href="'.base_url('Sendemail/verify').'/'.$to.'/'.$user_rand.'">Click Here</a> to activate your account</p>';  
                                    $emailContent .= '<h4 style="padding-left:2%">Welcome '.$studArr[$i]->firstname.',</h4>';
                                    // $emailContent .='<tr style="padding-left:2%"><td style="height:8px"></td></tr>';
                                    $emailContent .= '<p style="padding-left:2%">Thanks for the registration on college ERP.</p>';
                                    $emailContent .= '<p style="padding-left:2%">Please use following credentials for login,</p>';
                                    $emailContent .= '<p style="padding-left:2%">Login URL - https://yofundo.in/erp</p>';
                                    $emailContent .= '<p style="padding-left:2%">Username - '.$to.'</p>';
                                    $emailContent .= '<p style="padding-left:2%">Password - Password01$</p>';                     
                                    $emailContent .='<tr style="padding-left:2%"><td style="height:20px"></td></tr>';
                                    $emailContent .= "<tr><td style='background-color: #243854;padding: 2%;text-align: center;font-size: 13px;'><p style='margin-top:6px;'><a href='https://www.yofundo.com/' target='_blank' style='text-decoration:none;color: #60d2ff;'>www.yofundo.in</a></p></td></tr></table></body></html>";              
    
    
                                    $config['protocol']    = 'smtp';
                                    $config['smtp_host']    = 'ssl://smtp.gmail.com';
                                    $config['smtp_port']    = '465';      
    
                                    $config['smtp_user']    = 'sarvdip.pol@jhamobi.com';    //Important
                                    $config['smtp_pass']    = 'wwzniamzxsujrcoo';  //Important
    
                                    $config['charset']    = 'utf-8';
                                    $config['newline']    = "\r\n";
                                    $config['mailtype'] = 'html'; // or html
                                    $config['validation'] = TRUE; // bool whether to validate email or not           
    
                                   $this->email->initialize($config);
                                   $this->email->set_mailtype("html");
                                   $this->email->from($from);
                                   $this->email->to($to);
                                   $this->email->subject($subject);
                                   $this->email->message($emailContent);
                                   $this->email->send();
                                  
                                }
                            }
                        }
                    }
                }
            }  
        }

        $message = "Data SyncUp Successfully!!!";
        echo "<script type='text/javascript'>alert('$message');</script>";
        // $data['cname'] = '';
        // $this->load->view('temp/header');
        // $this->load->view('registeredStudentListLms', $data);
        // $this->load->view('temp/footer');
        redirect('/crm/registeredStudentListLms', 'refresh');
    }

    public function viewGrades(){
        $arr = $this->input->get('id');
        $arrInfo = explode(',', $arr);
        $id = $arrInfo[1];
        // print_r($arrInfo);
        // die('--'.$id);
        $urldata = $this->config->item('api_url').'show_student_grades.php'; 
        $postData = '{  "id": "'.$id.'"}';
        $res = $this->common->schoolname_data($urldata,$postData);
        // print_r($res['data']); die('---');
        $data['grade_info'] = $res['data'];
        $data['id'] = $arrInfo[1];
        $data['name'] = $arrInfo[2].' '.$arrInfo[3];
        $data['email'] = $arrInfo[4];
        $data['phone2'] = $arrInfo[5];
        $this->load->view('temp/header');
        $this->load->view('displayGrades', $data);
        $this->load->view('temp/footer');
    }

    public function fetch_record(){
        $mystr = $this->input->get('mystr', TRUE);       
        $arrInfo = explode(',', $mystr);
        $cid = $arrInfo[0];
        $uid = $arrInfo[1];
        $urldata = $this->config->item('api_url').'show_student_grades_by_course.php'; 
        $postData = '{  "id": "'.$uid.'", "course_id": "'.$cid.'"}';
        $res = $this->common->schoolname_data($urldata,$postData);
        $this->load->view('temp/header');
        $this->load->view('displayDetailsGrades', $res);
        $this->load->view('temp/footer');
        // echo json_encode($res);
        // print_r($res);
        // die($uid.'---'.$cid);
    }
    public function fetch_quiz_details(){
        $mystr = $this->input->get('mystr', TRUE);       
        $arrInfo = explode(',', $mystr);
        $quiz_name = $arrInfo[0];
        $course_name = $arrInfo[1];
        $quiz_id = $arrInfo[2];
        $course_id = $arrInfo[3];
        $user_id = $arrInfo[4];
        $grades = $arrInfo[5];
        // die($quiz_name.'---'.$course_name.'---'.$quiz_id.'---'.$course_id.'---'.$user_id);
        $urldata = $this->config->item('api_url').'show_student_grades_by_quiz.php'; 
        $postData = '{  "quiz_id": "'.$quiz_id.'", "user_id": "'.$user_id.'" , "grades": "'.$grades.'"}';
        $res = $this->common->schoolname_data($urldata,$postData);
        $res['course_name'] = $course_name;
        $res['quiz_name'] = $quiz_name;
        $this->load->view('temp/header');
        $this->load->view('displayDetailsQuiz', $res);
        $this->load->view('temp/footer');
    }
}