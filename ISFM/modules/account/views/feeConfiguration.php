<!-- BEGIN PAGE LEVEL STYLES -->

<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
       <!--  <div class="row">
            <div class="col-md-12">
                
                <h3 class="page-title">
                    <?php echo lang('fee_config'); ?> <small></small>
                </h3>
                 <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_fees'); ?>
                    </li>
                    <li>
                        <?php echo lang('fee_config'); ?>
                    </li>
                   
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12 ">
                <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('fee_config'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        echo form_open_multipart('feesManagement/feeDetails', $form_attributs);
                        ?>
                        <div class="form-body">
                            <?php
                            if (!empty($success)) {
                                echo $success;
                            }
                            ?>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('fee_plan');?></label>
                             <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="" name="feePlaname" required="required">
                               
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('fee_category'); ?> 
                            <span class="requiredStar" required> * </span></label>
                            <div class="col-md-6">
                                <!-- <input type="text" class="form-control" placeholder="Fee Category Name eg:Admission, Registraion, Transportation" name="feeCategory" required="required"> -->
                                <select class="form-control" name="feeCategory">
                                    <option><?php echo lang('stu_sel_cla_select');?></option>
                                    <option value="Admission Fee">Admission Fee</option>
                                    <option value="Registration Fee">Registration Fee</option>
                                    <option value="Transportation Fee">Transportation Fee</option>
                                </select>
                               
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('fee_amt'); ?> <span class="requiredStar" required> * </span></label>
                            <div class="col-md-3">
                                <input type="text" class="form-control" placeholder="Enter Amount Here" name="feeamt" id="feeamt" required="required" onchange="taxApply()">
                                <div id="checkName" class="col-md-12"></div>
                            </div>
                            <div class="col-md-2 control-label">
                                <input type="checkbox" id="c02" name="feeLumpsum" class="istax" onclick="lumpsumApply()" value="yesLumpsum">
                            </div>
                            <label class="col-md-3 control-label"><?php echo lang('fee_lumpsum'); ?></label>
                        </div>

                        <div class="form-group" style="display: none;">
                            <div class="col-md-3 control-label">
                                <input type="hidden" id="feeTaxable" name="feeTaxable" class="istax"   value="yesTaxable">
                            </div>
                            <label class="col-md-2 control-label"><?php echo lang('fee_taxable'); ?></label>
                            
                        </div>

                            <div class="form-group taxInpercent">
                                
                                    <label class="col-md-2 control-label"><?php echo lang('tax_cgst'); ?> </label>                                
                                    <div class="col-md-1">
                                    <?php foreach ($getTaxdetail as $row) { ?>
                                        <input type="hidden" name="taxHideId" value="<?php echo $row['id']?>">
                                        <input type="hidden" class="form-control" placeholder="" name="cgst" id="cgst" required="required" readonly="reload" value="<?php echo $row['cgst'];?>"> 
                                    </div>
                                    <?php } ?>
                                    <div class="col-md-1">
                                        <input type="text" class="form-control" name="cgst_cal" id="cgst_cal" >                                      
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label"><?php echo lang('tax_sgst'); ?> </label>
                                    </div>
                                    <div class="col-md-1">
                                        <?php foreach ($getTaxdetail as $row) { ?>
                                            <input type="hidden"  class="form-control" placeholder="" name="sgst" id="sgst" required="required" readonly="reload" value="<?php echo $row['sgst'];?>">
                                        <?php } ?>  
                                    </div>
                                    <div class="col-md-1">
                                        <input type="text" class="form-control" name="sgst_calculate" id="sgst_calculate" >                                      
                                    </div>

                                    <label class="col-md-2 control-label"><?php echo lang('tax_igst'); ?> </label>                                
                                    <div class="col-md-1">
                                        <?php foreach ($getTaxdetail as $row) { ?>
                                            <input type="hidden"  class="form-control" placeholder="" name="igst" id="igst" required="required" readonly="reload" value="<?php echo $row['igst'];?>">
                                            <input type="hidden" name="totPer" id="totPer" value="<?php echo $row['total_percentage']?>">
                                        <?php } ?>  
                                    </div> 
                                    <div class="col-md-1">
                                        <input type="text" class="form-control" name="igst_calculate" id="igst_calculate" >                                      
                                    </div>

                                    <label class="col-md-3 control-label"><?php echo lang('total_amt'); ?></label>
                                    <div class="form-group">
                                        
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" placeholder="Total Amount Here" name="totAmt" id="totAmt" readonly="reload" >                                  
                                        </div>
                                    </div>                              
                            

                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('fee_downpay'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" placeholder="Enter Downpayment Amount Here" name="downpayAmt" id="downpayAmt"  onkeyup="get_finalPrice()">                                 
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" placeholder="Downpayment Amount with GST" name="downCalwithGst" id="downCalwithGst" readonly="reload">                                  
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" placeholder="Total" name="total_down" id="total_down" readonly="reload">                      
                                </div>
                            </div>
                                  
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tot_payableAmt'); ?> </label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" placeholder="Total Paying Amount" name="totpayAmt" id="totpayAmt" readonly="reload">                            
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" placeholder="With GST" name="totWithGST" id="totWithGST" readonly="reload">                            
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" placeholder="Total" name="payableWithGST" id="payableWithGST" readonly="reload">                            
                                </div>
                            </div>
                            <div class="form-group installment_per_div">
                                <label class="col-md-3 control-label"><?php echo lang('fee_install'); ?><span class="requiredStar"> * </span></label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control inst_period" name="feeInstallment_period" id="installment_num" onkeyup="press()">                               
                                </div>
                               
                            </div>
                         </div> 
                         <div class="portlet-body show_installment_div" >
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                
                            </table>
                            
                            <input type="hidden" name="inst_dt" id="inst_dt">
                        </div>                      
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" id="Button" class="btn green" name="submit" value="submit"><?php echo lang('tea_si'); ?></button>
                                <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <!-- <div class="row">
            <div class="col-md-12">
                <! BEGIN All account list->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php //echo lang('install_head'); ?> 
                        </div>
                    </div>
                    
                </div>
            </div>
        </div> -->

        <div class="col-md-12">
                <!-- BEGIN All account list-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('fee_detail'); ?> 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <?php echo lang('tax_id'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('fee_cat_name'); ?> 
                                    </th> 
                                    <th>
                                        <?php echo lang('fee_amt');?>
                                    </th>  
                                    <th>
                                        <?php echo lang('total_amt');?>
                                    </th> 
                                    <th>
                                        <?php echo lang('fee_downpay');?>
                                    </th>
                                    <th>
                                        <?php echo lang('tot_payableAmt');?>
                                    </th>
                                    
                                    <th><?php echo lang('fee_instal_head');?></th>
                                    <th><?php echo lang('fee_amt');?></th>
                                    <th><?php echo lang('fee_install_date');?></th>
                                    
                                                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach($fee_configDetail as $row) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['fee_category']; ?>
                                        </td> 
                                        <td>
                                            <?php echo $row['amount']; ?>
                                        </td> 
                                        <td>
                                            <?php echo $row['feetax_calculated_amt']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['downpayment']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['final_amount']; ?>
                                        </td>
                                         
                                        
                                        <!---<td>    
                                             <a class="btn btn-xs red" href="index.php/crm/deleteEntry?id=<?php //echo $row['id']; ?>&tbl_name=lead_source" onclick="javascript:return confirm('<?php //echo lang('dlte'); ?>')"> <i class="fa fa-trash-o"></i> <?php// echo lang('delete'); ?> </a>  
                                            
                                            <i data-id="<?php //echo $row['id'];?>" class="status_checks btn
                                                  <?php //echo ($row['status'])?
                                                  //'btn-success btn-xs': 'btn-danger btn-xs'?>">
                                                  <?php// echo ($row['status'])? 'Active' : 'Inactive'?>
                                              </i>
                                         </td> ---> 

                                         <td> 
                                            <?php if(isset ($row['installment'])) { echo $row['installment'];}?>
                                          </td>
                                          <td><?php echo $row['amount'];?></td>
                                          <td><?php if(isset ($row['installment_date'])) {echo $row['installment_date']; }?></td>                                    
                                    </tr>
                                <?php $i++;} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END All account list-->
            </div>
        </div>
    </div>
</div>

   <!-- <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script> 
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script type="text/javascript">

    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });

    
    function taxApply()
    {   
      
        //var taxes = $('.istax').is(":checked", true);
        //if(taxes)
            //$(".taxInpercent").show();
            var numVal1 = Number(document.getElementById("feeamt").value);         
            var numVal2 = Number(document.getElementById("totPer").value);
            var numVal3 = Number(document.getElementById("cgst").value);
            var numVal4 = Number(document.getElementById("sgst").value);
            var numVal5 = Number(document.getElementById("igst").value);
            //alert(numVal3);
            var getninepercnt = numVal1 * numVal3 /100;
            var get_sgstcal = numVal1 * numVal4 /100;
            var get_igstcal = numVal1 * numVal5 /100;
            var totalValue = numVal1 * numVal2 / 100 ;

            document.getElementById("sgst_calculate").value = get_sgstcal;
            document.getElementById("cgst_cal").value = getninepercnt;
            document.getElementById("igst_calculate").value = get_igstcal;
            document.getElementById("totAmt").value = totalValue + numVal1;

    }
    function lumpsumApply()
    {
        var lumpsum = $('.istax').is(":checked", true);
        if(lumpsum)
            $(".installment_per_div").hide();
            var numVal1 = Number(document.getElementById("feeamt").value);         
            var numVal2 = Number(document.getElementById("totPer").value);
            var numVal3 = Number(document.getElementById("cgst").value);
            var numVal4 = Number(document.getElementById("sgst").value);
            var numVal5 = Number(document.getElementById("igst").value);
            //alert(numVal3);
            var getninepercnt = numVal1 * numVal3 /100;
            var get_sgstcal = numVal1 * numVal4 /100;
            var get_igstcal = numVal1 * numVal5 /100;
            var totalValue = numVal1 * numVal2 / 100 ;

            document.getElementById("sgst_calculate").value = get_sgstcal;
            document.getElementById("cgst_cal").value = getninepercnt;
            document.getElementById("igst_calculate").value = get_igstcal;
            document.getElementById("totAmt").value = totalValue + numVal1;
            //$(".show_installment_div").hide();
    }

    function get_finalPrice()
    {
        var numVal1 = Number(document.getElementById("feeamt").value);         
        var numVal2 = Number(document.getElementById("totPer").value);
            //alert(numVa2);
        var totalValue = numVal1 * numVal2 / 100 ;
        document.getElementById("totAmt").value = totalValue + numVal1;

        var payingAmt = Number(document.getElementById("downpayAmt").value);

        var downCalculateGst = payingAmt * numVal2 / 100;

        document.getElementById("downCalwithGst").value = downCalculateGst;

        document.getElementById("total_down").value = payingAmt + downCalculateGst;

        document.getElementById("totpayAmt").value = document.getElementById("totAmt").value - document.getElementById("total_down").value;

        var payabletotamt = document.getElementById("totpayAmt").value;

        var totalwithGST = payabletotamt * numVal2 / 100;

        document.getElementById("totWithGST").value = totalwithGST;

        document.getElementById("payableWithGST").value =  parseInt(payabletotamt) + parseInt(totalwithGST);
    }

    function press()
    {
       var input1 = parseInt($("#payableWithGST").val(), 10);
       var input2 = parseInt($("#installment_num").val(), 10);
       var total2 = input1/input2;

        //document.getElementById("install_amont").value = total2;

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = dd + '/' + mm + '/' + yyyy; 
        //var tomorrow_date =today.dd+30 + "-" + today.mm+1 + "-" + today.yyyy;      

        var table = $("#sample_1");
        var rowNum = parseInt($("#installment_num").val(), 10);
       // var arr = [];
        var resultHtml = '';
        //var datetime = datepicker({ dateFormat: "yyyy-mm-dd" });
        
       
        resultHtml += ["<tr>", 

        '<th><?php echo lang('fee_instal_head'); ?></th>',
        '<th><?php echo lang('fee_amt'); ?></th>',
        '<th><?php echo lang('fee_install_date'); ?></th>',

        '</tr>'].join("\n");

        
        for(var i = 0 ; i < rowNum ; i++) 
        {
            
           
            var day='';
                        
            if(i==0)
            {
                 day = today;
                 //document.getElementById("inst_dt").value = day;
                 
            }
            else
            {
                day = "<input type='date' class='datetime' name='nextdate_"+i+"' id='nextdate_"+i+"' onchange='getInstalldt()'>";
                

            }


            resultHtml += ["<tr>", 
                 "<td>", 
                  (i+1),
                 "</td>",
                 "<td name='installment_amount' id='installment_amount'><input type='hidden'  value="+total2+">"+"<input type='text' name='concatval_"+i+"' id='concatval_"+i+"' value="+total2+">"+"</td>",
                
                    "<td name='today_dt' id='today_dt'>"+ day + "</td>",
                    
                    
                 '</tr>'].join("\n");

           
        }  
         
       
        table.html(resultHtml);
        return false;

 
    }

    function getInstalldt()
    {
        var x = document.getElementById("nextdate").value;
       
        var span_Text = document.getElementById("concatval").innerText;
        

    }

     
</script>

<script type="text/javascript">
   
$(document).ready(function(){
    $('.istax').click(function() 
    {
        $('.istax').not(this).prop('checked', false);
    });
});
</script>