<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CrmModule extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->dbforge();
    }

    public function allLeads() {
        $data = array();
        $query = $this->db->query("SELECT l.*,c.class_title,ls.name as lead_sor,loc.name as locatn,lst.name as stage FROM `lead` l LEFT JOIN class c ON l.class = c.id LEFT JOIN lead_source ls ON l.lead_source = ls.id LEFT JOIN lead_location loc ON l.location = loc.id LEFT JOIN lead_stage lst ON l.lead_stage = lst.id order by l.id desc");
        foreach ($query->result_array() as $row) {           
            $data[] = $row;            
        }
        for($i=0;$i<count($data);$i++){
            $id= $data[$i]['id'];
            $query = $this->db->query("SELECT createdAt as cdate FROM `lead_history` WHERE lead_id = $id ORDER BY id DESC LIMIT 1")->row();
            $data[$i]['cdate'] = $query->cdate;
            $query1 = $this->db->query("SELECT followup FROM `lead_history` WHERE lead_id = $id and followup != '' ORDER BY id DESC LIMIT 1")->row();
            if($query1) {
                $data[$i]['fdate'] = $query1->followup;
            }else{
                $data[$i]['fdate'] = '';
            }
        }
        return $data;
    }

    public function RegisteredStudents()
    {
        $data = array();
        $query = $this->db->query("SELECT * FROM registration_student");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;

    }

    public function approvStudList()
    {
        $data = array();
        $query = $this->db->query("SELECT * FROM registration_student where status = 1");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;

    }
    
    public function studDatafromdb($get_StudentsInfo)
    {
        /*$this->db->select('*');
        $this->db->from('registration_student');
        $this->db->where('id',$get_StudentsInfo);
        $query = $this->db->get();
        return $query->result_array();*/
        $data = array();
        $query = $this->db->query("SELECT * FROM registration_student where id = '$get_StudentsInfo'");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }
    
    public function getStudentsHistory($get_StudentsInfo)
    {
        $data = array();
        $query = $this->db->query("SELECT * FROM registration_history where registration_id = '$get_StudentsInfo'");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }
    public function StudentDetailUpadte($update_id,$studentsInfo)
    {
      return $sql = $this->db
                ->where('id',$update_id)
                ->update('registration_student',$studentsInfo);
    }
    public function updateStudStatus($stud_id,$status)
    {
        $data['status'] = $status;
        $this->db->where('id',$stud_id);
        $this->db->set('updated_at',date('Y-m-d H:i:s'));
        $this->db->update('registration_student',$data);
    }
    
    public function LeadtogetSource($update_lead_sourceId)
    {
        $data = array();
        $query = $this->db->query("SELECT * FROM lead_source where id=$update_lead_sourceId");
        foreach($query->result_array()as $row)
        {
            $data[] = $row;
        }
        return $data;
    }

    public function LeadtogetLocation($update_locationId)
    {
        $data = array();
        $query = $this->db->query("SELECT * FROM lead_location where id=$update_locationId");
        foreach($query->result_array()as $row)
        {
            $data[] = $row;
        }
        return $data;
    }

    public function LeadsById($id) {
        $data = array();
        $query = $this->db->query("SELECT l.*,c.class_title,ls.name as lead_sor,loc.name as locatn,lst.name as stage FROM `lead` l LEFT JOIN class c ON l.class = c.id LEFT JOIN lead_source ls ON l.lead_source = ls.id LEFT JOIN lead_location loc ON l.location = loc.id LEFT JOIN lead_stage lst ON l.lead_stage = lst.id where l.id=$id");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }
    public function LeadHisById($id) {
        $data = array();
        $query = $this->db->query("SELECT l.*,lst.name as stage_name,la.name as lead_activity_name FROM `lead_history` l LEFT JOIN lead_activity la ON l.lead_activity_id = la.id LEFT JOIN lead_stage lst ON l.stage_id = lst.id where l.lead_id=$id order by l.id DESC");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }
    

}
