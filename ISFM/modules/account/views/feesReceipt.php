<!-- <!DOCTYPE html>
<html>
  <head>
  <link href="<?php echo base_url()?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    <style>
        body {
            height: 842px;
            width: 595px;
            /* to centre page on screen*/
            margin-left: auto;
            margin-right: auto;
        }
        .main{
            border: 1px solid #000;
            text-align: center;
        }
        .title{
            text-align:center;
            font-size:32px; 
            color:#222; 
            letter-spacing:1px; 
            font-weight:400;
        }
        .row {
            margin:1px;
            border: 1px solid #000;
        }
        .pd{
            padding:0;
        }
        .borderLeft{
            border-left: 1px solid #000;
        }
        .borderRight{
            border-right: 1px solid #000;
        }
        .borderBottom{
            border-bottom: 1px solid #000;
        }
        #recTitle {
            margin: 11px;
            display: block;
        }
    </style>
  </head>
  <body>
    <button type="submit" onClick="printDiv('printableArea')" class="btn blue" name="submit" value="Print"><?php echo "Fees Receipt"; ?></button>
    <div class="main" id="printableArea">
        <span class="title">AKSHARA INTERNATIONAL SCHOOL</span>
        <div>Sr.No 109, Akshara Lane, Opp. Prerena Bhavan, wakad Pune - 411057</div>
        <div>CBSE AFFLIATION NUMBER 1130266(SCHOOL CODE:30217)</div>
        <div class="row">
            <div class="col-md-4 pd borderRight">
                <div class="borderBottom">one</div>
                <div>2</div>
            </div>
            <div class="col-md-4 pd borderRight">
                <span id="recTitle">RECEIPT</span>
            </div>
            <div class="col-md-4 pd">
                <div class="borderBottom">one</div>
                <div>2</div>
            </div>
        </div>
    </div>
  </body>
  <script >
    function printDiv(divName) {
        var divContents = document.getElementById("printableArea").innerHTML; 
        var a = window.open('', '', 'height=842px, width=595px'); 
        a.document.write('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"><html>'); 
        a.document.write('<body >'); 
        a.document.write(divContents); 
        a.document.write('</body></html>'); 
        a.document.close(); 
        a.print(); 
    }
</script>
</html> -->

<head>
  <link href="<?php echo base_url()?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    <style>
        body {
            height: 842px;
            width: 595px;
            /* to centre page on screen*/
            margin-left: auto;
            margin-right: auto;
        }
        .main{
            border: 1px solid #000;
        }
        .title{
            text-align:center;
            font-size:32px; 
            color:#222; 
            letter-spacing:1px; 
            font-weight:400;
        }
        .row {
            margin:0px 1px;
            /* border: 1px solid #000; */
        }
        .pd{
            padding: 0px 0px;
        }
        .borderLeft{
            border-left: 1px solid #000;
        }
        .borderRight{
            border-right: 1px solid #000;
        }
        .borderBottom{
            border-bottom: 1px solid #000;
        }
        .borderTop{
            border-top: 1px solid #000;
        }
        .pd10{
            padding : 10px;
        }
        .pd20{
            padding : 20px;
        }
        .pd40{
            padding: 40px;
        }
        #recTitle {
            margin: 8px;
            font-size: 18px;
            text-align: center;
        }
        .mg15{
            margin-top: 15px;
        }
        .borderTopDash{
            border-top: 1px dashed #6e6a6a;
        }
        .font11{
            font-size: 11px;
        }
        .sign{
            text-align: right;
            padding: 50 10 20;
        }
        .alignCenter{
            text-align: center;
        }
        .alignRight{
            text-align: right;
        }
        @media print{
            .card-footer{
                display:none
            }
            /* @page {
                size: auto;
                margin: 20mm 0mm 0mm;
            } */
            .row {
            margin:0px 1px;
            }
            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6,
      .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
           float: left;               
        }
        .col-sm-12 {
            width: 100%;
        }
        .col-sm-11 {
            width: 91.66666666666666%;
        }
        .col-sm-10 {
            width: 83.33333333333334%;
        }
        .col-sm-9 {
                width: 75%;
        }
        .col-sm-8 {
                width: 66.66666666666666%;
        }
        .col-sm-7 {
                width: 58.333333333333336%;
        }
        .col-sm-6 {
                width: 50%;
        }
        .col-sm-5 {
                width: 41.66666666666667%;
        }
        .col-sm-4 {
                width: 33.33333333333333%;
        }
        .col-sm-3 {
                width: 25%;
        }
        .col-sm-2 {
                width: 16.666666666666664%;
        }
        .col-sm-1 {
                width: 8.333333333333332%;
            }
            .main{
            border: 1px solid #000;
        }
        .title{
            text-align:center;
            font-size:32px; 
            color:#222; 
            letter-spacing:1px; 
            font-weight:400;
        }
        .row {
            margin:0px 2px;
            /* border: 1px solid #000; */
        }
        .pd{
            padding:0;
        }
        .borderLeft{
            border-left: 1px solid #000;
        }
        .borderRight{
            border-right: 1px solid #000;
        }
        .borderBottom{
            border-bottom: 1px solid #000;
        }
        .borderTop{
            border-top: 1px solid #000;
        }
        .pd10{
            padding : 10px;
        }
        .pd20{
            padding : 20px;
        }
        .pd40{
            padding: 40px;
        }
        #recTitle {
            margin: 8px;
            font-size: 18px;
            text-align: center;
        }
        .mg15{
            margin-top: 15px;
        }
        .borderTopDash{
            border-top: 1px dashed #6e6a6a;
        }
        .font11{
            font-size: 11px;
        }
        .sign{
            text-align: right;
            padding: 50 10 20;
        }
        .alignCenter{
            text-align: center;
        }
        .alignRight{
            text-align: right;
        }
        }
    </style>
  </head>

<script>
     print(): void {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
          <html>
            <head>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            </head>
            <style>

            </style>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`
        );
        popupWin.document.close();
    }
</script>



    <div class="main" id="printableArea">
        <span class="title">AKSHARA INTERNATIONAL SCHOOL</span>
        <div class="alignCenter">Sr.No 109, Akshara Lane, Opp. Prerena Bhavan, wakad Pune - 411057</div>
        <div class="alignCenter">CBSE AFFLIATION NUMBER 1130266(SCHOOL CODE:30217)</div>
        <div class="row borderRight borderLeft borderTop">
            <div class="col-sm-4 pd borderRight">
                <div class="borderBottom "><strong>Receipt No. :</strong>
                    AIS/202122/3332</div>
                <div class=""><strong>GR. NO:</strong> 2154/17</div>
            </div>
            <div class="col-sm-4 pd borderRight borderBottom ">
                <div id="recTitle"><strong>RECEIPT</strong></div>
            </div>
            <div class="col-sm-4 pd ">
                <div class="borderBottom  "><strong>Date :</strong> 21-11-2022</div>
                <div  class="borderBottom "><strong>Class :</strong> V F</div>
            </div>
        </div>
        <div class="row borderRight borderLeft borderTop">
            <div class="col-sm-4 pd ">
                <div class="borderRight"><strong>Enrollment No</strong></div>
                <div class="borderRight">26003322</div>
            </div>
            <div class="col-sm-8 pd20">                
            </div>
        </div>
        <div class="row borderRight borderLeft borderTop">
            <div class="col-sm-8 pd pd10 borderRight"><strong>School Transaction Refrence No:</strong> 2100005011122220</div>
            <div class="col-sm-4 pd"><strong>Bank Transaction No:</strong> WUTI005011122220</div>
        </div>
        <div class="row borderRight borderLeft borderTop">
            <div class="col-sm-12 pd"><strong>Name:</strong> PRATISHTHA JHA</div>
        </div>
        <div class="row borderRight borderLeft borderTop borderBottom">
            <div class="col-sm-12 pd "><strong>Parent Id/Name:</strong> P3146/ PRASANNA KUMAR</div>
        </div>
        <div class="mg15">
            <div class="row borderRight borderLeft borderTop">
                <div class="col-sm-3 pd borderRight alignCenter"><strong>Payment Mode</strong></div>
                <div class="col-sm-3 pd borderRight alignCenter"><strong>Bank Name</strong></div>
                <div class="col-sm-3 pd borderRight alignCenter"><strong>Cheque / DD No</strong></div>
                <div class="col-sm-3 pd alignCenter"><strong>Cheque/DD date </strong></div>
            </div>
            <div class="row borderRight borderLeft borderTop">
                <div class="col-sm-3 pd borderRight alignCenter">Online Payment</div>
                <div class="col-sm-3 pd borderRight alignCenter">-</div>
                <div class="col-sm-3 pd borderRight alignCenter">-</div>
                <div class="col-sm-3 pd alignCenter">-</div>
            </div>
            <div class="row borderRight borderLeft borderTop">
                <div class="col-sm-9 pd borderRight alignCenter"><strong>Particulars</strong></div>
                <div class="col-sm-3 pd alignCenter"><strong>INR</strong></div>
            </div>
            <div class="row borderRight borderLeft borderTop">
                <div class="col-sm-9 pd borderRight">School charger for Term 2(2021-2022)</div>
                <div class="col-sm-3 pd alignRight">7,200.00</div>
            </div>
            <div class="row borderRight borderLeft borderTopDash">
                <div class="col-sm-9 pd borderRight">Tution Fees for Term 2(2021-2022)</div>
                <div class="col-sm-3 pd alignRight">36,000.00</div>
            </div>
            
            <div class="row borderRight borderLeft borderTopDash">
                <div class="col-sm-9 pd borderRight">Covid Fees Relief for October (2021-2022)</div>
                <div class="col-sm-3 pd alignRight">-4,500.00</div>
            </div>
            <div class="row borderRight borderLeft borderTopDash">
                <div class="col-sm-9 pd pd40 borderRight"></div>
                <div class="col-sm-3 pd"></div>
            </div>
            <div class="row borderRight borderLeft borderTop">
                <div class="col-sm-9 pd borderRight alignRight"><strong>Total</strong></div>
                <div class="col-sm-3 pd alignRight">38,700.00</div>
            </div>
            <div class="row borderRight borderLeft borderTop borderBottom">
                <div class="col-sm-3 pd borderRight"><strong>Amount In Words</strong></div>
                <div class="col-sm-9 pd">Thirty Eight Thousands seven Hundreed</div>
            </div>            
        </div>
        <div class="font11"> Remark</div>
        <div class="font11"> School charges are inclusive of PTA fees RS. 50/- from session 2022-2023.</div>
        <div class=""> <strong>Verified by School Accountant</strong></div>
        <div class="sign"> <strong>Parent Name</strong>: PRASANNA JHA</div>
    </div>

<div class="card-footer">
    <button class="btn btn-info btn-sm float-right" onclick="print()">Print</button>
    <button class="btn btn-danger btn-sm" onclick="location.href = 'javascript:history.back()'"><?php echo lang('back'); ?></button>
</div>

