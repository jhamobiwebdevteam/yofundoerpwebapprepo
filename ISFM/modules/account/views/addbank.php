<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-
color="e34524" bolt-logo="<?=base_url('assets/images/jm.jpeg')?>"></script>

<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datepicker/css/datepicker3.css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i><?php echo lang('header_add_bank'); ?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php $form_attribut = array('class' => 'form-horizontal', 'role' => 'form');
                        echo form_open_multipart("account/addbank", $form_attribut);
                        ?>
                        <div class="form-body">
                            <?php
                            if (!empty($success)) {
                                echo $success;
                            }
                            ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('header_sname');?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-3">                          
                                    <select class="form-control" name="institute_id" id="institute_id" required>
                                        <option value="">Select Institute Name</option>
                                        <?php foreach($institutes['data'] as $key) { ?>
                                            <option value="<?php echo $key['id'];?>"><?php echo $key['name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        
                            <div class="form-group">   
                                <label class="col-md-3 control-label">Account Name<span class="requiredStar">*</span></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="account_name" required>
                                </div>                             
                                <label class="col-md-2 control-label">Account No<span class="requiredStar">*</span></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="account_no" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Bank Name<span class="requiredStar"> * </span></label>
                                <div class="col-md-3">
                                    <input class="form-control" type="text" name="bank_name" id="bank_name" required>
                                </div>
                                <label class="col-md-2 control-label">IFSC Code<span class="requiredStar">*</span></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="ifsc" required>
                                </div>                                
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Cheque leaf<span class="requiredStar"> * </span></label>
                                <div class="col-md-3">
                                     <input class="form-control" type="file" name="userfile" id="userfile" required accept="image/png, image/jpeg" />
                                </div>                                
                            </div>

                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" id="Button" class="btn green" name="submit" value="submit"><?php echo lang('tea_si'); ?></button>
                                    <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                                </div>
                            </div>

                            <?php echo form_close(); ?>
                        </div>
                    </div>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END PORTLET-->

                <div class="col-md-12">
                <!-- BEGIN All account list-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('header_add_bank'); ?> 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <?php echo lang('list_id'); ?>
                                    </th>
                                    <th>Institute Name</th>
                                    <th>Account Name</th>
                                    <th>Account Number</th>
                                    <th>Bank Name</th>
                                    <th>IFSC</th>
                                    <th>Cheque</th>
                                    <th>Status</th>
                                </tr>
                                 <?php $i=1; foreach ($bank_data as $row) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td><?php $data = $this->common->getSingleField('name','institute','lms_id',$row['institute_id']); if($data){echo $data[0]->name;}  ?></td>
                                        <td><?php echo $row['account_name'];?></td>
                                        <td><?php echo $row['account_no'];?></td>
                                        <td><?php echo $row['bank_name'];?></td>
                                        <td><?php echo $row['ifsc'];?></td>
                                        <td><a href="<?php echo base_url().'assets/uploads/cheque/'.$row['cheque_leaf'];?>" target="_blank">click me</td>
                                        <td>
                                        <i data-id="<?php echo $row['id'];?>" style="cursor: pointer;" class="status_checks btn-xs <?php echo ($row['status'])? 'btn-danger': 'btn-success'?>"><?php echo ($row['status'])? 'In-active' : 'Active'?>
                                            </i>
                                    </td>
                                    </tr>
                                <?php $i++;}?>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

<script type="text/javascript">

    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });


$(document).ready(function() {
        $('select[name="name"]').on('change', function() {
            var id = $(this).val();
            
            if(id) {
                $.ajax({
                    url: '<?php echo base_url();?>payment/studentsdata',
                    data: {'id': id },
                    type: "POST",
                    dataType: "json",
                    success:function(data) {
                      //console.log(data);
                       $("#mobile").val(data[0]['fathers_mobile']);
                       $("#email").val(data[0]['email']);
                       
                    }
                });
            }
        });
    });

    $(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '1' : '0';      
      var msg = (status=='0')? 'Active' : 'In-active';
      if(confirm("Are you sure to "+ msg)){
        var dataId = $(this).attr("data-id");       
        url = "<?php echo base_url().'account/updateStatus'?>";       
        $.ajax({
          type:"POST",
          url: url,
          data: {id:dataId,status:status},         
          success: function(response){   
            location.reload();
           console.log(response);
          }
        });
      }      
    });
</script>