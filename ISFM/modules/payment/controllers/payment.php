<?php 
    if (!defined('BASEPATH'))
    exit('No direct script access allowed');

    class Payment extends MX_Controller {

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         * 		http://example.com/index.php/home
         * 	- or -  
         * 		http://example.com/index.php/home/index
         */
        function __construct() {
            parent::__construct();
            $this->load->model('paymentmodel');
            // $this->load->model('homeModel');
            $this->load->helper('file');
            $this->load->helper('form');
            if (!$this->ion_auth->logged_in()) {
                redirect('auth/login');
            }
        }
    
        //This function will show the users dashboard
        public function index() 
        {
            if ($this->ion_auth->is_student()) 
            {
                $this->load->view('temp/stud_header');
                $this->load->view('payment');
                $this->load->view('temp/footer');
            }
            else
            {
                $this->load->view('temp/header');
                $this->load->view('payment');
                $this->load->view('temp/footer');
            }
        }

       

        
        public function success()
        {
            $this->load->view('temp/header');
            $this->load->view('success');
            $this->load->view('temp/footer');
        }
    
    
    }