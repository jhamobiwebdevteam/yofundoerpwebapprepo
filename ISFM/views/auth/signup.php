
<!DOCTYPE html>
<html lang="en" >
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo lang('system_title'); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <base href="<?php echo $this->config->base_url(); ?>">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="assets/global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/formValidation.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="assets/admin/layout/img/feviconlogo.jpeg"/>
        <style>
            .content{
                width:600px !important;
            }
            .help-block{
                margin-left: 17px !important;
            }
            .all-btn{
                width: 100%;
            }
        </style>
    </head>
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
        </div>
        <!-- END LOGO -->
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <div id="infoMessage"><?php //echo $message; ?></div>
            <!-- BEGIN LOGIN FORM -->
            <?php
            $attributes = array('class' => 'signup-form', 'role' => 'form', 'method'=>'post');
            echo form_open("auth/signup", $attributes);
            ?>
            <h3 class="form-title"><?php echo lang('signup_forn'); ?></h3>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span><?php echo lang('login_validation_message'); ?> </span>
            </div>
            <div class="form-group row">
                <div class="col-md-7">
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" data-validation="email" data-validation-error-msg="Email field is required." placeholder="<?php echo lang('signup_email'); ?>" name="email" id="email"/>
                    </div>
                </div>
                <div class="col-md-5">
                    <input type="button" id="generate_email_otp" class="btn green all-btn" value="<?php echo lang('generate_email_otp'); ?>">
                </div>
                <span class="help-block" id="gotp"></span>
                <input type="hidden" id="email_gotp" name="email_gotp">
            </div>
            <div class="form-group row">
                <div class="col-md-7">
                    <label class="control-label visible-ie8 visible-ie9">emailotp</label>
                    <div class="input-icon">
                        <input class="form-control placeholder-no-fix" type="test" autocomplete="off" data-validation="required" data-validation-error-msg="Email OTP field is required." placeholder="<?php echo lang('email_otp'); ?>" name="emailotp" id="emailotp"/>
                    </div>
                </div>
                <div class="col-md-5">
                    <input type="button" id="verify_email_otp" class="btn green all-btn" value="<?php echo lang('verify_email_otp'); ?>">
                </div>
                <span class="help-block" id="votp"></span>
            </div>
            <div class="form-group row">
                <div class="col-md-7">
                    <label class="control-label visible-ie8 visible-ie9">Mobile</label>
                    <div class="input-icon">
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" data-validation="required" data-validation-error-msg="Mobile number field is required." placeholder="<?php echo lang('signup_mobile'); ?>" name="mobile" id="mobile"/>
                    </div>
                </div>
                <div class="col-md-5">
                    <input type="button" id="generate_mobile_otp" class="btn green all-btn" value="<?php echo lang('generate_mobile_otp'); ?>">
                    </a>
                </div>
                <span class="help-block" id="motp"></span>
                <input type="hidden" id="mobile_gotp" name="mobile_gotp">
            </div>
            <div class="form-group row">
                <div class="col-md-7">
                    <label class="control-label visible-ie8 visible-ie9">Mobile OTP</label>
                    <div class="input-icon">
                        <input class="form-control placeholder-no-fix" type="test" autocomplete="off" data-validation="required" data-validation-error-msg="Mobile OTP field is required." placeholder="<?php echo lang('mobile_otp'); ?>" name="mobileotp" id="mobileotp"/>
                    </div>
                </div>
                <div class="col-md-5">
                <input type="button" id="verify_mobile_otp" class="btn green all-btn" value="<?php echo lang('verify_mobile_otp');?>">
                </div>
                <span class="help-block" id="mvotp"></span>
            </div>
            
            <div class="form-group row">
                <div class="col-md-12">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" data-validation="required" data-validation-error-msg="Password field is required." placeholder="<?php echo lang('login_password_label'); ?>" name="password"/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label class="control-label visible-ie8 visible-ie9">firstneme</label>
                    <div class="input-icon">
                        <input class="form-control placeholder-no-fix" type="test" autocomplete="off" data-validation="required" data-validation-error-msg="Firstname field is required." placeholder="<?php echo lang('firstname'); ?>" name="firstname"/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label class="control-label visible-ie8 visible-ie9">lastname</label>
                    <div class="input-icon">
                        <input class="form-control placeholder-no-fix" type="test" autocomplete="off" data-validation="required" data-validation-error-msg="Lastname field is required." placeholder="<?php echo lang('lastname'); ?>" name="lastname"/>
                    </div>
                </div>
            </div>
            <div class="form-actions">                
                <button id="button" type="submit" name="submit" value="submit" class="btn green pull-left">
                    <?php echo lang('signup'); ?> <i class="m-icon-swapright m-icon-white"></i>
                </button>
            </div>
            
            <?php echo form_close(); ?>

            <!-- END LOGIN FORM -->
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        
        <!-- END COPYRIGHT -->
        <!--Start form validation script-->
        <script src="assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
        <script>
            $.validate({modules: 'location, date, security, file'});
        </script>
        <!---End form validation script-->
        <script type="text/javascript">
            $('#generate_email_otp').on('click', function() {
                var email = $("#email").val();
                $("#gotp").text("Please wait Email is sending..");
                $.ajax({
                    url: '<?php echo base_url().'index.php/auth/genEmailOtp'?>',
                    data: {'email': email},
                    type: "POST",
                    dataType: "json",
                    success:function(data){
                       console.log(data);
                       if(data.status == 1){
                            $("#gotp").text(data.msg);
                            $("#email_gotp").val(data.otp);
                       }else{
                            $("#gotp").text(data.msg);
                       }
                    }
                });
            });
            
            $('#verify_email_otp').on('click', function() {
                var gotp = $("#email_gotp").val();
                var verifyotp = $("#emailotp").val();
                // alert(gotp+'----'+verifyotp);
                $("#votp").text("Please wait Email is verifing..");
                if(gotp == verifyotp){
                    $("#votp").text("Email varified successfully..");
                }else{
                    $("#votp").text("Email NOT varified..");
                }
                
            });
            
            $('#generate_mobile_otp').on('click', function() {
                var mobile = $("#mobile").val();
                $("#motp").text("Please wait OTP is sending..");
                $.ajax({
                    url: '<?php echo base_url().'index.php/auth/genMobileOtp'?>',
                    data: {'mobile': mobile},
                    type: "POST",
                    dataType: "json",
                    success:function(data){
                       console.log(data);
                       if(data.status == 1){
                            $("#motp").text(data.msg);
                            $("#mobile_gotp").val(data.otp);
                       }else{
                            $("#motp").text(data.msg);
                       }
                    }
                });
            });

            $('#verify_mobile_otp').on('click', function() {
                var motp = $("#mobile_gotp").val();
                var verifyotp = $("#mobileotp").val();
                // alert(gotp+'----'+verifyotp);
                $("#mvotp").text("Please wait Mobile is verifing..");
                if(motp == verifyotp){
                    $("#mvotp").text("Mobile Number varified successfully..");
                }else{
                    $("#mvotp").text("Mobile NOT varified..");
                }
                
            });
        </script>
    </body>
    <!-- END BODY -->
</html>