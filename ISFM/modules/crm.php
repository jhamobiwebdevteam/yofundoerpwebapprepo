<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Crm extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('crmmodule');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    //This function add lead in this function
    function addNewLead() { 
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }
        if ($this->input->post('submit', TRUE)) {
            $leadInfo = array(
                'fname' => $this->db->escape_like_str($this->input->post('first_name', TRUE)),
                'lname' => $this->db->escape_like_str($this->input->post('last_name', TRUE)),
                'mobile' => $this->db->escape_like_str($this->input->post('mobile', TRUE)),
                'email' => $this->db->escape_like_str($this->input->post('email', TRUE)),
                'lead_source' => $this->db->escape_like_str($this->input->post('lead_sourse', TRUE)),
                'class' => $this->db->escape_like_str($this->input->post('class', TRUE)),
                'location' => $this->db->escape_like_str($this->input->post('location', TRUE)),
                'lead_stage' => 1
            );
            if ($this->db->insert('lead', $leadInfo)) {
                $insert_id = $this->db->insert_id();
                $leadHis = array(
                    'lead_id' => $insert_id,
                    'lead_activity_id' => 1,
                    'stage_id' => 1,
                    'followup_remark' => "",
                    'lead_remark' => "",
                    'followup' => ""
                );
                $this->db->insert('lead_history', $leadHis);
               // $data['allAccount'] = $this->common->getAllData('account_title');
               $data['classTile'] = $this->common->getAllData('department');
               $data['leadSource'] = $this->common->getAllData('lead_source');
               $data['leadLocation'] = $this->common->getAllData('lead_location');
                $data['message'] = '<div class="alert alert-success alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
								<strong>Success ! </strong> Lead added successfully. 
							</div>';
                $this->load->view('temp/header');
                $this->load->view('addNewLead',$data);
                $this->load->view('temp/footer');
            }
        }else {
            $data['classTile'] = $this->common->getAllData('department');
            $data['leadSource'] = $this->common->getAllData('lead_source');
            $data['leadLocation'] = $this->common->getAllData('lead_location');
            $this->load->view('temp/header');
            $this->load->view('addNewLead',$data);
            $this->load->view('temp/footer');
        }
    }

    function editLead() {
        $id = $this->input->get('id');
        if ($this->input->post('submit', TRUE)) {
            $leadInfo = array(
                'fname' => $this->db->escape_like_str($this->input->post('first_name', TRUE)),
                'lname' => $this->db->escape_like_str($this->input->post('last_name', TRUE)),
                'mobile' => $this->db->escape_like_str($this->input->post('mobile', TRUE)),
                'email' => $this->db->escape_like_str($this->input->post('email', TRUE)),
                'lead_source' => $this->db->escape_like_str($this->input->post('lead_sourse', TRUE)),
                'class' => $this->db->escape_like_str($this->input->post('class', TRUE)),
                'location' => $this->db->escape_like_str($this->input->post('location', TRUE))
            );
            $this->db->where('id', $id);
            $this->db->update('lead', $leadInfo);
            $data['lead'] = $this->crmmodule->allLeads();
            $this->load->view('temp/header');
            $this->load->view('displayLeads', $data);
            $this->load->view('temp/footer');
        }else {
            $data['classTile'] = $this->common->getAllData('department');
            $data['leadSource'] = $this->common->getAllData('lead_source');
            $data['leadLocation'] = $this->common->getAllData('lead_location');
            $data['lead'] = $this->crmmodule->LeadsById($id);
            $this->load->view('temp/header');
            $this->load->view('editNewLead', $data);
            $this->load->view('temp/footer');
        }
    }

    function addLeadSource() { 
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }
        if ($this->input->post('submit', TRUE)) {
            $leadInfo = array(
                'name' => $this->db->escape_like_str($this->input->post('name', TRUE))
            );
            if ($this->db->insert('lead_source', $leadInfo)) {
                $data['message'] = '<div class="alert alert-success alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
								<strong>Success ! </strong> Lead added successfully. 
							</div>';
                $data['allRecords'] = $this->common->getAllData('lead_source');
                $this->load->view('temp/header');
                $this->load->view('addLeadSource',$data);
                $this->load->view('temp/footer');
            }
        }else {      
            $data['allRecords'] = $this->common->getAllData('lead_source');      
            $this->load->view('temp/header');
            $this->load->view('addLeadSource',$data);
            $this->load->view('temp/footer');
        }
    }
    function addLeadStage() { 
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }
        if ($this->input->post('submit', TRUE)) {
            $leadInfo = array(
                'name' => $this->db->escape_like_str($this->input->post('name', TRUE))
            );
            if ($this->db->insert('lead_stage', $leadInfo)) {
                $data['message'] = '<div class="alert alert-success alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
								<strong>Success ! </strong> Lead added successfully. 
							</div>';
                $data['allRecords'] = $this->common->getAllData('lead_stage');
                $this->load->view('temp/header');
                $this->load->view('addLeadStage',$data);
                $this->load->view('temp/footer');
            }
        }else {     
            $data['allRecords'] = $this->common->getAllData('lead_stage');       
            $this->load->view('temp/header');
            $this->load->view('addLeadStage',$data);
            $this->load->view('temp/footer');
        }
    }
    function addLocation() { 
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }
        if ($this->input->post('submit', TRUE)) {
            $leadInfo = array(
                'name' => $this->db->escape_like_str($this->input->post('location', TRUE))
            );
            if ($this->db->insert('lead_location', $leadInfo)) {
                $data['message'] = '<div class="alert alert-success alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
								<strong>Success ! </strong> Lead added successfully. 
							</div>';
                $data['allRecords'] = $this->common->getAllData('lead_location');
                $this->load->view('temp/header');
                $this->load->view('addLocation',$data);
                $this->load->view('temp/footer');
            }
        }else {
            $data['allRecords'] = $this->common->getAllData('lead_location');
            $this->load->view('temp/header');
            $this->load->view('addLocation',$data);
            $this->load->view('temp/footer');
        }
    }

    function addActivity() { 
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }
        if ($this->input->post('submit', TRUE)) {
            $leadInfo = array(
                'name' => $this->db->escape_like_str($this->input->post('activity', TRUE))
            );
            if ($this->db->insert('lead_activity', $leadInfo)) {
                $data['message'] = '<div class="alert alert-success alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
								<strong>Success ! </strong> Lead added successfully. 
							</div>';
                $data['allRecords'] = $this->crmmodule->getDataExceptId(1,'lead_activity');
                $this->load->view('temp/header');
                $this->load->view('addActivity',$data);
                $this->load->view('temp/footer');
            }
        }else {
            $data['allRecords'] = $this->crmmodule->getDataExceptId(1,'lead_activity');
            $this->load->view('temp/header');
            $this->load->view('addActivity',$data);
            $this->load->view('temp/footer');
        }
    }

    function displayLeads() { 
        $data['lead'] = $this->crmmodule->allLeads();
        $this->load->view('temp/header');
        $this->load->view('displayLeads', $data);
        $this->load->view('temp/footer');
    }
    public function checkEmail(){
        $value = $this->input->get('val');
        $query = $this->db->query("SELECT email FROM lead WHERE email='$value'");
        foreach($query->result_array() as $row){
            $email = $row['email'];
        }
        if(!empty($email)){           
            echo '<div class="row"><div class="alert alert-danger">
                    <strong>Notic: </strong> This email <b>'.$value.'</b> have in our database from past. One email account you can not use for two time.
                </div></div>';
        }
    }

    public function checkMobile(){
        $value = $this->input->get('val');
        if(strlen($value) != 10){
            echo '<div class="row"><div class="alert alert-danger">
            <strong>Notic: </strong> Please enter 10 digit phone number.
            </div></div>';
        }else{
            $query = $this->db->query("SELECT mobile FROM lead WHERE mobile='$value'");
            foreach($query->result_array() as $row){
                $mobile = $row['mobile'];
            }
            if(!empty($mobile)){           
                echo '<div class="row"><div class="alert alert-danger">
                        <strong>Notic: </strong> This phone number <b>'.$value.'</b> have in our database from past. One phone number you can not use for two time.
                    </div></div>';
            }
        }
    }
    
    public function checkName(){
        $value = $this->input->get('val');
        $tbl_name = $this->input->get('tbl_name');
        $query = $this->db->query("SELECT name FROM $tbl_name WHERE name='$value'");
        foreach($query->result_array() as $row){
            $name = $row['name'];
        }
        if(!empty($name)){           
            echo '<div class="row"><div class="alert alert-danger">
                    <strong>Notic: </strong> This name <b>'.$value.'</b> have in our database from past. One name you can not use for two time.
                </div></div>';
        }
        
    }

    //This function will delete master entry.
    public function deleteEntry() {
        $id = $this->input->get('id', TRUE);
        $tbl_name = $this->input->get('tbl_name', TRUE);
        $this->db->delete($tbl_name, array('id' => $id));
        $data['allRecords'] = $this->common->getAllData($tbl_name);
        $data['message'] = '<div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                            <strong>Success ! </strong>  Record deleted successfully. 
                    </div>';
        $this->load->view('temp/header');
        if($tbl_name === 'lead_source'){
            $this->load->view('addLeadSource',$data);
        }else if($tbl_name === 'lead_source'){
            $this->load->view('addLeadStage',$data);
        }else{
            $this->load->view('addLocation', $data);
        }
        $this->load->view('temp/footer');
    }

    public function leadDetails() {
        $id = $this->input->get('id');
        $stageId = $this->input->get('stage');
        $data['lead'] = $this->crmmodule->LeadsById($id);
        $data['leadHistory'] = $this->crmmodule->LeadHisById($id);
        $data['stages'] = $this->common->getAllData('lead_stage');
        $data['activity'] = $this->crmmodule->getDataExceptId(1,'lead_activity');
        $data['stages1'] = $this->crmmodule->getDataExceptId($stageId,'lead_stage');
         $this->load->view('temp/header');
        $this->load->view('leadDetails', $data);
        $this->load->view('temp/footer');
    }
    public function activity() {
        $lead_stage = $this->input->post('lead_stage', TRUE);
        $stageId = $this->input->post('stageId', TRUE);
        $lead_activity = $this->input->post('lead_activity', TRUE);
        $act_remark = $this->input->post('act_remark', TRUE);
        $fdate = $this->input->post('fdate', TRUE);
        $ftime = $this->input->post('ftime', TRUE);
        $leadId = $this->input->post('leadId', TRUE);
        $fremark = $this->input->post('fremark', TRUE);
        if($lead_stage == ''){            
            $lead_stage = $stageId;
        }
        $arr = array(
            'lead_stage' => $lead_stage
        );
        $this->db->where('id', $leadId);
        $this->db->update('lead', $arr);
        $leadHistory = array(
            'lead_id' => $leadId,
            'lead_activity_id' => $lead_activity,
            'lead_remark' => $act_remark,
            'stage_id' => $lead_stage,
            'followup' => date("Y-m-d H:i:s", strtotime($fdate)),
            'followup_remark' => $fremark
        );
       // die(print_r($leadHistory));
        if ($this->db->insert('lead_history', $leadHistory)) {
            echo "<script type='text/javascript'>alert('Lead Stage Updated successfully.');
            window.location.href='crm/displayLeads/';
            </script>";           
        }
    }

    
}
