<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('acc_eai'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        $id = $this->input->get('id');
                        echo form_open("account/editPaymentGateway?id=$id", $form_attributs);
                        ?>
                        <?php foreach ($pGatwayInfo as $row) { ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('header_sname');?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">                          
                                        <input class="form-control" type="text" name="institute_id"  value="<?php $data = $this->common->getSingleField('name','institute','lms_id',$row['institute_id']); if($data){echo $data[0]->name;} ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('pOption');?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">                          
                                        <input class="form-control" type="text" name="payment_option"  value="<?php echo $row['payment_option']; ?>" readonly>
                                    </div>
                                </div>
                                <?php 
                                if($row['payment_option'] == 'payu'){ ?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo lang('mkey'); ?> <span class="requiredStar"> *</span></label>
                                        <div class="col-md-6">
                                            <input class="form-control" type="text" name="mkey" id="mkey" value="<?php echo $row['merchant_key']; ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo lang('msalt'); ?>  <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input class="form-control" type="text" id="msalt" name="msalt" value="<?php echo $row['merchant_salt']; ?>">
                                        </div>
                                    </div>  <?php 
                                }else{ ?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo lang('mid'); ?> <span class="requiredStar"> *</span></label>
                                        <div class="col-md-6">
                                            <input class="form-control" type="text" name="mid" id="mid" value="<?php echo $row['merchant_id']; ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo lang('kid'); ?>  <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input class="form-control" type="text" placeholder="" name="kid" id="kid" value="<?php echo $row['key_id']; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo lang('ksecret'); ?>  <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input class="form-control" type="text" placeholder="" name="secret" id="secret" value="<?php echo $row['key_secret']; ?>">
                                        </div>
                                    </div>
                                    <?php     
                                }
                                ?>
                            </div>
                        <?php } ?>
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" class="btn green" name="submit" value="Update"><?php echo lang('save'); ?></button>
                                <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>


        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script>
    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
