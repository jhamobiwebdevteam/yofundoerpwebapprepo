<head>
  <link href="<?php echo base_url()?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    <style>
        body {
            height: 842px;
            width: 595px;
            /* to centre page on screen*/
            margin-left: auto;
            margin-right: auto;
        }
        .main{
            border: 1px solid #000;
        }
        .title{
            
            font-size:32px; 
            color:#222; 
            letter-spacing:1px; 
            
        }
        .row {
            margin:0px 1px;
            /* border: 1px solid #000; */
        }
        .pd{
            padding: 0px 0px;
        }
        .borderLeft{
            border-left: 1px solid #000;
            border-right: 1px solid #000;
        }
        .engcr{
            margin-top: 7%;
        }
        /*.borderRight{
            border-right: 1px solid #000;
        }
        .borderBottom{
            border-bottom: 1px solid #000;
        }*/
        .borderTop{
            border-top: 1px solid #000;
            border-radius: 5px;
            border-bottom: 1px solid #000;
        }
        .pd10{
            padding : 10px;
        }
        .pd20{
            padding : 20px;
        }
        .pd40{
            padding: 40px;
        }
        #recTitle {
            margin: 8px;
            font-size: 18px;
            text-align: center;
        }
        .mg15{
            margin-top: 15px;
        }
        .borderTopDash{
            border-top: 1px dashed #6e6a6a;
        }
        .font11{
            font-size: 11px;
        }
        .sign{
            text-align: right;
            padding: 50 10 20;
        }
        .alignCenter{
            text-align: center;
        }
        .alignRight{
            text-align: right;
        }
        @media print{
            .card-footer{
                display:none
            }
            /* @page {
                size: auto;
                margin: 20mm 0mm 0mm;
            } */
            .row {
            margin:0px 1px;
            }
            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6,
      .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
           float: left;               
        }
        .col-sm-12 {
            width: 100%;
        }
        .col-sm-11 {
            width: 91.66666666666666%;
        }
        .col-sm-10 {
            width: 83.33333333333334%;
        }
        .col-sm-9 {
                width: 75%;
        }
        .col-sm-8 {
                width: 66.66666666666666%;
        }
        .col-sm-7 {
                width: 58.333333333333336%;
        }
        .col-sm-6 {
                width: 50%;
        }
        .col-sm-5 {
                width: 41.66666666666667%;
        }
        .col-sm-4 {
                width: 33.33333333333333%;
        }
        .col-sm-3 {
                width: 25%;
        }
        .col-sm-2 {
                width: 16.666666666666664%;
        }
        .col-sm-1 {
                width: 8.333333333333332%;
            }
            .main{
            border: 1px solid #000;
        }
        .title{            
            font-size:32px; 
            color:#222; 
            letter-spacing:1px; 
            font-weight:400;
        }
        .row {
            margin:0px 2px;
            /* border: 1px solid #000; */
        }
        .pd{
            padding:0;
        }
        .borderLeft{
            border-left: 1px solid #000;
        }
        .borderRight{
            border-right: 1px solid #000;
        }
        .borderBottom{
            border-bottom: 1px solid #000;
        }
        .borderTop{
            border-top: 1px solid #000;
        }
        .pd10{
            padding : 10px;
        }
        .pd20{
            padding : 20px;
        }
        .pd40{
            padding: 40px;
        }
        #recTitle {
            margin: 8px;
            font-size: 18px;
            text-align: center;
        }
        .mg15{
            margin-top: 15px;
        }
        .borderTopDash{
            border-top: 1px dashed #6e6a6a;
        }
        .font11{
            font-size: 11px;
        }
        .sign{
            text-align: right;
            padding: 50 10 20;
        }
        .alignCenter{
            text-align: center;
        }
        .alignRight{
            text-align: right;
        }
        
        }
    </style>
  </head>

    <div class="col-md-12 main" id="printableArea">
        
        <span class="title"><center>
        <img src="<?php echo base_url();?>assets/images/univ_logo.png" style="width:10%;height:5%;"> UNIVERSITY OF PUNE</center></span>
        <div class="alignCenter"><b>GANESHKHIND, PUNE - 411 007</b></div>
        <div class="alignCenter"><b>STATEMENT OF MARKS FOR</b> B.Sc.(COMP.SC.)(2023) <b>EXAM - APRIL 2023</b></div>
        <div class="row borderRight borderLeft borderTop">
            <div class="col-sm-4 pd borderRight">
                <div class="borderBottom "><strong>SEAT NO. </strong>
                    AIS/202122/3332</div>
                <div class=""><strong>NAME</strong> SWATI BELURE</div>
            </div>
            <div class="col-sm-4 pd borderRight borderBottom ">
                <b>CENTER</b> ABCD
            </div>
            <div class="col-sm-4 pd ">
                <div class="borderBottom"><strong>PERM REG NO.</strong> 21-11-2022</div>
                <div  class="borderBottom"><strong>MOTHER</strong> SWATI</div>
            </div>
        </div>

        <div class="mg15">
            <div class="row borderRight">
                <div class="col-sm-8 pd borderRight alignCenter"><strong>COURSE NAME</strong></div>
                <div class="col-sm-4 pd alignCenter"><strong>MARKS OBTAINED</strong></div>
            </div>
            <div class="row borderRight">
                <div class="col-md-8 pd"></div>
                <div class="col-md-1 pd alignRight">INT.</div>
                <div class="col-md-1 pd alignRight">EXT.</div>
                <div class="col-md-2 pd alignRight">TOTAL</div>
            </div>
            <div class="row  ">
                <div class="col-md-1">21111</div>
                <div class="col-md-2">CS-211</div>
                <div class="col-md-5">DATA STRUCTURE USING C</div>
                <div class="col-md-1">08</div>
                <div class="col-md-1">29</div>
                <div class="col-md-offset-1 col-md-1">37</div>
            </div>
            
            <div class="row ">
                <div class="col-md-1">21112</div>
                <div class="col-md-2">CS-212</div>
                <div class="col-md-5">RELA. DATABASE MGNT SYSTEM (RDBMS)</div>
                <div class="col-md-1">07</div>
                <div class="col-md-1">23</div>
                <div class="col-md-offset-1 col-md-1">30</div>
            </div>
             <div class="row ">
                <div class="col-md-1">21211</div>
                <div class="col-md-2">MTC-211</div>
                <div class="col-md-5">LINEAR ALGEBRA</div>
                <div class="col-md-1">07</div>
                <div class="col-md-1">30</div>
                <div class="col-md-offset-1 col-md-1">37</div>
            </div>
             <div class="row ">
                <div class="col-md-1">21212</div>
                <div class="col-md-2">MTC-212</div>
                <div class="col-md-5">NUMERICAL ANALYSIS</div>
                <div class="col-md-1">06</div>
                <div class="col-md-1">22</div>
                <div class="col-md-offset-1 col-md-1">28</div>
            </div>
             <div class="row ">
                <div class="col-md-1">21312</div>
                <div class="col-md-2">ELC-211</div>
                <div class="col-md-5">MICROPROCESSOR ARCHITECTURE & PROG</div>
                <div class="col-md-1">07</div>
                <div class="col-md-1">25</div>
                <div class="col-md-offset-1 col-md-1">32</div>
            </div>
            <div class="row ">
                <div class="col-md-1">21312</div>
                <div class="col-md-2">ELC-212</div>
                <div class="col-md-5">COMMUNICATION PRINCIPLES</div>
                <div class="col-md-1">08</div>
                <div class="col-md-1">18</div>
                <div class="col-md-offset-1 col-md-1">26</div>
            </div>
            <div class="row">
                <div class="col-md-1">21511</div>
                <div class="col-md-2"></div>
                <div class="col-md-5">ENGLISH</div>
                <div class="col-md-1">08</div>
                <div class="col-md-1">27</div>
                <div class="col-md-offset-1 col-md-1">35</div>
            </div>
        </div>

        <div class="engcr">
             <div class="row">
                <div class="col-md-1">22111</div>
                <div class="col-md-2">CS- 221</div>
                <div class="col-md-5">OBJ. ORIENTED CONCEPTS $ PROG.IN C++</div>
                <div class="col-md-1">10</div>
                <div class="col-md-1">23</div>
                <div class="col-md-offset-1 col-md-1">33</div>
            </div>

            <div class="row">
                <div class="col-md-1">22112</div>
                <div class="col-md-2">CS- 222</div>
                <div class="col-md-5">SOFTWARE ENGINEERING</div>
                <div class="col-md-1">10</div>
                <div class="col-md-1">29</div>
                <div class="col-md-offset-1 col-md-1">39</div>
            </div>
            <div class="row">
                <div class="col-md-1">22113</div>
                <div class="col-md-2">CS- 224</div>
                <div class="col-md-5">PRACTICAL ASSIGNMENT LAB COURSE II</div>
                <div class="col-md-1">GRADE</div>
                <div class="col-md-1"></div>
                <div class="col-md-offset-1 col-md-1">A+</div>
            </div>
            <div class="row">
                <div class="col-md-1">22211</div>
                <div class="col-md-2">MTC- 221</div>
                <div class="col-md-5">COMPUTATIONAL GEOMATRY</div>
                <div class="col-md-1">9</div>
                <div class="col-md-1">22</div>
                <div class="col-md-offset-1 col-md-1">31</div>
            </div>
        </div>
    </div>

    <!--<div class="card-footer">
        <button class="btn btn-info btn-sm float-right" onclick="print()">Print</button>
        <button class="btn btn-danger btn-sm" onclick="location.href = 'javascript:history.back()'"><?php //echo lang('back'); ?></button>
        </div> -->