<!--Start page level style-->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!--Start page level style-->
<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- <div class="row">
            <div class="col-md-12">
                
                <h3 class="page-title">
                    <?php echo lang('header_feeScheme'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_fees'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_feeScheme'); ?>
                    </li>
                   
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12 ">
                <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('fee_info_heading'); ?>
                        </div>
                        
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable tabbable-custom tabbable-full-width">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a class="nav-link active" data-toggle="tab" href="#tabs-1">Basic Info</a>
                                </li>
                                <li class="disabled" style="pointer-events: none;cursor: default;opacity 0.6;">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-2">Fees Payment Schedule</a>
                                </li>
                            
                            </ul><!-- Tab panes -->
                         <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form', 'method'=>'post');
                        echo form_open_multipart('feesManagement/feeScheme_detail', $form_attributs);
                        ?>
                        <div class="form-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tabs-1">

                                    <?php
                                    if (!empty($success)) {
                                        echo $success;
                                    }
                                    ?>

                                    <div class="form-group col-md-12">
                                        <label class="col-md-2 control-label"><?php echo lang('header_sname');?> <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                        <!-- <input type="text" class="form-control" name="school_name"> -->                                    
                                            <select class="form-control" name="school_name" id="school_name" required>
                                                <option value="">Select School Name</option>
                                                <?php foreach($data['data'] as $key) { ?>
                                                    <option value="<?php echo $key['id'];?>"><?php echo $key['name'];?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                   
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> <?php echo lang('clas_class_title'); ?> <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" placeholder="<?php echo lang('clas_title_plash'); ?>" name="class_title" id="class_title" data-validation="required" data-validation-error-msg="<?php echo lang('clas_cls_tit_requi'); ?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo lang('fee_category'); ?> 
                                        <span class="requiredStar" required> * </span></label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" placeholder="Fee Category Name eg:Admission, Registraion, Transportation" name="feeCategory" required="required"> -->
                                            <select class="form-control" name="feeCategory" id="feeCategory" required>
                                                <option><?php echo lang('stu_sel_cla_select');?></option>
                                                <option value="Admission Fee">Admission Fee</option>
                                                <option value="Registration Fee">Registration Fee</option>
                                                <option value="Transportation Fee">Transportation Fee</option>
                                            </select>
                                           
                                        </div>
                                    </div>

                                    <div class="form-actions fluid">
                                        <div class="col-md-offset-3 col-md-6">                                             
                                            <a  href="#tabs-2" data-toggle="tab" class="btn default" >Next</a>         
                                        </div>
                                    </div>
                                </div>


                                <div class="tab-pane" id="tabs-2">
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Is fees on Installment basis?</label>

                                        <div class="col-md-1">
                                            <input type="checkbox" class="form-control feesYearly"  name="yesYearly" value="yesFeeYearly" onclick="yearlyFees()">
                                        </div>
                                        <label class="col-md-1 control-label">Yes</label>
                                        
                                        <div class="col-md-1">
                                            <input type="checkbox" class="form-control no_checkbox"  name="yesMonthly" value="noYearly" onclick="noYearlyFees()">                                                           
                                        </div>
                                        <label class="col-md-1 control-label">No</label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Total Amount</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="totAmt" id="totAmt">
                                        </div>                                      
                                    </div>

                                    <div class="form-group hidediv_ofMonths" style="display:none;">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Jan </label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="month" value="jan" onclick="selectmonth()" id="month">                                
                                            </div>
                                            <label class="col-md-2 control-label">Feb </label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="month" value="feb" onclick="selectmonth()" id="month">                                
                                            </div>
                                            <label class="col-md-2 control-label">Mar </label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="month" value="mar" onclick="selectmonth()" id="month">                                
                                            </div>
                                            <label class="col-md-2 control-label">Apr </label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="apr" value="apr" onclick="selectmonth()" id="month">                                
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            
                                            <label class="col-md-2 control-label">May </label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="may" value="may" onclick="selectmonth()">                                
                                            </div>
                                            <label class="col-md-2 control-label">June </label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="june" value="june" onclick="selectmonth()">                                
                                            </div>
                                            <label class="col-md-2 control-label">July</label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="jule" value="jule" onclick="selectmonth()">                                
                                            </div>
                                            <label class="col-md-2 control-label">Aug</label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="aug" value="aug" onclick="selectmonth()">                                
                                            </div>
                                        </div>
                                    
                                        <div class="form-group">
                                             <label class="col-md-2 control-label">Sept</label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="sept" value="sept" onclick="selectmonth()">                                
                                            </div>
                                            <label class="col-md-2 control-label">Oct</label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="oct" value="oct" onclick="selectmonth()">                                
                                            </div>
                                            <label class="col-md-2 control-label">Nov</label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="nov" value="nov" onclick="selectmonth()">                                
                                            </div>
                                            <label class="col-md-2 control-label">Dec</label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="dec" value="dec" onclick="selectmonth()">                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="portlet-body show_month_div" >
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                                
                                        </table>
                                    </div>


                                    <div class="form-group show_div" style="display: none;">
                                        <!-- <label class="col-md-2 control-label">Installment Start Date</label>
                                        <div class="col-md-4">
                                            <input type="date" class="form-control" name="installment_startdate" id="installment_startdate">
                                        </div>
                                    
                                        <label class="col-md-2 control-label">Installment End Date</label>
                                        <div class="col-md-4">
                                            <input type="date" class="form-control" name="installment_enddate" id="installment_enddate">
                                        </div> -->

                                        <div class="form-group installment_per_div">
                                            <label class="col-md-2 control-label">No. of Installments<span class="requiredStar"> * </span></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control inst_period" name="no_of_installment" id="installment_num" onkeyup="press()">                               
                                            </div>
                                       
                                        </div>
                                    </div>

                                    <div class="portlet-body show_installment_div" >
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                            
                                        </table>
                                        
                                       
                                    </div> 

                                    <div class="form-actions fluid">
                                        <div class="col-md-offset-3 col-md-6">
                                            <!-- <button type="" id="Button" class="btn green" name="submit" value="submit">Update</button> -->
                                            <button type="submit" class="btn green" name="submit" value="submit"><?php echo lang('save');?></button>     
                                            
                                            <a  href="#tabs-1" data-toggle="tab" class="btn default">Back</a>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <?php echo form_close(); ?>
                       
                    </div>
                </div>
            </div>
            </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    function yearlyFees()
    {
        var lumpsum = $('.feesYearly').is(":checked", true);
        if(lumpsum)
           
            $(".show_div").show();

    }

    function noYearlyFees()
    {
        var monthly = $('.no_checkbox').is(":checked", true);
        if(monthly)
            $(".hidediv_ofMonths").show();
            $(".show_div").hide();
    }

    function press()
    {
        //var start_date = $("#installment_startdate").val();
        //var end_date = $("#installment_enddate").val();        
       
        //var total2 = "";
        var table = $("#sample_1");
        var rowNum = parseInt($("#installment_num").val(), 10);
       
        var resultHtml = '';
                
       //today = dd + '/' + mm + '/' + yyyy;
        resultHtml += ["<tr>", 

        '<th><?php echo lang('fee_instal_head'); ?></th>',
        '<th><?php echo lang('fee_amt'); ?></th>',
        '<th>Installment Start Date</th>',
        '<th>Installment End Date</th>',


        '</tr>'].join("\n");


        
        for(var i = 0 ; i < rowNum ; i++) 
        {       
           
            var day_dt = "<input type='date' class='datetime' name='getinstmntodat_"+i+"' id='getinstmntodat_"+i+"' required>";
            var enddt = "<input type='date' class='datetime' name='enddate_"+i+"' id='enddate_"+i+"' required>";
            $( "#getinstmntodat_" ).datepicker({ minDate: 0}); 
            resultHtml += ["<tr>", 
                 "<td>", 
                  (i+1),
                 "</td>",
                 "<td name='installment_amount' id='installment_amount' >"+"<input type='text' name='instalamt_"+i+"' id='instalamtconcat_"+i+"' onkeyup='getinstamount("+i+")'>"+"</td>",
                
                    "<td name='today_dt_strt' id='today_dt_strt'>"+ day_dt + "</td>",
                    "<td name='today_dt_end' id='today_dt_end'>"+ enddt +"</td>",
                    
                    
                 '</tr>'].join("\n");

           
        }  
         
       
        table.html(resultHtml);
        return false;

    }



    function getinstamount(text_id)
    { //debugger;
        var instalment_amount = 0;
        var res = 0;
        var totalamt = document.getElementById("totAmt").value;
        var instalnumber = document.getElementById("installment_num").value;

       for(var h=0;h<instalnumber;h++)
        {

             instalment_amount = document.getElementById("instalamtconcat_"+h).value;
             if(instalment_amount!='undefined'||instalment_amount!=undefined)
             {
                 res = Number(res) + Number(instalment_amount);
             }

             
        }
        
        if(res>totalamt)
        {
            alert("Installment Amount Should be less than total amount.");
        }
        
           
    }

    function selectmonth()
    {
        var monthlyFee = document.getElementById("totAmt").value;
        var mm = $('.month_checkbox:checked').size();
        
        var totamout = monthlyFee/mm;

        var table2 = $("#sample_2");
        var resultHtmlview = '';
                
       //today = dd + '/' + mm + '/' + yyyy;
        resultHtmlview += ["<tr>", 

        '<th>Srno</th>',
        '<th>Amount</th>',
        '<th>Date</th>',

        '</tr>'].join("\n");

        
        for(var j = 0 ; j < mm ; j++) 
        {            
            var dateofmonthly_installment = "<input type='date' class='datetime' name='tddate_"+j+"' id='tddate_"+j+"' onchange='getmonthlyInstalldt()'>";
            resultHtmlview += ["<tr>", 
                 "<td>", 
                  (j+1),
                 "</td>",
                 "<td name='monthly_inst_amount' id='monthly_inst_amount'>"+totamout+"</td>",
                 "<td name='monthly_installment_dt' id='monthly_installment_dt'>"+ dateofmonthly_installment + "</td>",
                   
                 '</tr>'].join("\n");
        }

        table2.html(resultHtmlview);
        return false;
        

    }

    function getmonthlyInstalldt()
    {
        var modt = document.getElementById("tddate").value;
        //console.log(modt);

    }

</script>
