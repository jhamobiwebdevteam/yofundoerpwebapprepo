<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- <div class="row">
            <div class="col-md-12">
                
                <h3 class="page-title">
                    <?php echo lang('header_tax'); ?> <small></small>
                </h3>
                 <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_fees'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_tax'); ?>
                    </li>
                   
                    <li id="result" class="pull-right topClock"></li>
                </ul>
               
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12 ">
                <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('tax_info_heading'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        echo form_open_multipart('feesManagement/addTaxDetail', $form_attributs);
                        ?>
                        <div class="form-body">
                            <?php
                            if (!empty($success)) {
                                echo $success;
                            }
                            ?>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('tax_name'); ?> <span class="requiredStar"> * </span></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Tax Name eg:GST" name="taxname" required="required">
                                <div id="checkName" class="col-md-12"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('tax_sgst'); ?> <span class="requiredStar"> * </span></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="SGST" name="tax_sgst" required="required">
                                <div id="checkName" class="col-md-12"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('tax_cgst'); ?> <span class="requiredStar"> * </span></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="CGST" name="tax_cgst" required="required">
                                <div id="checkName" class="col-md-12"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('tax_igst'); ?> <span class="requiredStar"> * </span></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="IGST" name="tax_igst" required="required">
                                <div id="checkName" class="col-md-12"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('tax_total_per'); ?> <span class="requiredStar"> * </span></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Total Percentage" name="tax_total_per" required="required">
                                <div id="checkName" class="col-md-12"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('tax_frm_dt'); ?> <span class="requiredStar"> * </span></label>
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="tax_frmdate" required="required">
                                <div id="checkName" class="col-md-12"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('tax_to_dt'); ?> <span class="requiredStar"> * </span></label>
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="tax_todate" required="required">
                                <div id="checkName" class="col-md-12"></div>
                            </div>
                        </div>
                                                        
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" id="Button" class="btn green" name="submit" value="submit"><?php echo lang('tea_si'); ?></button>
                                <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <div class="col-md-12">
                <!-- BEGIN All account list-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('tax_list'); ?> 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <?php echo lang('tax_id'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('tax_name'); ?> 
                                    </th> 
                                    <th>
                                        <?php echo lang('tax_frm_dt');?>
                                    </th>  
                                    <th>
                                        <?php echo lang('tax_to_dt');?>
                                    </th> 
                                    <th>
                                        <?php echo lang('tax_cgst');?>
                                    </th>
                                    <th>
                                        <?php echo lang('tax_sgst');?>
                                    </th>
                                    <th>
                                        <?php echo lang('tax_igst');?>
                                    </th>
                                    <th>
                                        <?php echo lang('tax_total_per');?>
                                    </th>
                                    <th>
                                        <?php echo lang('tax_status'); ?> 
                                    </th>                                 
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($tax_info as $row) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['tax_name']; ?>
                                        </td> 
                                        <td>
                                            <?php echo $row['from_date']; ?>
                                        </td> 
                                        <td>
                                            <?php echo $row['to_date']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['cgst']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['sgst']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['igst']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['total_percentage']; ?>
                                        </td>
                                        <td>    
                                            <!-- <a class="btn btn-xs red" href="index.php/crm/deleteEntry?id=<?php //echo $row['id']; ?>&tbl_name=lead_source" onclick="javascript:return confirm('<?php //echo lang('dlte'); ?>')"> <i class="fa fa-trash-o"></i> <?php// echo lang('delete'); ?> </a>  -->
                                            
                                            <i data-id="<?php echo $row['id'];?>" class="status_checks btn
                                                  <?php echo ($row['status'])?
                                                  'btn-success btn-xs': 'btn-danger btn-xs'?>">
                                                  <?php echo ($row['status'])? 'Active' : 'Inactive'?>
                                              </i>
                                         </td>  

                                         <td>
                                          </td>                                    
                                    </tr>
                                <?php $i++;} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END All account list-->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script type="text/javascript">

    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
$(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '0' : '1';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure want to "+ msg)){
        var current_element = $(this);
        var dataId = $(this).attr("data-id");
        //alert(dataId);
        //url = "http://localhost/codeingniter/Users/status_update";
        url = "<?php echo base_url().'feesManagement/status_update'?>";
        //alert(url);
        $.ajax({
          type:"POST",
          url: url,
          data: {id:dataId,status:status},
         
          success: function(response)
          {   
            alert('Successfully Activated!')
            location.reload();
           //console.log(response);
          }
        });
      }      
    });
</script>