
<?php
// $description        = "Product Description";
// $txnid              = date("YmdHis");     
// $key_id             = "rzp_test_S2a1GfK3VOga3S";
// $currency_code      = "INR";            
// $total              = (1* 100); // 100 = 1 indian rupees
// $amount             = 1;
// $merchant_order_id  = "ABC-".date("YmdHis");
// $card_holder_name   = 'Someshwar Badade';
// $email              = 'someshbadade@gmail.com';
// $phone              = '8806056756';
// $name               = "RazorPay Jhamobi";
?>
<style>
    .lbl{
        padding-top: 9px;
        margin-bottom: 0;
        text-align: left;
    }
</style>
<div class="page-content-wrapper">
    <div class="page-content">

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
            <div class="col-md-12">

            <!-- BEGIN PORTLET-->
            <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i><?php echo lang('header_payment'); ?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                    
                    <form name="razorpay-form" class="form-horizontal" id="razorpay-form" action="<?php echo $callback_url; ?>" method="POST">
                    <div class="form-group">
                                <label class="col-md-3 control-label">Transaction Id : </label>
                                <label class="col-md-2 lbl"><?= $txnid; ?></label>
                            </div>
                    <div class="form-group">
                                <label class="col-md-3 control-label">Name : </label>
                                <label class="col-md-2 lbl"><?=$card_holder_name?></label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email : </label>
                                <label class="col-md-2 lbl"><?=$email?></label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Phone : </label>
                                <label class="col-md-2 lbl"><?=$phone?></label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Amount : </label>
                                <label class="col-md-2 lbl"><?=$amount?> <?=$currency_code?></label>
                            </div>
                    <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" />
                    <input type="hidden" name="merchant_order_id" id="merchant_order_id" value="<?php echo $merchant_order_id; ?>"/>
                    <input type="hidden" name="merchant_trans_id" id="merchant_trans_id" value="<?php echo $txnid; ?>"/>
                    <input type="hidden" name="merchant_product_info_id" id="merchant_product_info_id" value="<?php echo $description; ?>"/>
                    <input type="hidden" name="merchant_surl_id" id="merchant_surl_id" value="<?php echo $surl; ?>"/>
                    <input type="hidden" name="merchant_furl_id" id="merchant_furl_id" value="<?php echo $furl; ?>"/>
                    <input type="hidden" name="card_holder_name_id" id="card_holder_name_id" value="<?php echo $card_holder_name; ?>"/>
                    <input type="hidden" name="merchant_total" id="merchant_total" value="<?php echo $total; ?>"/>
                    <input type="hidden" name="merchant_amount" id="merchant_amount" value="<?php echo $amount; ?>"/>

                   

                </form>
                <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                            <!-- <input type="submit" value="Pay" class="btn green" /> -->
                            <input  id="pay-btn" type="submit" onclick="razorpaySubmit(this);" value="Pay Now" class="btn green" />
                                <!-- <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button> -->
                            </div>
                        </div>
                    </div>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END PORTLET-->
                
            </div>
    </div>
    
    
 </div>
</div>
<!-- END CONTENT -->
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script>
        var options = {
            key:            "<?php echo $key_id; ?>",
            amount:         "<?php echo $total; ?>",
            name:           "<?php echo $name; ?>",
            description:    "Order # <?php echo $merchant_order_id; ?>",
            netbanking:     true,
            currency:       "<?php echo $currency_code; ?>", // INR
            prefill: {
                name:       "<?php echo $card_holder_name; ?>",
                email:      "<?php echo $email; ?>",
                contact:    "<?php echo $phone; ?>"
            },
            notes: {
                soolegal_order_id: "<?php echo $merchant_order_id; ?>",
            },
            handler: function (transaction) {
                document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;
                document.getElementById('razorpay-form').submit();
            },
            "modal": {
                "ondismiss": function(){
                    location.reload()
                }
            }
        };

        var razorpay_pay_btn, instance;
        function razorpaySubmit(el) {
            if(typeof Razorpay == 'undefined') {
                setTimeout(razorpaySubmit, 200);
                if(!razorpay_pay_btn && el) {
                    razorpay_pay_btn    = el;
                    el.disabled         = true;
                    el.value            = 'Please wait...';  
                }
            } else {
                if(!instance) {
                    instance = new Razorpay(options);
                    if(razorpay_pay_btn) {
                    razorpay_pay_btn.disabled   = false;
                    razorpay_pay_btn.value      = "Pay Now";
                    }
                }
                instance.open();
            }
        }  
    </script>
