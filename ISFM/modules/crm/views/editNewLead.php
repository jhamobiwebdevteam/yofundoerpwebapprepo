<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('header_a_lead_edit'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php  echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_crm'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_master'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_a_lead_edit'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('crm_gtifnt'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        foreach ($lead as $row) {
                            $id = $row['id'];
                            $fname = $row['fname'];
                            $lname = $row['lname'];
                            $email = $row['email'];
                            $mobile = $row['mobile'];
                            $lead_source = $row['lead_source'];
                            $class = $row['class'];
                            $location = $row['location'];
                        }
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        echo form_open_multipart("crm/editLead?id=$id", $form_attributs);
                        ?>
                        <div class="form-body">
                            <?php
                            if (!empty($success)) {
                                echo $success;
                            }
                            ?>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_fn'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="First Name" name="first_name" data-validation="required" data-validation-error-msg="" value = <?php echo $fname; ?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_ln'); ?> <span class="requiredStar">  </span></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Last Name" name="last_name" data-validation-error-msg="" value = <?php echo $lname; ?>>
                                </div>
                            </div>                          
                          
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_pn'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="mobile" onchange="checkMobile(this.value)" placeholder=""  data-validation="required" maxlength="10" minlength = "10" data-validation-error-msg="" value = <?php echo $mobile; ?>>
                                    <div id="checkMobile" class="col-md-12"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_eml'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" onchange="checkEmail(this.value)" placeholder="demo@demo.com" name="email" id="name" placeholder=""  data-validation="required" data-validation-error-msg="" value = <?php echo $email; ?>>
                                    <div id="checkEmail" class="col-md-12"></div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_ls'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <select class="form-control" name="lead_sourse" data-validation="required" data-validation-error-msg="You have to select anyone.">
                                        <option value=""><?php echo lang('select'); ?> </option>
                                        <?php foreach ($leadSource as $row) { ?>
                                            <option value="<?php echo $row['id']; ?>" <?php if($row['id'] == $lead_source){?> selected <?php } ?>><?php echo $row['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_cls'); ?> <span class="requiredStar">  </span></label>
                                <div class="col-md-6">
                                    <select name="class" class="form-control" data-validation-error-msg="">
                                        <option value="0"><?php echo lang('select'); ?></option>
                                        <?php foreach ($classTile as $row) { ?>
                                            <option value="<?php echo $row['id']; ?>" <?php if($row['id'] == $class){?> selected <?php } ?> ><?php echo $row['class_title']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_loc'); ?> <span class="requiredStar">  </span></label>
                                <div class="col-md-6">
                                    <select name="location" class="form-control"  data-validation-error-msg="">
                                    <option value=""><?php echo lang('select'); ?> </option>
                                    <?php foreach ($leadLocation as $row) { ?>
                                        <option value="<?php echo $row['id']; ?>" <?php if($row['id'] == $location){?> selected <?php } ?>><?php echo $row['name']; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" id="Button" class="btn green" name="submit" value="submit"><?php echo lang('tea_si'); ?></button>
                                <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL script -->
<script type="text/javascript" src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/components-form-tools.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate();</script>
<script>
    jQuery(document).ready(function () {
            ComponentsFormTools.init();
        });
        function checkEmail(str) {
            var xmlhttp;
            if (str.length === 0) {
                document.getElementById("checkEmail").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                    console.log('hi');
                    document.getElementById("checkEmail").innerHTML = xmlhttp.responseText;
                    if(xmlhttp.responseText != ''){
                        document.getElementById("Button").disabled = true;
                        document.getElementById("Button").setAttribute("style", "cursor:no-drop;    pointer-events: auto;");
                    }else{
                        document.getElementById("Button").disabled = false;
                        document.getElementById("Button").setAttribute("style", "cursor:pointer;    pointer-events: auto;");
                    }
                    
                }
            };
            xmlhttp.open("GET", "index.php/crm/checkEmail?val=" + str, true);
            xmlhttp.send();
            
        }
    
        function checkMobile(str) {
            var xmlhttp;
            if (str.length === 0) {
                document.getElementById("checkMobile").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                    document.getElementById("checkMobile").innerHTML = xmlhttp.responseText;
                    if(xmlhttp.responseText != ''){
                        document.getElementById("Button").disabled = true;
                        document.getElementById("Button").setAttribute("style", "cursor:no-drop;    pointer-events: auto;");
                    }else{
                        document.getElementById("Button").disabled = false;
                        document.getElementById("Button").setAttribute("style", "cursor:pointer;    pointer-events: auto;");
                    }
                    
                }
            };
            xmlhttp.open("GET", "index.php/crm/checkMobile?val=" + str, true);
            xmlhttp.send();
            
        }
    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
<script type="text/javascript">
    var RecaptchaOptions = {
        theme: 'custom',
        custom_theme_widget: 'recaptcha_widget'
    };
</script>
<!-- END PAGE LEVEL script -->