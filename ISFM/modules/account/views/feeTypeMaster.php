<!--Start page level style-->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!--Start page level style-->
<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 ">
                <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('fee_info_heading'); ?>
                        </div>
                        
                    </div>
                    <div class="portlet-body">
                         <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form', 'method'=>'post');
                        echo form_open_multipart('account/feeTypeMaster', $form_attributs);
                        ?>
                          <div class="form-body">
                            <?php
                            if (!empty($success)) {
                                echo $success;
                            }
                            ?>
                     
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tax_name'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="fee_categ_name" required>                      
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('acc_amount'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-4">
                                <input type="text" class="form-control" name="amount" required>               
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('prefix'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-4">
                                <input type="text" class="form-control" name="prefix" required>               
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lib_description'); ?><span class="requiredStar"> * </span></label>
                                <div class="col-md-4">
                                <textarea name="descp" rows="4" cols="50" class="form-control" required></textarea>               
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" id="Button" class="btn green" name="submit" value="submit"><?php echo lang('save'); ?></button>
                                    <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                                </div>
                            </div>
                        
                        </div>
                        <?php echo form_close(); ?>
                       
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        

           
            <div class="col-md-12">
                    <!-- BEGIN All account list-->
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <?php echo lang('fee_detail'); ?> 
                                </div>
                               
                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th><?php echo lang('tax_id'); ?></th>
                                            <th> <?php echo lang('fee_cat_name'); ?> </th>  
                                            <th> <?php echo lang('prefix');?></th>  
                                            <th> <?php echo lang('lib_description');?></th>
                                            <th> <?php echo lang('acc_amount');?></th>   
                                                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; foreach ($fee_info as $row) { ?>
                                            <tr>
                                                <td> <?php echo $i; ?></td>
                                                <td> <?php echo $row['fee_category']; ?></td> 
                                                <td><?php echo $row['recpt_no_prefix']; ?> </td> 
                                                <td><?php echo $row['description']; ?> </td> 
                                                <td><?php echo $row['amount']; ?> </td> 
                                            </tr>
                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
            </div>
          

        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script>
    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>