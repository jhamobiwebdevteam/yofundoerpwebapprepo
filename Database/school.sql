-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2022 at 08:43 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_title`
--

CREATE TABLE `account_title` (
  `id` int(11) NOT NULL,
  `account_title` varchar(100) NOT NULL,
  `category` varchar(20) NOT NULL,
  `description` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_title`
--

INSERT INTO `account_title` (`id`, `account_title`, `category`, `description`) VALUES
(1, 'Rent', 'Expense', ''),
(2, 'Student Fee', 'Income', ''),
(3, 'aa', 'Income', 'asaaa'),
(4, 'bb', 'Income', 'tt');

-- --------------------------------------------------------

--
-- Table structure for table `add_exam`
--

CREATE TABLE `add_exam` (
  `id` int(11) NOT NULL,
  `year` int(5) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `start_date` varchar(30) NOT NULL,
  `class_id` int(11) NOT NULL,
  `total_time` varchar(10) NOT NULL,
  `publish` varchar(50) NOT NULL,
  `final` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_exam`
--

INSERT INTO `add_exam` (`id`, `year`, `exam_title`, `start_date`, `class_id`, `total_time`, `publish`, `final`, `status`) VALUES
(4, 2018, 'Test Exam 1', '09/06/2018', 1, '1 Hour 30 ', 'Publish', 'NoFinal', 'NoResult'),
(5, 2018, 'FInal Exam', '08/10/2018', 3, '30 Minute', 'Not Publish', 'Final', 'NoResult');

-- --------------------------------------------------------

--
-- Table structure for table `bank_details`
--

CREATE TABLE `bank_details` (
  `id` int(11) NOT NULL,
  `bank_name` varchar(50) NOT NULL,
  `account_no` varchar(60) NOT NULL,
  `ifsc_code` varchar(50) NOT NULL,
  `micr_code` varchar(50) NOT NULL,
  `bank_address` varchar(150) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bank_details`
--

INSERT INTO `bank_details` (`id`, `bank_name`, `account_no`, `ifsc_code`, `micr_code`, `bank_address`, `created_at`) VALUES
(1, 'test1', 'sbi345666433', 'ssbb3344', 'dff3333', 'hinjewadi,pune', '2022-06-03 12:10:56'),
(2, 'test2', '345345', 'test22', 'test444', 'ffff', '2022-06-03 12:19:19'),
(3, 'test2', '345345', 'test22', 'test444', 'ffff', '2022-06-03 12:22:05'),
(4, 'ttt333', 'dsfsadf444', 'fds444', 'sdf555', 'sdfs', '2022-06-03 12:25:30'),
(5, 'sdff', 'sdf', 'dsf', 'sdf', 'dfs', '2022-06-03 12:37:25'),
(6, 'ICICI BANK LTD.', '123456789', 'sbc4433', 'mmkk33', 'pune', '2022-06-03 12:53:10'),
(7, 'SBI LTD.', 'si9988777766', 'SBI87978', 'S789999', 'Parli', '2022-06-03 12:57:07');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `isbn_no` varchar(20) NOT NULL,
  `book_no` int(11) NOT NULL,
  `books_title` varchar(100) NOT NULL,
  `authore` varchar(150) NOT NULL,
  `category` varchar(100) NOT NULL,
  `edition` varchar(100) NOT NULL,
  `pages` int(11) NOT NULL,
  `language` varchar(30) NOT NULL,
  `uploderTitle` varchar(100) NOT NULL,
  `books_amount` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `issu_date` int(11) NOT NULL,
  `due_date` int(11) NOT NULL,
  `issu_member_no` int(11) NOT NULL,
  `cover_photo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `date`, `isbn_no`, `book_no`, `books_title`, `authore`, `category`, `edition`, `pages`, `language`, `uploderTitle`, `books_amount`, `status`, `issu_date`, `due_date`, `issu_member_no`, `cover_photo`) VALUES
(1, 1464559200, '978-3-16-148410-0', 20161, 'Book Test 1', 'Author Test 1', 'English', '16 th', 1500, 'English', 'Headmaster', 1, 'Not Available', 1475245412, 1475186400, 1, 'f7cebda75f3c1c09ef31e2e525db8805.gif'),
(2, 1464559200, '978-3-16-148410-1', 20162, 'Book Test 2', 'Author Test 2', 'Bangla ', '16 th', 1500, 'Bangla', 'Headmaster', 2, 'Available', 0, 0, 0, 'eee468e20c7387d459487dafda58709d.gif'),
(3, 1464559200, '978-3-16-148410-2', 20163, 'Book Test 3', 'Author Test 3', 'English', '16 th', 522, 'English', 'Headmaster', 3, 'Available', 0, 0, 0, '6583655141ea7c33d3981bbd7332c586.gif'),
(4, 1464559200, '978-3-16-148410-3', 20164, 'Book Test 4', 'Author Test 4', 'Bangla ', '16 th', 520, 'Bangla', 'Headmaster', 4, 'Available', 0, 0, 0, 'ceabf75a2a7dead8862b7821d3fd3bd4.gif'),
(5, 1464559200, '978-3-16-148410-4', 20165, 'Book Test 5', 'Author Test 5', 'Bangla ', '14 th', 711, 'Bangla', 'Headmaster', 5, 'Available', 0, 0, 0, '9d4b20f013a24ebc882c84b0b344e526.gif'),
(6, 1464559200, '978-3-16-148410-5', 20166, 'Book Test 6', 'Author Test 6', 'English', '10 th', 485, 'English', 'Headmaster', 6, 'Available', 0, 0, 0, '1d28dd98b150b33ad4a708a95031408a.gif'),
(7, 1464559200, '978-3-16-148410-6', 20167, 'Book Test 7', 'Author Test 77', 'English', '9 th', 485, 'English', 'Headmaster', 7, 'Available', 0, 0, 0, '036337321c644b23bd7916f3e4b3eca4.gif'),
(8, 1464559200, '978-3-16-148410-7', 20168, 'Book Test 8', 'Author Test 8', 'English', '14 th', 950, 'English', 'Headmaster', 8, 'Available', 0, 0, 0, '3b174671512c28eaf95be33657e2af69.gif');

-- --------------------------------------------------------

--
-- Table structure for table `books_category`
--

CREATE TABLE `books_category` (
  `id` int(11) NOT NULL,
  `category_creator` varchar(100) NOT NULL,
  `category_title` varchar(100) NOT NULL,
  `parent_category` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `books_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books_category`
--

INSERT INTO `books_category` (`id`, `category_creator`, `category_title`, `parent_category`, `description`, `books_amount`) VALUES
(1, 'Headmaster', 'English', '', '', 0),
(2, 'Headmaster', 'Story ', 'English', '', 0),
(3, 'Headmaster', 'Bangla ', '', '', 0),
(4, 'Headmaster', 'Story', 'Bangla ', '', 0),
(5, 'Headmaster', 'poem', 'English', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `calender_events`
--

CREATE TABLE `calender_events` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `start_date` varchar(15) NOT NULL,
  `end_date` varchar(15) NOT NULL,
  `color` varchar(15) NOT NULL,
  `url` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `calender_events`
--

INSERT INTO `calender_events` (`id`, `title`, `start_date`, `end_date`, `color`, `url`, `user_id`) VALUES
(1, 'test', '05-05-2016', '06-05-2016', 'red', 'test.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `class_title` varchar(50) NOT NULL,
  `school_name` varchar(100) NOT NULL,
  `class_group` varchar(150) NOT NULL,
  `section` varchar(100) NOT NULL,
  `section_student_capacity` varchar(5) NOT NULL,
  `classCode` int(11) NOT NULL,
  `student_amount` int(11) NOT NULL,
  `attendance_percentices_daily` int(11) NOT NULL,
  `attend_percentise_yearly` int(11) NOT NULL,
  `month_fee` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `class` (`id`, `class_title`, `school_name`, `class_group`, `section`, `section_student_capacity`, `classCode`, `student_amount`, `attendance_percentices_daily`, `attend_percentise_yearly`, `month_fee`) VALUES
(2, 'Grade 2', '', '', '', '40', 2, 13, 100, 100, 'September'),
(3, 'Grade 3', '', '', '', '40', 3, 13, 75, 75, ''),
(4, 'Grade 4', '', '', '', '50', 4, 4, 100, 100, ''),
(5, 'Grade 5', '', '', 'Section A,Section B', '50', 5, 5, 60, 60, ''),
(6, 'Grade 6', '', '', 'Section A,Section B', '50', 6, 5, 80, 80, ''),
(7, 'Grade 7', '', '', 'Section A,Section B', '50', 7, 7, 0, 0, ''),
(8, 'Grade 8', '', '', 'Section A,Section B', '50', 8, 5, 0, 0, ''),
(9, 'Grade 9', '', 'Science,Business Study,Arts', 'Section A,Section B,Section C,Section D', '50', 9, 3, 0, 0, ''),
(10, 'Grade 10', '', 'Science,Business Study,Arts', 'Section A,Section B,Section C,Section D', '50', 10, 18, 0, 0, ''),
(11, 'test class,test2,test3', '50', '', 'a,b,c', '', 0, 0, 0, 0, ''),
(12, 'test class,test2,test3', '50', '', 'a,b,c', '', 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `class_routine`
--

CREATE TABLE `class_routine` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `day_title` varchar(50) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `subject_teacher` varchar(200) NOT NULL,
  `start_time` varchar(30) NOT NULL,
  `end_time` varchar(30) NOT NULL,
  `room_number` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_routine`
--

INSERT INTO `class_routine` (`id`, `class_id`, `section`, `day_title`, `subject`, `subject_teacher`, `start_time`, `end_time`, `room_number`) VALUES
(1, 1, '', 'Sunday', 'AMAR BANGLA BOI', 'Willie B. Quint', '9.00am', '9.30', '101'),
(2, 1, '', 'Sunday', 'ENGLISH FOR TODAY', 'Fredrick V. Keyes', '9.31am', '10.00am', '101'),
(3, 1, '', 'Sunday', 'PRIMARY MATHEMATICS', 'mumar abboud', '10.01am', '10.30am', '101'),
(4, 1, '', 'Sunday', 'PRIMARY MATHEMATICS', 'mumar abboud', '10.01am', '10.30am', '101'),
(5, 2, '', 'Sunday', 'AMAR BANGLA BOI', 'Inayah Asfour', '9.00am', '9.30am', '102'),
(6, 2, '', 'Sunday', 'ENGLISH FOR TODAY', 'mumar abboud', '9.31am', '10.00am', '102'),
(7, 2, '', 'Sunday', 'PRIMARY MATHEMATICS', 'Fredrick V. Keyes', '10.01am', '10.30am', '102'),
(8, 3, '', 'Sunday', 'AMAR BANGLA BOI', 'Willie B. Quint', '9.00am', '9.30am', '103'),
(9, 3, '', 'Sunday', 'ENGLISH FOR TODAY', 'Fredrick V. Keyes', '9.31am', '10.00am', '103'),
(10, 3, '', 'Sunday', 'PRIMARY MATHEMATICS', 'mumar abboud', '10.01am', '10.30am', '103'),
(11, 3, '', 'Sunday', 'BANGLADESH AND GLOBAL STUDIES', 'Inayah Asfour', '10.30am', '11.00am', '103'),
(12, 3, '', 'Monday', 'ENGLISH FOR TODAY', 'Fredrick V. Keyes', '9.00am', '9.30', '103'),
(13, 3, '', 'Monday', 'PRIMARY MATHEMATICS', 'mumar abboud', '9.31am', '10.00am', '103');

-- --------------------------------------------------------

--
-- Table structure for table `class_students`
--

CREATE TABLE `class_students` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `roll_number` int(11) DEFAULT NULL,
  `student_id` varchar(100) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_title` varchar(50) NOT NULL,
  `section` varchar(150) NOT NULL,
  `student_title` varchar(100) NOT NULL,
  `attendance_percentices_daily` int(11) NOT NULL,
  `optional_sub` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_students`
--

INSERT INTO `class_students` (`id`, `year`, `user_id`, `roll_number`, `student_id`, `class_id`, `class_title`, `section`, `student_title`, `attendance_percentices_daily`, `optional_sub`) VALUES
(1, 2018, 4, 1, '201601001', 1, 'Class 1', 'Section A,Section B,Section C,Section D', 'Benjamin D. Lampe', 100, ''),
(2, 2018, 12, 2, '201601002', 1, 'Class 1', 'Section A,Section B,Section C,Section D', 'Rahim Hasan', 100, ''),
(3, 2018, 13, 3, '201601003', 1, 'Class 1', 'Section A,Section B,Section C,Section D', 'Junayed Hak', 100, ''),
(4, 2018, 16, 4, '201601004', 1, 'Class 1', '', 'Razia Akture', 0, ''),
(5, 2018, 17, 1, '201602001', 2, 'Class 2', '', 'Abdullah  hossain', 100, ''),
(6, 2018, 18, 2, '201602002', 2, 'Class 2', '', 'Sujana Ahmed', 100, ''),
(7, 2018, 19, 3, '201602003', 2, 'Class 2', '', 'Mahmud Hasan', 100, ''),
(8, 2018, 20, 4, '201602004', 2, 'Class 2', '', 'Mahbuba Akter', 100, ''),
(9, 2018, 21, 5, '201602005', 2, 'Class 2', '', 'Irfan Hossain', 100, ''),
(10, 2018, 22, 6, '201602006', 2, 'Class 2', '', 'Imran Hasan', 100, ''),
(11, 2018, 23, 5, '201601005', 1, 'Class 1', '', 'Polash Sarder', 100, ''),
(12, 2018, 24, 6, '201601006', 1, 'Class 1', '', 'Sumon Akon', 0, ''),
(13, 2018, 25, 1, '201603001', 3, 'Class 3', '', 'Farjana Akter', 0, ''),
(14, 2018, 26, 2, '201603002', 3, 'Class 3', '', 'Akram Hossain', 100, ''),
(15, 2018, 27, 3, '201603003', 3, 'Class 3', '', 'Alamin Saeder', 100, ''),
(16, 2018, 28, 4, '201603004', 3, 'Class 3', '', 'Sabina Sumi', 100, ''),
(17, 2018, 29, 1, '201604001', 4, 'Class 4', '', 'Sanjida Hossain', 100, ''),
(18, 2018, 30, 2, '201604002', 4, 'Class 4', '', 'Kawser  Shikder', 100, ''),
(19, 2018, 31, 3, '201604003', 4, 'Class 4', '', 'Shohana Akter', 100, ''),
(20, 2018, 32, 4, '201604004', 4, 'Class 4', '', 'Juthi Khanam', 100, ''),
(21, 2018, 33, 1, '201605001', 5, 'Class 5', '', 'Tanjila Akter', 100, ''),
(22, 2018, 34, 2, '201605002', 5, 'Class 5', '', 'Nusrat Jahan', 100, ''),
(23, 2018, 35, 3, '201605003', 5, 'Class 5', '', 'Amina Akter', 100, ''),
(24, 2018, 36, 4, '201605004', 5, 'Class 5', '', 'Ebrahim Khondokar', 0, ''),
(25, 2018, 37, 5, '201605005', 5, 'Class 5', '', 'Mintu  Fokir', 0, ''),
(26, 2018, 38, 1, '201606001', 6, 'Class 6', 'Section A', 'Shohid Islam', 100, ''),
(27, 2018, 39, 2, '201606002', 6, 'Class 6', 'Section B', 'Khadija Akter', 100, ''),
(28, 2018, 40, 3, '201606003', 6, 'Class 6', 'Section A', 'Maruf Hossain', 100, ''),
(29, 2018, 41, 4, '201606004', 6, 'Class 6', 'Section B', 'Mitu  Akter', 100, ''),
(30, 2018, 42, 5, '201606005', 6, 'Class 6', 'Section A', 'Rayhan  Kebria', 0, ''),
(32, 2016, 44, 2, '201607002', 7, 'Class 7', 'Section A,Section B,Section C,Section D', 'Airin Akter', 0, ''),
(33, 2016, 45, 3, '201607003', 7, 'Class 7', '', 'Hemel Hossain', 0, ''),
(34, 2016, 46, 4, '201607004', 7, 'Class 7', '', 'Mou Akter', 0, ''),
(35, 2016, 47, 5, '201607005', 7, 'Class 7', '', 'Dedar  Hossain', 0, ''),
(36, 2016, 48, 6, '201607006', 7, 'Class 7', '', 'Samia Akter', 0, ''),
(37, 2016, 49, 1, '201608001', 8, 'Class 8', '', 'Al Mamun', 0, ''),
(38, 2016, 50, 2, '201608002', 8, 'Class 8', '', 'Jiniya Hoq', 0, ''),
(39, 2016, 51, 3, '201608003', 8, 'Class 8', '', 'Junayed Ahmed', 0, ''),
(40, 2016, 52, 4, '201608004', 8, 'Class 8', '', 'Priya Ahmed', 0, ''),
(41, 2016, 53, 5, '201608005', 8, 'Class 8', '', 'Mithu Khondokar', 0, ''),
(42, 2016, 54, 1, '201609001', 9, 'Class 9', '', 'Rasel Hossain', 0, ''),
(43, 2016, 56, 2, '201609002', 9, 'Class 9', '', 'Test Name', 0, ''),
(44, 2021, 57, 1, '202110001', 10, 'Class 10', 'Section A', 't1 ', 0, ''),
(45, 2021, 58, 2, '202110002', 10, 'Class 10', 'Section A', 't2 t2', 0, ''),
(46, 2021, 59, 3, '202110003', 10, 'Class 10', 'Section A', 't3 t3', 0, ''),
(47, 2021, 61, 4, '202110004', 10, 'Class 10', 'Section A', 't4 t4', 0, ''),
(48, 2021, 0, 5, '202110005', 10, 'Class 10', 'Section A', 't3 t3', 0, ''),
(49, 2021, 0, 6, '202110006', 10, 'Class 10', 'Section A', 't3 t3', 0, ''),
(50, 2021, 0, 7, '202110007', 10, 'Class 10', 'Section A', 't3 t3', 0, ''),
(51, 2021, 0, 8, '202110008', 10, 'Class 10', 'Section A', 't3 t3', 0, ''),
(52, 2021, 0, 9, '202110009', 10, 'Class 10', 'Section A', 't3 t3', 0, ''),
(53, 2021, 0, 10, '202110010', 10, 'Class 10', 'Section A', 't3 t3', 0, ''),
(54, 2021, 0, 11, '202110011', 10, 'Class 10', 'Section A', 't3 t3', 0, ''),
(55, 2021, 0, 12, '202110012', 10, 'Class 10', 'Section A', 't3 t3', 0, ''),
(56, 2021, 0, 13, '202110013', 10, 'Class 10', 'Section A', 't3 t3', 0, ''),
(57, 2021, 0, 14, '202110014', 10, 'Class 10', 'Section A', 't5 t5', 0, ''),
(58, 2021, 0, 15, '202110015', 10, 'Class 10', 'Section A', 't3 t3', 0, ''),
(59, 2021, 0, 16, '202110016', 10, 'Class 10', 'Section A', 't5 t5', 0, ''),
(60, 2021, 0, 17, 't6.9999999905', 10, 'Class 10', 'Section A', 't6 t6', 0, ''),
(61, 2021, 0, 18, 't7.9999999907', 10, 'Class 10', 'Section A', 't7 t7', 0, ''),
(62, 2021, 65, 0, '', 1, 'Class 1', '', 'pavan patil', 0, ''),
(63, 2021, 66, 7, '202102007', 2, 'Class 2', '', 'jha mobi', 0, ''),
(66, 2022, 2, 8, '202202008', 2, 'Class 2', '', 'urvi mule', 0, ''),
(67, 2022, 3, 5, '202203005', 3, 'Class 3', '', 'test mule', 0, ''),
(68, 2022, 4, 0, '', 3, 'Class 3', '', 'test mule', 0, ''),
(69, 2022, 5, 9, '202202009', 2, 'Class 2', '', 'test test', 0, ''),
(70, 2022, 6, 6, '202203006', 3, 'Class 3', '', 'test bbb', 0, ''),
(71, 2022, 7, 8, '202201008', 1, 'Class 1', '', 'urvi mule', 0, ''),
(72, 2022, 8, 7, '202203007', 3, 'Class 3', '', 'test teat', 0, ''),
(73, 2022, 9, 9, '202201009', 1, 'Class 1', '', 'shiv teat', 0, ''),
(74, 2022, 10, 9, '202201009', 1, 'Class 1', '', 'shiv teat', 0, ''),
(76, 2022, 12, 0, '', 1, 'Class 1', '', 'shiv teat', 0, ''),
(77, 2022, 13, 10, '202201010', 1, 'Class 1', '', 'test test', 0, ''),
(78, 2022, 14, 0, '', 1, 'Class 1', '', 'test test', 0, ''),
(79, 2022, 15, 10, '202202010', 2, 'Class 2', '', 'Rudra test', 0, ''),
(80, 2022, 16, 0, '', 1, 'Class 1', '', 'urvi mule', 0, ''),
(81, 2022, 17, 0, '', 1, 'Class 1', '', 'urvi mule', 0, ''),
(84, 2022, 20, 0, '', 2, 'Class 2', '', 'test teat', 0, ''),
(85, 2022, 21, 8, '202203008', 3, 'Class 3', '', 'test dsfsdf', 0, ''),
(86, 2022, 22, 0, '', 3, 'Class 3', '', 'test dsfsdf', 0, ''),
(87, 2022, 23, 0, '', 3, 'Class 3', '', 'test dsfsdf', 0, ''),
(88, 2022, 24, 11, '202202011', 2, 'Class 2', '', 'test test', 0, ''),
(89, 2022, 69, 12, '202202012', 2, 'Class 2', '', 'swati', 0, ''),
(90, 2022, 71, 11, '202201011', 1, 'Class 1', '', 'swati testing', 0, ''),
(91, 2022, 72, 12, '202201012', 1, 'Class 1', '', 'Avni Belure', 0, ''),
(92, 2022, 74, 9, '202203009', 3, 'Class 3', '', 'Bhargavi belure', 0, ''),
(93, 2022, 76, 3, '202209003', 9, 'Grade 9', '', 'pushpa choudhary', 0, ''),
(94, 2022, 77, 10, '202203010', 3, 'Grade 3', '', 'sonakshi bhosale', 0, ''),
(95, 2022, 79, 7, '202207007', 7, 'Grade 7', '', 'testswati bbb', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `class_subject`
--

CREATE TABLE `class_subject` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `subject_title` varchar(100) NOT NULL,
  `group` varchar(100) NOT NULL,
  `subject_teacher` varchar(100) NOT NULL,
  `edition` varchar(100) NOT NULL,
  `writer_name` varchar(100) NOT NULL,
  `optional` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_subject`
--

INSERT INTO `class_subject` (`id`, `year`, `class_id`, `subject_title`, `group`, `subject_teacher`, `edition`, `writer_name`, `optional`) VALUES
(1, 2018, 1, 'AMAR BANGLA BOI', '', 'Willie B. Quint', '2016', 'NCTB', 0),
(2, 2018, 1, 'ENGLISH FOR TODAY', '', 'Fredrick V. Keyes', '2016', 'NCTB', 0),
(3, 2018, 1, 'PRIMARY MATHEMATICS', '', 'mumar abboud', '2016', 'NCTB', 0),
(4, 2018, 2, 'AMAR BANGLA BOI', '', 'Inayah Asfour', '2016', 'NCTB', 0),
(5, 2018, 2, 'ENGLISH FOR TODAY', '', 'mumar abboud', '2016', 'NCTB', 0),
(6, 2018, 2, 'PRIMARY MATHEMATICS', '', 'Fredrick V. Keyes', '2016', 'NCTB', 0),
(7, 2018, 2, 'PRIMARY MATHEMATICS', '', 'Fredrick V. Keyes', '2016', 'NCTB', 0),
(8, 2018, 3, 'AMAR BANGLA BOI', '', 'Willie B. Quint', '2016', 'NCTB', 0),
(9, 2018, 3, 'ENGLISH FOR TODAY', '', 'Fredrick V. Keyes', '2016', 'NCTB', 0),
(10, 2018, 3, 'PRIMARY MATHEMATICS', '', 'mumar abboud', '2016', 'NCTB', 0),
(11, 2018, 3, 'BANGLADESH AND GLOBAL STUDIES', '', 'Inayah Asfour', '2016', 'NCTB', 0),
(12, 2018, 3, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(13, 2018, 3, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(14, 2018, 3, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(15, 2018, 3, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(16, 2018, 3, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(17, 2018, 4, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(18, 2018, 4, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(19, 2018, 4, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(20, 2018, 4, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(21, 2018, 4, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(22, 2018, 4, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(23, 2018, 4, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(24, 2018, 4, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(25, 2018, 4, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(26, 2018, 5, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(27, 2018, 5, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(28, 2018, 5, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(29, 2018, 5, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(30, 2018, 5, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(31, 2016, 5, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(32, 2016, 5, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(33, 2016, 5, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(34, 2016, 5, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(35, 2016, 6, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(36, 2016, 6, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(37, 2016, 6, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(38, 2016, 6, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(39, 2016, 6, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(40, 2016, 6, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(41, 2016, 6, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(42, 2016, 6, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(43, 2016, 6, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(44, 2016, 7, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(45, 2016, 7, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(46, 2016, 7, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(47, 2016, 7, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(48, 2016, 7, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(49, 2016, 7, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(50, 2016, 7, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(51, 2016, 7, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(52, 2016, 7, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(53, 2016, 8, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(54, 2016, 8, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(55, 2016, 8, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(56, 2016, 8, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(57, 2016, 8, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(58, 2016, 8, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(59, 2016, 8, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(60, 2016, 8, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(61, 2016, 8, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(62, 2016, 9, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(63, 2016, 9, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(64, 2016, 9, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(65, 2016, 9, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(66, 2016, 9, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(67, 2016, 9, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(68, 2016, 9, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(69, 2016, 9, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(70, 2016, 9, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(71, 2016, 10, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(72, 2016, 10, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(73, 2016, 10, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(74, 2016, 10, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(75, 2016, 10, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(76, 2016, 10, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(77, 2016, 10, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(78, 2016, 10, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(79, 2016, 10, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(80, 2016, 9, 'AGRICULTURE STUDIES', '', '', '2016', 'NCTB', 1),
(81, 2016, 10, 'AGRICULTURE STUDIES', '', '', '2016', 'NCTB', 1),
(82, 2016, 10, 'ARTS & CRAFTS', '', '', '2016', 'NCTB', 1),
(83, 2016, 10, 'ARTS & CRAFTS', '', '', '2016', 'NCTB', 1),
(84, 2016, 9, 'MUSIC', '', '', '2016', 'NCTB', 1),
(85, 2016, 10, 'MUSIC', '', '', '2016', 'NCTB', 1),
(86, 2022, 2, 'testswati', '', '', 'II', 'xyz', 0);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `logo` varchar(150) NOT NULL,
  `time_zone` varchar(150) NOT NULL,
  `school_name` varchar(150) NOT NULL,
  `starting_year` varchar(50) NOT NULL,
  `headmaster_name` varchar(150) NOT NULL,
  `address` varchar(150) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `currenct` varchar(50) NOT NULL,
  `country` varchar(150) NOT NULL,
  `language` text NOT NULL,
  `msg_apai_email` varchar(100) NOT NULL,
  `msg_hash_number` varchar(100) NOT NULL,
  `msg_sender_title` varchar(100) NOT NULL,
  `countryPhonCode` varchar(5) NOT NULL,
  `t_a_s_p` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`id`, `logo`, `time_zone`, `school_name`, `starting_year`, `headmaster_name`, `address`, `phone`, `email`, `currenct`, `country`, `language`, `msg_apai_email`, `msg_hash_number`, `msg_sender_title`, `countryPhonCode`, `t_a_s_p`) VALUES
(1, '', 'UP5', 'Goila Model High School', '12/12/1883', 'Mr. Jahirul Huque', 'Goila, Aghoiljhara, Barisal', '0123456789', 'info@goila.com', 'fa fa-money', 'Bangladesh', '', '', '', '', '', 'abcd123');

-- --------------------------------------------------------

--
-- Table structure for table `config_week_day`
--

CREATE TABLE `config_week_day` (
  `id` int(11) NOT NULL,
  `day_name` varchar(20) NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config_week_day`
--

INSERT INTO `config_week_day` (`id`, `day_name`, `status`) VALUES
(1, 'Sunday', 'Open'),
(2, 'Monday', 'Open'),
(3, 'Tuesday', 'Open'),
(4, 'Wednesday', 'Open'),
(5, 'Thursday', 'Open'),
(6, 'Friday', 'Holyday'),
(7, 'Saturday', 'Holyday');

-- --------------------------------------------------------

--
-- Table structure for table `daily_attendance`
--

CREATE TABLE `daily_attendance` (
  `id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `student_id` varchar(150) NOT NULL,
  `class_title` varchar(30) NOT NULL,
  `section` varchar(100) NOT NULL,
  `days_amount` varchar(20) NOT NULL,
  `roll_no` int(11) NOT NULL,
  `present_or_absent` varchar(2) NOT NULL,
  `student_title` varchar(100) NOT NULL,
  `class_amount_monthly` int(11) NOT NULL,
  `class_amount_yearly` int(11) NOT NULL,
  `attend_amount_monthly` int(11) NOT NULL,
  `attend_amount_yearly` int(11) NOT NULL,
  `percentise_month` int(11) NOT NULL,
  `percentise_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_attendance`
--

INSERT INTO `daily_attendance` (`id`, `date`, `user_id`, `student_id`, `class_title`, `section`, `days_amount`, `roll_no`, `present_or_absent`, `student_title`, `class_amount_monthly`, `class_amount_yearly`, `attend_amount_monthly`, `attend_amount_yearly`, `percentise_month`, `percentise_year`) VALUES
(1, '1464991200', '4', '201601001', 'Class 1', 'Section A,Section B,Section C,Section D', '', 1, 'P', 'Benjamin D. Lampe', 1, 1, 1, 1, 100, 100),
(2, '1464991200', '12', '201601002', 'Class 1', 'Section A,Section B,Section C,Section D', '', 2, 'P', 'Rahim Hasan', 1, 1, 1, 1, 100, 100),
(3, '1464991200', '13', '201601003', 'Class 1', 'Section A,Section B,Section C,Section D', '', 3, 'P', 'Junayed Hak', 1, 1, 1, 1, 100, 100),
(4, '1464991200', '16', '201601004', 'Class 1', '', '', 4, 'A', 'Razia Akture', 1, 1, 0, 0, 0, 0),
(5, '1464991200', '23', '201601005', 'Class 1', '', '', 5, 'P', 'Polash Sarder', 1, 1, 1, 1, 100, 100),
(6, '1464991200', '24', '201601006', 'Class 1', '', '', 6, 'A', 'Sumon Akon', 1, 1, 0, 0, 0, 0),
(7, '1464991200', '17', '201602001', 'Class 2', '', '', 1, 'P', 'Abdullah  hossain', 1, 1, 1, 1, 100, 100),
(8, '1464991200', '18', '201602002', 'Class 2', '', '', 2, 'P', 'Sujana Ahmed', 1, 1, 1, 1, 100, 100),
(9, '1464991200', '19', '201602003', 'Class 2', '', '', 3, 'P', 'Mahmud Hasan', 1, 1, 1, 1, 100, 100),
(10, '1464991200', '20', '201602004', 'Class 2', '', '', 4, 'P', 'Mahbuba Akter', 1, 1, 1, 1, 100, 100),
(11, '1464991200', '21', '201602005', 'Class 2', '', '', 5, 'P', 'Irfan Hossain', 1, 1, 1, 1, 100, 100),
(12, '1464991200', '22', '201602006', 'Class 2', '', '', 6, 'P', 'Imran Hasan', 1, 1, 1, 1, 100, 100),
(13, '1464991200', '25', '201603001', 'Class 3', '', '', 1, 'A', 'Farjana Akter', 1, 1, 0, 0, 0, 0),
(14, '1464991200', '26', '201603002', 'Class 3', '', '', 2, 'P', 'Akram Hossain', 1, 1, 1, 1, 100, 100),
(15, '1464991200', '27', '201603003', 'Class 3', '', '', 3, 'P', 'Alamin Saeder', 1, 1, 1, 1, 100, 100),
(16, '1464991200', '28', '201603004', 'Class 3', '', '', 4, 'P', 'Sabina Sumi', 1, 1, 1, 1, 100, 100),
(17, '1464991200', '29', '201604001', 'Class 4', '', '', 1, 'P', 'Sanjida Hossain', 1, 1, 1, 1, 100, 100),
(18, '1464991200', '30', '201604002', 'Class 4', '', '', 2, 'P', 'Kawser  Shikder', 1, 1, 1, 1, 100, 100),
(19, '1464991200', '31', '201604003', 'Class 4', '', '', 3, 'P', 'Shohana Akter', 1, 1, 1, 1, 100, 100),
(20, '1464991200', '32', '201604004', 'Class 4', '', '', 4, 'P', 'Juthi Khanam', 1, 1, 1, 1, 100, 100),
(21, '1464991200', '33', '201605001', 'Class 5', '', '', 1, 'P', 'Tanjila Akter', 1, 1, 1, 1, 100, 100),
(22, '1464991200', '34', '201605002', 'Class 5', '', '', 2, 'P', 'Nusrat Jahan', 1, 1, 1, 1, 100, 100),
(23, '1464991200', '35', '201605003', 'Class 5', '', '', 3, 'P', 'Amina Akter', 1, 1, 1, 1, 100, 100),
(24, '1464991200', '36', '201605004', 'Class 5', '', '', 4, 'A', 'Ebrahim Khondokar', 1, 1, 0, 0, 0, 0),
(25, '1464991200', '37', '201605005', 'Class 5', '', '', 5, 'A', 'Mintu  Fokir', 1, 1, 0, 0, 0, 0),
(26, '1464991200', '38', '201606001', 'Class 6', 'Section A', '', 1, 'P', 'Shohid Islam', 1, 1, 1, 1, 100, 100),
(27, '1464991200', '39', '201606002', 'Class 6', 'Section B', '', 2, 'P', 'Khadija Akter', 1, 1, 1, 1, 100, 100),
(28, '1464991200', '40', '201606003', 'Class 6', 'Section A', '', 3, 'P', 'Maruf Hossain', 1, 1, 1, 1, 100, 100),
(29, '1464991200', '41', '201606004', 'Class 6', 'Section B', '', 4, 'P', 'Mitu  Akter', 1, 1, 1, 1, 100, 100),
(30, '1464991200', '42', '201606005', 'Class 6', 'Section A', '', 5, 'A', 'Rayhan  Kebria', 1, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dormitory`
--

CREATE TABLE `dormitory` (
  `id` int(11) NOT NULL,
  `dormitory_name` varchar(100) NOT NULL,
  `dormitory_for` varchar(100) NOT NULL,
  `room_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dormitory`
--

INSERT INTO `dormitory` (`id`, `dormitory_name`, `dormitory_for`, `room_amount`) VALUES
(1, 'Boys Hostel', 'Only for male', 125),
(2, 'Girls Hostel', 'Only for female', 40),
(3, 'Teachers Dormitory', 'Only for teachers (Male Teacher)', 10),
(4, 'xyz', 'Only for female', 65),
(5, 'test', 'Only for male', 500),
(6, 'test', 'Only for male', 500);

-- --------------------------------------------------------

--
-- Table structure for table `dormitory_bed`
--

CREATE TABLE `dormitory_bed` (
  `id` int(11) NOT NULL,
  `dormitory_id` int(11) NOT NULL,
  `dormitory_name` varchar(100) NOT NULL,
  `room_number` varchar(50) NOT NULL,
  `bed_number` varchar(50) NOT NULL,
  `student_id` int(11) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `class` varchar(50) NOT NULL,
  `roll_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dormitory_bed`
--

INSERT INTO `dormitory_bed` (`id`, `dormitory_id`, `dormitory_name`, `room_number`, `bed_number`, `student_id`, `student_name`, `class`, `roll_number`) VALUES
(1, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 1', 201601001, 'Benjamin D. Lampe', '1', 1),
(2, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 2', 0, '', '', 0),
(3, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 3', 0, '', '', 0),
(4, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 4', 0, '', '', 0),
(5, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 5', 0, '', '', 0),
(6, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 6', 0, '', '', 0),
(7, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 7', 0, '', '', 0),
(8, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 8', 0, '', '', 0),
(9, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 9', 0, '', '', 0),
(10, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 10', 0, '', '', 0),
(11, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 1', 0, '', '', 0),
(12, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 2', 0, '', '', 0),
(13, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 3', 0, '', '', 0),
(14, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 4', 0, '', '', 0),
(15, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 5', 0, '', '', 0),
(16, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 6', 0, '', '', 0),
(17, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 7', 0, '', '', 0),
(18, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 8', 0, '', '', 0),
(19, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 9', 0, '', '', 0),
(20, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 10', 0, '', '', 0),
(21, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 1', 0, '', '', 0),
(22, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 2', 0, '', '', 0),
(23, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 3', 0, '', '', 0),
(24, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 4', 0, '', '', 0),
(25, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 5', 0, '', '', 0),
(26, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 6', 0, '', '', 0),
(27, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 7', 0, '', '', 0),
(28, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 8', 0, '', '', 0),
(29, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 9', 0, '', '', 0),
(30, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 10', 0, '', '', 0),
(31, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 1', 0, '', '', 0),
(32, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 2', 0, '', '', 0),
(33, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 3', 0, '', '', 0),
(34, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 4', 0, '', '', 0),
(35, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 5', 0, '', '', 0),
(36, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 6', 0, '', '', 0),
(37, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 7', 0, '', '', 0),
(38, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 8', 0, '', '', 0),
(39, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 9', 0, '', '', 0),
(40, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 10', 0, '', '', 0),
(41, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 1', 0, '', '', 0),
(42, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 2', 0, '', '', 0),
(43, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 3', 0, '', '', 0),
(44, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 4', 0, '', '', 0),
(45, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 5', 0, '', '', 0),
(46, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 6', 0, '', '', 0),
(47, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 7', 0, '', '', 0),
(48, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 8', 0, '', '', 0),
(49, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 9', 0, '', '', 0),
(50, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 10', 0, '', '', 0),
(51, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 1', 0, '', '', 0),
(52, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 2', 0, '', '', 0),
(53, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 3', 0, '', '', 0),
(54, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 4', 0, '', '', 0),
(55, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 5', 0, '', '', 0),
(56, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 6', 0, '', '', 0),
(57, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 7', 0, '', '', 0),
(58, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 8', 0, '', '', 0),
(59, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 9', 0, '', '', 0),
(60, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 10', 0, '', '', 0),
(61, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 1', 0, '', '', 0),
(62, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 2', 0, '', '', 0),
(63, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 3', 0, '', '', 0),
(64, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 4', 0, '', '', 0),
(65, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 5', 0, '', '', 0),
(66, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 6', 0, '', '', 0),
(67, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 7', 0, '', '', 0),
(68, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 8', 0, '', '', 0),
(69, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 9', 0, '', '', 0),
(70, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 10', 0, '', '', 0),
(71, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 1', 0, '', '', 0),
(72, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 2', 0, '', '', 0),
(73, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 3', 0, '', '', 0),
(74, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 4', 0, '', '', 0),
(75, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 5', 0, '', '', 0),
(76, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 6', 0, '', '', 0),
(77, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 7', 0, '', '', 0),
(78, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 8', 0, '', '', 0),
(79, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 9', 0, '', '', 0),
(80, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 10', 0, '', '', 0),
(81, 1, 'Boys Hostel', 'Room No: 7', 'Seat No: 1', 201602001, 'Abdullah  hossain', '2', 1),
(82, 1, 'Boys Hostel', 'Room No: 7', 'Seat No: 2', 0, '', '', 0),
(83, 1, 'Boys Hostel', 'Room No: 7', 'Seat No: 3', 0, '', '', 0),
(84, 1, 'Boys Hostel', 'Room No: 7', 'Seat No: 4', 0, '', '', 0),
(85, 1, 'Boys Hostel', 'Room No: 7', 'Seat No: 5', 0, '', '', 0),
(86, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 1', 0, '', '', 0),
(87, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 2', 0, '', '', 0),
(88, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 3', 0, '', '', 0),
(89, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 4', 0, '', '', 0),
(90, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 5', 0, '', '', 0),
(91, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 6', 0, '', '', 0),
(92, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 7', 0, '', '', 0),
(93, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 8', 0, '', '', 0),
(94, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 9', 0, '', '', 0),
(95, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 10', 0, '', '', 0),
(96, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 11', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dormitory_room`
--

CREATE TABLE `dormitory_room` (
  `id` int(11) NOT NULL,
  `dormitory_id` int(11) NOT NULL,
  `dormitory_name` varchar(100) NOT NULL,
  `room` varchar(50) NOT NULL,
  `bed_amount` int(11) NOT NULL,
  `free_seat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dormitory_room`
--

INSERT INTO `dormitory_room` (`id`, `dormitory_id`, `dormitory_name`, `room`, `bed_amount`, `free_seat`) VALUES
(1, 1, 'Boys Hostel', 'Room No: 1', 10, 10),
(2, 1, 'Boys Hostel', 'Room No: 2', 10, 10),
(3, 1, 'Boys Hostel', 'Room No: 3', 0, 0),
(4, 1, 'Boys Hostel', 'Room No: 4', 0, 0),
(5, 1, 'Boys Hostel', 'Room No: 5', 0, 0),
(6, 1, 'Boys Hostel', 'Room No: 6', 0, 0),
(7, 1, 'Boys Hostel', 'Room No: 7', 5, 5),
(8, 1, 'Boys Hostel', 'Room No: 8', 0, 0),
(9, 1, 'Boys Hostel', 'Room No: 9', 0, 0),
(10, 1, 'Boys Hostel', 'Room No: 10', 0, 0),
(11, 1, 'Boys Hostel', 'Room No: 11', 0, 0),
(12, 1, 'Boys Hostel', 'Room No: 12', 0, 0),
(13, 1, 'Boys Hostel', 'Room No: 13', 0, 0),
(14, 1, 'Boys Hostel', 'Room No: 14', 0, 0),
(15, 1, 'Boys Hostel', 'Room No: 15', 0, 0),
(16, 1, 'Boys Hostel', 'Room No: 16', 0, 0),
(17, 1, 'Boys Hostel', 'Room No: 17', 0, 0),
(18, 1, 'Boys Hostel', 'Room No: 18', 0, 0),
(19, 1, 'Boys Hostel', 'Room No: 19', 0, 0),
(20, 1, 'Boys Hostel', 'Room No: 20', 0, 0),
(21, 1, 'Boys Hostel', 'Room No: 21', 0, 0),
(22, 1, 'Boys Hostel', 'Room No: 22', 0, 0),
(23, 1, 'Boys Hostel', 'Room No: 23', 0, 0),
(24, 1, 'Boys Hostel', 'Room No: 24', 0, 0),
(25, 1, 'Boys Hostel', 'Room No: 25', 0, 0),
(26, 1, 'Boys Hostel', 'Room No: 26', 0, 0),
(27, 1, 'Boys Hostel', 'Room No: 27', 0, 0),
(28, 1, 'Boys Hostel', 'Room No: 28', 0, 0),
(29, 1, 'Boys Hostel', 'Room No: 29', 0, 0),
(30, 1, 'Boys Hostel', 'Room No: 30', 0, 0),
(31, 1, 'Boys Hostel', 'Room No: 31', 0, 0),
(32, 1, 'Boys Hostel', 'Room No: 32', 0, 0),
(33, 1, 'Boys Hostel', 'Room No: 33', 0, 0),
(34, 1, 'Boys Hostel', 'Room No: 34', 0, 0),
(35, 1, 'Boys Hostel', 'Room No: 35', 0, 0),
(36, 1, 'Boys Hostel', 'Room No: 36', 0, 0),
(37, 1, 'Boys Hostel', 'Room No: 37', 0, 0),
(38, 1, 'Boys Hostel', 'Room No: 38', 0, 0),
(39, 1, 'Boys Hostel', 'Room No: 39', 0, 0),
(40, 1, 'Boys Hostel', 'Room No: 40', 0, 0),
(41, 1, 'Boys Hostel', 'Room No: 41', 0, 0),
(42, 1, 'Boys Hostel', 'Room No: 42', 0, 0),
(43, 1, 'Boys Hostel', 'Room No: 43', 0, 0),
(44, 1, 'Boys Hostel', 'Room No: 44', 0, 0),
(45, 1, 'Boys Hostel', 'Room No: 45', 0, 0),
(46, 1, 'Boys Hostel', 'Room No: 46', 0, 0),
(47, 1, 'Boys Hostel', 'Room No: 47', 0, 0),
(48, 1, 'Boys Hostel', 'Room No: 48', 0, 0),
(49, 1, 'Boys Hostel', 'Room No: 49', 0, 0),
(50, 1, 'Boys Hostel', 'Room No: 50', 0, 0),
(51, 1, 'Boys Hostel', 'Room No: 51', 0, 0),
(52, 1, 'Boys Hostel', 'Room No: 52', 0, 0),
(53, 1, 'Boys Hostel', 'Room No: 53', 0, 0),
(54, 1, 'Boys Hostel', 'Room No: 54', 0, 0),
(55, 1, 'Boys Hostel', 'Room No: 55', 0, 0),
(56, 1, 'Boys Hostel', 'Room No: 56', 0, 0),
(57, 1, 'Boys Hostel', 'Room No: 57', 0, 0),
(58, 1, 'Boys Hostel', 'Room No: 58', 0, 0),
(59, 1, 'Boys Hostel', 'Room No: 59', 0, 0),
(60, 1, 'Boys Hostel', 'Room No: 60', 0, 0),
(61, 1, 'Boys Hostel', 'Room No: 61', 0, 0),
(62, 1, 'Boys Hostel', 'Room No: 62', 0, 0),
(63, 1, 'Boys Hostel', 'Room No: 63', 0, 0),
(64, 1, 'Boys Hostel', 'Room No: 64', 0, 0),
(65, 1, 'Boys Hostel', 'Room No: 65', 0, 0),
(66, 1, 'Boys Hostel', 'Room No: 66', 0, 0),
(67, 1, 'Boys Hostel', 'Room No: 67', 0, 0),
(68, 1, 'Boys Hostel', 'Room No: 68', 0, 0),
(69, 1, 'Boys Hostel', 'Room No: 69', 0, 0),
(70, 1, 'Boys Hostel', 'Room No: 70', 0, 0),
(71, 1, 'Boys Hostel', 'Room No: 71', 0, 0),
(72, 1, 'Boys Hostel', 'Room No: 72', 0, 0),
(73, 1, 'Boys Hostel', 'Room No: 73', 0, 0),
(74, 1, 'Boys Hostel', 'Room No: 74', 0, 0),
(75, 1, 'Boys Hostel', 'Room No: 75', 0, 0),
(76, 1, 'Boys Hostel', 'Room No: 76', 0, 0),
(77, 1, 'Boys Hostel', 'Room No: 77', 0, 0),
(78, 1, 'Boys Hostel', 'Room No: 78', 0, 0),
(79, 1, 'Boys Hostel', 'Room No: 79', 0, 0),
(80, 1, 'Boys Hostel', 'Room No: 80', 0, 0),
(81, 1, 'Boys Hostel', 'Room No: 81', 0, 0),
(82, 1, 'Boys Hostel', 'Room No: 82', 0, 0),
(83, 1, 'Boys Hostel', 'Room No: 83', 0, 0),
(84, 1, 'Boys Hostel', 'Room No: 84', 0, 0),
(85, 1, 'Boys Hostel', 'Room No: 85', 0, 0),
(86, 1, 'Boys Hostel', 'Room No: 86', 0, 0),
(87, 1, 'Boys Hostel', 'Room No: 87', 0, 0),
(88, 1, 'Boys Hostel', 'Room No: 88', 0, 0),
(89, 1, 'Boys Hostel', 'Room No: 89', 0, 0),
(90, 1, 'Boys Hostel', 'Room No: 90', 0, 0),
(91, 1, 'Boys Hostel', 'Room No: 91', 0, 0),
(92, 1, 'Boys Hostel', 'Room No: 92', 0, 0),
(93, 1, 'Boys Hostel', 'Room No: 93', 0, 0),
(94, 1, 'Boys Hostel', 'Room No: 94', 0, 0),
(95, 1, 'Boys Hostel', 'Room No: 95', 0, 0),
(96, 1, 'Boys Hostel', 'Room No: 96', 0, 0),
(97, 1, 'Boys Hostel', 'Room No: 97', 0, 0),
(98, 1, 'Boys Hostel', 'Room No: 98', 0, 0),
(99, 1, 'Boys Hostel', 'Room No: 99', 0, 0),
(100, 1, 'Boys Hostel', 'Room No: 100', 0, 0),
(101, 1, 'Boys Hostel', 'Room No: 101', 0, 0),
(102, 1, 'Boys Hostel', 'Room No: 102', 0, 0),
(103, 1, 'Boys Hostel', 'Room No: 103', 0, 0),
(104, 1, 'Boys Hostel', 'Room No: 104', 0, 0),
(105, 1, 'Boys Hostel', 'Room No: 105', 0, 0),
(106, 1, 'Boys Hostel', 'Room No: 106', 0, 0),
(107, 1, 'Boys Hostel', 'Room No: 107', 0, 0),
(108, 1, 'Boys Hostel', 'Room No: 108', 0, 0),
(109, 1, 'Boys Hostel', 'Room No: 109', 0, 0),
(110, 1, 'Boys Hostel', 'Room No: 110', 0, 0),
(111, 1, 'Boys Hostel', 'Room No: 111', 0, 0),
(112, 1, 'Boys Hostel', 'Room No: 112', 0, 0),
(113, 1, 'Boys Hostel', 'Room No: 113', 0, 0),
(114, 1, 'Boys Hostel', 'Room No: 114', 0, 0),
(115, 1, 'Boys Hostel', 'Room No: 115', 0, 0),
(116, 1, 'Boys Hostel', 'Room No: 116', 0, 0),
(117, 1, 'Boys Hostel', 'Room No: 117', 0, 0),
(118, 1, 'Boys Hostel', 'Room No: 118', 0, 0),
(119, 1, 'Boys Hostel', 'Room No: 119', 0, 0),
(120, 1, 'Boys Hostel', 'Room No: 120', 0, 0),
(121, 1, 'Boys Hostel', 'Room No: 121', 0, 0),
(122, 1, 'Boys Hostel', 'Room No: 122', 0, 0),
(123, 1, 'Boys Hostel', 'Room No: 123', 0, 0),
(124, 1, 'Boys Hostel', 'Room No: 124', 0, 0),
(125, 1, 'Boys Hostel', 'Room No: 125', 0, 0),
(126, 2, 'Girls Hostel', 'Room No: 1', 11, 11),
(127, 2, 'Girls Hostel', 'Room No: 2', 10, 10),
(128, 2, 'Girls Hostel', 'Room No: 3', 10, 10),
(129, 2, 'Girls Hostel', 'Room No: 4', 0, 0),
(130, 2, 'Girls Hostel', 'Room No: 5', 0, 0),
(131, 2, 'Girls Hostel', 'Room No: 6', 0, 0),
(132, 2, 'Girls Hostel', 'Room No: 7', 0, 0),
(133, 2, 'Girls Hostel', 'Room No: 8', 0, 0),
(134, 2, 'Girls Hostel', 'Room No: 9', 0, 0),
(135, 2, 'Girls Hostel', 'Room No: 10', 0, 0),
(136, 2, 'Girls Hostel', 'Room No: 11', 0, 0),
(137, 2, 'Girls Hostel', 'Room No: 12', 0, 0),
(138, 2, 'Girls Hostel', 'Room No: 13', 0, 0),
(139, 2, 'Girls Hostel', 'Room No: 14', 0, 0),
(140, 2, 'Girls Hostel', 'Room No: 15', 0, 0),
(141, 2, 'Girls Hostel', 'Room No: 16', 0, 0),
(142, 2, 'Girls Hostel', 'Room No: 17', 0, 0),
(143, 2, 'Girls Hostel', 'Room No: 18', 0, 0),
(144, 2, 'Girls Hostel', 'Room No: 19', 0, 0),
(145, 2, 'Girls Hostel', 'Room No: 20', 0, 0),
(146, 2, 'Girls Hostel', 'Room No: 21', 0, 0),
(147, 2, 'Girls Hostel', 'Room No: 22', 0, 0),
(148, 2, 'Girls Hostel', 'Room No: 23', 0, 0),
(149, 2, 'Girls Hostel', 'Room No: 24', 0, 0),
(150, 2, 'Girls Hostel', 'Room No: 25', 0, 0),
(151, 2, 'Girls Hostel', 'Room No: 26', 0, 0),
(152, 2, 'Girls Hostel', 'Room No: 27', 0, 0),
(153, 2, 'Girls Hostel', 'Room No: 28', 0, 0),
(154, 2, 'Girls Hostel', 'Room No: 29', 0, 0),
(155, 2, 'Girls Hostel', 'Room No: 30', 0, 0),
(156, 2, 'Girls Hostel', 'Room No: 31', 0, 0),
(157, 2, 'Girls Hostel', 'Room No: 32', 0, 0),
(158, 2, 'Girls Hostel', 'Room No: 33', 0, 0),
(159, 2, 'Girls Hostel', 'Room No: 34', 0, 0),
(160, 2, 'Girls Hostel', 'Room No: 35', 0, 0),
(161, 2, 'Girls Hostel', 'Room No: 36', 0, 0),
(162, 2, 'Girls Hostel', 'Room No: 37', 0, 0),
(163, 2, 'Girls Hostel', 'Room No: 38', 0, 0),
(164, 2, 'Girls Hostel', 'Room No: 39', 0, 0),
(165, 2, 'Girls Hostel', 'Room No: 40', 0, 0),
(166, 3, 'Teachers Dormitory', 'Room No: 1', 10, 10),
(167, 3, 'Teachers Dormitory', 'Room No: 2', 10, 10),
(168, 3, 'Teachers Dormitory', 'Room No: 3', 0, 0),
(169, 3, 'Teachers Dormitory', 'Room No: 4', 0, 0),
(170, 3, 'Teachers Dormitory', 'Room No: 5', 0, 0),
(171, 3, 'Teachers Dormitory', 'Room No: 6', 0, 0),
(172, 3, 'Teachers Dormitory', 'Room No: 7', 0, 0),
(173, 3, 'Teachers Dormitory', 'Room No: 8', 0, 0),
(174, 3, 'Teachers Dormitory', 'Room No: 9', 0, 0),
(175, 3, 'Teachers Dormitory', 'Room No: 10', 0, 0),
(176, 4, 'xyz', 'Room No: 1', 0, 0),
(177, 4, 'xyz', 'Room No: 2', 0, 0),
(178, 4, 'xyz', 'Room No: 3', 0, 0),
(179, 4, 'xyz', 'Room No: 4', 0, 0),
(180, 4, 'xyz', 'Room No: 5', 0, 0),
(181, 4, 'xyz', 'Room No: 6', 0, 0),
(182, 4, 'xyz', 'Room No: 7', 0, 0),
(183, 4, 'xyz', 'Room No: 8', 0, 0),
(184, 4, 'xyz', 'Room No: 9', 0, 0),
(185, 4, 'xyz', 'Room No: 10', 0, 0),
(186, 4, 'xyz', 'Room No: 11', 0, 0),
(187, 4, 'xyz', 'Room No: 12', 0, 0),
(188, 4, 'xyz', 'Room No: 13', 0, 0),
(189, 4, 'xyz', 'Room No: 14', 0, 0),
(190, 4, 'xyz', 'Room No: 15', 0, 0),
(191, 4, 'xyz', 'Room No: 16', 0, 0),
(192, 4, 'xyz', 'Room No: 17', 0, 0),
(193, 4, 'xyz', 'Room No: 18', 0, 0),
(194, 4, 'xyz', 'Room No: 19', 0, 0),
(195, 4, 'xyz', 'Room No: 20', 0, 0),
(196, 4, 'xyz', 'Room No: 21', 0, 0),
(197, 4, 'xyz', 'Room No: 22', 0, 0),
(198, 4, 'xyz', 'Room No: 23', 0, 0),
(199, 4, 'xyz', 'Room No: 24', 0, 0),
(200, 4, 'xyz', 'Room No: 25', 0, 0),
(201, 4, 'xyz', 'Room No: 26', 0, 0),
(202, 4, 'xyz', 'Room No: 27', 0, 0),
(203, 4, 'xyz', 'Room No: 28', 0, 0),
(204, 4, 'xyz', 'Room No: 29', 0, 0),
(205, 4, 'xyz', 'Room No: 30', 0, 0),
(206, 4, 'xyz', 'Room No: 31', 0, 0),
(207, 4, 'xyz', 'Room No: 32', 0, 0),
(208, 4, 'xyz', 'Room No: 33', 0, 0),
(209, 4, 'xyz', 'Room No: 34', 0, 0),
(210, 4, 'xyz', 'Room No: 35', 0, 0),
(211, 4, 'xyz', 'Room No: 36', 0, 0),
(212, 4, 'xyz', 'Room No: 37', 0, 0),
(213, 4, 'xyz', 'Room No: 38', 0, 0),
(214, 4, 'xyz', 'Room No: 39', 0, 0),
(215, 4, 'xyz', 'Room No: 40', 0, 0),
(216, 4, 'xyz', 'Room No: 41', 0, 0),
(217, 4, 'xyz', 'Room No: 42', 0, 0),
(218, 4, 'xyz', 'Room No: 43', 0, 0),
(219, 4, 'xyz', 'Room No: 44', 0, 0),
(220, 4, 'xyz', 'Room No: 45', 0, 0),
(221, 4, 'xyz', 'Room No: 46', 0, 0),
(222, 4, 'xyz', 'Room No: 47', 0, 0),
(223, 4, 'xyz', 'Room No: 48', 0, 0),
(224, 4, 'xyz', 'Room No: 49', 0, 0),
(225, 4, 'xyz', 'Room No: 50', 0, 0),
(226, 4, 'xyz', 'Room No: 51', 0, 0),
(227, 4, 'xyz', 'Room No: 52', 0, 0),
(228, 4, 'xyz', 'Room No: 53', 0, 0),
(229, 4, 'xyz', 'Room No: 54', 0, 0),
(230, 4, 'xyz', 'Room No: 55', 0, 0),
(231, 4, 'xyz', 'Room No: 56', 0, 0),
(232, 4, 'xyz', 'Room No: 57', 0, 0),
(233, 4, 'xyz', 'Room No: 58', 0, 0),
(234, 4, 'xyz', 'Room No: 59', 0, 0),
(235, 4, 'xyz', 'Room No: 60', 0, 0),
(236, 4, 'xyz', 'Room No: 61', 0, 0),
(237, 4, 'xyz', 'Room No: 62', 0, 0),
(238, 4, 'xyz', 'Room No: 63', 0, 0),
(239, 4, 'xyz', 'Room No: 64', 0, 0),
(240, 4, 'xyz', 'Room No: 65', 0, 0),
(241, 5, 'test', 'Room No: 1', 0, 0),
(242, 5, 'test', 'Room No: 2', 0, 0),
(243, 5, 'test', 'Room No: 3', 0, 0),
(244, 5, 'test', 'Room No: 4', 0, 0),
(245, 5, 'test', 'Room No: 5', 0, 0),
(246, 5, 'test', 'Room No: 6', 0, 0),
(247, 5, 'test', 'Room No: 7', 0, 0),
(248, 5, 'test', 'Room No: 8', 0, 0),
(249, 5, 'test', 'Room No: 9', 0, 0),
(250, 5, 'test', 'Room No: 10', 0, 0),
(251, 5, 'test', 'Room No: 11', 0, 0),
(252, 5, 'test', 'Room No: 12', 0, 0),
(253, 5, 'test', 'Room No: 13', 0, 0),
(254, 5, 'test', 'Room No: 14', 0, 0),
(255, 5, 'test', 'Room No: 15', 0, 0),
(256, 5, 'test', 'Room No: 16', 0, 0),
(257, 5, 'test', 'Room No: 17', 0, 0),
(258, 5, 'test', 'Room No: 18', 0, 0),
(259, 5, 'test', 'Room No: 19', 0, 0),
(260, 5, 'test', 'Room No: 20', 0, 0),
(261, 5, 'test', 'Room No: 21', 0, 0),
(262, 5, 'test', 'Room No: 22', 0, 0),
(263, 5, 'test', 'Room No: 23', 0, 0),
(264, 5, 'test', 'Room No: 24', 0, 0),
(265, 5, 'test', 'Room No: 25', 0, 0),
(266, 5, 'test', 'Room No: 26', 0, 0),
(267, 5, 'test', 'Room No: 27', 0, 0),
(268, 5, 'test', 'Room No: 28', 0, 0),
(269, 5, 'test', 'Room No: 29', 0, 0),
(270, 5, 'test', 'Room No: 30', 0, 0),
(271, 5, 'test', 'Room No: 31', 0, 0),
(272, 5, 'test', 'Room No: 32', 0, 0),
(273, 6, 'test', 'Room No: 1', 0, 0),
(274, 5, 'test', 'Room No: 33', 0, 0),
(275, 6, 'test', 'Room No: 2', 0, 0),
(276, 5, 'test', 'Room No: 34', 0, 0),
(277, 6, 'test', 'Room No: 3', 0, 0),
(278, 5, 'test', 'Room No: 35', 0, 0),
(279, 6, 'test', 'Room No: 4', 0, 0),
(280, 5, 'test', 'Room No: 36', 0, 0),
(281, 6, 'test', 'Room No: 5', 0, 0),
(282, 5, 'test', 'Room No: 37', 0, 0),
(283, 6, 'test', 'Room No: 6', 0, 0),
(284, 5, 'test', 'Room No: 38', 0, 0),
(285, 6, 'test', 'Room No: 7', 0, 0),
(286, 5, 'test', 'Room No: 39', 0, 0),
(287, 6, 'test', 'Room No: 8', 0, 0),
(288, 5, 'test', 'Room No: 40', 0, 0),
(289, 6, 'test', 'Room No: 9', 0, 0),
(290, 5, 'test', 'Room No: 41', 0, 0),
(291, 6, 'test', 'Room No: 10', 0, 0),
(292, 5, 'test', 'Room No: 42', 0, 0),
(293, 6, 'test', 'Room No: 11', 0, 0),
(294, 5, 'test', 'Room No: 43', 0, 0),
(295, 6, 'test', 'Room No: 12', 0, 0),
(296, 5, 'test', 'Room No: 44', 0, 0),
(297, 6, 'test', 'Room No: 13', 0, 0),
(298, 5, 'test', 'Room No: 45', 0, 0),
(299, 6, 'test', 'Room No: 14', 0, 0),
(300, 5, 'test', 'Room No: 46', 0, 0),
(301, 6, 'test', 'Room No: 15', 0, 0),
(302, 5, 'test', 'Room No: 47', 0, 0),
(303, 6, 'test', 'Room No: 16', 0, 0),
(304, 5, 'test', 'Room No: 48', 0, 0),
(305, 6, 'test', 'Room No: 17', 0, 0),
(306, 5, 'test', 'Room No: 49', 0, 0),
(307, 6, 'test', 'Room No: 18', 0, 0),
(308, 5, 'test', 'Room No: 50', 0, 0),
(309, 6, 'test', 'Room No: 19', 0, 0),
(310, 5, 'test', 'Room No: 51', 0, 0),
(311, 6, 'test', 'Room No: 20', 0, 0),
(312, 5, 'test', 'Room No: 52', 0, 0),
(313, 6, 'test', 'Room No: 21', 0, 0),
(314, 5, 'test', 'Room No: 53', 0, 0),
(315, 6, 'test', 'Room No: 22', 0, 0),
(316, 5, 'test', 'Room No: 54', 0, 0),
(317, 6, 'test', 'Room No: 23', 0, 0),
(318, 5, 'test', 'Room No: 55', 0, 0),
(319, 6, 'test', 'Room No: 24', 0, 0),
(320, 5, 'test', 'Room No: 56', 0, 0),
(321, 6, 'test', 'Room No: 25', 0, 0),
(322, 5, 'test', 'Room No: 57', 0, 0),
(323, 6, 'test', 'Room No: 26', 0, 0),
(324, 5, 'test', 'Room No: 58', 0, 0),
(325, 6, 'test', 'Room No: 27', 0, 0),
(326, 5, 'test', 'Room No: 59', 0, 0),
(327, 6, 'test', 'Room No: 28', 0, 0),
(328, 5, 'test', 'Room No: 60', 0, 0),
(329, 6, 'test', 'Room No: 29', 0, 0),
(330, 5, 'test', 'Room No: 61', 0, 0),
(331, 6, 'test', 'Room No: 30', 0, 0),
(332, 5, 'test', 'Room No: 62', 0, 0),
(333, 6, 'test', 'Room No: 31', 0, 0),
(334, 5, 'test', 'Room No: 63', 0, 0),
(335, 6, 'test', 'Room No: 32', 0, 0),
(336, 5, 'test', 'Room No: 64', 0, 0),
(337, 6, 'test', 'Room No: 33', 0, 0),
(338, 5, 'test', 'Room No: 65', 0, 0),
(339, 6, 'test', 'Room No: 34', 0, 0),
(340, 5, 'test', 'Room No: 66', 0, 0),
(341, 6, 'test', 'Room No: 35', 0, 0),
(342, 5, 'test', 'Room No: 67', 0, 0),
(343, 6, 'test', 'Room No: 36', 0, 0),
(344, 5, 'test', 'Room No: 68', 0, 0),
(345, 6, 'test', 'Room No: 37', 0, 0),
(346, 5, 'test', 'Room No: 69', 0, 0),
(347, 6, 'test', 'Room No: 38', 0, 0),
(348, 5, 'test', 'Room No: 70', 0, 0),
(349, 6, 'test', 'Room No: 39', 0, 0),
(350, 5, 'test', 'Room No: 71', 0, 0),
(351, 6, 'test', 'Room No: 40', 0, 0),
(352, 5, 'test', 'Room No: 72', 0, 0),
(353, 6, 'test', 'Room No: 41', 0, 0),
(354, 5, 'test', 'Room No: 73', 0, 0),
(355, 6, 'test', 'Room No: 42', 0, 0),
(356, 5, 'test', 'Room No: 74', 0, 0),
(357, 6, 'test', 'Room No: 43', 0, 0),
(358, 5, 'test', 'Room No: 75', 0, 0),
(359, 6, 'test', 'Room No: 44', 0, 0),
(360, 5, 'test', 'Room No: 76', 0, 0),
(361, 6, 'test', 'Room No: 45', 0, 0),
(362, 5, 'test', 'Room No: 77', 0, 0),
(363, 6, 'test', 'Room No: 46', 0, 0),
(364, 5, 'test', 'Room No: 78', 0, 0),
(365, 6, 'test', 'Room No: 47', 0, 0),
(366, 5, 'test', 'Room No: 79', 0, 0),
(367, 6, 'test', 'Room No: 48', 0, 0),
(368, 5, 'test', 'Room No: 80', 0, 0),
(369, 6, 'test', 'Room No: 49', 0, 0),
(370, 5, 'test', 'Room No: 81', 0, 0),
(371, 6, 'test', 'Room No: 50', 0, 0),
(372, 5, 'test', 'Room No: 82', 0, 0),
(373, 6, 'test', 'Room No: 51', 0, 0),
(374, 5, 'test', 'Room No: 83', 0, 0),
(375, 6, 'test', 'Room No: 52', 0, 0),
(376, 5, 'test', 'Room No: 84', 0, 0),
(377, 6, 'test', 'Room No: 53', 0, 0),
(378, 5, 'test', 'Room No: 85', 0, 0),
(379, 6, 'test', 'Room No: 54', 0, 0),
(380, 5, 'test', 'Room No: 86', 0, 0),
(381, 6, 'test', 'Room No: 55', 0, 0),
(382, 5, 'test', 'Room No: 87', 0, 0),
(383, 6, 'test', 'Room No: 56', 0, 0),
(384, 5, 'test', 'Room No: 88', 0, 0),
(385, 6, 'test', 'Room No: 57', 0, 0),
(386, 5, 'test', 'Room No: 89', 0, 0),
(387, 6, 'test', 'Room No: 58', 0, 0),
(388, 5, 'test', 'Room No: 90', 0, 0),
(389, 6, 'test', 'Room No: 59', 0, 0),
(390, 5, 'test', 'Room No: 91', 0, 0),
(391, 6, 'test', 'Room No: 60', 0, 0),
(392, 5, 'test', 'Room No: 92', 0, 0),
(393, 6, 'test', 'Room No: 61', 0, 0),
(394, 5, 'test', 'Room No: 93', 0, 0),
(395, 6, 'test', 'Room No: 62', 0, 0),
(396, 5, 'test', 'Room No: 94', 0, 0),
(397, 6, 'test', 'Room No: 63', 0, 0),
(398, 5, 'test', 'Room No: 95', 0, 0),
(399, 6, 'test', 'Room No: 64', 0, 0),
(400, 5, 'test', 'Room No: 96', 0, 0),
(401, 6, 'test', 'Room No: 65', 0, 0),
(402, 5, 'test', 'Room No: 97', 0, 0),
(403, 6, 'test', 'Room No: 66', 0, 0),
(404, 5, 'test', 'Room No: 98', 0, 0),
(405, 6, 'test', 'Room No: 67', 0, 0),
(406, 5, 'test', 'Room No: 99', 0, 0),
(407, 6, 'test', 'Room No: 68', 0, 0),
(408, 5, 'test', 'Room No: 100', 0, 0),
(409, 6, 'test', 'Room No: 69', 0, 0),
(410, 5, 'test', 'Room No: 101', 0, 0),
(411, 6, 'test', 'Room No: 70', 0, 0),
(412, 5, 'test', 'Room No: 102', 0, 0),
(413, 6, 'test', 'Room No: 71', 0, 0),
(414, 5, 'test', 'Room No: 103', 0, 0),
(415, 6, 'test', 'Room No: 72', 0, 0),
(416, 5, 'test', 'Room No: 104', 0, 0),
(417, 6, 'test', 'Room No: 73', 0, 0),
(418, 5, 'test', 'Room No: 105', 0, 0),
(419, 6, 'test', 'Room No: 74', 0, 0),
(420, 5, 'test', 'Room No: 106', 0, 0),
(421, 6, 'test', 'Room No: 75', 0, 0),
(422, 5, 'test', 'Room No: 107', 0, 0),
(423, 6, 'test', 'Room No: 76', 0, 0),
(424, 5, 'test', 'Room No: 108', 0, 0),
(425, 6, 'test', 'Room No: 77', 0, 0),
(426, 5, 'test', 'Room No: 109', 0, 0),
(427, 6, 'test', 'Room No: 78', 0, 0),
(428, 5, 'test', 'Room No: 110', 0, 0),
(429, 6, 'test', 'Room No: 79', 0, 0),
(430, 5, 'test', 'Room No: 111', 0, 0),
(431, 6, 'test', 'Room No: 80', 0, 0),
(432, 5, 'test', 'Room No: 112', 0, 0),
(433, 6, 'test', 'Room No: 81', 0, 0),
(434, 5, 'test', 'Room No: 113', 0, 0),
(435, 6, 'test', 'Room No: 82', 0, 0),
(436, 5, 'test', 'Room No: 114', 0, 0),
(437, 6, 'test', 'Room No: 83', 0, 0),
(438, 5, 'test', 'Room No: 115', 0, 0),
(439, 6, 'test', 'Room No: 84', 0, 0),
(440, 5, 'test', 'Room No: 116', 0, 0),
(441, 6, 'test', 'Room No: 85', 0, 0),
(442, 5, 'test', 'Room No: 117', 0, 0),
(443, 6, 'test', 'Room No: 86', 0, 0),
(444, 5, 'test', 'Room No: 118', 0, 0),
(445, 6, 'test', 'Room No: 87', 0, 0),
(446, 5, 'test', 'Room No: 119', 0, 0),
(447, 6, 'test', 'Room No: 88', 0, 0),
(448, 5, 'test', 'Room No: 120', 0, 0),
(449, 6, 'test', 'Room No: 89', 0, 0),
(450, 5, 'test', 'Room No: 121', 0, 0),
(451, 6, 'test', 'Room No: 90', 0, 0),
(452, 5, 'test', 'Room No: 122', 0, 0),
(453, 6, 'test', 'Room No: 91', 0, 0),
(454, 5, 'test', 'Room No: 123', 0, 0),
(455, 6, 'test', 'Room No: 92', 0, 0),
(456, 5, 'test', 'Room No: 124', 0, 0),
(457, 6, 'test', 'Room No: 93', 0, 0),
(458, 5, 'test', 'Room No: 125', 0, 0),
(459, 6, 'test', 'Room No: 94', 0, 0),
(460, 5, 'test', 'Room No: 126', 0, 0),
(461, 6, 'test', 'Room No: 95', 0, 0),
(462, 5, 'test', 'Room No: 127', 0, 0),
(463, 6, 'test', 'Room No: 96', 0, 0),
(464, 5, 'test', 'Room No: 128', 0, 0),
(465, 6, 'test', 'Room No: 97', 0, 0),
(466, 5, 'test', 'Room No: 129', 0, 0),
(467, 6, 'test', 'Room No: 98', 0, 0),
(468, 5, 'test', 'Room No: 130', 0, 0),
(469, 6, 'test', 'Room No: 99', 0, 0),
(470, 5, 'test', 'Room No: 131', 0, 0),
(471, 6, 'test', 'Room No: 100', 0, 0),
(472, 5, 'test', 'Room No: 132', 0, 0),
(473, 6, 'test', 'Room No: 101', 0, 0),
(474, 5, 'test', 'Room No: 133', 0, 0),
(475, 6, 'test', 'Room No: 102', 0, 0),
(476, 5, 'test', 'Room No: 134', 0, 0),
(477, 6, 'test', 'Room No: 103', 0, 0),
(478, 5, 'test', 'Room No: 135', 0, 0),
(479, 6, 'test', 'Room No: 104', 0, 0),
(480, 5, 'test', 'Room No: 136', 0, 0),
(481, 6, 'test', 'Room No: 105', 0, 0),
(482, 5, 'test', 'Room No: 137', 0, 0),
(483, 6, 'test', 'Room No: 106', 0, 0),
(484, 5, 'test', 'Room No: 138', 0, 0),
(485, 6, 'test', 'Room No: 107', 0, 0),
(486, 5, 'test', 'Room No: 139', 0, 0),
(487, 6, 'test', 'Room No: 108', 0, 0),
(488, 5, 'test', 'Room No: 140', 0, 0),
(489, 6, 'test', 'Room No: 109', 0, 0),
(490, 5, 'test', 'Room No: 141', 0, 0),
(491, 6, 'test', 'Room No: 110', 0, 0),
(492, 5, 'test', 'Room No: 142', 0, 0),
(493, 6, 'test', 'Room No: 111', 0, 0),
(494, 5, 'test', 'Room No: 143', 0, 0),
(495, 6, 'test', 'Room No: 112', 0, 0),
(496, 5, 'test', 'Room No: 144', 0, 0),
(497, 6, 'test', 'Room No: 113', 0, 0),
(498, 5, 'test', 'Room No: 145', 0, 0),
(499, 6, 'test', 'Room No: 114', 0, 0),
(500, 5, 'test', 'Room No: 146', 0, 0),
(501, 6, 'test', 'Room No: 115', 0, 0),
(502, 5, 'test', 'Room No: 147', 0, 0),
(503, 6, 'test', 'Room No: 116', 0, 0),
(504, 5, 'test', 'Room No: 148', 0, 0),
(505, 6, 'test', 'Room No: 117', 0, 0),
(506, 5, 'test', 'Room No: 149', 0, 0),
(507, 6, 'test', 'Room No: 118', 0, 0),
(508, 5, 'test', 'Room No: 150', 0, 0),
(509, 6, 'test', 'Room No: 119', 0, 0),
(510, 5, 'test', 'Room No: 151', 0, 0),
(511, 6, 'test', 'Room No: 120', 0, 0),
(512, 5, 'test', 'Room No: 152', 0, 0),
(513, 6, 'test', 'Room No: 121', 0, 0),
(514, 5, 'test', 'Room No: 153', 0, 0),
(515, 6, 'test', 'Room No: 122', 0, 0),
(516, 5, 'test', 'Room No: 154', 0, 0),
(517, 6, 'test', 'Room No: 123', 0, 0),
(518, 5, 'test', 'Room No: 155', 0, 0),
(519, 6, 'test', 'Room No: 124', 0, 0),
(520, 5, 'test', 'Room No: 156', 0, 0),
(521, 6, 'test', 'Room No: 125', 0, 0),
(522, 5, 'test', 'Room No: 157', 0, 0),
(523, 6, 'test', 'Room No: 126', 0, 0),
(524, 5, 'test', 'Room No: 158', 0, 0),
(525, 6, 'test', 'Room No: 127', 0, 0),
(526, 5, 'test', 'Room No: 159', 0, 0),
(527, 6, 'test', 'Room No: 128', 0, 0),
(528, 5, 'test', 'Room No: 160', 0, 0),
(529, 6, 'test', 'Room No: 129', 0, 0),
(530, 5, 'test', 'Room No: 161', 0, 0),
(531, 6, 'test', 'Room No: 130', 0, 0),
(532, 5, 'test', 'Room No: 162', 0, 0),
(533, 6, 'test', 'Room No: 131', 0, 0),
(534, 5, 'test', 'Room No: 163', 0, 0),
(535, 6, 'test', 'Room No: 132', 0, 0),
(536, 5, 'test', 'Room No: 164', 0, 0),
(537, 6, 'test', 'Room No: 133', 0, 0),
(538, 5, 'test', 'Room No: 165', 0, 0),
(539, 6, 'test', 'Room No: 134', 0, 0),
(540, 5, 'test', 'Room No: 166', 0, 0),
(541, 6, 'test', 'Room No: 135', 0, 0),
(542, 5, 'test', 'Room No: 167', 0, 0),
(543, 6, 'test', 'Room No: 136', 0, 0),
(544, 5, 'test', 'Room No: 168', 0, 0),
(545, 6, 'test', 'Room No: 137', 0, 0),
(546, 5, 'test', 'Room No: 169', 0, 0),
(547, 6, 'test', 'Room No: 138', 0, 0),
(548, 5, 'test', 'Room No: 170', 0, 0),
(549, 6, 'test', 'Room No: 139', 0, 0),
(550, 5, 'test', 'Room No: 171', 0, 0),
(551, 6, 'test', 'Room No: 140', 0, 0),
(552, 5, 'test', 'Room No: 172', 0, 0),
(553, 6, 'test', 'Room No: 141', 0, 0),
(554, 5, 'test', 'Room No: 173', 0, 0),
(555, 6, 'test', 'Room No: 142', 0, 0),
(556, 5, 'test', 'Room No: 174', 0, 0),
(557, 6, 'test', 'Room No: 143', 0, 0),
(558, 5, 'test', 'Room No: 175', 0, 0),
(559, 6, 'test', 'Room No: 144', 0, 0),
(560, 5, 'test', 'Room No: 176', 0, 0),
(561, 6, 'test', 'Room No: 145', 0, 0),
(562, 5, 'test', 'Room No: 177', 0, 0),
(563, 6, 'test', 'Room No: 146', 0, 0),
(564, 5, 'test', 'Room No: 178', 0, 0),
(565, 6, 'test', 'Room No: 147', 0, 0),
(566, 5, 'test', 'Room No: 179', 0, 0),
(567, 6, 'test', 'Room No: 148', 0, 0),
(568, 5, 'test', 'Room No: 180', 0, 0),
(569, 6, 'test', 'Room No: 149', 0, 0),
(570, 5, 'test', 'Room No: 181', 0, 0),
(571, 6, 'test', 'Room No: 150', 0, 0),
(572, 5, 'test', 'Room No: 182', 0, 0),
(573, 6, 'test', 'Room No: 151', 0, 0),
(574, 5, 'test', 'Room No: 183', 0, 0),
(575, 6, 'test', 'Room No: 152', 0, 0),
(576, 5, 'test', 'Room No: 184', 0, 0),
(577, 6, 'test', 'Room No: 153', 0, 0),
(578, 5, 'test', 'Room No: 185', 0, 0),
(579, 6, 'test', 'Room No: 154', 0, 0),
(580, 5, 'test', 'Room No: 186', 0, 0),
(581, 6, 'test', 'Room No: 155', 0, 0),
(582, 5, 'test', 'Room No: 187', 0, 0),
(583, 6, 'test', 'Room No: 156', 0, 0),
(584, 5, 'test', 'Room No: 188', 0, 0),
(585, 6, 'test', 'Room No: 157', 0, 0),
(586, 5, 'test', 'Room No: 189', 0, 0),
(587, 6, 'test', 'Room No: 158', 0, 0),
(588, 5, 'test', 'Room No: 190', 0, 0),
(589, 6, 'test', 'Room No: 159', 0, 0),
(590, 5, 'test', 'Room No: 191', 0, 0),
(591, 6, 'test', 'Room No: 160', 0, 0),
(592, 5, 'test', 'Room No: 192', 0, 0),
(593, 6, 'test', 'Room No: 161', 0, 0),
(594, 5, 'test', 'Room No: 193', 0, 0),
(595, 6, 'test', 'Room No: 162', 0, 0),
(596, 5, 'test', 'Room No: 194', 0, 0),
(597, 6, 'test', 'Room No: 163', 0, 0),
(598, 5, 'test', 'Room No: 195', 0, 0),
(599, 6, 'test', 'Room No: 164', 0, 0),
(600, 5, 'test', 'Room No: 196', 0, 0),
(601, 6, 'test', 'Room No: 165', 0, 0),
(602, 5, 'test', 'Room No: 197', 0, 0),
(603, 6, 'test', 'Room No: 166', 0, 0),
(604, 5, 'test', 'Room No: 198', 0, 0),
(605, 6, 'test', 'Room No: 167', 0, 0),
(606, 5, 'test', 'Room No: 199', 0, 0),
(607, 6, 'test', 'Room No: 168', 0, 0),
(608, 5, 'test', 'Room No: 200', 0, 0),
(609, 6, 'test', 'Room No: 169', 0, 0),
(610, 5, 'test', 'Room No: 201', 0, 0),
(611, 6, 'test', 'Room No: 170', 0, 0),
(612, 5, 'test', 'Room No: 202', 0, 0),
(613, 6, 'test', 'Room No: 171', 0, 0),
(614, 5, 'test', 'Room No: 203', 0, 0),
(615, 6, 'test', 'Room No: 172', 0, 0),
(616, 5, 'test', 'Room No: 204', 0, 0),
(617, 6, 'test', 'Room No: 173', 0, 0),
(618, 5, 'test', 'Room No: 205', 0, 0),
(619, 6, 'test', 'Room No: 174', 0, 0),
(620, 5, 'test', 'Room No: 206', 0, 0),
(621, 6, 'test', 'Room No: 175', 0, 0),
(622, 5, 'test', 'Room No: 207', 0, 0),
(623, 6, 'test', 'Room No: 176', 0, 0),
(624, 5, 'test', 'Room No: 208', 0, 0),
(625, 6, 'test', 'Room No: 177', 0, 0),
(626, 5, 'test', 'Room No: 209', 0, 0),
(627, 6, 'test', 'Room No: 178', 0, 0),
(628, 5, 'test', 'Room No: 210', 0, 0),
(629, 6, 'test', 'Room No: 179', 0, 0),
(630, 5, 'test', 'Room No: 211', 0, 0),
(631, 6, 'test', 'Room No: 180', 0, 0),
(632, 5, 'test', 'Room No: 212', 0, 0),
(633, 6, 'test', 'Room No: 181', 0, 0),
(634, 5, 'test', 'Room No: 213', 0, 0),
(635, 6, 'test', 'Room No: 182', 0, 0),
(636, 5, 'test', 'Room No: 214', 0, 0),
(637, 6, 'test', 'Room No: 183', 0, 0),
(638, 5, 'test', 'Room No: 215', 0, 0),
(639, 6, 'test', 'Room No: 184', 0, 0),
(640, 5, 'test', 'Room No: 216', 0, 0),
(641, 6, 'test', 'Room No: 185', 0, 0),
(642, 5, 'test', 'Room No: 217', 0, 0),
(643, 6, 'test', 'Room No: 186', 0, 0),
(644, 5, 'test', 'Room No: 218', 0, 0),
(645, 6, 'test', 'Room No: 187', 0, 0),
(646, 5, 'test', 'Room No: 219', 0, 0),
(647, 6, 'test', 'Room No: 188', 0, 0),
(648, 5, 'test', 'Room No: 220', 0, 0),
(649, 6, 'test', 'Room No: 189', 0, 0),
(650, 5, 'test', 'Room No: 221', 0, 0),
(651, 6, 'test', 'Room No: 190', 0, 0),
(652, 5, 'test', 'Room No: 222', 0, 0),
(653, 6, 'test', 'Room No: 191', 0, 0),
(654, 5, 'test', 'Room No: 223', 0, 0),
(655, 6, 'test', 'Room No: 192', 0, 0),
(656, 5, 'test', 'Room No: 224', 0, 0),
(657, 6, 'test', 'Room No: 193', 0, 0),
(658, 5, 'test', 'Room No: 225', 0, 0),
(659, 6, 'test', 'Room No: 194', 0, 0),
(660, 5, 'test', 'Room No: 226', 0, 0),
(661, 6, 'test', 'Room No: 195', 0, 0),
(662, 5, 'test', 'Room No: 227', 0, 0),
(663, 6, 'test', 'Room No: 196', 0, 0),
(664, 5, 'test', 'Room No: 228', 0, 0),
(665, 6, 'test', 'Room No: 197', 0, 0),
(666, 5, 'test', 'Room No: 229', 0, 0),
(667, 6, 'test', 'Room No: 198', 0, 0),
(668, 5, 'test', 'Room No: 230', 0, 0),
(669, 6, 'test', 'Room No: 199', 0, 0),
(670, 5, 'test', 'Room No: 231', 0, 0),
(671, 6, 'test', 'Room No: 200', 0, 0),
(672, 5, 'test', 'Room No: 232', 0, 0),
(673, 6, 'test', 'Room No: 201', 0, 0),
(674, 5, 'test', 'Room No: 233', 0, 0),
(675, 6, 'test', 'Room No: 202', 0, 0),
(676, 5, 'test', 'Room No: 234', 0, 0),
(677, 6, 'test', 'Room No: 203', 0, 0),
(678, 5, 'test', 'Room No: 235', 0, 0),
(679, 6, 'test', 'Room No: 204', 0, 0),
(680, 5, 'test', 'Room No: 236', 0, 0),
(681, 6, 'test', 'Room No: 205', 0, 0),
(682, 5, 'test', 'Room No: 237', 0, 0),
(683, 6, 'test', 'Room No: 206', 0, 0),
(684, 5, 'test', 'Room No: 238', 0, 0),
(685, 6, 'test', 'Room No: 207', 0, 0),
(686, 5, 'test', 'Room No: 239', 0, 0),
(687, 6, 'test', 'Room No: 208', 0, 0),
(688, 5, 'test', 'Room No: 240', 0, 0),
(689, 6, 'test', 'Room No: 209', 0, 0),
(690, 5, 'test', 'Room No: 241', 0, 0),
(691, 6, 'test', 'Room No: 210', 0, 0),
(692, 5, 'test', 'Room No: 242', 0, 0),
(693, 6, 'test', 'Room No: 211', 0, 0),
(694, 5, 'test', 'Room No: 243', 0, 0),
(695, 6, 'test', 'Room No: 212', 0, 0),
(696, 5, 'test', 'Room No: 244', 0, 0),
(697, 6, 'test', 'Room No: 213', 0, 0),
(698, 5, 'test', 'Room No: 245', 0, 0),
(699, 6, 'test', 'Room No: 214', 0, 0),
(700, 5, 'test', 'Room No: 246', 0, 0),
(701, 6, 'test', 'Room No: 215', 0, 0),
(702, 5, 'test', 'Room No: 247', 0, 0),
(703, 6, 'test', 'Room No: 216', 0, 0),
(704, 5, 'test', 'Room No: 248', 0, 0),
(705, 6, 'test', 'Room No: 217', 0, 0),
(706, 5, 'test', 'Room No: 249', 0, 0),
(707, 6, 'test', 'Room No: 218', 0, 0),
(708, 5, 'test', 'Room No: 250', 0, 0),
(709, 6, 'test', 'Room No: 219', 0, 0),
(710, 5, 'test', 'Room No: 251', 0, 0),
(711, 6, 'test', 'Room No: 220', 0, 0),
(712, 5, 'test', 'Room No: 252', 0, 0),
(713, 6, 'test', 'Room No: 221', 0, 0),
(714, 5, 'test', 'Room No: 253', 0, 0),
(715, 6, 'test', 'Room No: 222', 0, 0),
(716, 5, 'test', 'Room No: 254', 0, 0),
(717, 6, 'test', 'Room No: 223', 0, 0),
(718, 5, 'test', 'Room No: 255', 0, 0),
(719, 6, 'test', 'Room No: 224', 0, 0),
(720, 5, 'test', 'Room No: 256', 0, 0),
(721, 6, 'test', 'Room No: 225', 0, 0),
(722, 5, 'test', 'Room No: 257', 0, 0),
(723, 6, 'test', 'Room No: 226', 0, 0),
(724, 5, 'test', 'Room No: 258', 0, 0),
(725, 6, 'test', 'Room No: 227', 0, 0),
(726, 5, 'test', 'Room No: 259', 0, 0),
(727, 6, 'test', 'Room No: 228', 0, 0),
(728, 5, 'test', 'Room No: 260', 0, 0),
(729, 6, 'test', 'Room No: 229', 0, 0),
(730, 5, 'test', 'Room No: 261', 0, 0),
(731, 6, 'test', 'Room No: 230', 0, 0),
(732, 5, 'test', 'Room No: 262', 0, 0),
(733, 6, 'test', 'Room No: 231', 0, 0),
(734, 5, 'test', 'Room No: 263', 0, 0),
(735, 6, 'test', 'Room No: 232', 0, 0),
(736, 5, 'test', 'Room No: 264', 0, 0),
(737, 6, 'test', 'Room No: 233', 0, 0),
(738, 5, 'test', 'Room No: 265', 0, 0),
(739, 6, 'test', 'Room No: 234', 0, 0),
(740, 5, 'test', 'Room No: 266', 0, 0),
(741, 6, 'test', 'Room No: 235', 0, 0),
(742, 5, 'test', 'Room No: 267', 0, 0),
(743, 6, 'test', 'Room No: 236', 0, 0),
(744, 5, 'test', 'Room No: 268', 0, 0),
(745, 6, 'test', 'Room No: 237', 0, 0),
(746, 5, 'test', 'Room No: 269', 0, 0),
(747, 6, 'test', 'Room No: 238', 0, 0),
(748, 5, 'test', 'Room No: 270', 0, 0),
(749, 6, 'test', 'Room No: 239', 0, 0),
(750, 5, 'test', 'Room No: 271', 0, 0),
(751, 6, 'test', 'Room No: 240', 0, 0),
(752, 5, 'test', 'Room No: 272', 0, 0),
(753, 6, 'test', 'Room No: 241', 0, 0),
(754, 5, 'test', 'Room No: 273', 0, 0),
(755, 6, 'test', 'Room No: 242', 0, 0),
(756, 5, 'test', 'Room No: 274', 0, 0),
(757, 6, 'test', 'Room No: 243', 0, 0),
(758, 5, 'test', 'Room No: 275', 0, 0),
(759, 6, 'test', 'Room No: 244', 0, 0),
(760, 5, 'test', 'Room No: 276', 0, 0),
(761, 6, 'test', 'Room No: 245', 0, 0),
(762, 5, 'test', 'Room No: 277', 0, 0),
(763, 6, 'test', 'Room No: 246', 0, 0),
(764, 5, 'test', 'Room No: 278', 0, 0),
(765, 6, 'test', 'Room No: 247', 0, 0),
(766, 5, 'test', 'Room No: 279', 0, 0),
(767, 6, 'test', 'Room No: 248', 0, 0),
(768, 5, 'test', 'Room No: 280', 0, 0),
(769, 6, 'test', 'Room No: 249', 0, 0),
(770, 5, 'test', 'Room No: 281', 0, 0),
(771, 6, 'test', 'Room No: 250', 0, 0),
(772, 5, 'test', 'Room No: 282', 0, 0),
(773, 6, 'test', 'Room No: 251', 0, 0),
(774, 5, 'test', 'Room No: 283', 0, 0),
(775, 6, 'test', 'Room No: 252', 0, 0),
(776, 5, 'test', 'Room No: 284', 0, 0),
(777, 6, 'test', 'Room No: 253', 0, 0),
(778, 5, 'test', 'Room No: 285', 0, 0),
(779, 6, 'test', 'Room No: 254', 0, 0),
(780, 5, 'test', 'Room No: 286', 0, 0),
(781, 6, 'test', 'Room No: 255', 0, 0),
(782, 5, 'test', 'Room No: 287', 0, 0),
(783, 6, 'test', 'Room No: 256', 0, 0),
(784, 5, 'test', 'Room No: 288', 0, 0),
(785, 6, 'test', 'Room No: 257', 0, 0),
(786, 5, 'test', 'Room No: 289', 0, 0),
(787, 6, 'test', 'Room No: 258', 0, 0),
(788, 5, 'test', 'Room No: 290', 0, 0),
(789, 6, 'test', 'Room No: 259', 0, 0),
(790, 5, 'test', 'Room No: 291', 0, 0),
(791, 6, 'test', 'Room No: 260', 0, 0),
(792, 5, 'test', 'Room No: 292', 0, 0),
(793, 6, 'test', 'Room No: 261', 0, 0),
(794, 5, 'test', 'Room No: 293', 0, 0),
(795, 6, 'test', 'Room No: 262', 0, 0),
(796, 5, 'test', 'Room No: 294', 0, 0),
(797, 6, 'test', 'Room No: 263', 0, 0),
(798, 5, 'test', 'Room No: 295', 0, 0),
(799, 6, 'test', 'Room No: 264', 0, 0),
(800, 5, 'test', 'Room No: 296', 0, 0),
(801, 6, 'test', 'Room No: 265', 0, 0),
(802, 5, 'test', 'Room No: 297', 0, 0),
(803, 6, 'test', 'Room No: 266', 0, 0),
(804, 5, 'test', 'Room No: 298', 0, 0),
(805, 6, 'test', 'Room No: 267', 0, 0),
(806, 5, 'test', 'Room No: 299', 0, 0),
(807, 6, 'test', 'Room No: 268', 0, 0),
(808, 5, 'test', 'Room No: 300', 0, 0),
(809, 6, 'test', 'Room No: 269', 0, 0),
(810, 5, 'test', 'Room No: 301', 0, 0),
(811, 6, 'test', 'Room No: 270', 0, 0),
(812, 5, 'test', 'Room No: 302', 0, 0),
(813, 6, 'test', 'Room No: 271', 0, 0),
(814, 5, 'test', 'Room No: 303', 0, 0),
(815, 6, 'test', 'Room No: 272', 0, 0),
(816, 5, 'test', 'Room No: 304', 0, 0),
(817, 6, 'test', 'Room No: 273', 0, 0),
(818, 5, 'test', 'Room No: 305', 0, 0),
(819, 6, 'test', 'Room No: 274', 0, 0),
(820, 5, 'test', 'Room No: 306', 0, 0),
(821, 6, 'test', 'Room No: 275', 0, 0),
(822, 5, 'test', 'Room No: 307', 0, 0),
(823, 6, 'test', 'Room No: 276', 0, 0),
(824, 5, 'test', 'Room No: 308', 0, 0),
(825, 6, 'test', 'Room No: 277', 0, 0),
(826, 5, 'test', 'Room No: 309', 0, 0),
(827, 6, 'test', 'Room No: 278', 0, 0),
(828, 5, 'test', 'Room No: 310', 0, 0),
(829, 6, 'test', 'Room No: 279', 0, 0),
(830, 5, 'test', 'Room No: 311', 0, 0),
(831, 6, 'test', 'Room No: 280', 0, 0),
(832, 5, 'test', 'Room No: 312', 0, 0),
(833, 6, 'test', 'Room No: 281', 0, 0),
(834, 5, 'test', 'Room No: 313', 0, 0),
(835, 6, 'test', 'Room No: 282', 0, 0),
(836, 5, 'test', 'Room No: 314', 0, 0),
(837, 6, 'test', 'Room No: 283', 0, 0),
(838, 5, 'test', 'Room No: 315', 0, 0),
(839, 6, 'test', 'Room No: 284', 0, 0),
(840, 5, 'test', 'Room No: 316', 0, 0),
(841, 6, 'test', 'Room No: 285', 0, 0),
(842, 5, 'test', 'Room No: 317', 0, 0),
(843, 6, 'test', 'Room No: 286', 0, 0),
(844, 5, 'test', 'Room No: 318', 0, 0),
(845, 6, 'test', 'Room No: 287', 0, 0),
(846, 5, 'test', 'Room No: 319', 0, 0),
(847, 6, 'test', 'Room No: 288', 0, 0),
(848, 5, 'test', 'Room No: 320', 0, 0),
(849, 6, 'test', 'Room No: 289', 0, 0),
(850, 5, 'test', 'Room No: 321', 0, 0),
(851, 6, 'test', 'Room No: 290', 0, 0),
(852, 5, 'test', 'Room No: 322', 0, 0),
(853, 6, 'test', 'Room No: 291', 0, 0),
(854, 5, 'test', 'Room No: 323', 0, 0),
(855, 6, 'test', 'Room No: 292', 0, 0),
(856, 5, 'test', 'Room No: 324', 0, 0),
(857, 6, 'test', 'Room No: 293', 0, 0),
(858, 5, 'test', 'Room No: 325', 0, 0),
(859, 6, 'test', 'Room No: 294', 0, 0),
(860, 5, 'test', 'Room No: 326', 0, 0),
(861, 6, 'test', 'Room No: 295', 0, 0),
(862, 5, 'test', 'Room No: 327', 0, 0),
(863, 6, 'test', 'Room No: 296', 0, 0),
(864, 5, 'test', 'Room No: 328', 0, 0),
(865, 6, 'test', 'Room No: 297', 0, 0),
(866, 5, 'test', 'Room No: 329', 0, 0),
(867, 6, 'test', 'Room No: 298', 0, 0),
(868, 5, 'test', 'Room No: 330', 0, 0),
(869, 6, 'test', 'Room No: 299', 0, 0),
(870, 5, 'test', 'Room No: 331', 0, 0),
(871, 6, 'test', 'Room No: 300', 0, 0),
(872, 5, 'test', 'Room No: 332', 0, 0),
(873, 6, 'test', 'Room No: 301', 0, 0),
(874, 5, 'test', 'Room No: 333', 0, 0),
(875, 6, 'test', 'Room No: 302', 0, 0),
(876, 5, 'test', 'Room No: 334', 0, 0),
(877, 6, 'test', 'Room No: 303', 0, 0),
(878, 5, 'test', 'Room No: 335', 0, 0),
(879, 6, 'test', 'Room No: 304', 0, 0),
(880, 5, 'test', 'Room No: 336', 0, 0),
(881, 6, 'test', 'Room No: 305', 0, 0),
(882, 5, 'test', 'Room No: 337', 0, 0),
(883, 6, 'test', 'Room No: 306', 0, 0),
(884, 5, 'test', 'Room No: 338', 0, 0),
(885, 6, 'test', 'Room No: 307', 0, 0),
(886, 5, 'test', 'Room No: 339', 0, 0),
(887, 6, 'test', 'Room No: 308', 0, 0),
(888, 5, 'test', 'Room No: 340', 0, 0),
(889, 6, 'test', 'Room No: 309', 0, 0),
(890, 5, 'test', 'Room No: 341', 0, 0),
(891, 6, 'test', 'Room No: 310', 0, 0),
(892, 5, 'test', 'Room No: 342', 0, 0),
(893, 6, 'test', 'Room No: 311', 0, 0),
(894, 5, 'test', 'Room No: 343', 0, 0),
(895, 6, 'test', 'Room No: 312', 0, 0),
(896, 5, 'test', 'Room No: 344', 0, 0),
(897, 6, 'test', 'Room No: 313', 0, 0),
(898, 5, 'test', 'Room No: 345', 0, 0),
(899, 6, 'test', 'Room No: 314', 0, 0),
(900, 5, 'test', 'Room No: 346', 0, 0),
(901, 6, 'test', 'Room No: 315', 0, 0),
(902, 5, 'test', 'Room No: 347', 0, 0),
(903, 6, 'test', 'Room No: 316', 0, 0),
(904, 5, 'test', 'Room No: 348', 0, 0),
(905, 6, 'test', 'Room No: 317', 0, 0),
(906, 5, 'test', 'Room No: 349', 0, 0),
(907, 6, 'test', 'Room No: 318', 0, 0),
(908, 5, 'test', 'Room No: 350', 0, 0),
(909, 6, 'test', 'Room No: 319', 0, 0),
(910, 5, 'test', 'Room No: 351', 0, 0),
(911, 6, 'test', 'Room No: 320', 0, 0),
(912, 5, 'test', 'Room No: 352', 0, 0),
(913, 6, 'test', 'Room No: 321', 0, 0),
(914, 5, 'test', 'Room No: 353', 0, 0),
(915, 6, 'test', 'Room No: 322', 0, 0),
(916, 5, 'test', 'Room No: 354', 0, 0),
(917, 6, 'test', 'Room No: 323', 0, 0),
(918, 5, 'test', 'Room No: 355', 0, 0),
(919, 6, 'test', 'Room No: 324', 0, 0),
(920, 5, 'test', 'Room No: 356', 0, 0),
(921, 6, 'test', 'Room No: 325', 0, 0),
(922, 5, 'test', 'Room No: 357', 0, 0),
(923, 6, 'test', 'Room No: 326', 0, 0),
(924, 5, 'test', 'Room No: 358', 0, 0),
(925, 6, 'test', 'Room No: 327', 0, 0),
(926, 5, 'test', 'Room No: 359', 0, 0),
(927, 6, 'test', 'Room No: 328', 0, 0),
(928, 5, 'test', 'Room No: 360', 0, 0),
(929, 6, 'test', 'Room No: 329', 0, 0),
(930, 5, 'test', 'Room No: 361', 0, 0),
(931, 6, 'test', 'Room No: 330', 0, 0),
(932, 5, 'test', 'Room No: 362', 0, 0),
(933, 6, 'test', 'Room No: 331', 0, 0),
(934, 5, 'test', 'Room No: 363', 0, 0),
(935, 6, 'test', 'Room No: 332', 0, 0),
(936, 5, 'test', 'Room No: 364', 0, 0),
(937, 6, 'test', 'Room No: 333', 0, 0),
(938, 5, 'test', 'Room No: 365', 0, 0),
(939, 6, 'test', 'Room No: 334', 0, 0),
(940, 5, 'test', 'Room No: 366', 0, 0),
(941, 6, 'test', 'Room No: 335', 0, 0),
(942, 5, 'test', 'Room No: 367', 0, 0),
(943, 6, 'test', 'Room No: 336', 0, 0),
(944, 5, 'test', 'Room No: 368', 0, 0),
(945, 6, 'test', 'Room No: 337', 0, 0),
(946, 5, 'test', 'Room No: 369', 0, 0),
(947, 6, 'test', 'Room No: 338', 0, 0),
(948, 5, 'test', 'Room No: 370', 0, 0),
(949, 6, 'test', 'Room No: 339', 0, 0),
(950, 5, 'test', 'Room No: 371', 0, 0),
(951, 6, 'test', 'Room No: 340', 0, 0),
(952, 5, 'test', 'Room No: 372', 0, 0),
(953, 6, 'test', 'Room No: 341', 0, 0),
(954, 5, 'test', 'Room No: 373', 0, 0),
(955, 6, 'test', 'Room No: 342', 0, 0),
(956, 5, 'test', 'Room No: 374', 0, 0),
(957, 6, 'test', 'Room No: 343', 0, 0),
(958, 5, 'test', 'Room No: 375', 0, 0),
(959, 6, 'test', 'Room No: 344', 0, 0),
(960, 5, 'test', 'Room No: 376', 0, 0),
(961, 6, 'test', 'Room No: 345', 0, 0),
(962, 5, 'test', 'Room No: 377', 0, 0),
(963, 6, 'test', 'Room No: 346', 0, 0),
(964, 5, 'test', 'Room No: 378', 0, 0),
(965, 6, 'test', 'Room No: 347', 0, 0),
(966, 5, 'test', 'Room No: 379', 0, 0),
(967, 6, 'test', 'Room No: 348', 0, 0),
(968, 5, 'test', 'Room No: 380', 0, 0),
(969, 6, 'test', 'Room No: 349', 0, 0),
(970, 5, 'test', 'Room No: 381', 0, 0),
(971, 6, 'test', 'Room No: 350', 0, 0),
(972, 5, 'test', 'Room No: 382', 0, 0),
(973, 6, 'test', 'Room No: 351', 0, 0),
(974, 5, 'test', 'Room No: 383', 0, 0),
(975, 6, 'test', 'Room No: 352', 0, 0),
(976, 5, 'test', 'Room No: 384', 0, 0),
(977, 6, 'test', 'Room No: 353', 0, 0),
(978, 5, 'test', 'Room No: 385', 0, 0),
(979, 6, 'test', 'Room No: 354', 0, 0),
(980, 5, 'test', 'Room No: 386', 0, 0),
(981, 6, 'test', 'Room No: 355', 0, 0),
(982, 5, 'test', 'Room No: 387', 0, 0),
(983, 6, 'test', 'Room No: 356', 0, 0),
(984, 5, 'test', 'Room No: 388', 0, 0),
(985, 6, 'test', 'Room No: 357', 0, 0),
(986, 5, 'test', 'Room No: 389', 0, 0),
(987, 6, 'test', 'Room No: 358', 0, 0),
(988, 5, 'test', 'Room No: 390', 0, 0),
(989, 6, 'test', 'Room No: 359', 0, 0),
(990, 5, 'test', 'Room No: 391', 0, 0),
(991, 6, 'test', 'Room No: 360', 0, 0),
(992, 5, 'test', 'Room No: 392', 0, 0),
(993, 6, 'test', 'Room No: 361', 0, 0),
(994, 5, 'test', 'Room No: 393', 0, 0),
(995, 6, 'test', 'Room No: 362', 0, 0),
(996, 5, 'test', 'Room No: 394', 0, 0),
(997, 6, 'test', 'Room No: 363', 0, 0),
(998, 5, 'test', 'Room No: 395', 0, 0),
(999, 6, 'test', 'Room No: 364', 0, 0),
(1000, 5, 'test', 'Room No: 396', 0, 0),
(1001, 6, 'test', 'Room No: 365', 0, 0),
(1002, 5, 'test', 'Room No: 397', 0, 0),
(1003, 6, 'test', 'Room No: 366', 0, 0),
(1004, 5, 'test', 'Room No: 398', 0, 0),
(1005, 6, 'test', 'Room No: 367', 0, 0),
(1006, 5, 'test', 'Room No: 399', 0, 0),
(1007, 6, 'test', 'Room No: 368', 0, 0),
(1008, 5, 'test', 'Room No: 400', 0, 0),
(1009, 6, 'test', 'Room No: 369', 0, 0),
(1010, 5, 'test', 'Room No: 401', 0, 0),
(1011, 6, 'test', 'Room No: 370', 0, 0),
(1012, 5, 'test', 'Room No: 402', 0, 0),
(1013, 6, 'test', 'Room No: 371', 0, 0),
(1014, 6, 'test', 'Room No: 372', 0, 0),
(1015, 5, 'test', 'Room No: 403', 0, 0),
(1016, 6, 'test', 'Room No: 373', 0, 0),
(1017, 5, 'test', 'Room No: 404', 0, 0),
(1018, 6, 'test', 'Room No: 374', 0, 0),
(1019, 5, 'test', 'Room No: 405', 0, 0),
(1020, 6, 'test', 'Room No: 375', 0, 0),
(1021, 5, 'test', 'Room No: 406', 0, 0),
(1022, 6, 'test', 'Room No: 376', 0, 0),
(1023, 5, 'test', 'Room No: 407', 0, 0),
(1024, 6, 'test', 'Room No: 377', 0, 0),
(1025, 5, 'test', 'Room No: 408', 0, 0),
(1026, 6, 'test', 'Room No: 378', 0, 0),
(1027, 5, 'test', 'Room No: 409', 0, 0),
(1028, 6, 'test', 'Room No: 379', 0, 0),
(1029, 5, 'test', 'Room No: 410', 0, 0),
(1030, 6, 'test', 'Room No: 380', 0, 0),
(1031, 5, 'test', 'Room No: 411', 0, 0),
(1032, 6, 'test', 'Room No: 381', 0, 0),
(1033, 5, 'test', 'Room No: 412', 0, 0),
(1034, 6, 'test', 'Room No: 382', 0, 0),
(1035, 5, 'test', 'Room No: 413', 0, 0),
(1036, 6, 'test', 'Room No: 383', 0, 0),
(1037, 5, 'test', 'Room No: 414', 0, 0),
(1038, 6, 'test', 'Room No: 384', 0, 0),
(1039, 5, 'test', 'Room No: 415', 0, 0),
(1040, 6, 'test', 'Room No: 385', 0, 0),
(1041, 5, 'test', 'Room No: 416', 0, 0),
(1042, 6, 'test', 'Room No: 386', 0, 0),
(1043, 5, 'test', 'Room No: 417', 0, 0),
(1044, 6, 'test', 'Room No: 387', 0, 0),
(1045, 5, 'test', 'Room No: 418', 0, 0),
(1046, 6, 'test', 'Room No: 388', 0, 0),
(1047, 5, 'test', 'Room No: 419', 0, 0),
(1048, 6, 'test', 'Room No: 389', 0, 0),
(1049, 5, 'test', 'Room No: 420', 0, 0),
(1050, 6, 'test', 'Room No: 390', 0, 0),
(1051, 5, 'test', 'Room No: 421', 0, 0),
(1052, 6, 'test', 'Room No: 391', 0, 0),
(1053, 5, 'test', 'Room No: 422', 0, 0),
(1054, 6, 'test', 'Room No: 392', 0, 0),
(1055, 5, 'test', 'Room No: 423', 0, 0),
(1056, 6, 'test', 'Room No: 393', 0, 0),
(1057, 5, 'test', 'Room No: 424', 0, 0),
(1058, 6, 'test', 'Room No: 394', 0, 0),
(1059, 5, 'test', 'Room No: 425', 0, 0),
(1060, 6, 'test', 'Room No: 395', 0, 0),
(1061, 5, 'test', 'Room No: 426', 0, 0),
(1062, 6, 'test', 'Room No: 396', 0, 0),
(1063, 5, 'test', 'Room No: 427', 0, 0),
(1064, 6, 'test', 'Room No: 397', 0, 0),
(1065, 5, 'test', 'Room No: 428', 0, 0),
(1066, 6, 'test', 'Room No: 398', 0, 0),
(1067, 5, 'test', 'Room No: 429', 0, 0),
(1068, 6, 'test', 'Room No: 399', 0, 0),
(1069, 5, 'test', 'Room No: 430', 0, 0),
(1070, 6, 'test', 'Room No: 400', 0, 0),
(1071, 5, 'test', 'Room No: 431', 0, 0),
(1072, 6, 'test', 'Room No: 401', 0, 0),
(1073, 5, 'test', 'Room No: 432', 0, 0),
(1074, 6, 'test', 'Room No: 402', 0, 0),
(1075, 5, 'test', 'Room No: 433', 0, 0),
(1076, 6, 'test', 'Room No: 403', 0, 0),
(1077, 5, 'test', 'Room No: 434', 0, 0),
(1078, 6, 'test', 'Room No: 404', 0, 0),
(1079, 5, 'test', 'Room No: 435', 0, 0),
(1080, 6, 'test', 'Room No: 405', 0, 0),
(1081, 5, 'test', 'Room No: 436', 0, 0),
(1082, 6, 'test', 'Room No: 406', 0, 0),
(1083, 5, 'test', 'Room No: 437', 0, 0),
(1084, 6, 'test', 'Room No: 407', 0, 0),
(1085, 5, 'test', 'Room No: 438', 0, 0),
(1086, 6, 'test', 'Room No: 408', 0, 0),
(1087, 5, 'test', 'Room No: 439', 0, 0),
(1088, 6, 'test', 'Room No: 409', 0, 0),
(1089, 5, 'test', 'Room No: 440', 0, 0),
(1090, 6, 'test', 'Room No: 410', 0, 0),
(1091, 5, 'test', 'Room No: 441', 0, 0),
(1092, 6, 'test', 'Room No: 411', 0, 0),
(1093, 5, 'test', 'Room No: 442', 0, 0),
(1094, 6, 'test', 'Room No: 412', 0, 0),
(1095, 5, 'test', 'Room No: 443', 0, 0),
(1096, 6, 'test', 'Room No: 413', 0, 0),
(1097, 5, 'test', 'Room No: 444', 0, 0),
(1098, 6, 'test', 'Room No: 414', 0, 0),
(1099, 5, 'test', 'Room No: 445', 0, 0),
(1100, 6, 'test', 'Room No: 415', 0, 0),
(1101, 5, 'test', 'Room No: 446', 0, 0),
(1102, 6, 'test', 'Room No: 416', 0, 0),
(1103, 5, 'test', 'Room No: 447', 0, 0),
(1104, 6, 'test', 'Room No: 417', 0, 0),
(1105, 5, 'test', 'Room No: 448', 0, 0),
(1106, 6, 'test', 'Room No: 418', 0, 0),
(1107, 5, 'test', 'Room No: 449', 0, 0),
(1108, 6, 'test', 'Room No: 419', 0, 0),
(1109, 5, 'test', 'Room No: 450', 0, 0),
(1110, 6, 'test', 'Room No: 420', 0, 0),
(1111, 5, 'test', 'Room No: 451', 0, 0),
(1112, 6, 'test', 'Room No: 421', 0, 0),
(1113, 5, 'test', 'Room No: 452', 0, 0),
(1114, 6, 'test', 'Room No: 422', 0, 0),
(1115, 5, 'test', 'Room No: 453', 0, 0),
(1116, 6, 'test', 'Room No: 423', 0, 0),
(1117, 5, 'test', 'Room No: 454', 0, 0),
(1118, 6, 'test', 'Room No: 424', 0, 0),
(1119, 5, 'test', 'Room No: 455', 0, 0),
(1120, 6, 'test', 'Room No: 425', 0, 0),
(1121, 5, 'test', 'Room No: 456', 0, 0),
(1122, 6, 'test', 'Room No: 426', 0, 0),
(1123, 5, 'test', 'Room No: 457', 0, 0),
(1124, 6, 'test', 'Room No: 427', 0, 0),
(1125, 5, 'test', 'Room No: 458', 0, 0),
(1126, 6, 'test', 'Room No: 428', 0, 0),
(1127, 5, 'test', 'Room No: 459', 0, 0),
(1128, 6, 'test', 'Room No: 429', 0, 0),
(1129, 5, 'test', 'Room No: 460', 0, 0),
(1130, 6, 'test', 'Room No: 430', 0, 0),
(1131, 5, 'test', 'Room No: 461', 0, 0),
(1132, 6, 'test', 'Room No: 431', 0, 0),
(1133, 5, 'test', 'Room No: 462', 0, 0),
(1134, 6, 'test', 'Room No: 432', 0, 0),
(1135, 5, 'test', 'Room No: 463', 0, 0),
(1136, 6, 'test', 'Room No: 433', 0, 0),
(1137, 5, 'test', 'Room No: 464', 0, 0),
(1138, 6, 'test', 'Room No: 434', 0, 0),
(1139, 5, 'test', 'Room No: 465', 0, 0),
(1140, 6, 'test', 'Room No: 435', 0, 0),
(1141, 5, 'test', 'Room No: 466', 0, 0),
(1142, 6, 'test', 'Room No: 436', 0, 0),
(1143, 5, 'test', 'Room No: 467', 0, 0),
(1144, 6, 'test', 'Room No: 437', 0, 0),
(1145, 5, 'test', 'Room No: 468', 0, 0),
(1146, 6, 'test', 'Room No: 438', 0, 0),
(1147, 5, 'test', 'Room No: 469', 0, 0),
(1148, 5, 'test', 'Room No: 470', 0, 0),
(1149, 6, 'test', 'Room No: 439', 0, 0),
(1150, 5, 'test', 'Room No: 471', 0, 0),
(1151, 6, 'test', 'Room No: 440', 0, 0),
(1152, 5, 'test', 'Room No: 472', 0, 0),
(1153, 6, 'test', 'Room No: 441', 0, 0),
(1154, 5, 'test', 'Room No: 473', 0, 0),
(1155, 6, 'test', 'Room No: 442', 0, 0),
(1156, 5, 'test', 'Room No: 474', 0, 0),
(1157, 6, 'test', 'Room No: 443', 0, 0),
(1158, 5, 'test', 'Room No: 475', 0, 0),
(1159, 6, 'test', 'Room No: 444', 0, 0),
(1160, 5, 'test', 'Room No: 476', 0, 0),
(1161, 6, 'test', 'Room No: 445', 0, 0),
(1162, 5, 'test', 'Room No: 477', 0, 0),
(1163, 6, 'test', 'Room No: 446', 0, 0),
(1164, 5, 'test', 'Room No: 478', 0, 0),
(1165, 6, 'test', 'Room No: 447', 0, 0),
(1166, 5, 'test', 'Room No: 479', 0, 0),
(1167, 6, 'test', 'Room No: 448', 0, 0),
(1168, 5, 'test', 'Room No: 480', 0, 0),
(1169, 6, 'test', 'Room No: 449', 0, 0),
(1170, 5, 'test', 'Room No: 481', 0, 0),
(1171, 6, 'test', 'Room No: 450', 0, 0),
(1172, 5, 'test', 'Room No: 482', 0, 0),
(1173, 6, 'test', 'Room No: 451', 0, 0),
(1174, 5, 'test', 'Room No: 483', 0, 0),
(1175, 6, 'test', 'Room No: 452', 0, 0),
(1176, 5, 'test', 'Room No: 484', 0, 0),
(1177, 6, 'test', 'Room No: 453', 0, 0),
(1178, 5, 'test', 'Room No: 485', 0, 0),
(1179, 6, 'test', 'Room No: 454', 0, 0),
(1180, 5, 'test', 'Room No: 486', 0, 0),
(1181, 6, 'test', 'Room No: 455', 0, 0),
(1182, 5, 'test', 'Room No: 487', 0, 0),
(1183, 6, 'test', 'Room No: 456', 0, 0),
(1184, 5, 'test', 'Room No: 488', 0, 0),
(1185, 6, 'test', 'Room No: 457', 0, 0),
(1186, 5, 'test', 'Room No: 489', 0, 0),
(1187, 6, 'test', 'Room No: 458', 0, 0),
(1188, 5, 'test', 'Room No: 490', 0, 0),
(1189, 6, 'test', 'Room No: 459', 0, 0),
(1190, 5, 'test', 'Room No: 491', 0, 0),
(1191, 6, 'test', 'Room No: 460', 0, 0),
(1192, 5, 'test', 'Room No: 492', 0, 0),
(1193, 5, 'test', 'Room No: 493', 0, 0),
(1194, 6, 'test', 'Room No: 461', 0, 0),
(1195, 5, 'test', 'Room No: 494', 0, 0),
(1196, 6, 'test', 'Room No: 462', 0, 0),
(1197, 5, 'test', 'Room No: 495', 0, 0),
(1198, 6, 'test', 'Room No: 463', 0, 0),
(1199, 5, 'test', 'Room No: 496', 0, 0),
(1200, 6, 'test', 'Room No: 464', 0, 0),
(1201, 5, 'test', 'Room No: 497', 0, 0),
(1202, 6, 'test', 'Room No: 465', 0, 0),
(1203, 5, 'test', 'Room No: 498', 0, 0),
(1204, 6, 'test', 'Room No: 466', 0, 0),
(1205, 5, 'test', 'Room No: 499', 0, 0),
(1206, 6, 'test', 'Room No: 467', 0, 0),
(1207, 5, 'test', 'Room No: 500', 0, 0),
(1208, 6, 'test', 'Room No: 468', 0, 0),
(1209, 6, 'test', 'Room No: 469', 0, 0),
(1210, 6, 'test', 'Room No: 470', 0, 0),
(1211, 6, 'test', 'Room No: 471', 0, 0),
(1212, 6, 'test', 'Room No: 472', 0, 0),
(1213, 6, 'test', 'Room No: 473', 0, 0),
(1214, 6, 'test', 'Room No: 474', 0, 0),
(1215, 6, 'test', 'Room No: 475', 0, 0),
(1216, 6, 'test', 'Room No: 476', 0, 0),
(1217, 6, 'test', 'Room No: 477', 0, 0),
(1218, 6, 'test', 'Room No: 478', 0, 0),
(1219, 6, 'test', 'Room No: 479', 0, 0),
(1220, 6, 'test', 'Room No: 480', 0, 0),
(1221, 6, 'test', 'Room No: 481', 0, 0),
(1222, 6, 'test', 'Room No: 482', 0, 0),
(1223, 6, 'test', 'Room No: 483', 0, 0),
(1224, 6, 'test', 'Room No: 484', 0, 0),
(1225, 6, 'test', 'Room No: 485', 0, 0),
(1226, 6, 'test', 'Room No: 486', 0, 0),
(1227, 6, 'test', 'Room No: 487', 0, 0),
(1228, 6, 'test', 'Room No: 488', 0, 0),
(1229, 6, 'test', 'Room No: 489', 0, 0),
(1230, 6, 'test', 'Room No: 490', 0, 0),
(1231, 6, 'test', 'Room No: 491', 0, 0),
(1232, 6, 'test', 'Room No: 492', 0, 0),
(1233, 6, 'test', 'Room No: 493', 0, 0),
(1234, 6, 'test', 'Room No: 494', 0, 0),
(1235, 6, 'test', 'Room No: 495', 0, 0),
(1236, 6, 'test', 'Room No: 496', 0, 0),
(1237, 6, 'test', 'Room No: 497', 0, 0),
(1238, 6, 'test', 'Room No: 498', 0, 0),
(1239, 6, 'test', 'Room No: 499', 0, 0),
(1240, 6, 'test', 'Room No: 500', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `employe`
--

CREATE TABLE `employe` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `father_name` varchar(150) NOT NULL,
  `mother_name` varchar(150) NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `present_address` varchar(150) NOT NULL,
  `permanent_address` varchar(150) NOT NULL,
  `job_title_post` varchar(100) NOT NULL,
  `working_hour` varchar(20) NOT NULL,
  `salary_amount` varchar(100) NOT NULL,
  `educational_qualifation_1` varchar(300) NOT NULL,
  `educational_qualifation_2` varchar(300) NOT NULL,
  `educational_qualifation_3` varchar(300) NOT NULL,
  `educational_qualifation_4` varchar(300) NOT NULL,
  `educational_qualifation_5` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `enquiry_tbl`
--

CREATE TABLE `enquiry_tbl` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `class_id` varchar(10) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `city` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `enquiry_tbl`
--

INSERT INTO `enquiry_tbl` (`id`, `name`, `class_id`, `email`, `mobile`, `city`, `created_at`) VALUES
(1, 'test', '2', '2', '2', '2', '2022-07-08 08:53:19'),
(2, 'xyz', '3', 'xxx@gmail.com', '556677889', 'pune', '2022-07-08 08:55:32'),
(3, 'SwatiB', '5', 'sss@gmail.com', '5444545', 'pune', '2022-07-08 08:56:17'),
(4, 'anjali', '7', 'anju@gmail.com', '4456456', 'pune', '2022-07-08 08:57:42'),
(5, 'swati', '7', 'swati.belure@blucorsys.com', '7878878888', 'parli', '2022-07-08 08:58:55'),
(6, 'sss', '5', 'sss@gmail.com', '345435344', 'mumbai', '2022-07-08 09:04:06'),
(7, 'test', '2', 'xxx@gmail.com', '08877667788', 'pune', '2022-07-08 09:04:40'),
(8, 'test', '4', 'xxx@gmail.com', '08877667788', 'pune', '2022-07-08 09:05:03');

-- --------------------------------------------------------

--
-- Table structure for table `exam_attendanc`
--

CREATE TABLE `exam_attendanc` (
  `id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `student_title` varchar(100) NOT NULL,
  `class_id` int(11) NOT NULL,
  `roll_no` varchar(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `exam_title` varchar(150) NOT NULL,
  `exam_subject` varchar(100) NOT NULL,
  `attendance` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_attendanc`
--

INSERT INTO `exam_attendanc` (`id`, `date`, `user_id`, `student_id`, `student_title`, `class_id`, `roll_no`, `section`, `exam_title`, `exam_subject`, `attendance`) VALUES
(1, '10/06/2016', 4, '201601001', 'Benjamin D. Lampe', 1, '1', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'AMAR BANGLA BOI', 'P'),
(2, '10/06/2016', 12, '201601002', 'Rahim Hasan', 1, '2', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'AMAR BANGLA BOI', 'P'),
(3, '10/06/2016', 13, '201601003', 'Junayed Hak', 1, '3', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'AMAR BANGLA BOI', 'P'),
(4, '10/06/2016', 16, '201601004', 'Razia Akture', 1, '4', '', 'Test Exam 1', 'AMAR BANGLA BOI', 'P'),
(5, '10/06/2016', 23, '201601005', 'Polash Sarder', 1, '5', '', 'Test Exam 1', 'AMAR BANGLA BOI', 'P'),
(6, '10/06/2016', 24, '201601006', 'Sumon Akon', 1, '6', '', 'Test Exam 1', 'AMAR BANGLA BOI', 'P'),
(7, '10/06/2016', 4, '201601001', 'Benjamin D. Lampe', 1, '1', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'ENGLISH FOR TODAY', 'P'),
(8, '10/06/2016', 12, '201601002', 'Rahim Hasan', 1, '2', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'ENGLISH FOR TODAY', 'P'),
(9, '10/06/2016', 13, '201601003', 'Junayed Hak', 1, '3', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'ENGLISH FOR TODAY', 'P'),
(10, '10/06/2016', 16, '201601004', 'Razia Akture', 1, '4', '', 'Test Exam 1', 'ENGLISH FOR TODAY', 'P'),
(11, '10/06/2016', 23, '201601005', 'Polash Sarder', 1, '5', '', 'Test Exam 1', 'ENGLISH FOR TODAY', 'P'),
(12, '10/06/2016', 24, '201601006', 'Sumon Akon', 1, '6', '', 'Test Exam 1', 'ENGLISH FOR TODAY', 'P'),
(13, '10/06/2016', 4, '201601001', 'Benjamin D. Lampe', 1, '1', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'PRIMARY MATHEMATICS', 'P'),
(14, '10/06/2016', 12, '201601002', 'Rahim Hasan', 1, '2', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'PRIMARY MATHEMATICS', 'P'),
(15, '10/06/2016', 13, '201601003', 'Junayed Hak', 1, '3', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'PRIMARY MATHEMATICS', 'P'),
(16, '10/06/2016', 16, '201601004', 'Razia Akture', 1, '4', '', 'Test Exam 1', 'PRIMARY MATHEMATICS', 'P'),
(17, '10/06/2016', 23, '201601005', 'Polash Sarder', 1, '5', '', 'Test Exam 1', 'PRIMARY MATHEMATICS', 'P'),
(18, '10/06/2016', 24, '201601006', 'Sumon Akon', 1, '6', '', 'Test Exam 1', 'PRIMARY MATHEMATICS', 'P'),
(19, '08/10/2016', 25, '201603001', 'Farjana Akter', 3, '1', '', 'FInal Exam', 'AMAR BANGLA BOI', 'P'),
(20, '08/10/2016', 26, '201603002', 'Akram Hossain', 3, '2', '', 'FInal Exam', 'AMAR BANGLA BOI', 'P'),
(21, '08/10/2016', 27, '201603003', 'Alamin Saeder', 3, '3', '', 'FInal Exam', 'AMAR BANGLA BOI', 'P'),
(22, '08/10/2016', 28, '201603004', 'Sabina Sumi', 3, '4', '', 'FInal Exam', 'AMAR BANGLA BOI', 'P');

-- --------------------------------------------------------

--
-- Table structure for table `exam_grade`
--

CREATE TABLE `exam_grade` (
  `id` int(11) NOT NULL,
  `grade_name` varchar(30) NOT NULL,
  `point` varchar(4) NOT NULL,
  `number_form` varchar(5) NOT NULL,
  `number_to` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_grade`
--

INSERT INTO `exam_grade` (`id`, `grade_name`, `point`, `number_form`, `number_to`) VALUES
(1, 'F', '0', '0', '32'),
(2, 'D', '1', '33', '39'),
(3, 'C', '2', '40', '49'),
(4, 'B', '3', '50', '59'),
(5, 'A-', '3.5', '60', '69'),
(6, 'A', '4', '70', '79'),
(7, 'A+', '5', '80', '100');

-- --------------------------------------------------------

--
-- Table structure for table `exam_routine`
--

CREATE TABLE `exam_routine` (
  `id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_date` varchar(30) NOT NULL,
  `exam_subject` varchar(100) NOT NULL,
  `subject_code` varchar(15) NOT NULL,
  `rome_number` varchar(10) NOT NULL,
  `start_time` varchar(10) NOT NULL,
  `end_time` varchar(30) NOT NULL,
  `exam_shift` varchar(50) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_routine`
--

INSERT INTO `exam_routine` (`id`, `exam_id`, `exam_date`, `exam_subject`, `subject_code`, `rome_number`, `start_time`, `end_time`, `exam_shift`, `status`) VALUES
(1, 4, '9/06/2018', 'AMAR BANGLA BOI', '101', '101', '09.00am', '10.30am', 'Morning shift', 'Result'),
(2, 4, '10/06/2018', 'ENGLISH FOR TODAY', '102', '101', '09.00am', '10.30am', 'Morning shift', 'Result'),
(3, 4, '11/06/2018', 'PRIMARY MATHEMATICS', '103', '101', '09.00am', '10.30am', 'Morning shift', 'Result'),
(4, 5, '08/10/2018', 'AMAR BANGLA BOI', '101', '101', '10.30am', '11.00am', 'Morning shift', 'Result');

-- --------------------------------------------------------

--
-- Table structure for table `fees_type_master`
--

CREATE TABLE `fees_type_master` (
  `id` int(11) NOT NULL,
  `fee_plan_name` varchar(200) NOT NULL,
  `fee_category` varchar(100) NOT NULL,
  `tax_applicable` varchar(100) NOT NULL DEFAULT 'no',
  `amount` int(200) NOT NULL,
  `is_lumpsum` varchar(100) NOT NULL,
  `is_tax` varchar(100) NOT NULL,
  `final_amount` int(100) NOT NULL,
  `downpayment` int(100) NOT NULL,
  `feetax_calculated_amt` int(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fees_type_master`
--

INSERT INTO `fees_type_master` (`id`, `fee_plan_name`, `fee_category`, `tax_applicable`, `amount`, `is_lumpsum`, `is_tax`, `final_amount`, `downpayment`, `feetax_calculated_amt`, `created_at`) VALUES
(1, 'registration for 2022-2023', 'Admission Fees', 'no', 30000, '', '8', 10000, 0, 0, '2022-03-04 13:58:25'),
(2, 'registration for 2022-2023', 'Registration Fees', 'no', 25000, 'Yes', '0', 12500, 0, 0, '2022-03-04 13:58:28'),
(3, 'registration for 2022-2023', 'Admission Fees', 'no', 50000, 'No', '8', 16666, 0, 0, '2022-03-04 13:58:47'),
(4, '', 'test', 'no', 5000, '', '1', 3900, 2000, 5900, '2022-03-04 13:58:32'),
(5, '', 'test', 'no', 5000, '', '1', 3900, 2000, 5900, '2022-03-04 13:58:35'),
(6, '', 'test', 'no', 5000, '', '1', 3900, 2000, 5900, '2022-03-04 13:58:38'),
(7, 'registration for 2022-2023', 'Admission Fees', 'no', 4000, 'No', '1', 2000, 0, 0, '2022-03-04 13:58:40'),
(8, '', 'Registration Fee', 'no', 40000, '', '1', 42200, 5000, 47200, '2022-03-04 13:58:43'),
(9, '', 'Registration Fee', 'no', 40000, '', '1', 42200, 5000, 47200, '2022-03-04 13:58:52'),
(10, '', 'Registration Fee', 'no', 40000, '', '1', 42200, 5000, 47200, '2022-03-04 13:58:55'),
(11, '', 'Admisiion fee', 'no', 20000, '', '1', 13600, 10000, 23600, '2022-03-04 13:58:58'),
(12, '', 'Registration Fee', 'no', 15000, '', '1', 12700, 5000, 17700, '2022-03-04 13:59:00'),
(13, '', 'Registration Fee', 'no', 45000, '', '1', 43100, 10000, 53100, '2022-03-04 13:59:03'),
(45, '', 'Admisiion fee', 'yesTaxable', 11000, '', '1', 7980, 5000, 12980, '2022-03-10 07:32:54'),
(46, '', 'sss', 'no', 40000, 'yesLumpsum', '', 0, 0, 0, '2022-03-10 09:22:38'),
(47, '', 'test', 'yesTaxable', 23000, '', '1', 26140, 1000, 27140, '2022-03-10 09:44:32'),
(48, '', 'Admisiion fee', 'yesTaxable', 16000, '', '1', 13880, 5000, 18880, '2022-03-10 09:51:29'),
(49, '', 'test', 'no', 23000, 'yesLumpsum', '', 0, 0, 0, '2022-03-14 08:38:34'),
(50, '', 'Registration Fee', 'yesTaxable', 6000, '', '1', 2080, 5000, 7080, '2022-03-14 09:06:55'),
(51, '', 'test', 'no', 8000, 'yesLumpsum', '', 0, 0, 0, '2022-03-14 10:28:03'),
(52, '', 'test', 'no', 4000, 'yesLumpsum', '', 0, 0, 0, '2022-03-14 11:37:24'),
(53, '', 'test', 'no', 4000, 'yesLumpsum', '', 0, 0, 0, '2022-03-14 11:38:36'),
(54, '', 'Exam fees', 'yes', 0, '', '', 0, 0, 0, '2022-03-15 06:04:06'),
(55, '', 'Exam fees', 'yes', 0, '', '', 0, 0, 0, '2022-03-15 06:06:15'),
(56, '', 'Admission Fee', 'no', 12000, 'yesLumpsum', '', 0, 0, 0, '2022-03-15 10:26:00'),
(57, 'test', 'Registration Fee', 'no', 34000, '', '1', 35120, 5000, 40120, '2022-03-15 17:15:24'),
(58, 'test', 'Registration Fee', 'no', 34000, '', '1', 35120, 5000, 40120, '2022-03-15 17:18:44'),
(59, 'rr', 'Select...', 'no', 15000, '', '1', 14700, 3000, 17700, '2022-03-15 17:28:48'),
(60, 'rr', 'Select...', 'no', 15000, '', '1', 14700, 3000, 17700, '2022-03-16 06:11:06'),
(61, 'ss', 'Admission Fee', 'no', 55000, '', '1', 59900, 5000, 64900, '2022-03-16 08:13:24'),
(62, '', 'Registration Fee', 'no', 23000, 'yesLumpsum', '1', 18101, 11800, 27140, '2022-03-17 12:21:47'),
(63, 'registration 2022-2023', 'Registration Fee', 'no', 22000, '', '1', 0, 0, 25960, '2022-03-17 12:24:06'),
(64, 'registration 2022-2023', 'Registration Fee', 'no', 22000, 'yesLumpsum', '1', 23670, 5900, 25960, '2022-03-17 12:24:36'),
(65, 'Registration of 2022-2023', 'Admission Fee', 'no', 21000, 'yesLumpsum', '1', 22278, 5900, 24780, '2022-03-17 12:26:44'),
(66, '', 'ttt444', 'yes', 0, '', '', 0, 0, 0, '2022-06-01 08:06:59'),
(67, 'aaa for 2022', 'Registration Fee', 'no', 55000, '', '8', 62720, 5600, 61600, '2022-06-01 09:50:25');

-- --------------------------------------------------------

--
-- Table structure for table `fee_item`
--

CREATE TABLE `fee_item` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_item`
--

INSERT INTO `fee_item` (`id`, `year`, `class_id`, `title`, `amount`) VALUES
(1, 2018, 1, 'test', 100),
(2, 2018, 2, 'aaa', 500),
(3, 2018, 1, 'aaa', 100),
(4, 2018, 3, 'Exam Fee', 300);

-- --------------------------------------------------------

--
-- Table structure for table `fee_payment_record`
--

CREATE TABLE `fee_payment_record` (
  `id` int(11) NOT NULL,
  `school_name` varchar(100) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `receipt_no` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `student_id` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `email_id` varchar(250) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `payment_mode` varchar(100) NOT NULL,
  `bank_account` varchar(200) NOT NULL,
  `transaction_no` varchar(160) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fee_payment_record`
--

INSERT INTO `fee_payment_record` (`id`, `school_name`, `amount`, `receipt_no`, `date`, `student_id`, `mobile`, `email_id`, `user_id`, `payment_mode`, `bank_account`, `transaction_no`, `created_at`) VALUES
(1, '', '30000', '123444', '2022-05-25', '1', '8600118800', 'swati@gmail.com', '', 'indian_rupee', 'sss22223333', 'ffff455555', '2022-06-08 10:09:20'),
(2, '', '40000', 'ddd88877', '2022-05-28', '3', '7878788', 'swatibelure88@gmail.com', '', 'indian_rupee', 'dddd99999', 'ererere5655', '2022-06-08 10:09:29'),
(9, '', '0', '123456', '2022-09-08', '1', '8600118800', 'swati@gmail.com', '', 'cheque', '256663322', '123456', '2022-09-08 07:17:58'),
(10, 'Sample School', '0', '123456', '2022-09-13', '2', 'dsf', 'xxx@gmail.com', '', 'cash_pay', '123456test', '0', '2022-09-13 09:17:35'),
(12, 'Sample School', '0', '564', '2022-09-16', '26', '+917766889956', 'avni@gmail.com', '72', 'cash_pay', '66655', '0', '2022-09-16 13:10:40');

-- --------------------------------------------------------

--
-- Table structure for table `fee_scheme`
--

CREATE TABLE `fee_scheme` (
  `id` int(11) NOT NULL,
  `school_name` varchar(100) NOT NULL,
  `class_title` varchar(300) NOT NULL,
  `fee_name` varchar(100) NOT NULL,
  `is_installment` varchar(100) NOT NULL,
  `no_of_installment` varchar(50) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fee_scheme`
--

INSERT INTO `fee_scheme` (`id`, `school_name`, `class_title`, `fee_name`, `is_installment`, `no_of_installment`, `amount`, `created_at`, `updated_at`) VALUES
(1, '50', 'grade1, grade2', 'Admission Fee', '', '', '', '2022-11-09 11:01:38', '2022-11-10 14:06:37'),
(2, '241', 'grade1, grade3', 'Registration Fee', '', '', '', '2022-11-09 11:02:20', '2022-11-10 14:06:37'),
(5, '50', 'grade1, grade2', 'Admission Fee', 'yesFeeYearly', '2', '23000', '2022-11-22 12:50:22', '2022-11-22 12:50:22'),
(6, '50', 'grade1, grade2', 'Registration Fee', 'yesFeeYearly', '2', '20000', '2022-11-22 12:58:52', '2022-11-22 12:58:52'),
(7, '50', 'grade1, grade2', 'Transportation Fee', 'yesFeeYearly', '2', '20000', '2022-11-22 13:02:19', '2022-11-22 13:02:19'),
(8, '50', 'test class,test2,test3', 'Admission Fee', 'yesFeeYearly', '2', '20000', '2022-11-23 09:45:58', '2022-11-23 09:45:58'),
(9, '50', 'test class,test2,test3', 'Admission Fee', 'yesFeeYearly', '2', '20000', '2022-11-23 09:46:16', '2022-11-23 09:46:16'),
(10, '50', 'test class,test2,test3', 'Admission Fee', 'yesFeeYearly', '2', '20000', '2022-11-23 09:47:26', '2022-11-23 09:47:26'),
(11, '50', 'test class,test2,test3', 'Admission Fee', 'yesFeeYearly', '2', '20000', '2022-11-23 09:48:53', '2022-11-23 09:48:53'),
(12, '50', 'test class,test2,test3', 'Admission Fee', 'yesFeeYearly', '2', '20000', '2022-11-23 09:49:08', '2022-11-23 09:49:08'),
(13, '50', 'class3,class4', 'Admission Fee', 'yesFeeYearly', '2', '25000', '2022-11-24 09:13:57', '2022-11-24 09:13:57'),
(14, '50', 'grade1, grade2', 'Admission Fee', 'yesFeeYearly', '2', '6000', '2022-11-24 09:16:44', '2022-11-24 09:16:44'),
(15, '50', 'grade1, grade2', 'Admission Fee', 'yesFeeYearly', '2', '6000', '2022-11-24 09:18:03', '2022-11-24 09:18:03'),
(16, '50', 'grade1, grade2', 'Admission Fee', 'yesFeeYearly', '2', '20000', '2022-11-24 10:04:21', '2022-11-24 10:04:21'),
(17, '50', 'grade1, grade2', 'Admission Fee', 'yesFeeYearly', '2', '20000', '2022-11-24 10:04:32', '2022-11-24 10:04:32'),
(18, '261', 'grade1, grade2', 'Registration Fee', 'yesFeeYearly', '2', '5000', '2022-11-24 10:06:02', '2022-11-24 10:06:02'),
(19, '261', 'grade1, grade2', 'Registration Fee', 'yesFeeYearly', '2', '5000', '2022-11-24 10:06:20', '2022-11-24 10:06:20'),
(20, '261', 'grade1, grade2', 'Registration Fee', 'yesFeeYearly', '2', '5000', '2022-11-24 10:07:37', '2022-11-24 10:07:37');

-- --------------------------------------------------------

--
-- Table structure for table `fee_scheme_installment`
--

CREATE TABLE `fee_scheme_installment` (
  `id` int(11) NOT NULL,
  `no_of_installment` varchar(100) NOT NULL,
  `fee_scheme_id` varchar(100) NOT NULL,
  `installment_start_dt` varchar(100) NOT NULL,
  `installment_end_dt` varchar(100) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fee_scheme_installment`
--

INSERT INTO `fee_scheme_installment` (`id`, `no_of_installment`, `fee_scheme_id`, `installment_start_dt`, `installment_end_dt`, `create_at`) VALUES
(1, '2', '20', '2022-11-25', '2022-11-27', '2022-11-24 10:07:37'),
(2, '2', '20', '2022-11-28', '2022-11-30', '2022-11-24 10:07:37');

-- --------------------------------------------------------

--
-- Table structure for table `final_result`
--

CREATE TABLE `final_result` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `total_mark` varchar(100) NOT NULL,
  `final_grade` varchar(10) NOT NULL,
  `maride_list` varchar(150) NOT NULL,
  `status` varchar(150) NOT NULL,
  `point` varchar(11) NOT NULL,
  `fail_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `final_result`
--

INSERT INTO `final_result` (`id`, `class_id`, `section`, `exam_id`, `exam_title`, `student_id`, `student_name`, `total_mark`, `final_grade`, `maride_list`, `status`, `point`, `fail_amount`) VALUES
(1, 1, '', 4, 'Test Exam 1', '201601001', 'Benjamin D. Lampe', '249', 'A', '', 'Pass', '4.67', 0),
(2, 1, '', 4, 'Test Exam 1', '201601002', 'Rahim Hasan', '241', 'A', '', 'Pass', '4.33', 0),
(3, 1, '', 4, 'Test Exam 1', '201601003', 'Junayed Hak', '254', 'A', '', 'Pass', '4.67', 0),
(4, 1, '', 4, 'Test Exam 1', '201601004', 'Razia Akture', '239', 'A', '', 'Pass', '4.67', 0),
(5, 1, '', 4, 'Test Exam 1', '201601005', 'Polash Sarder', '230', 'A', '', 'Pass', '4.5', 0),
(6, 1, '', 4, 'Test Exam 1', '201601006', 'Sumon Akon', '234', 'A', '', 'Pass', '4.67', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(3, 'student', 'This user is student\'s groups member.'),
(4, 'teacher', 'This user is teacher\'s groups member.'),
(5, 'parents', 'This user is parent\'s groups member.'),
(6, 'accountant', 'This user is accountent\'s groups member.'),
(7, 'library_man', 'The library man can manage library and library\'s account information'),
(8, '4th_class_employ', ''),
(9, 'driver', '');

-- --------------------------------------------------------

--
-- Table structure for table `installment_master`
--

CREATE TABLE `installment_master` (
  `id` int(11) NOT NULL,
  `installment` varchar(100) NOT NULL,
  `amount` varchar(200) NOT NULL,
  `installment_date` date NOT NULL,
  `tax_id` int(100) NOT NULL,
  `fee_plan_name_id` int(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `installment_master`
--

INSERT INTO `installment_master` (`id`, `installment`, `amount`, `installment_date`, `tax_id`, `fee_plan_name_id`, `created_at`) VALUES
(2, 'yes', '14066.666666666666', '2022-03-10', 1, 0, '2022-03-10 07:01:31'),
(3, 'yes', '21100', '2022-03-10', 1, 0, '2022-03-10 07:17:33'),
(4, 'yes', '18606.666666666668', '2022-03-10', 1, 0, '2022-03-10 07:23:08'),
(5, 'yes', '18606.666666666668', '2022-04-10', 1, 0, '2022-03-10 07:23:08'),
(6, 'yes', '18606.666666666668', '2022-05-10', 1, 0, '2022-03-10 07:23:08'),
(7, 'yes', '7600', '2022-03-10', 1, 0, '2022-03-10 07:25:58'),
(8, 'yes', '7600', '2022-04-10', 1, 0, '2022-03-10 07:25:58'),
(9, 'yes', '7600', '2022-05-10', 1, 0, '2022-03-10 07:25:58'),
(10, 'yes', '7600', '2022-06-10', 1, 0, '2022-03-10 07:25:58'),
(11, 'yes', '2660', '2022-03-10', 1, 0, '2022-03-10 07:32:54'),
(12, 'yes', '2660', '2022-04-11', 1, 0, '2022-03-10 07:32:54'),
(13, 'yes', '2660', '2022-05-11', 1, 0, '2022-03-10 07:32:54'),
(14, 'yes', '3470', '2022-03-10', 1, 48, '2022-03-10 09:51:29'),
(15, 'yes', '3470', '2022-04-10', 1, 48, '2022-03-10 09:51:29'),
(16, 'yes', '3470', '2022-05-10', 1, 48, '2022-03-10 09:51:29'),
(17, 'yes', '3470', '2022-06-10', 1, 48, '2022-03-10 09:51:29'),
(18, 'yes', '2080', '2022-03-14', 1, 50, '2022-03-14 09:06:55'),
(19, 'yes', '17560', '2022-03-15', 1, 57, '2022-03-15 17:15:24'),
(20, 'yes', '17560', '0000-00-00', 1, 57, '2022-03-15 17:15:24'),
(21, 'yes', '17560', '2022-03-15', 1, 58, '2022-03-15 17:18:44'),
(22, 'yes', '17560', '0000-00-00', 1, 58, '2022-03-15 17:18:44'),
(23, 'yes', '4900', '2022-03-15', 1, 59, '2022-03-15 17:28:48'),
(24, 'yes', '4900', '2022-04-30', 1, 59, '2022-03-15 17:28:48'),
(25, 'yes', '4900', '2022-06-30', 1, 59, '2022-03-15 17:28:48'),
(26, 'yes', '4900', '2022-03-16', 1, 60, '2022-03-16 06:11:06'),
(27, 'yes', '4900', '2022-04-30', 1, 60, '2022-03-16 06:11:06'),
(28, 'yes', '4900', '2022-06-30', 1, 60, '2022-03-16 06:11:06'),
(29, 'yes', '30000', '2022-03-16', 1, 61, '2022-03-16 08:13:24'),
(30, 'yes', '29900', '2022-05-16', 1, 61, '2022-03-16 08:13:24'),
(31, 'yes', '31360', '2022-06-01', 8, 67, '2022-06-01 09:50:25'),
(32, 'yes', '31360', '2022-07-15', 8, 67, '2022-06-01 09:50:25');

-- --------------------------------------------------------

--
-- Table structure for table `inven_category`
--

CREATE TABLE `inven_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `details` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inven_category`
--

INSERT INTO `inven_category` (`id`, `category_name`, `details`) VALUES
(1, 'Furniture', ''),
(2, 'Khata', ''),
(3, 'Book', '');

-- --------------------------------------------------------

--
-- Table structure for table `inve_item`
--

CREATE TABLE `inve_item` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `category` varchar(50) NOT NULL,
  `item` varchar(200) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `total_rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inve_item`
--

INSERT INTO `inve_item` (`id`, `vendor_id`, `category`, `item`, `quantity`, `rate`, `discount`, `total_rate`) VALUES
(1, 1, '1', 'Chair', 40, 110, 0, 5500),
(2, 2, '1', 'Table', 20, 750, 0, 15000),
(3, 1, '2', 'Whit Khata', 1000, 15, 0, 15000),
(4, 2, '2', 'Pen', 2000, 5, 0, 9000),
(5, 1, '3', 'Story Book', 50, 220, 0, 11000);

-- --------------------------------------------------------

--
-- Table structure for table `issu_item`
--

CREATE TABLE `issu_item` (
  `id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `user_type` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `issu_item`
--

INSERT INTO `issu_item` (`id`, `date`, `user_type`, `user_id`, `item_id`, `quantity`, `rate`, `total_price`, `status`) VALUES
(1, 1464818400, 'Employee', 2, 1, 10, 110, 1100, 'Due');

-- --------------------------------------------------------

--
-- Table structure for table `lead`
--

CREATE TABLE `lead` (
  `id` int(11) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `lead_source` int(11) NOT NULL,
  `class` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `lead_stage` int(11) NOT NULL,
  `updated_at` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead`
--

INSERT INTO `lead` (`id`, `fname`, `lname`, `mobile`, `email`, `lead_source`, `class`, `location`, `created_at`, `lead_stage`, `updated_at`) VALUES
(1, 'sarvdip', 'pol', '9665341899', 'student@student.com', 2, 1, 0, '2021-08-18 05:44:49', 7, '2021-08-18 11:14:49'),
(2, 'testing', 'aa', '8308608578', 'student@student1.com', 3, 2, 2, '2021-08-18 08:54:53', 1, '2021-08-18 11:14:49'),
(3, 'prasanna', 'jha mm', '8999999999', 'sarv@gmail.com', 1, 3, 3, '2021-08-19 05:35:52', 7, '2022-04-20 13:47:07'),
(4, 'swati', 'belure', '7757881702', 'swatee23@gmail.com', 2, 1, 1, '2022-02-16 11:45:09', 1, '2021-08-18 11:14:49'),
(5, 'swatibbb', 'belure', '9988887799', 'admin@gmail.com', 3, 3, 1, '2022-03-07 09:50:46', 1, '2022-05-12 15:11:39');

-- --------------------------------------------------------

--
-- Table structure for table `lead_activity`
--

CREATE TABLE `lead_activity` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_activity`
--

INSERT INTO `lead_activity` (`id`, `name`) VALUES
(1, 'Lead Created'),
(2, 'Intrested'),
(3, 'call back');

-- --------------------------------------------------------

--
-- Table structure for table `lead_history`
--

CREATE TABLE `lead_history` (
  `id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `lead_activity_id` int(11) NOT NULL,
  `lead_remark` varchar(200) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `followup` varchar(20) NOT NULL,
  `followup_remark` varchar(500) NOT NULL,
  `action_description` varchar(300) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_history`
--

INSERT INTO `lead_history` (`id`, `lead_id`, `lead_activity_id`, `lead_remark`, `stage_id`, `followup`, `followup_remark`, `action_description`, `createdAt`) VALUES
(1, 1, 1, '', 1, '', '', '', '2021-08-18 05:44:49'),
(2, 1, 2, '', 7, '2021-08-20 14:02:00', 'call', '', '2021-08-18 08:32:56'),
(3, 2, 1, '', 1, '', '', '', '2021-08-18 08:54:53'),
(4, 3, 1, '', 1, '', '', '', '2021-08-19 05:35:52'),
(5, 3, 2, 'abc', 7, '2021-08-20 15:09:00', '', '', '2021-08-19 05:40:43'),
(6, 4, 1, '', 1, '', '', '', '2022-02-16 11:45:09'),
(7, 5, 1, '', 1, '', '', '', '2022-03-07 09:50:46'),
(8, 5, 2, 'xyz', 7, '1970-01-01 01:00:00', '', '', '2022-04-14 13:38:36'),
(16, 5, 0, '', 0, '', '', '{\"lead_source\":\"news paper\",\"class\":\"2\",\"location\":\"Pune\"}', '2022-05-10 10:00:15'),
(17, 5, 1, '', 5, '', '', '{\"lead_source\":\"Social Media\",\"class\":\"1\",\"location\":\"USA\"}', '2022-05-10 10:16:59'),
(18, 5, 1, '', 5, '', '', '{\"lead_source\":\"Walk in\",\"class\":\"3\",\"location\":\"Pune\"}', '2022-05-12 13:11:39'),
(19, 5, 2, 'plz call', 1, '2022-09-22 12:29:00', 'make followup', '', '2022-09-17 06:59:38');

-- --------------------------------------------------------

--
-- Table structure for table `lead_location`
--

CREATE TABLE `lead_location` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_location`
--

INSERT INTO `lead_location` (`id`, `name`) VALUES
(1, 'Pune'),
(2, 'Mumbai'),
(3, 'USA');

-- --------------------------------------------------------

--
-- Table structure for table `lead_source`
--

CREATE TABLE `lead_source` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_source`
--

INSERT INTO `lead_source` (`id`, `name`) VALUES
(1, 'news paper'),
(2, 'Social Media'),
(3, 'Walk in'),
(6, 'xyz');

-- --------------------------------------------------------

--
-- Table structure for table `lead_stage`
--

CREATE TABLE `lead_stage` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_stage`
--

INSERT INTO `lead_stage` (`id`, `name`) VALUES
(1, 'enquiry'),
(5, 'Dead'),
(6, 'Warm'),
(7, 'Hot');

-- --------------------------------------------------------

--
-- Table structure for table `leave_application`
--

CREATE TABLE `leave_application` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_title` varchar(150) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `jobtype` text NOT NULL,
  `leave_start` int(11) NOT NULL,
  `leave_end` int(11) NOT NULL,
  `application_date` int(11) NOT NULL,
  `reason` varchar(500) NOT NULL,
  `cheack_by` varchar(150) NOT NULL,
  `status` text NOT NULL,
  `cheack_statuse` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `library_member`
--

CREATE TABLE `library_member` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `fine` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `library_member`
--

INSERT INTO `library_member` (`id`, `user_id`, `title`, `fine`) VALUES
(1, 4, 'Benjamin D. Lampe', 0),
(2, 4, 'Benjamin D. Lampe', 0),
(3, 12, 'Rahim Hasan', 0),
(4, 13, 'Junayed Hak', 0),
(5, 1, 'Headmaster', 0),
(6, 2, 'Helen K Helton', 0),
(7, 6, 'Robert D. Franco', 0),
(8, 7, 'Michael R. Kemp', 0),
(9, 8, 'Willie B. Quint', 0),
(10, 9, 'Fredrick V. Keyes', 0),
(11, 10, 'mumar abboud', 0),
(12, 11, 'Inayah Asfour', 0);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `massage`
--

CREATE TABLE `massage` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` varchar(50) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `read_unread` int(1) NOT NULL,
  `date` int(11) NOT NULL,
  `sender_delete` int(11) NOT NULL,
  `receiver_delete` int(11) NOT NULL,
  `class` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `massage`
--

INSERT INTO `massage` (`id`, `sender_id`, `receiver_id`, `message`, `subject`, `read_unread`, `date`, `sender_delete`, `receiver_delete`, `class`) VALUES
(1, 1, '4', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 1, 1465806801, 1, 1, ''),
(2, 1, '12', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(3, 1, '13', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(4, 1, '16', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(5, 1, '23', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(6, 1, '24', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(7, 1, '4', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 1, 1465807087, 1, 1, ''),
(8, 1, '12', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(9, 1, '13', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(10, 1, '16', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(11, 1, '23', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(12, 1, '24', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `nack_table`
--

CREATE TABLE `nack_table` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `doc_title` varchar(100) NOT NULL,
  `doc_description` varchar(300) NOT NULL,
  `file` varchar(300) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nack_table`
--

INSERT INTO `nack_table` (`id`, `name`, `doc_title`, `doc_description`, `file`, `created_at`) VALUES
(1, 'bb', 'bb', 'sdfasdf', 'college.jpeg', '2022-09-05 08:36:50'),
(2, 'swati', 'pdf document', 'hi hello', '1182603913b31a117f7f3ecca328c6e0.jpeg', '2022-09-05 08:41:43'),
(3, 'urvi', 'test', 'test', '8f022f7a99ffe889e117f5695fae495e.png', '2022-09-05 08:46:08'),
(4, 'bhargav', 'test', 'test', '71b159ec826e06b3875d89a60c6f811c.png', '2022-09-05 08:47:34'),
(5, 'test', 'test', 'test', '97a345612d3080e55c0f862c3f22998d.png', '2022-09-06 13:59:23');

-- --------------------------------------------------------

--
-- Table structure for table `notice_board`
--

CREATE TABLE `notice_board` (
  `id` int(11) NOT NULL,
  `date` varchar(11) NOT NULL,
  `sender` varchar(50) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `notice` varchar(1000) NOT NULL,
  `receiver` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice_board`
--

INSERT INTO `notice_board` (`id`, `date`, `sender`, `subject`, `notice`, `receiver`) VALUES
(1, '13/06/2016', 'Headmaster', 'Test notice for all users. Test notice for all users. Test notice for all users.', '<p><span style=\\\"font-size:14px\\\">This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. </span></p>\\r\\n', 'all');

-- --------------------------------------------------------

--
-- Table structure for table `parents_info`
--

CREATE TABLE `parents_info` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `parents_name` varchar(100) NOT NULL,
  `relation` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parents_info`
--

INSERT INTO `parents_info` (`id`, `user_id`, `class_id`, `student_id`, `parents_name`, `relation`, `email`, `phone`) VALUES
(1, 14, 1, 201601001, 'John E. Williams  Deidra D. Shaw ', 'Parents', 'parents@parents.com', '+8801245852315'),
(2, 15, 1, 201601002, 'Jafor Uddin Julakha Begum', 'Parents', 'Jafor@Jafor.com', '+8801245852315'),
(3, 60, 10, 202110003, 'p3 p3', 'father', 't3f@admin.com', '9999999990'),
(4, 62, 10, 202110004, 'p4 p4', 'mother', 't4f@admin.com', '9999999991'),
(5, 63, 10, 202110014, 'p5 p5', 'father', 't5f@admin.com', '9999999905'),
(6, 64, 10, 0, 't7 t7', 'father', 't7f@admin.com', '9999999907');

-- --------------------------------------------------------

--
-- Table structure for table `registration_history`
--

CREATE TABLE `registration_history` (
  `id` int(11) NOT NULL,
  `registration_id` int(50) NOT NULL,
  `students_name` varchar(100) NOT NULL,
  `action` varchar(50) NOT NULL,
  `by_whom` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registration_history`
--

INSERT INTO `registration_history` (`id`, `registration_id`, `students_name`, `action`, `by_whom`, `description`, `created_at`) VALUES
(1, 1, 'urvi muledddvvv', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-04-29 07:27:05'),
(2, 2, 'testing testing', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-05-04 10:44:38'),
(3, 3, 'swaraj testddd', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-04-29 07:27:12'),
(4, 1, 'urvivvv muledddvvv', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-04-29 07:27:22'),
(5, 1, 'urvi mule', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-04-29 07:27:25'),
(6, 1, 'prapti mule', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-04-29 07:27:28'),
(7, 1, 'Bhumi mule', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-04-29 07:27:32'),
(8, 1, 'Bhumi mule', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-04-29 07:27:35'),
(9, 1, 'Bhumi mule', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-04-29 07:27:38'),
(10, 1, 'Bhumivv mule', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-04-29 07:29:25'),
(11, 3, 'swaraj testddd', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-05-02 08:45:30'),
(12, 2, 'testing testing', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-05-04 08:08:53'),
(13, 2, 'sarvdeep testing', 'Students Info Updated', 'headmaster', 'Students name changed', '2022-05-04 12:20:03'),
(14, 2, 'sarvdeep testing', 'Students Info Updated', 'headmaster', '{\"midname\":\"sarva\"}', '2022-05-05 09:37:08'),
(15, 2, 'sarvdeep testing', 'Students Info Updated', 'headmaster', '{\"midname\":\" testing foraction\",\"nationality\":\"Indian\"}', '2022-05-05 09:39:42'),
(16, 2, 'sarvdeep testing', 'Students Info Updated', 'headmaster', '{\"midname\":\" testing foraction\",\"nationality\":\"Canadian\",\"caste\":\"Open\",\"religion\":\"Hindu\",\"birth_pl', '2022-05-05 11:13:45'),
(17, 2, 'sarvdeep testing', 'Students Info Updated', 'headmaster', '{\"midname\":\" testing foraction\",\"nationality\":\"Canadian\",\"caste\":\"OBC\",\"religion\":\"Christian\",\"birth', '2022-05-05 11:39:42'),
(18, 2, 'sarvdeep testing', 'Students Info Updated', 'headmaster', '{\"midname\":\" testing foraction\",\"nationality\":\"Indian\",\"caste\":\"OBC\",\"religion\":\"Hindu\",\"birth_place', '2022-05-05 11:46:39'),
(19, 3, 'swaraj patil', 'Students Info Updated', 'headmaster', '{\"midname\":\"manoj\",\"nationality\":\"American\",\"caste\":\"Select\",\"religion\":\"Sikh\",\"birth_place\":\"test\",', '2022-05-05 12:10:31'),
(20, 3, 'swaraj patil', 'Students Info Updated', 'headmaster', '{\"midname\":\"mmmm\",\"nationality\":\"American\",\"caste\":\"Select\",\"religion\":\"Sikh\",\"birth_place\":\"test\",\"', '2022-05-05 14:07:49'),
(21, 3, 'swaraj patil', 'Students Info Updated', 'headmaster', '{\"midname\":\"manoj\"}', '2022-05-05 14:17:36'),
(22, 3, 'swaraj testing patil', 'Students Info Updated', 'headmaster', '{\"middle_name\":\"testing manoj\"}', '2022-05-06 06:22:29'),
(23, 3, 'swaraj testing patil', 'Students Info Updated', 'headmaster', '{\"nationality\":\"Canadian\"}', '2022-05-06 07:39:02'),
(24, 3, 'swaraj testing patil', 'Students Info Updated', 'headmaster', '{\"nationality\":\"American\"}', '2022-05-06 07:40:38'),
(25, 26, 'Avni Belure', 'Students Info Updated', 'headmaster', '{\"mothers_firstname\":\"Kavita\",\"original_tc\":\"\"}', '2022-05-17 10:45:57'),
(26, 26, 'Avni Belure', 'Students Info Updated', 'headmaster', '[]', '2022-05-17 10:49:47'),
(27, 26, 'Avni Belure', 'Students Info Updated', 'headmaster', '{\"original_tc\":\"\"}', '2022-05-17 10:55:23'),
(28, 26, 'Avni Belure', 'Students Info Updated', 'headmaster', '{\"original_tc\":\"submited\"}', '2022-05-17 10:56:24'),
(29, 2, 'sarvdeep testing', 'Students Info Updated', 'headmaster', '[]', '2022-07-09 12:46:04'),
(30, 3, 'swaraj testing patil', 'Students Info Updated', 'headmaster', '[]', '2022-07-11 11:45:54');

-- --------------------------------------------------------

--
-- Table structure for table `registration_student`
--

CREATE TABLE `registration_student` (
  `id` int(11) NOT NULL,
  `school_name` varchar(100) NOT NULL,
  `academic_year` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `class_id` varchar(60) NOT NULL,
  `student_id` int(50) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `dob` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `gender` varchar(60) NOT NULL,
  `caste` varchar(60) NOT NULL,
  `religion` varchar(50) NOT NULL,
  `student_photo` varchar(200) NOT NULL,
  `birth_place` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `pin_code` int(10) NOT NULL,
  `fathers_firstname` varchar(50) NOT NULL,
  `fathers_midname` varchar(100) NOT NULL,
  `fathers_lastname` varchar(100) NOT NULL,
  `fathers_mobile` varchar(15) NOT NULL,
  `fathers_occup` varchar(40) NOT NULL,
  `mothers_occup` varchar(50) NOT NULL,
  `mothers_firstname` varchar(60) NOT NULL,
  `mothers_midname` varchar(60) NOT NULL,
  `mothers_lastname` varchar(60) NOT NULL,
  `mothers_mobile` varchar(20) NOT NULL,
  `mothers_qualification` varchar(50) NOT NULL,
  `fathers_qualification` varchar(50) NOT NULL,
  `mother_toung` varchar(100) NOT NULL,
  `anuual_income` varchar(100) NOT NULL,
  `isthr_medical_condition` varchar(40) NOT NULL,
  `isthr_undergoing_treatment` varchar(50) NOT NULL,
  `is_child_taking_medication` varchar(50) NOT NULL,
  `food_allergy` varchar(50) NOT NULL,
  `medical_fitness_cert` varchar(40) NOT NULL DEFAULT 'notsubmitted',
  `adoption_document` varchar(50) NOT NULL DEFAULT 'not_submitted',
  `passport_photos` varchar(50) NOT NULL DEFAULT 'not_submitted',
  `passport_pic_both_parent` varchar(50) NOT NULL DEFAULT 'not_submitted',
  `original_birth_cert` varchar(50) NOT NULL DEFAULT 'not_submitted',
  `pan_card_copy_parent` varchar(50) NOT NULL DEFAULT 'not_submitted',
  `adhar_copy_parent` varchar(50) NOT NULL DEFAULT 'not_submitted',
  `caste_certificate` varchar(50) NOT NULL DEFAULT 'not_submitted',
  `original_bonafied` varchar(50) NOT NULL DEFAULT 'not_submitted',
  `original_tc` varchar(50) NOT NULL DEFAULT 'not_submitted',
  `residence_proof` varchar(50) NOT NULL DEFAULT 'not_submitted',
  `if_foreign_nationals_copy` varchar(50) NOT NULL,
  `copy_report_card` varchar(50) NOT NULL,
  `status` int(50) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registration_student`
--

INSERT INTO `registration_student` (`id`, `school_name`, `academic_year`, `user_id`, `class_id`, `student_id`, `first_name`, `middle_name`, `last_name`, `dob`, `email`, `nationality`, `gender`, `caste`, `religion`, `student_photo`, `birth_place`, `address`, `pin_code`, `fathers_firstname`, `fathers_midname`, `fathers_lastname`, `fathers_mobile`, `fathers_occup`, `mothers_occup`, `mothers_firstname`, `mothers_midname`, `mothers_lastname`, `mothers_mobile`, `mothers_qualification`, `fathers_qualification`, `mother_toung`, `anuual_income`, `isthr_medical_condition`, `isthr_undergoing_treatment`, `is_child_taking_medication`, `food_allergy`, `medical_fitness_cert`, `adoption_document`, `passport_photos`, `passport_pic_both_parent`, `original_birth_cert`, `pan_card_copy_parent`, `adhar_copy_parent`, `caste_certificate`, `original_bonafied`, `original_tc`, `residence_proof`, `if_foreign_nationals_copy`, `copy_report_card`, `status`, `created_at`, `updated_at`) VALUES
(26, '', '2022-2023', 72, '1', 202201012, 'Avni', 'Sagar', 'Belure', '2022-08-17', 'avni@gmail.com', 'Indian', 'female', 'Open', 'Hindu', '08b42f6f18f62e063b5521bb28bda007.jpg', 'Latur', 'Banglore ', 567711, 'Sagar', 'Shivling', 'Belure', '+917766889956', 'Employer', 'Housewife', 'Kavita', 'Sagar', 'Belure', '9988776689', 'Engineer', 'Engineer', 'Marathi', '3 Lack', 'no', 'no', 'no', 'no', '', '', '', '', '', '', 'submited', '', '', 'submited', '', '', '', 1, '2022-07-09 13:09:00', '2022-07-09 15:09:00'),
(27, '', '2022-23', 74, '3', 202203009, 'Bhargavi', 'Sagar', 'belure', '08/01/2014', 'bhargavi@gmail.com', 'Indian', 'female', 'Open', 'Hindu', 'e625f4e1f0d0c984b7d8c3c9f0e42904.jpg', 'Latur', 'Banglore ', 887711, 'Sagar', 'Shivling', 'Belure', '+919988990083', 'Business', 'Housewife', 'Kavitaa', 'Sagar', 'belure', '9987766778', 'BE', 'BE', 'Marathi', '2 lack', 'no', 'no', 'no', 'no', 'submited', '', '', '', 'submited', '', '', '', '', '', '', '', '', 0, '2022-05-17 11:34:49', ''),
(28, '50', '235', 75, '7', 202207007, 'vinod', 'jj', 'mule', '07/08/1990', 'vvv@gmail.com', 'Indian', 'male', 'Open', 'Hindu', '', 'Pune', ' pune', 456889, 'ss', 'bb', 'mule', '+919856278956', 'Business', 'Housewife', 'ssc', 'ss', '', '', '', '', 'Select', '200000', '', '', '', '', '', '', '', '', '', '', 'submited', '', '', '', '', '', '', 0, '2022-08-29 09:13:31', ''),
(29, '231', '232', 76, '9', 202209003, 'pushpa', 'su', 'choudhary', '02/11/1988', 'ppp@gmail.com', 'Indian', 'female', 'Open', 'Hindu', '', 'latur', ' latur', 431515, 'suresh', 'mm', 'choudhary', '+919988856789', 'Teachers', 'Teachers', 'bb', 'su', '', '', '', '', 'Select', '120000', '', '', '', '', '', '', '', '', '', '', 'submited', '', '', '', '', '', '', 0, '2022-08-29 09:24:31', ''),
(30, '241', '242', 77, '3', 202203010, 'sonakshi', 'sangram', 'bhosale', '17/07/2014', 'sona@gmail.com', 'Indian', 'female', 'Open', 'Hindu', '15237c7c8da82e51ac5115c6c62d39c8.png', 'dhule', ' pune', 422556, 'sangram', 'bb', 'test', '12345', 'Farmer', 'Teachers', 'test', 'test', 'test', 'test2', 'test', 'test', 'English', '343434', 'no', 'no', 'no', 'no', '', '', '', '', '', '', 'submited', '', '', 'submited', '', '', '', 0, '2022-10-11 09:46:05', '2022-10-11 11:46:05'),
(31, '241', '242', 79, '7', 202207007, 'testswati', 'testswatio', 'bbb', '02/04/1996', 'sss@gmail.com', 'Indian', 'female', 'Open', 'Hindu', '', 'ppp', ' p', 411055, 'shivling', 'momtest', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 'no', 'no', 'no', '', '', '', '', '', '', '', '', '', 'submited', '', '', '', 0, '2022-11-11 14:17:49', '');

-- --------------------------------------------------------

--
-- Table structure for table `result_action`
--

CREATE TABLE `result_action` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `publish` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result_action`
--

INSERT INTO `result_action` (`id`, `class_id`, `exam_id`, `exam_title`, `status`, `publish`) VALUES
(1, 1, 4, 'Test Exam 1', 'Complete', 'Publish');

-- --------------------------------------------------------

--
-- Table structure for table `result_shit`
--

CREATE TABLE `result_shit` (
  `id` int(11) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `student_id` varchar(100) NOT NULL,
  `exam_subject` varchar(100) NOT NULL,
  `mark` varchar(10) NOT NULL,
  `point` varchar(5) NOT NULL,
  `grade` varchar(5) NOT NULL,
  `roll_number` int(11) NOT NULL,
  `result` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result_shit`
--

INSERT INTO `result_shit` (`id`, `exam_title`, `exam_id`, `class_id`, `section`, `student_name`, `student_id`, `exam_subject`, `mark`, `point`, `grade`, `roll_number`, `result`) VALUES
(1, 'Test Exam 1', 4, 1, '', 'Benjamin D. Lampe', '201601001', 'AMAR BANGLA BOI', '87', '5', 'A+', 1, 'Pass'),
(2, 'Test Exam 1', 4, 1, '', 'Rahim Hasan', '201601002', 'AMAR BANGLA BOI', '78', '4', 'A', 2, 'Pass'),
(3, 'Test Exam 1', 4, 1, '', 'Junayed Hak', '201601003', 'AMAR BANGLA BOI', '89', '5', 'A+', 3, 'Pass'),
(4, 'Test Exam 1', 4, 1, '', 'Razia Akture', '201601004', 'AMAR BANGLA BOI', '82', '5', 'A+', 4, 'Pass'),
(5, 'Test Exam 1', 4, 1, '', 'Polash Sarder', '201601005', 'AMAR BANGLA BOI', '81', '5', 'A+', 5, 'Pass'),
(6, 'Test Exam 1', 4, 1, '', 'Sumon Akon', '201601006', 'AMAR BANGLA BOI', '80', '5', 'A+', 6, 'Pass'),
(7, 'Test Exam 1', 4, 1, '', 'Benjamin D. Lampe', '201601001', 'ENGLISH FOR TODAY', '75', '4', 'A', 1, 'Pass'),
(8, 'Test Exam 1', 4, 1, '', 'Rahim Hasan', '201601002', 'ENGLISH FOR TODAY', '85', '5', 'A+', 2, 'Pass'),
(9, 'Test Exam 1', 4, 1, '', 'Junayed Hak', '201601003', 'ENGLISH FOR TODAY', '76', '4', 'A', 3, 'Pass'),
(10, 'Test Exam 1', 4, 1, '', 'Razia Akture', '201601004', 'ENGLISH FOR TODAY', '82', '5', 'A+', 4, 'Pass'),
(11, 'Test Exam 1', 4, 1, '', 'Polash Sarder', '201601005', 'ENGLISH FOR TODAY', '81', '5', 'A+', 5, 'Pass'),
(12, 'Test Exam 1', 4, 1, '', 'Sumon Akon', '201601006', 'ENGLISH FOR TODAY', '74', '4', 'A', 6, 'Pass'),
(13, 'Test Exam 1', 4, 1, '', 'Benjamin D. Lampe', '201601001', 'PRIMARY MATHEMATICS', '87', '5', 'A+', 1, 'Pass'),
(14, 'Test Exam 1', 4, 1, '', 'Rahim Hasan', '201601002', 'PRIMARY MATHEMATICS', '78', '4', 'A', 2, 'Pass'),
(15, 'Test Exam 1', 4, 1, '', 'Junayed Hak', '201601003', 'PRIMARY MATHEMATICS', '89', '5', 'A+', 3, 'Pass'),
(16, 'Test Exam 1', 4, 1, '', 'Razia Akture', '201601004', 'PRIMARY MATHEMATICS', '75', '4', 'A', 4, 'Pass'),
(17, 'Test Exam 1', 4, 1, '', 'Polash Sarder', '201601005', 'PRIMARY MATHEMATICS', '68', '3.5', 'A-', 5, 'Pass'),
(18, 'Test Exam 1', 4, 1, '', 'Sumon Akon', '201601006', 'PRIMARY MATHEMATICS', '80', '5', 'A+', 6, 'Pass'),
(19, 'FInal Exam', 5, 3, '', 'Farjana Akter', '201603001', 'AMAR BANGLA BOI', '84', '5', 'A+', 1, 'Pass'),
(20, 'FInal Exam', 5, 3, '', 'Akram Hossain', '201603002', 'AMAR BANGLA BOI', '75', '4', 'A', 2, 'Pass'),
(21, 'FInal Exam', 5, 3, '', 'Alamin Saeder', '201603003', 'AMAR BANGLA BOI', '66', '3', 'B', 3, 'Pass'),
(22, 'FInal Exam', 5, 3, '', 'Sabina Sumi', '201603004', 'AMAR BANGLA BOI', '68', '3', 'B', 4, 'Pass'),
(23, 'FInal Exam', 5, 3, '', 'Farjana Akter', '201603001', 'AMAR BANGLA BOI', '84', '5', 'A+', 1, 'Pass'),
(24, 'FInal Exam', 5, 3, '', 'Akram Hossain', '201603002', 'AMAR BANGLA BOI', '75', '4', 'A', 2, 'Pass'),
(25, 'FInal Exam', 5, 3, '', 'Alamin Saeder', '201603003', 'AMAR BANGLA BOI', '66', '3', 'B', 3, 'Pass'),
(26, 'FInal Exam', 5, 3, '', 'Sabina Sumi', '201603004', 'AMAR BANGLA BOI', '68', '3', 'B', 4, 'Pass');

-- --------------------------------------------------------

--
-- Table structure for table `result_submition_info`
--

CREATE TABLE `result_submition_info` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `exam_title` varchar(150) NOT NULL,
  `date` varchar(50) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `submited` varchar(50) NOT NULL,
  `teacher` varchar(100) NOT NULL,
  `exam_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result_submition_info`
--

INSERT INTO `result_submition_info` (`id`, `class_id`, `section`, `exam_title`, `date`, `subject`, `submited`, `teacher`, `exam_id`) VALUES
(1, 1, '', 'Test Exam 1', '10/06/2016', 'AMAR BANGLA BOI', '1', 'Willie B. Quint', 4),
(2, 1, '', 'Test Exam 1', '10/06/2016', 'ENGLISH FOR TODAY', '1', 'Fredrick V. Keyes', 4),
(3, 1, '', 'Test Exam 1', '11/06/2016', 'PRIMARY MATHEMATICS', '1', 'Willie B. Quint', 4),
(4, 3, '', 'FInal Exam', '08/10/2016', 'AMAR BANGLA BOI', '1', 'Headmaster', 5),
(5, 3, '', 'FInal Exam', '08/10/2016', 'AMAR BANGLA BOI', '1', 'Headmaster', 5);

-- --------------------------------------------------------

--
-- Table structure for table `role_based_access`
--

CREATE TABLE `role_based_access` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(1) NOT NULL,
  `das_top_info` int(1) NOT NULL,
  `das_grab_chart` int(1) NOT NULL,
  `das_class_info` int(1) NOT NULL,
  `das_message` int(1) NOT NULL,
  `das_employ_attend` int(1) NOT NULL,
  `das_notice` int(1) NOT NULL,
  `das_calender` int(1) NOT NULL,
  `admission` int(1) NOT NULL,
  `all_student_info` int(1) NOT NULL,
  `stud_edit_delete` int(1) NOT NULL,
  `stu_own_info` int(1) NOT NULL,
  `teacher_info` int(1) NOT NULL,
  `add_teacher` int(1) NOT NULL,
  `teacher_details` int(1) NOT NULL,
  `teacher_edit_delete` int(1) NOT NULL,
  `all_parents_info` int(1) NOT NULL,
  `own_parents_info` int(1) NOT NULL,
  `make_parents_id` int(1) NOT NULL,
  `parents_edit_dlete` int(1) NOT NULL,
  `add_employee` int(1) NOT NULL,
  `employee_list` int(1) NOT NULL,
  `employ_attendance` int(1) NOT NULL,
  `empl_atte_view` int(1) NOT NULL,
  `add_new_class` int(1) NOT NULL,
  `all_class_info` int(1) NOT NULL,
  `class_details` int(1) NOT NULL,
  `class_delete` int(1) NOT NULL,
  `class_promotion` int(1) NOT NULL,
  `add_class_routine` int(1) NOT NULL,
  `own_class_routine` int(1) NOT NULL,
  `all_class_routine` int(1) NOT NULL,
  `rutin_edit_delete` int(1) NOT NULL,
  `attendance_preview` int(1) NOT NULL,
  `take_studence_atten` int(1) NOT NULL,
  `edit_student_atten` int(1) NOT NULL,
  `add_subject` int(1) NOT NULL,
  `all_subject` int(1) NOT NULL,
  `assin_optio_sub` int(1) NOT NULL,
  `make_suggestion` int(1) NOT NULL,
  `all_suggestion` int(1) NOT NULL,
  `own_suggestion` int(1) NOT NULL,
  `add_exam_gread` int(1) NOT NULL,
  `exam_gread` int(1) NOT NULL,
  `gread_edit_dele` int(1) NOT NULL,
  `add_exam_routin` int(1) NOT NULL,
  `all_exam_routine` int(1) NOT NULL,
  `own_exam_routine` int(1) NOT NULL,
  `exam_attend_preview` int(1) NOT NULL,
  `approve_result` int(1) NOT NULL,
  `view_result` int(1) NOT NULL,
  `all_mark_sheet` int(1) NOT NULL,
  `own_mark_sheet` int(1) NOT NULL,
  `take_exam_attend` int(1) NOT NULL,
  `change_exam_attendance` int(1) NOT NULL,
  `make_result` int(1) NOT NULL,
  `add_category` int(1) NOT NULL,
  `all_category` int(1) NOT NULL,
  `edit_delete_category` int(1) NOT NULL,
  `add_books` int(1) NOT NULL,
  `all_books` int(1) NOT NULL,
  `edit_delete_books` int(1) NOT NULL,
  `add_library_mem` int(1) NOT NULL,
  `memb_list` int(1) NOT NULL,
  `issu_return` int(1) NOT NULL,
  `add_dormitories` int(1) NOT NULL,
  `add_set_dormi` int(1) NOT NULL,
  `set_member_bed` int(1) NOT NULL,
  `dormi_report` int(1) NOT NULL,
  `add_transport` int(1) NOT NULL,
  `all_transport` int(1) NOT NULL,
  `transport_edit_dele` int(1) NOT NULL,
  `add_account_title` int(1) NOT NULL,
  `edit_dele_acco` int(1) NOT NULL,
  `trensection` int(1) NOT NULL,
  `fee_collection` int(1) NOT NULL,
  `all_slips` int(1) NOT NULL,
  `own_slip` int(1) NOT NULL,
  `slip_edit_delete` int(1) NOT NULL,
  `pay_salary` int(1) NOT NULL,
  `creat_notice` int(1) NOT NULL,
  `send_message` int(1) NOT NULL,
  `vendor` int(1) NOT NULL,
  `delet_vendor` int(1) NOT NULL,
  `add_inv_cat` int(1) NOT NULL,
  `inve_item` int(1) NOT NULL,
  `delete_inve_ite` int(1) NOT NULL,
  `delete_inv_cat` int(1) NOT NULL,
  `inve_issu` int(1) NOT NULL,
  `delete_inven_issu` int(1) NOT NULL,
  `check_leav_appli` int(1) NOT NULL,
  `setting_accounts` int(1) NOT NULL,
  `other_setting` int(1) NOT NULL,
  `front_setings` int(1) NOT NULL,
  `set_optional` int(1) NOT NULL,
  `setting_manage_user` int(1) NOT NULL,
  `crm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_based_access`
--

INSERT INTO `role_based_access` (`id`, `user_id`, `group_id`, `das_top_info`, `das_grab_chart`, `das_class_info`, `das_message`, `das_employ_attend`, `das_notice`, `das_calender`, `admission`, `all_student_info`, `stud_edit_delete`, `stu_own_info`, `teacher_info`, `add_teacher`, `teacher_details`, `teacher_edit_delete`, `all_parents_info`, `own_parents_info`, `make_parents_id`, `parents_edit_dlete`, `add_employee`, `employee_list`, `employ_attendance`, `empl_atte_view`, `add_new_class`, `all_class_info`, `class_details`, `class_delete`, `class_promotion`, `add_class_routine`, `own_class_routine`, `all_class_routine`, `rutin_edit_delete`, `attendance_preview`, `take_studence_atten`, `edit_student_atten`, `add_subject`, `all_subject`, `assin_optio_sub`, `make_suggestion`, `all_suggestion`, `own_suggestion`, `add_exam_gread`, `exam_gread`, `gread_edit_dele`, `add_exam_routin`, `all_exam_routine`, `own_exam_routine`, `exam_attend_preview`, `approve_result`, `view_result`, `all_mark_sheet`, `own_mark_sheet`, `take_exam_attend`, `change_exam_attendance`, `make_result`, `add_category`, `all_category`, `edit_delete_category`, `add_books`, `all_books`, `edit_delete_books`, `add_library_mem`, `memb_list`, `issu_return`, `add_dormitories`, `add_set_dormi`, `set_member_bed`, `dormi_report`, `add_transport`, `all_transport`, `transport_edit_dele`, `add_account_title`, `edit_dele_acco`, `trensection`, `fee_collection`, `all_slips`, `own_slip`, `slip_edit_delete`, `pay_salary`, `creat_notice`, `send_message`, `vendor`, `delet_vendor`, `add_inv_cat`, `inve_item`, `delete_inve_ite`, `delete_inv_cat`, `inve_issu`, `delete_inven_issu`, `check_leav_appli`, `setting_accounts`, `other_setting`, `front_setings`, `set_optional`, `setting_manage_user`, `crm`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 2, 6, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),
(3, 4, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 6, 7, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 7, 8, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 8, 4, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 9, 4, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 10, 4, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 11, 4, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 12, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 13, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(12, 14, 5, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(13, 15, 5, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 16, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 17, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(16, 18, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(17, 19, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(18, 20, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(19, 21, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 22, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(21, 23, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(22, 24, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(23, 25, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(24, 26, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(25, 27, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 28, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(27, 29, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(28, 30, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 31, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 32, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(31, 33, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(32, 34, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(33, 35, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(34, 36, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(35, 37, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(36, 38, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(37, 39, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(38, 40, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(39, 41, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(40, 42, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(41, 43, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(42, 44, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(43, 45, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(44, 46, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(45, 47, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(46, 48, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(47, 49, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(48, 50, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(49, 51, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(50, 52, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(51, 53, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(52, 54, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(53, 56, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(54, 57, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(55, 58, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(56, 59, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(57, 60, 5, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(58, 61, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(59, 62, 5, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(60, 63, 5, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(61, 64, 5, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(62, 65, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(63, 66, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(64, 68, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(65, 1, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(66, 2, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(67, 3, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(68, 4, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(69, 5, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(70, 6, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(71, 7, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(72, 8, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(73, 9, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(74, 10, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(75, 11, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(76, 12, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(77, 13, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(78, 14, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(79, 15, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(80, 16, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(81, 17, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(82, 18, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(83, 19, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(84, 20, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(85, 21, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(86, 22, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(87, 23, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(88, 24, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(89, 69, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(90, 71, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(91, 72, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(92, 74, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(93, 76, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(94, 77, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(95, 79, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `month` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `employ_title` varchar(100) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `method` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `institute`
--

CREATE TABLE `institute` (
  `id` int(11) NOT NULL,
  `school_name` varchar(50) NOT NULL,
  `school_id` varchar(100) NOT NULL,
  `school_grpname` varchar(50) NOT NULL,
  `sub_location` varchar(50) NOT NULL,
  `city` varchar(100) NOT NULL,
  `pincode` varchar(30) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_num` varchar(10) NOT NULL,
  `description` varchar(20) NOT NULL,
  `reg_date` varchar(30) NOT NULL,
  `foundation_year` varchar(20) NOT NULL,
  `school_email` varchar(100) NOT NULL,
  `school_reg_no` int(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `upodated_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `institute`
--

INSERT INTO `institute` (`id`, `school_name`, `school_id`, `school_grpname`, `sub_location`, `city`, `pincode`, `admin_name`, `admin_num`, `description`, `reg_date`, `foundation_year`, `school_email`, `school_reg_no`, `created_at`, `upodated_at`) VALUES
(1, 'abc', '123', 'dd', 'bb', 'pune', '14444', 'sss', '12455', 'desc', '2022-08-25', '22-23', 'abc@gmail.com', 26655, '2022-08-24 07:15:59', ''),
(2, 'new high school', '55', 'nnn', 'ppp', 'goa', '422669', 'fff', '123', 'sadfsdf', '2022-08-27', '1990', 'new@gmail.com', 9988, '2022-08-24 07:43:23', ''),
(3, 'new high school', '55', 'nnn', 'ppp', 'kolhapur', '422669', 'fff', '123', 'sadfsdf', '2022-08-27', '1990', 'new@gmail.com', 9988, '2022-08-24 09:47:20', ''),
(4, 'new high school', '55', 'nnn', 'ppp', 'mum', '422669', 'fff', '123', 'sadfsdf', '2022-08-27', '1990', 'new@gmail.com', 9988, '2022-08-24 09:47:37', ''),
(7, '50-Sample School', '568', 'sss', 'hh', 'beed', '431515', 'dsa', '56789', 'test', '2022-08-30', '22-23', 'sample@gmail.com', 98556655, '2022-08-30 07:20:56', ''),
(12, 'test44', '333', 'test2', 'ggg', 'mumbai', '34567', 'testing admin', '98888888', 'testing', '2022-10-21', '2022', 'admin@gmail.com', 123, '2022-10-20 13:56:37', '');

-- --------------------------------------------------------

--
-- Table structure for table `set_salary`
--

CREATE TABLE `set_salary` (
  `id` int(11) NOT NULL,
  `year` int(5) NOT NULL,
  `employ_user_id` int(11) NOT NULL,
  `employe_title` varchar(100) NOT NULL,
  `job_post` varchar(50) NOT NULL,
  `basic` int(11) NOT NULL,
  `treatment` int(11) NOT NULL,
  `increased` int(11) NOT NULL,
  `others` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `month` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `set_salary`
--

INSERT INTO `set_salary` (`id`, `year`, `employ_user_id`, `employe_title`, `job_post`, `basic`, `treatment`, `increased`, `others`, `total`, `month`) VALUES
(1, 2018, 1, 'Headmaster', 'Headmaster', 15000, 2000, 0, 500, 17500, 0);

-- --------------------------------------------------------

--
-- Table structure for table `slip`
--

CREATE TABLE `slip` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` varchar(10) NOT NULL,
  `date` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `item_id` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `dues` int(11) NOT NULL,
  `advance` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `paid` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `edit_by` varchar(100) NOT NULL,
  `status` text NOT NULL,
  `mathod` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slip`
--

INSERT INTO `slip` (`id`, `year`, `month`, `date`, `class_id`, `student_id`, `item_id`, `amount`, `dues`, `advance`, `total`, `paid`, `balance`, `edit_by`, `status`, `mathod`) VALUES
(1, 2016, 'September', 0, 1, 201601001, '1,3', 200, 0, 0, 200, 200, 0, '', 'Paid', 'Card'),
(2, 2016, 'September', 0, 1, 201601002, '1,3', 200, 100, 0, 200, 100, 0, '', 'Not Clear', 'Card'),
(3, 2016, 'September', 0, 1, 201601003, '1,3', 200, 0, 0, 200, 0, 0, '', 'Unpaid', ''),
(4, 2016, 'September', 0, 1, 201601004, '1,3', 200, 0, 0, 200, 0, 0, '', 'Unpaid', ''),
(5, 2016, 'September', 0, 1, 201601005, '1,3', 200, 0, 0, 200, 0, 0, '', 'Unpaid', ''),
(6, 2016, 'September', 0, 1, 201601006, '1,3', 200, 0, 0, 200, 0, 0, '', 'Unpaid', ''),
(7, 2016, 'September', 0, 2, 201602001, '2', 500, 0, 0, 500, 2000, 1500, '', 'Paid', 'Cash'),
(8, 2016, 'September', 0, 2, 201602002, '2', 500, 0, 0, 500, 1200, 700, '', 'Paid', 'Card'),
(9, 2016, 'September', 0, 2, 201602003, '2', 500, 300, 0, 500, 200, 0, '', 'Not Clear', 'Cash'),
(10, 2016, 'September', 0, 2, 201602004, '2', 500, 0, 0, 500, 500, 0, '', 'Paid', 'Cash'),
(11, 2016, 'September', 0, 2, 201602005, '2', 500, 0, 0, 500, 500, 0, '', 'Paid', 'Cash'),
(12, 2016, 'September', 0, 2, 201602006, '2', 500, 0, 0, 500, 500, 0, '', 'Paid', 'Cash');

-- --------------------------------------------------------

--
-- Table structure for table `student_info`
--

CREATE TABLE `student_info` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `pid` int(11) NOT NULL,
  `roll_number` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_nam` varchar(100) NOT NULL,
  `farther_name` varchar(150) NOT NULL,
  `mother_name` varchar(150) NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `sex` varchar(30) NOT NULL,
  `present_address` varchar(300) NOT NULL,
  `permanent_address` varchar(300) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `father_occupation` varchar(150) NOT NULL,
  `father_incom_range` varchar(100) NOT NULL,
  `mother_occupation` varchar(100) NOT NULL,
  `student_photo` varchar(200) NOT NULL,
  `last_class_certificate` text NOT NULL,
  `t_c` text NOT NULL,
  `national_birth_certificate` text NOT NULL,
  `academic_transcription` text NOT NULL,
  `testimonial` text NOT NULL,
  `documents_info` varchar(500) NOT NULL,
  `starting_year` int(11) NOT NULL,
  `transfer_year` int(11) NOT NULL,
  `transfer_to` text NOT NULL,
  `transfer_reason` text NOT NULL,
  `tc_appli_approved_by` text NOT NULL,
  `passing_year` int(11) NOT NULL,
  `compleat_level` text NOT NULL,
  `registration_number` text NOT NULL,
  `certificates_status` text NOT NULL,
  `admission_year` int(11) NOT NULL,
  `admission_class` varchar(100) NOT NULL,
  `admission_roll` int(5) NOT NULL,
  `admission_form_no` int(11) NOT NULL,
  `admission_test_result` int(11) NOT NULL,
  `tc_form` varchar(150) NOT NULL,
  `blood` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_info`
--

INSERT INTO `student_info` (`id`, `year`, `user_id`, `student_id`, `pid`, `roll_number`, `class_id`, `student_nam`, `farther_name`, `mother_name`, `birth_date`, `sex`, `present_address`, `permanent_address`, `phone`, `father_occupation`, `father_incom_range`, `mother_occupation`, `student_photo`, `last_class_certificate`, `t_c`, `national_birth_certificate`, `academic_transcription`, `testimonial`, `documents_info`, `starting_year`, `transfer_year`, `transfer_to`, `transfer_reason`, `tc_appli_approved_by`, `passing_year`, `compleat_level`, `registration_number`, `certificates_status`, `admission_year`, `admission_class`, `admission_roll`, `admission_form_no`, `admission_test_result`, `tc_form`, `blood`) VALUES
(2, 2018, 4, '201601001', 0, 1, 1, 'Benjamin D. Lampe', 'John E. Williams', 'Deidra D. Shaw', '11/02/2010', 'Male', ' 1110 Hillcrest Circle\\\\nPlymouth, MN 55441 ', '1110 Hillcrest Circle\\\\nPlymouth, MN 55441', '763496-3372', 'Business', '1250', 'Housewife', 'f1829afa3cdbed1c643b000b1ea6ab02.jpg', '', '', 'submited', '', '', 'File No - 2016 (01)', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O+'),
(3, 2018, 12, '201601002', 0, 2, 1, 'Rahim Hasan', 'Jafor Uddin', 'Julakha Begum', '20/11/2010', 'Male', ' Test address     ', ' Test address', '+8801245852315', 'Business', '20000', 'Housewife', '21e39ae6c4f966a8657d11d36084b8fc.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A+'),
(4, 2018, 13, '201601003', 0, 3, 1, 'Junayed Hak', 'Raise Hak', 'Lima Khatun', '20/12/2011', 'Male', ' Test address ', ' Test address', '+8801245852315', 'Business', '20000', 'Housewife', '7667367fd4bded46db7e2ee3010f987f.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A+'),
(5, 2018, 16, '201601004', 0, 4, 1, 'Razia Akture', 'Karim Khan', 'Kole Begum', '11/05/2007', 'Female', ' Test address', ' Test address', '+8801245852315', 'Business', '20150', 'Housewife', 'f4498b9e3c9d2f933e4026cb0801105c.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A-'),
(6, 2018, 17, '201602001', 0, 1, 2, 'Abdullah  hossain', 'Lutfor Rahman', 'Amina begum', '11/06/2008', 'Male', 'test address', 'test address', '+88012458523546', 'Business', '20000', 'Housewife', '2408f2dc2b3af87133555dab5815f027.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B+'),
(7, 2018, 18, '201602002', 0, 2, 2, 'Sujana Ahmed', 'Josim Sarder', 'Lima Khatun', '09/10/2008', 'Female', ' test address', 'test address', '+88012458523546', 'Teachers', '20150', 'Housewife', 'c2b15925a33cddc0ca58a2c504c66bff.jpg', '', '', '', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A+'),
(8, 2018, 19, '201602003', 0, 3, 2, 'Mahmud Hasan', 'Razzak Hasan', 'Kohinur khatun', '12/07/2008', 'Male', ' test address', 'test address', '+880+8801245852', 'Banker', '2050', 'Housewife', '10dd794d22c7c0ca38b89508c2febc41.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'AB+'),
(9, 2018, 20, '201602004', 0, 4, 2, 'Mahbuba Akter', 'Salim Morshed', 'Julakha Begum', '06/03/2008', 'Female', ' test address', 'test address', '+8801245852354', 'Business', '2150', 'Housewife', 'aa3fab97275a0a85652bfe7fbfa91666.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O+'),
(10, 2018, 21, '201602005', 0, 5, 2, 'Irfan Hossain', 'Momin Hossain', 'Nargis Begum', '22/11/2008', 'Male', ' test address', 'test address', '+88012458578945', 'Car Driver', '20000', 'Housewife', '50875054077eb4436e78b8f2796761a8.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B-'),
(11, 2018, 22, '201602006', 0, 6, 2, 'Imran Hasan', 'Oliuil Hasan', 'Marufa Begum', '25/09/2008', 'Male', ' test address', 'test address', '+8801245854521', 'Business', '2100', 'Housewife', '9e01047b759770110ed29b254db908d7.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B+'),
(12, 2018, 23, '201601005', 0, 5, 1, 'Polash Sarder', 'Khalil Sarder', 'Jhorna Begum', '22/05/2007', 'Male', ' test address', 'test address', '+88012458529632', 'Business', '2080', 'Housewife', '41e56e4c5efe83fa80966da3ea23c98f.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O+'),
(13, 2018, 24, '201601006', 0, 6, 1, 'Sumon Akon', 'Julhash Akon', 'Moyna Begum', '09/08/2007', 'Male', ' test address', 'test address', '+8801245852385', 'Banker', '2090', 'Housewife', '8337f3befe383f4770df79b1c01cc8ae.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O-'),
(14, 2018, 25, '201603001', 0, 1, 3, 'Farjana Akter', 'samsul Haq', 'lota begum', '02/06/2009', 'Female', ' test address', 'test address', '+88012458527412', 'Employer', '2000', 'Housewife', 'c2da4f488e8b188bf8cc3bf1a2049b2f.png', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O+'),
(15, 2018, 26, '201603002', 0, 2, 3, 'Akram Hossain', 'Manik Hossain', 'Aliya Begum', '07/11/2009', 'Male', ' test address', 'test address', '+88012458529632', 'Car Driver', '1900', 'Housewife', '58b17c784b9f7eca12209969b01bc0a9.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A-'),
(16, 2018, 27, '201603003', 0, 3, 3, 'Alamin Saeder', 'Hemel Sarder', 'Aliya Begum', '09/12/2009', 'Male', ' test address', 'test address', '+880+8801245852', 'Employer', '2100', 'Housewife', '1fa9258bf0ffdc9ebe01d17154c25e56.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O+'),
(17, 2018, 28, '201603004', 0, 4, 3, 'Sabina Sumi', 'Jafor Uddin', 'Mala Kahtun', '20/05/2005', 'Female', 'Test Address', 'Test Address', '+88012458523546', 'Business', '52412', 'Housewife', 'e0237b45f347083352f7cd500286914f.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'AB+'),
(18, 2018, 29, '201604001', 0, 1, 4, 'Sanjida Hossain', 'Mahbub Hossain', 'Nilima Begum', '11/02/2009', 'Female', ' test address', 'test address', '+8801245852354', 'Business', '21220', 'Housewife', 'e2cee6b429e9423c627d8a34027ca44c.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'AB+'),
(19, 2018, 30, '201604002', 0, 2, 4, 'Kawser  Shikder', 'Moslem Shikder', 'Selina begum', '15/12/2009', 'Male', ' test address', 'test address', '+88012458578452', 'Business', '2100', 'Housewife', 'e63eb2b5e250aa6b773c795054dea21a.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O-'),
(20, 2018, 31, '201604003', 0, 3, 4, 'Shohana Akter', 'Aslam Hossain', 'Rahima Begum', '10/03/2009', 'Female', ' test address', 'test address', '+88012458523532', 'Employer', '2150', 'Housewife', '9e8641a879671c2b4a31e77c197a9d3f.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B+'),
(21, 2018, 32, '201604004', 0, 4, 4, 'Juthi Khanam', 'Iqbal hossen', 'Aysha Begum', '04/11/2009', 'Female', ' test address', 'test address', '+88012458578478', 'Banker', '20000', 'Housewife', '5c99d031c2aaa455c46b467194ebe04f.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A-'),
(22, 2018, 33, '201605001', 0, 1, 5, 'Tanjila Akter', 'Mojammel Hossain', 'Afia Begum', '10/12/2010', 'Female', ' test address', 'test address', '+88012458524521', 'Employer', '2180', 'Housewife', '88b05c84d9940f9827b4f020bffb46df.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O+'),
(23, 2018, 34, '201605002', 0, 2, 5, 'Nusrat Jahan', 'Altaf Hossain', 'Nurjahan Akter', '17/02/2010', 'Female', ' test address', 'test address', '+88012458527452', 'Car Driver', '20000', 'Housewife', 'ea1e8ed789f4d1f4a5780a31a075e9a8.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B+'),
(24, 2018, 35, '201605003', 0, 3, 5, 'Amina Akter', 'Mostofa Sarder', 'Rokaya Begum', '05/10/2010', 'Female', ' terst address', 'test address', '+88012458523652', 'Banker', '2160', 'Housewife', 'bf45f3417020caab83136fc790a837a4.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A-'),
(25, 2018, 36, '201605004', 0, 4, 5, 'Ebrahim Khondokar', 'Julhash Khondokar', 'Lija Akter', '03/11/2010', 'Male', ' test address', 'test address', '+88012458451397', 'Business', '20000', 'Housewife', 'd1ccdd70ebb675f762aa6de55ecc5927.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A+'),
(26, 2018, 37, '201605005', 0, 5, 5, 'Mintu  Fokir', 'Sultan Fokir', 'Julakha Begum', '12/04/2010', 'Male', ' test address', 'test address', '+880+8801245789', 'Teachers', '2080', 'Housewife', 'efae644abc3a8621ba3703b861c5117d.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B-'),
(27, 2018, 38, '201606001', 0, 1, 6, 'Shohid Islam', 'Lotif Sharker', 'Jhumur Begum', '11/01/2010', 'Male', ' test address', 'test address', '+88012458529632', 'Employer', '2080', 'Housewife', '840aa12c41963600fd7993e55aa7d7ab.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'AB+'),
(28, 2018, 39, '201606002', 0, 2, 6, 'Khadija Akter', 'Salman Hossain', 'Nuri Begum', '30/05/2010', 'Female', ' test address', 'test address', '+88012458578478', 'Employer', '2150', 'Housewife', '2d0f6d650516b7d5f2c958ed73c88a22.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'AB-'),
(29, 2018, 40, '201606003', 0, 3, 6, 'Maruf Hossain', 'Mokhles sarder', 'Najma Begum', '22/10/2010', 'Male', ' test address', 'test address', '+88012458523546', 'Employer', '2150', 'Housewife', '5149696a702583dfa0f89ab3e77e4057.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'AB+'),
(30, 2018, 41, '201606004', 0, 4, 6, 'Mitu  Akter', 'Karim Khan', 'Rukan Habeeba', '10/02/2010', 'Female', ' test address', 'test address', '+880+8801245789', 'Employer', '2080', 'Housewife', 'c71ff3b9a9436344ffd8dc8bc5b57856.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A-'),
(31, 2018, 42, '201606005', 0, 5, 6, 'Rayhan  Kebria', 'Kabir Hossen', 'Munmun Khanam', '01/11/2010', 'Male', ' test address', 'test address', '+880+8801245789', 'Business', '2150', 'Housewife', '6cd5fd41b32dd2cffaa1a397bfeb8fd1.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B-'),
(32, 2016, 43, '201607001', 0, 1, 7, 'Hemel Hossain', 'Liakot Hossain', 'Jhumur Begum', '10/02/2011', 'Male', ' test address', 'test address', '+880+8801245789', 'Business', '2100', 'Housewife', 'c3a19c6dfbb930525400d7bcdb9867cf.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A-'),
(33, 2016, 44, '201607002', 0, 2, 0, 'Airin Akter', 'Harun Shikder', 'Aliya Begum', '22/05/2011', 'Female', ' test address ', 'rest address', '+880+8801245452', 'Banker', '2155', 'Housewife', '4196bf4c30a4f94165fd82bfa98980d4.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O+'),
(34, 2016, 45, '201607003', 0, 3, 7, 'Hemel Hossain', 'samsul Haq', 'Jhumur Begum', '10/02/2011', 'Male', ' test address', 'test address', '+8801245852354', 'Employer', '2150', 'Housewife', '855cfe1befcd31038add98bb6605c227.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B+'),
(35, 2016, 46, '201607004', 0, 4, 7, 'Mou Akter', 'Salim Ahmed', 'Selina begum', '14/06/2011', 'Female', 'test address', 'trest  address', '+880+8801245752', 'Business', '2150', 'Housewife', '551bcf2eddf4aa49d629cadf543d7e33.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A+'),
(36, 2016, 47, '201607005', 0, 5, 7, 'Dedar  Hossain', 'Mujammel Hossain', 'Fatima Begum', '12/08/2011', 'Male', ' test address', 'test address', '+880+8801247821', 'Teachers', '2100', 'Housewife', 'd8d9124d3a3f01e59538995a499a600a.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A-'),
(37, 2016, 48, '201607006', 0, 6, 7, 'Samia Akter', ' Osman Hossen', 'Rina Begum', '13/10/2011', 'Female', ' test address', 'test address', '+880+8801241020', 'Car Driver', '20000', 'Housewife', '992d1504661e0f1a6fb7f1b87da0b8be.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B+'),
(38, 2016, 49, '201608001', 0, 1, 8, 'Al Mamun', 'Mnowar Hossen', 'Munia Begum', '11/08/2012', 'Male', ' test address', 'test address', '+880+8801245854', 'Employer', '2080', 'Housewife', '637e0e2d01bf463d9791cc77cac00485.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'AB+'),
(39, 2016, 50, '201608002', 0, 2, 8, 'Jiniya Hoq', 'Arman Hossain', 'Habiba Begum', '14/07/2012', 'Female', ' test address', 'test address', '+8801245852354', 'Employer', '2015', 'Housewife', '4e211b4b8cedfa67d84414d5ae412c48.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'AB-'),
(40, 2016, 51, '201608003', 0, 3, 8, 'Junayed Ahmed', 'Obaydul Hossain', 'Kole Begum', '22/12/2011', 'Male', ' test address', 'test address', '+880+8801245852', 'Car Driver', '2100', 'Housewife', '8d37adbd2454c51222d14936cbb3f125.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'AB+'),
(41, 2016, 52, '201608004', 0, 4, 8, 'Priya Ahmed', 'Liakot Hossain', 'Najma Begum', '11/03/2011', 'Female', ' test address', 'test address', '+880+8801245852', 'Business', '20000', 'Housewife', 'e7b45f5e5a62e632ba6bb7d504d25299.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B-'),
(42, 2016, 53, '201608005', 0, 5, 8, 'Mithu Khondokar', 'Jalal Khondokar', 'Bokul Begum', '21/04/2012', 'Male', ' test address', 'test address', '+8801245852354', 'Teachers', '2100', 'Housewife', 'e3bc44fb33852bf16b2387800debd943.jpg', '', '', '', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B+'),
(43, 2016, 54, '201609001', 0, 1, 9, 'Rasel Hossain', 'Abdul Hakim', 'Rabaya Begum', '15/05/2012', 'Male', ' test address', 'test address', '+880+8801245852', 'Banker', '2015', 'Housewife', '061d3b9631a8a0d7acddae6efa0106ae.jpg', '', '', 'submited', '', '', 'Student 2016', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A-'),
(44, 2016, 56, '201609002', 0, 2, 9, 'Test Name', 'sdfsd', 'sdfsdf', '22/09/2016', 'Male', ' szdas', ' sdfsdf', '+88001245885', 'Employer', '20000', 'Business', '48f0d67ff796756b0efa6109727eb2fc.jpg', '', '', '', '', '', 'cv', 2016, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A+'),
(45, 2021, 57, '202110001', 0, 1, 10, 't1 ', 't1', 't1', '01/01/2000', 'Male', ' t1', '', '919999999999', '', '', '', '', '', '', '', '', '', 't1', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', ''),
(46, 2021, 58, '202110002', 0, 2, 10, 't2 t2', 't2', 't2', '09-09-2004', 'male', 't2', '', '9999999999', '', '', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', ''),
(47, 2021, 59, '202110003', 0, 3, 10, 't3 t3', 'p3', 'm3', '15-05-04', 'male', 'mumbai', '', '9999999900', '', '', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', ''),
(48, 2021, 61, '202110004', 0, 4, 10, 't4 t4', 'p4', 'm4', '20-04-08', 'female', 'pune', '', '9999999911', '', '', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', ''),
(49, 2021, 0, '202110005', 0, 5, 10, 't3 t3', 'p3', 'm3', '15-05-04', 'male', 'mumbai', '', '9999999900', '', '', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', ''),
(56, 2021, 0, '202110012', 60, 12, 10, 't3 t3', 'p3', 'm3', '15-05-04', 'male', 'mumbai', '', '9999999900', '', '', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', ''),
(57, 2021, 0, '202110013', 60, 13, 10, 't3 t3', 'p3', 'm3', '15-05-04', 'male', 'mumbai', '', '9999999900', '', '', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', ''),
(58, 2021, 0, '202110014', 5, 14, 10, 't5 t5', 'p5', 'm5', '15-05-08', 'male', 'mumbai', '', '9999999905', '', '', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', ''),
(59, 2021, 0, 't3.9999999900', 3, 15, 10, 't3 t3', 'p3', 'm3', '15-05-04', 'male', 'mumbai', '', '9999999900', '', '', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', ''),
(60, 2021, 0, 't5.9999999905', 5, 16, 10, 't5 t5', 'p5', 'm5', '15-05-08', 'male', 'mumbai', '', '9999999905', '', '', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', ''),
(61, 2021, 0, 't6.9999999905', 5, 17, 10, 't6 t6', 'p6', 'm6', '15-05-10', 'female', 'kolhapur', '', '9999999905', '', '', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', ''),
(62, 2021, 0, 't7.9999999907', 6, 18, 10, 't7 t7', 'p7', 'm7', '15-05-18', 'female', 'USA', '', '9999999907', '', '', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', ''),
(63, 2021, 65, '', 0, 0, 1, 'pavan patil', 'b', 'm', '12/12/1990', 'Male', ' kop', '', '917709147068', 'Business', '', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B+'),
(64, 2021, 66, '202102007', 0, 7, 2, 'jha mobi', 'jha', 'jha', '01/12/1990', 'Male', ' pune', '', '919665341899', 'Employer', '1000000', '', '', '', '', '', '', '', '', 2021, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B-'),
(65, 2022, 68, '202201007', 0, 7, 0, 'urvi mule', 'mahadev', 'swati', '14/11/2015', 'Female', ' wakad ', 'wakad', '+918600171072', 'Business', '5 lack', 'Housewife', 'df3bfc2ba4f9e0e9104791a491e540a5.jpg', '', 'submited', '', 'submited', '', 'nothing', 2022, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O-'),
(66, 2022, 69, '202202012', 0, 12, 2, 'swati', 'test', 'test', '11/11/2011', '', 'wakad', '', '088776677884646', 'Banker', '5 lack', 'Housewife', '422f0a1e5d2a327bc97d5b5b7d1fc5ab.jpg', '', '', 'submited', '', '', '', 2022, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `subjects_mark`
--

CREATE TABLE `subjects_mark` (
  `id` int(11) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `class` varchar(100) NOT NULL,
  `section` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `roll_number` int(11) NOT NULL,
  `mark` int(11) NOT NULL,
  `grade` varchar(30) NOT NULL,
  `statud` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suggestion`
--

CREATE TABLE `suggestion` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `author_name` varchar(150) NOT NULL,
  `class` varchar(20) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `suggestion_title` varchar(150) NOT NULL,
  `suggestion` varchar(2500) NOT NULL,
  `date` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suggestion`
--

INSERT INTO `suggestion` (`id`, `author_id`, `author_name`, `class`, `subject`, `suggestion_title`, `suggestion`, `date`) VALUES
(2, 8, 'Willie B. Quint', 'Class 1', '', 'This is test suggestion.', '<p>This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.</p>\\r\\n', 1466152654);

-- --------------------------------------------------------

--
-- Table structure for table `tax_master`
--

CREATE TABLE `tax_master` (
  `id` int(11) NOT NULL,
  `tax_name` varchar(60) NOT NULL,
  `sgst` varchar(60) NOT NULL,
  `cgst` varchar(50) NOT NULL,
  `igst` varchar(50) NOT NULL,
  `total_percentage` varchar(100) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tax_master`
--

INSERT INTO `tax_master` (`id`, `tax_name`, `sgst`, `cgst`, `igst`, `total_percentage`, `from_date`, `to_date`, `status`, `created_at`) VALUES
(1, 'GST', '9', '9', '0', '18', '2021-04-01', '2022-03-31', 0, '2022-06-01 07:43:44'),
(8, 'test', '6', '6', '0', '12', '2022-02-20', '2022-02-23', 1, '2022-06-01 07:43:49'),
(11, 'ss', '8\\%', '7\\%', '0', '15\\%', '2022-06-09', '2022-06-10', 0, '2022-06-01 07:42:01'),
(12, 'ss', '8\\%', '7\\%', '0', '15\\%', '2022-06-09', '2022-06-10', 0, '2022-06-01 07:42:19'),
(13, 'GST', '0', '8', '8', '16\\%', '2022-11-29', '2022-12-06', 0, '2022-11-28 07:01:31');

-- --------------------------------------------------------

--
-- Table structure for table `teachers_info`
--

CREATE TABLE `teachers_info` (
  `id` int(11) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `farther_name` varchar(150) NOT NULL,
  `mother_name` varchar(150) NOT NULL,
  `birth_date` varchar(150) NOT NULL,
  `sex` varchar(30) NOT NULL,
  `present_address` varchar(300) NOT NULL,
  `permanent_address` varchar(300) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `position` varchar(150) NOT NULL,
  `working_hour` varchar(50) NOT NULL,
  `educational_qualification_1` varchar(500) NOT NULL,
  `educational_qualification_2` varchar(500) NOT NULL,
  `educational_qualification_3` varchar(500) NOT NULL,
  `educational_qualification_4` varchar(500) NOT NULL,
  `educational_qualification_5` varchar(500) NOT NULL,
  `teachers_photo` varchar(200) NOT NULL,
  `cv` varchar(30) NOT NULL,
  `educational_certificat` varchar(30) NOT NULL,
  `exprieance_certificatte` varchar(30) NOT NULL,
  `files_info` varchar(500) NOT NULL,
  `index_no` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers_info`
--

INSERT INTO `teachers_info` (`id`, `user_id`, `fullname`, `farther_name`, `mother_name`, `birth_date`, `sex`, `present_address`, `permanent_address`, `phone`, `subject`, `position`, `working_hour`, `educational_qualification_1`, `educational_qualification_2`, `educational_qualification_3`, `educational_qualification_4`, `educational_qualification_5`, `teachers_photo`, `cv`, `educational_certificat`, `exprieance_certificatte`, `files_info`, `index_no`) VALUES
(1, '8', 'willie b. quinttt', 'Kevin A. Robledo', 'Mary T. McQuay', '12/05/1956', 'Male', '3133 Pointe Lane\\\\\\\\r\\\\\\\\nHollywood, FL 33020', '3133 Pointe Lane\\\\\\\\r\\\\\\\\nHollywood, FL 33020', '+8801245852315', 'English', 'Assistant Headmaster', 'Part time', 'SSC,Test School,A+,1978', 'HSC,Test College,A+,1980', 'Graduation ( Test ),Test University,A,1984', 'Masters Degree,Test University,A,1986', '', 'ac197cff181d9c58027800912c3e0855.png', 'submited', 'submited', 'submited', 'Teacher 2016', '12004'),
(2, '9', 'fredrick vbb keyes', 'Anthony T. Andrews', 'Mary J. Dahl', '20/12/1970', 'Male', '712 Beechwood Drive\\\\r\\\\nChurchville, MD 21028 ', '712 Beechwood Drive\\\\r\\\\nChurchville, MD 21028 ', '+8801245852315', 'Mathematics', 'Senior Teacher', 'Full time', 'SSC,Test School,A+,1988', 'HSC,Test College,A,1990', 'Graduation ( Test ),Test University,A,1994', 'Masters Degree,Test University,A,1996', '', '0f8649af98ca9c0e85957db7e9778191.png', 'submited', 'submited', 'submited', 'Teacher 2016', '12006'),
(3, '10', 'mumar abboud', 'Hadi Shakir Essa', 'Yamha Dhakiyah Mikhail', '11/11/1980', 'Male', '71 City Walls Rd\\\\\\\\r\\\\\\\\nCLOCK FACE\\\\\\\\r\\\\\\\\nWA9 6BG', '71 City Walls Rd\\\\\\\\r\\\\\\\\nCLOCK FACE\\\\\\\\r\\\\\\\\nWA9 6BG', '+8801245852315', 'Bangla ', 'Teacher', 'Full time', 'SSC,Test School,A+,1998', 'HSC,Test College,A,2000', 'Graduation ( Test ),Test University,A,2004', 'Masters Degree,Test University,B,2006', '', '3566d34fc7ce565ba8841d0eaf537b28.png', 'submited', 'submited', 'submited', 'Teacher 2016', '12051'),
(4, '11', 'Inayah Asfour', 'Fatin Husayn', 'Rukan Habeeba', '12/12/1980', 'Female', '31 Clasper Way\\r\\nHEST BANK\\r\\nLA2 2HF ', '31 Clasper Way\\r\\nHEST BANK\\r\\nLA2 2HF ', '+8801245852315', 'Science', 'Teacher', 'Full time', 'SSC,Test School,A+,1998', 'HSC,Test College,A+,2000', 'Graduation ( Test ),Test University,A,2004', 'Masters Degree,Test University,A,2006', '', 'f342f5b3412a5c4be681878ff0462e09.png', 'submited', 'submited', 'submited', 'Teacher 2016', '12056');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_attendance`
--

CREATE TABLE `teacher_attendance` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `employ_id` int(11) NOT NULL,
  `employ_title` varchar(150) NOT NULL,
  `present_or_absent` text NOT NULL,
  `attend_time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_attendance`
--

INSERT INTO `teacher_attendance` (`id`, `year`, `date`, `employ_id`, `employ_title`, `present_or_absent`, `attend_time`) VALUES
(1, 2018, 1464991200, 1, 'Headmaster', 'Present', '02:33 pm'),
(2, 2018, 1464991200, 2, 'Helen K Helton', 'Present', '02:34 pm'),
(3, 2018, 1464991200, 6, 'Robert D. Franco', 'Present', '02:34 pm'),
(4, 2018, 1464991200, 7, 'Michael R. Kemp', 'Present', '02:34 pm'),
(5, 2018, 1464991200, 8, 'Willie B. Quint', 'Absent', ''),
(6, 2018, 1464991200, 9, 'Fredrick V. Keyes', 'Present', '02:34 pm'),
(7, 2018, 1464991200, 10, 'mumar abboud', 'Absent', ''),
(8, 2018, 1464991200, 11, 'Inayah Asfour', 'Present', '02:34 pm'),
(9, 2018, 1466287200, 1, 'Headmaster', 'Present', '11:46 pm'),
(10, 2018, 1466287200, 2, 'Helen K Helton', 'Absent', ''),
(11, 2018, 1466287200, 6, 'Robert D. Franco', 'Present', '11:47 pm'),
(12, 2018, 1466287200, 7, 'Michael R. Kemp', 'Present', '11:47 pm'),
(13, 2018, 1466287200, 8, 'Willie B. Quint', 'Absent', ''),
(14, 2018, 1466287200, 9, 'Fredrick V. Keyes', 'Present', '11:47 pm'),
(15, 2018, 1466287200, 10, 'mumar abboud', 'Absent', ''),
(16, 2018, 1466287200, 11, 'Inayah Asfour', 'Present', '11:47 pm'),
(17, 2018, 1472767200, 1, 'Headmaster', 'Absent', ''),
(18, 2018, 1472767200, 2, 'Helen K Helton', 'Absent', ''),
(19, 2018, 1472767200, 6, 'Robert D. Franco', 'Absent', ''),
(20, 2018, 1472767200, 7, 'Michael R. Kemp', 'Absent', ''),
(21, 2018, 1472767200, 8, 'Willie B. Quint', 'Absent', ''),
(22, 2018, 1472767200, 9, 'Fredrick V. Keyes', 'Absent', ''),
(23, 2018, 1472767200, 10, 'mumar abboud', 'Absent', ''),
(24, 2018, 1472767200, 11, 'Inayah Asfour', 'Absent', '');

-- --------------------------------------------------------

--
-- Table structure for table `transection`
--

CREATE TABLE `transection` (
  `id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `acco_id` int(11) NOT NULL,
  `category` varchar(10) NOT NULL,
  `amount` int(11) NOT NULL,
  `balance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transection`
--

INSERT INTO `transection` (`id`, `date`, `acco_id`, `category`, `amount`, `balance`) VALUES
(1, 1463025600, 1, 'Expense', 16200, 6200),
(2, 1475186400, 2, 'Income', 34000, 30200),
(3, 1475186400, 1, 'Expense', 10000, 10200);

-- --------------------------------------------------------

--
-- Table structure for table `transport`
--

CREATE TABLE `transport` (
  `id` int(11) NOT NULL,
  `rout_title` varchar(200) NOT NULL,
  `start_end` varchar(300) NOT NULL,
  `vicles_amount` varchar(20) NOT NULL,
  `descriptions` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transport`
--

INSERT INTO `transport` (`id`, `rout_title`, `start_end`, `vicles_amount`, `descriptions`) VALUES
(1, 'gandhi nagar', 'chinchwad to gandhi nagar', '500', 'october fees');

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE `userinfo` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `farther_name` varchar(50) NOT NULL,
  `mother_name` varchar(50) NOT NULL,
  `birth_date` varchar(15) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `present_address` varchar(200) NOT NULL,
  `permanent_address` varchar(200) NOT NULL,
  `working_hour` varchar(30) NOT NULL,
  `educational_qualification_1` varchar(200) NOT NULL,
  `educational_qualification_2` varchar(200) NOT NULL,
  `educational_qualification_3` varchar(200) NOT NULL,
  `educational_qualification_4` varchar(200) NOT NULL,
  `educational_qualification_5` varchar(200) NOT NULL,
  `users_photo` varchar(200) NOT NULL,
  `cv` varchar(30) NOT NULL,
  `educational_certificat` varchar(30) NOT NULL,
  `exprieance_certificatte` varchar(30) NOT NULL,
  `files_info` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`id`, `user_id`, `group_id`, `full_name`, `farther_name`, `mother_name`, `birth_date`, `sex`, `present_address`, `permanent_address`, `working_hour`, `educational_qualification_1`, `educational_qualification_2`, `educational_qualification_3`, `educational_qualification_4`, `educational_qualification_5`, `users_photo`, `cv`, `educational_certificat`, `exprieance_certificatte`, `files_info`, `phone`) VALUES
(1, 2, 0, 'Helen K Helton', 'Chris R. Darling', 'Stephanie T. Hinson', '31/12/1978', 'Female', '3129 Eastland Avenue\\\\\\\\nHattiesburg, MS 39402 ', '3129 Eastland Avenue\\\\\\\\nHattiesburg, MS 39402 ', 'Full time', 'SSC,Test International School,5,1998', 'HSC,Test International College,4,2000', 'BBA,Test International University,3,2004', '', '', '6e2a86d6a6645f4f52d8c92976084147.png', 'submited', 'submited', 'on', 'Employe File 2010', '6013745038'),
(2, 6, 7, 'Robert D. Franco', 'Daniel T. Rodriguez', 'Sandra A. Foster', '27/05/2016', 'Male', '384 Rogers Street\\r\\nCincinnati, OH 45202 ', '384 Rogers Street\\r\\nCincinnati, OH 45202 ', 'Full time', 'SSC,Test School,A,2010', 'HSC,Test College,A,2012', '', '', '', '6e6fb5f286e6aef4cf0d711d591dab77.png', 'submited', 'submited', 'submited', 'Employee 2016', '+88012458523546'),
(3, 7, 8, 'Michael R. Kemp', 'Brian F. Massey', 'Barbara S. Williams', '10/01/1997', 'Male', '1329 Dovetail Drive\\r\\nSchaumburg, IL 60173 ', '1329 Dovetail Drive\\r\\nSchaumburg, IL 60173 ', 'Full time', 'SSC,Test School,A,2006', 'HSC,Test College,A,2008', '', '', '', 'bd6459ab36d89b61f3b59fbec7a6546a.png', 'submited', 'submited', 'submited', 'Employee 2016', '+88012458523546');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `profile_image` varchar(100) NOT NULL,
  `user_status` varchar(100) NOT NULL,
  `leave_status` varchar(15) NOT NULL,
  `leave_start` int(11) NOT NULL,
  `leave_end` int(11) NOT NULL,
  `salary` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `phone`, `profile_image`, `user_status`, `leave_status`, `leave_start`, `leave_end`, `salary`) VALUES
(1, '127.0.0.1', 'headmaster', '$2y$10$3wcU..ldhualnrq/Rc46A.uuxOZ5qqfwrbiPxq6stEnsBxkbeRqJi', NULL, 'admin@jhamobi.com', NULL, 'HBj4C30st5pOHbjpHojzGu4667ad49e75655b131', 1420113369, 'IcD7gVAwU5DDX4jTuWOVXe', 1268889823, 1669874897, 1, 'headmaster', '', '1234567890', 'admin.png', 'Employee', 'Available', 1447196400, 1449874800, 1),
(2, '103.19.253.234', 'Helen K Helton', '$2y$10$3wcU..ldhualnrq/Rc46A.uuxOZ5qqfwrbiPxq6stEnsBxkbeRqJi', NULL, 'accountent@accountent.com', NULL, NULL, NULL, NULL, 1460832200, 1650530504, 1, 'Helen K', 'Helton', '6013745038', '6e2a86d6a6645f4f52d8c92976084147.png', 'Employee', 'Available', 0, 0, 0),
(4, '163.53.151.114', 'Benjamin D. Lampe', '$2y$10$3wcU..ldhualnrq/Rc46A.uuxOZ5qqfwrbiPxq6stEnsBxkbeRqJi', NULL, 'student@student.com', NULL, NULL, NULL, NULL, 1462440816, 1657176016, 1, 'Benjamin', 'D. Lampe', '763496-3372', 'f1829afa3cdbed1c643b000b1ea6ab02.jpg', '', '', 0, 0, 0),
(6, '::1', 'Robert D. Franco', '$2y$10$3wcU..ldhualnrq/Rc46A.uuxOZ5qqfwrbiPxq6stEnsBxkbeRqJi', NULL, 'librayan@librayan.com', NULL, NULL, NULL, NULL, 1464371447, 1476255999, 1, 'Robert D.', 'Franco', '+88012458523546', '6e6fb5f286e6aef4cf0d711d591dab77.png', 'Employee', 'Available', 0, 0, 0),
(7, '::1', 'Michael R. Kemp', '$2y$10$3wcU..ldhualnrq/Rc46A.uuxOZ5qqfwrbiPxq6stEnsBxkbeRqJi', NULL, 'driver@driver.com', NULL, NULL, NULL, NULL, 1464410002, 1464410002, 1, 'Michael R.', 'Kemp', '+88012458523546', 'bd6459ab36d89b61f3b59fbec7a6546a.png', 'Employee', 'Available', 0, 0, 0),
(8, '::1', 'willie b. quinttt', '$2y$10$3wcU..ldhualnrq/Rc46A.uuxOZ5qqfwrbiPxq6stEnsBxkbeRqJi', NULL, 'teacher@teacher.com', NULL, NULL, NULL, NULL, 1464418997, 1648544859, 1, 'Willie B.', 'Quinttt', '+8801245852315', 'ac197cff181d9c58027800912c3e0855.png', 'Employee', 'Available', 0, 0, 0),
(9, '::1', 'fredrick vbb keyes', '$2y$08$L9r7/FCRlUzu9utQNUMBdOQLdmhd1zru9dmIh9TQKgxkkLhvuLQwS', NULL, 'fredrickvkeyes@inbound.plus', NULL, NULL, NULL, NULL, 1464419454, 1465590774, 1, 'Fredrick Vbb', 'Keyes', '+8801245852315', '0f8649af98ca9c0e85957db7e9778191.png', 'Employee', 'Available', 0, 0, 0),
(10, '::1', 'mumar abboud', '$2y$08$VNNakNjro8sKwpMpd5yoiOAkYMirUZR3Ijhg68Pbi/vl5wRr/zaAW', NULL, 'abboud@inbound.com', NULL, NULL, NULL, NULL, 1464421270, 1464421270, 1, 'Mumar', 'Abboud', '+8801245852315', '3566d34fc7ce565ba8841d0eaf537b28.png', 'Employee', 'Available', 0, 0, 0),
(12, '::1', 'Rahim Hasan', '$2y$08$wzEhfWmcCIHnZncAqgGqm.7Own9JW5nJ3bm4zOwAziW2nLf/6zFq2', NULL, 'rahim@rahim.com', NULL, NULL, NULL, NULL, 1464523831, 1464523831, 1, 'Rahim', 'Hasan', '+8801245852315', '21e39ae6c4f966a8657d11d36084b8fc.jpg', '', '', 0, 0, 0),
(13, '::1', 'Junayed Hak', '$2y$08$Rn2z8QDqcPHSUqdOK5qAlu.6f.9TklC2iAVrcaL/yRsuAZ4ra/vMu', NULL, 'junayed@junayed.com', NULL, NULL, NULL, NULL, 1464524017, 1464524017, 1, 'Junayed', 'Hak', '+8801245852315', '7667367fd4bded46db7e2ee3010f987f.jpg', '', '', 0, 0, 0),
(14, '::1', 'John E. Williams  Deidra D. Shaw ', '$2y$08$PILzSWooOdWWMwuVZNGzlePvMbAPCypnCKC1nwLKqP2f9AW8Tfiym', NULL, 'parents@parents.com', NULL, NULL, NULL, NULL, 1464543329, 1476249922, 1, 'John E. Williams ', 'Deidra D. Shaw ', '+8801245852315', '381700c24030d799489320dd5b734c3a.png', '', '', 0, 0, 0),
(15, '::1', 'Jafor Uddin Julakha Begum', '$2y$08$u5gaDPimVAF0JRswU5nvP.6iRYNNyJbyoJhiKVq4.DaSM3GW/i/2W', NULL, 'jafor@jafor.com', NULL, NULL, NULL, NULL, 1464543517, 1464543517, 1, 'Jafor Uddin', 'Julakha Begum', '+8801245852315', '96d721209bf9c71dc6c6cb3902c12b89.png', '', '', 0, 0, 0),
(16, '::1', 'Razia Akture', '$2y$08$n0btDbZk997d8XQKJAAiKuoYkB664EauFFJIjsMbH1NLTFX5Xwmjy', NULL, 'razia@razia.com', NULL, NULL, NULL, NULL, 1464845013, 1464845013, 1, 'Razia', 'Akture', '+8801245852315', 'f4498b9e3c9d2f933e4026cb0801105c.jpg', '', '', 0, 0, 0),
(17, '::1', 'Abdullah  hossain', '$2y$08$147fSivwLuSVNNGgiITlTuacM4V.RVngX3UjFNQ5GrvIz1nj9/hQq', NULL, 'abdullah@abdullah.com', NULL, NULL, NULL, NULL, 1464845456, 1464845456, 1, 'Abdullah', ' hossain', '+88012458523546', '2408f2dc2b3af87133555dab5815f027.jpg', '', '', 0, 0, 0),
(20, '::1', 'Mahbuba Akter', '$2y$08$gyI/8LRrr4EO8CuZP16K.uoTTj0MaUnZ3IfUrXEqu/funYC3Fxm3G', NULL, 'mahbuba@mahbuba.com', NULL, NULL, NULL, NULL, 1464846366, 1464846366, 1, 'Mahbuba', 'Akter', '+8801245852354', 'aa3fab97275a0a85652bfe7fbfa91666.jpg', '', '', 0, 0, 0),
(21, '::1', 'Irfan Hossain', '$2y$08$1kM0mGEaaGtyPG2V7AdB9eUz1PUniNbNp.zAf8DgWYiblteRTj8OC', NULL, 'irfan@irfan.com', NULL, NULL, NULL, NULL, 1464846640, 1464846640, 1, 'Irfan', 'Hossain', '+88012458578945', '50875054077eb4436e78b8f2796761a8.jpg', '', '', 0, 0, 0),
(22, '::1', 'Imran Hasan', '$2y$08$DGE9z7S3rBe9xERGEGXMGOtmTdhJxq7t4PLHBW79OlKTYMf0EUwOa', NULL, 'imran@imran.com', NULL, NULL, NULL, NULL, 1464847183, 1464847183, 1, 'Imran', 'Hasan', '+8801245854521', '9e01047b759770110ed29b254db908d7.jpg', '', '', 0, 0, 0),
(23, '::1', 'Polash Sarder', '$2y$08$kv6XLhcRJt/CYCm9FAv.YeVukijbEE89EE2GDoVN6bwIPGnA/vAR.', NULL, 'polash@polash.com', NULL, NULL, NULL, NULL, 1464847571, 1464847571, 1, 'Polash', 'Sarder', '+88012458529632', '41e56e4c5efe83fa80966da3ea23c98f.jpg', '', '', 0, 0, 0),
(24, '::1', 'Sumon Akon', '$2y$08$Q7ip./kPwq3Lq2IZlvjgw.kNOLjQptLMTFSObrfvckzYFE9RvoiXC', NULL, 'julhash@julhash.com', NULL, NULL, NULL, NULL, 1464848198, 1464848198, 1, 'Sumon', 'Akon', '+8801245852385', '8337f3befe383f4770df79b1c01cc8ae.jpg', '', '', 0, 0, 0),
(25, '::1', 'Farjana Akter', '$2y$08$7kfUaE9rgSWVXWTgu9eDceTqXhAhPhcgse4bjgoz/NKS5lAmXalWO', NULL, 'farjana@farjana.com', NULL, NULL, NULL, NULL, 1464848494, 1464848494, 1, 'Farjana', 'Akter', '+88012458527412', 'c2da4f488e8b188bf8cc3bf1a2049b2f.png', '', '', 0, 0, 0),
(26, '::1', 'Akram Hossain', '$2y$08$Zh7ycj5rxqwXQroYDArGkeGXYGuYAI4WRLPkSJJ1icjz0QdQq.h9q', NULL, 'akram@akramn.com', NULL, NULL, NULL, NULL, 1464849008, 1464849008, 1, 'Akram', 'Hossain', '+88012458529632', '58b17c784b9f7eca12209969b01bc0a9.jpg', '', '', 0, 0, 0),
(27, '::1', 'Alamin Saeder', '$2y$08$G.uY4OQzAmysV7E5Efj9/u.Yp5U7s9rjifjYYpqqeyij8R/jEr7Aq', NULL, 'alamin@alamin.com', NULL, NULL, NULL, NULL, 1464849538, 1464849538, 1, 'Alamin', 'Saeder', '+880+880124585231', '1fa9258bf0ffdc9ebe01d17154c25e56.jpg', '', '', 0, 0, 0),
(28, '::1', 'Sabina Sumi', '$2y$08$es7XiUiPiPORCL2TZy6etOXOEWsADSNmKMe1QZjrKo/SBwkyrn74a', NULL, 'sabina@sabina.com', NULL, NULL, NULL, NULL, 1464850013, 1464850013, 1, 'Sabina', 'Sumi', '+88012458523546', 'e0237b45f347083352f7cd500286914f.jpg', '', '', 0, 0, 0),
(29, '::1', 'Sanjida Hossain', '$2y$08$GZ14P2ZiZZsn95oqJjYyVeYobTl4wPIBW80o./VxP9VuRiibKoJS2', NULL, 'sanjida@sanjida.com', NULL, NULL, NULL, NULL, 1464850675, 1464850675, 1, 'Sanjida', 'Hossain', '+8801245852354', 'e2cee6b429e9423c627d8a34027ca44c.jpg', '', '', 0, 0, 0),
(30, '::1', 'Kawser  Shikder', '$2y$08$PlTRvMrvbtdYPJ7YfBdsS.nsx6Ret6fyb3UWYMzQ2KVHo3ysAA3g2', NULL, 'kawser@kawser.com', NULL, NULL, NULL, NULL, 1464851021, 1464851021, 1, 'Kawser', ' Shikder', '+880124585784521', 'e63eb2b5e250aa6b773c795054dea21a.jpg', '', '', 0, 0, 0),
(31, '::1', 'Shohana Akter', '$2y$08$Mc46am1HSrpmzhyo5a6bo.K4/ShuiJC2Ub0yLBpMHtITDGsBECcAG', NULL, 'shohana@shohana.com', NULL, NULL, NULL, NULL, 1464851240, 1464851240, 1, 'Shohana', 'Akter', '+88012458523532', '9e8641a879671c2b4a31e77c197a9d3f.jpg', '', '', 0, 0, 0),
(32, '::1', 'Juthi Khanam', '$2y$08$QneLBSPZAKN/U3HuDfVaWOYAEARd5A0tg2TblW.v1zCperCZdXvEK', NULL, 'juthi@juthi.com', NULL, NULL, NULL, NULL, 1464851435, 1464851435, 1, 'Juthi', 'Khanam', '+88012458578478541', '5c99d031c2aaa455c46b467194ebe04f.jpg', '', '', 0, 0, 0),
(33, '::1', 'Tanjila Akter', '$2y$08$BLnthpFLflyaPNBNWfjseOiK3x6wO2tuotSI5d8dz2SOm4QfJBfYe', NULL, 'tanjila@tanjila.com', NULL, NULL, NULL, NULL, 1464875715, 1464875715, 1, 'Tanjila', 'Akter', '+88012458524521', '88b05c84d9940f9827b4f020bffb46df.jpg', '', '', 0, 0, 0),
(34, '::1', 'Nusrat Jahan', '$2y$08$7NK58SXiWYJqYYXZ07zLkO8O3b/RM4B.vIAKW82MD07t7sUFffaPS', NULL, 'nusrat@nusrat.com', NULL, NULL, NULL, NULL, 1464875957, 1464875957, 1, 'Nusrat', 'Jahan', '+88012458527452', 'ea1e8ed789f4d1f4a5780a31a075e9a8.jpg', '', '', 0, 0, 0),
(35, '::1', 'Amina Akter', '$2y$08$UZupiBsejH37dLxkYpXv4OEF1mJaGTvSxZ6tf.XigdzjJwZ3U6/H2', NULL, 'amina@amina.com', NULL, NULL, NULL, NULL, 1464877185, 1464877185, 1, 'Amina', 'Akter', '+880124585236521', 'bf45f3417020caab83136fc790a837a4.jpg', '', '', 0, 0, 0),
(36, '::1', 'Ebrahim Khondokar', '$2y$08$xgtkYt4lMgpXXFOOlajcRebiI5EAkJWUy6IBLtZIZgZwAzwjtF3xe', NULL, 'ebrahim@ebrahim.com', NULL, NULL, NULL, NULL, 1464877671, 1464877671, 1, 'Ebrahim', 'Khondokar', '+88012458451397', 'd1ccdd70ebb675f762aa6de55ecc5927.jpg', '', '', 0, 0, 0),
(37, '::1', 'Mintu  Fokir', '$2y$08$b3TqIITatEpTZkCbdpprQeSdTx3CttfFrdDqZYktkrSk/DnIflJLu', NULL, 'mintu@mintu.com', NULL, NULL, NULL, NULL, 1464877919, 1464877919, 1, 'Mintu ', 'Fokir', '+880+8801245789', 'efae644abc3a8621ba3703b861c5117d.jpg', '', '', 0, 0, 0),
(38, '::1', 'Shohid Islam', '$2y$08$zbtQsdM7dkj3947YVvge9.bI.8Lqy19lcGAdFZjxN9NtgPxCsUrka', NULL, 'shohid@shohid.com', NULL, NULL, NULL, NULL, 1464879564, 1464879564, 1, 'Shohid', 'Islam', '+88012458529632', '840aa12c41963600fd7993e55aa7d7ab.jpg', '', '', 0, 0, 0),
(39, '::1', 'Khadija Akter', '$2y$08$7hmmafAM8gcBt9wkqxSbueCRPrHO/mUPJOt.WaGKo7Ync6WeMJ2Eq', NULL, 'khadija@khadija.com', NULL, NULL, NULL, NULL, 1464879732, 1464879732, 1, 'Khadija', 'Akter', '+88012458578478541', '2d0f6d650516b7d5f2c958ed73c88a22.jpg', '', '', 0, 0, 0),
(40, '::1', 'Maruf Hossain', '$2y$08$EwLh2n9Cop2M547YQTvtQOlt3xPEcRLYov16rGyJAxzGpNrsqu.fu', NULL, 'maruf@maruf.com', NULL, NULL, NULL, NULL, 1464880270, 1464880270, 1, 'Maruf', 'Hossain', '+88012458523546', '5149696a702583dfa0f89ab3e77e4057.jpg', '', '', 0, 0, 0),
(41, '::1', 'Mitu  Akter', '$2y$08$01.eZKT4HgziWSYp2arNbuO5lv0mmt3ySROXk/GP4lTMLTbI3BZhe', NULL, 'mitu@mitu.com', NULL, NULL, NULL, NULL, 1464880835, 1464880835, 1, 'Mitu ', 'Akter', '+880+8801245789', 'c71ff3b9a9436344ffd8dc8bc5b57856.jpg', '', '', 0, 0, 0),
(42, '::1', 'Rayhan  Kebria', '$2y$08$lY5fbvbsrcRmHRRFGAULju7.Bv5CizGDnSoFyk7/G40EF4.Ssg.hy', NULL, 'rayhan@rayhan.com', NULL, NULL, NULL, NULL, 1464881271, 1464881271, 1, 'Rayhan ', 'Kebria', '+880+8801245789', '6cd5fd41b32dd2cffaa1a397bfeb8fd1.jpg', '', '', 0, 0, 0),
(44, '::1', 'Airin Akter', '$2y$08$vwOLZ3iDXgLMd8h/5nol5eRVXsffnChVvr2svpW9M3FSekFTQhEKO', NULL, 'airin@airin.com', NULL, NULL, NULL, NULL, 1465232118, 1465232118, 1, 'Airin', 'Akter', '+880+88012454521', '4196bf4c30a4f94165fd82bfa98980d4.jpg', '', '', 0, 0, 0),
(45, '::1', 'Hemel Hossain', '$2y$08$WEeMR9DUWabbl.OsMj/IhOFHz/97zHB/9ChTi7/Q8NyYg3psS6pdy', NULL, 'hemel@hemel.com', NULL, NULL, NULL, NULL, 1465232452, 1465232452, 1, 'Hemel', 'Hossain', '+8801245852354', '855cfe1befcd31038add98bb6605c227.jpg', '', '', 0, 0, 0),
(46, '::1', 'Mou Akter', '$2y$08$HazDgna9I.Oko0yy39DDVu8K8dW04X22gMdKGBoyk5HFli9fgYM0.', NULL, 'mou@mou.com', NULL, NULL, NULL, NULL, 1465232764, 1465232764, 1, 'Mou', 'Akter', '+880+88012457521', '551bcf2eddf4aa49d629cadf543d7e33.jpg', '', '', 0, 0, 0),
(47, '::1', 'Dedar  Hossain', '$2y$08$Roieo2U7BvS879vVJ26LsOUxWan/mqulrGCjGR9QwnY7HhOOTQzQ2', NULL, 'dedar@dedar.com', NULL, NULL, NULL, NULL, 1465232995, 1465232995, 1, 'Dedar ', 'Hossain', '+880+88012478214', 'd8d9124d3a3f01e59538995a499a600a.jpg', '', '', 0, 0, 0),
(48, '::1', 'Samia Akter', '$2y$08$LXMgXm9JUcDLuMjNWKJB8ORNhABddQ4/NOUZa5SLQbd8gVSkGEeC2', NULL, 'samia@samia.com', NULL, NULL, NULL, NULL, 1465233355, 1465233355, 1, 'Samia', 'Akter', '+880+8801241020', '992d1504661e0f1a6fb7f1b87da0b8be.jpg', '', '', 0, 0, 0),
(49, '::1', 'Al Mamun', '$2y$08$rkBs.snMZ/zDUed8Pt1LgeRt4GRYJE3c6SCE1qBJtfuiFjO5JygR.', NULL, 'mamun@mamun.com', NULL, NULL, NULL, NULL, 1465233538, 1465233538, 1, 'Al', 'Mamun', '+880+8801245854210', '637e0e2d01bf463d9791cc77cac00485.jpg', '', '', 0, 0, 0),
(50, '::1', 'Jiniya Hoq', '$2y$08$Z1JAJ.CnEnt1XM.7aNW3YeUHuMH5TLGW0nDVnCJ5bmVvh/nPIjgh6', NULL, 'jiniya@jiniya.com', NULL, NULL, NULL, NULL, 1465233738, 1465233738, 1, 'Jiniya', 'Hoq', '+8801245852354', '4e211b4b8cedfa67d84414d5ae412c48.jpg', '', '', 0, 0, 0),
(51, '::1', 'Junayed Ahmed', '$2y$08$nBhsj/9wBPdBFbP1g2VhxeChN2jB7ixs3Q9ULn9p1mi1nkQ4ChfU6', NULL, 'junayed@junayed09.com', NULL, NULL, NULL, NULL, 1465235475, 1465235475, 1, 'Junayed', 'Ahmed', '+880+880124585231', '8d37adbd2454c51222d14936cbb3f125.jpg', '', '', 0, 0, 0),
(52, '::1', 'Priya Ahmed', '$2y$08$7Z95o70V64sKc/8EQ/ZskOA2Fx/wO0nSfKSlcZwRJ6K8UzhnrWorS', NULL, 'priya@priya.com', NULL, NULL, NULL, NULL, 1465235730, 1465235730, 1, 'Priya', 'Ahmed', '+880+8801245852541', 'e7b45f5e5a62e632ba6bb7d504d25299.jpg', '', '', 0, 0, 0),
(53, '::1', 'Mithu Khondokar', '$2y$08$95cLc.PFzQI61Z/OpHbLg.KT0XcIg4MEY9HhXi1wzo7ixHNjp/a1y', NULL, 'mithu@mithu.com', NULL, NULL, NULL, NULL, 1465236023, 1465236023, 1, 'Mithu', 'Khondokar', '+8801245852354', 'e3bc44fb33852bf16b2387800debd943.jpg', '', '', 0, 0, 0),
(54, '::1', 'Rasel Hossain', '$2y$08$/VLCV2D3DgBptLsJ6PCufOYn1ZEuv8fEJ1NyStRvLCG4PJyMAHjfO', NULL, 'rasel@rasel.com', NULL, NULL, NULL, NULL, 1465236257, 1465236257, 1, 'Rasel', 'Hossain', '+880+88012458527854', '061d3b9631a8a0d7acddae6efa0106ae.jpg', '', '', 0, 0, 0),
(55, '::1', 'Test Name', '$2y$08$IKl/v5tDDJHFnn7rWrSsCOHMX6J5oTf9cw8yKeZa2i1wJoqnHbLOi', NULL, 'abc@abc.com', NULL, NULL, NULL, NULL, 1475243042, 1475243042, 1, 'Test', 'Name', '+88001245885', '874c4c955d8043a1205ccb2d388b927f.png', '', '', 0, 0, 0),
(56, '::1', 'Test Name1', '$2y$08$Bhzm5MF0BXB4HvIgmqRa/O2lgxAW8Pgua65dk9ejkdwdQ91ZL8lEy', NULL, 'abcd@abcd.com', NULL, NULL, NULL, NULL, 1475910937, 1475910937, 1, 'Test', 'Name', '+88001245885', '48f0d67ff796756b0efa6109727eb2fc.jpg', '', '', 0, 0, 0),
(57, '::1', 't1 ', '$2y$08$Uqn9RG8fwBl3.Q3M3MYgOu/Ow.C.yu6KtqRbVQwozlxauRGCeVGKC', NULL, 't1@admin.com', NULL, NULL, NULL, NULL, 1632375987, 1632392215, 1, 't1', '', '919999999999', '', '', '', 0, 0, 0),
(58, '::1', 't2 t2', '$2y$08$j9tPuFsobVDaNytfI0rufeD158uY4oZHpXr1QfrgVK53dhSMSh4Ju', NULL, 't2@admin.com', NULL, NULL, NULL, NULL, 1632394524, 1632394524, 1, 't2', 't2', '9999999999', '', '', '', 0, 0, 0),
(59, '::1', 't3 t3', '$2y$08$p8bepvdTJ75qRC9iaQ/oBOMTL.GlLtZ/eWtMgKGz68ct83Ddkj8c.', NULL, 't3@admin.com', NULL, NULL, NULL, NULL, 1632806510, 1632806510, 1, 't3', 't3', '9999999900', '', '', '', 0, 0, 0),
(60, '::1', 'p3 p3', '$2y$08$NR0R8Rq4i97KmJdWilVDu.Ic9hAizk14duqHTKZg2vCLbQt2D7Brq', NULL, 't3f@admin.com', NULL, NULL, NULL, NULL, 1632806510, 1632806755, 1, 'p3', 'p3', '9999999990', '', '', '', 0, 0, 0),
(61, '::1', 't4 t4', '$2y$08$Z79nZWtYjE4wJQm5pQCKQOTBhDNEILo5pd9Rxm2poJvDBb8WYtibe', NULL, 't4@admin.com', NULL, NULL, NULL, NULL, 1632806511, 1632806511, 1, 't4', 't4', '9999999911', '', '', '', 0, 0, 0),
(62, '::1', 'p4 p4', '$2y$08$Qs9oJ3rbWPfQPHc65btZaurjs3eEsm.hLe/pdDRJaLz0SBma950oK', NULL, 't4f@admin.com', NULL, NULL, NULL, NULL, 1632806511, 1652810014, 1, 'p4', 'p4', '9999999991', '', '', '', 0, 0, 0),
(63, '::1', 'p5 p5', '$2y$08$saWahkxE.TTO3Ia5VXRQCuDsvsLi.AR7oTD7nyj3sAhCTc.luQWfG', NULL, 't5f@admin.com', NULL, NULL, NULL, NULL, 1632913095, 1632913095, 1, 'p5', 'p5', '9999999905', '', '', '', 0, 0, 0),
(64, '::1', 't7 t7', '$2y$08$7IhgCAH.BJ/ZWyZ2JLDImeOjOagwXelWDk3zimc5ewkvfj3v6UH0q', NULL, 't7f@admin.com', NULL, NULL, NULL, NULL, 1633006670, 1652782604, 1, 't7', 't7', '9999999907', '', '', '', 0, 0, 0),
(65, '::1', 'pavan patil', '$2y$08$H5RIp.cYUK9jS3PGKuNSaOrKoptcG1vGpF.GARWXI4FpD3hIaN4l.', NULL, 'pavan@admin.com', NULL, NULL, NULL, NULL, 1639132809, 1639132809, 1, 'pavan', 'patil', '9177091470', '', '', '', 0, 0, 0),
(66, '::1', 'jha mobi', '$2y$08$mzBzZ3THasY3MiUfO2FDaerE3QmaiyumlKWvasoeAI7kzHCxCx1HC', NULL, 'jha@admin.com', NULL, NULL, NULL, 'pZurx1qRG.UPA27oKj7vt.', 1639372773, 1652809817, 1, 'jha', 'mobi', '919665341899', '', '', '', 0, 0, 0),
(67, '::1', 'urvi mule', '$2y$08$YymdANyqn1id.2GI9mYHmOxmTNOEWEdzZl8nh07YEvXNluL0le7Em', NULL, 'xxx@gmail.com', NULL, NULL, NULL, NULL, 1648197515, 1652265078, 1, 'urvi', 'mule', '08877667876', 'e0295a9728c85ef227e27fa61c2064b3.jpg', '', '', 0, 0, 0),
(69, '::1', 'swati', '$2y$08$8.uMEMu/olHGkMOs/wguC.K1I7L1ZbmR.mUgkuoeb7jSZGMNb1Xyu', NULL, 'demo@gmail.com', NULL, NULL, NULL, NULL, 1649679898, 1652809757, 1, 'sss', 'bbb', '08877667788', '422f0a1e5d2a327bc97d5b5b7d1fc5ab.jpg', '', '', 0, 0, 0),
(70, '::1', 'test testing', '$2y$08$kOlNvh3kePmaSpUmrpqf.Ok6u1nHmdVHS2XFgxZcv6KwCsnxhJ6Ty', NULL, 'test@gmail.com', NULL, NULL, NULL, NULL, 1649680596, 1650530641, 1, 'test', 'testing', '+915555666688', '4e50e7e9a7044e268f480c13c20048c0.jpg', '', '', 0, 0, 0),
(71, '::1', 'swati testing', '$2y$08$kf/xHWVYPw0tXrqh0OxNbesty6.qvp3k8DFzIzHYK7juEaKE/Zsli', NULL, 'ad11@gmail.com', NULL, NULL, NULL, NULL, 1649749834, 1652178003, 1, 'swati', 'testing', '+914466777890', '0eb6f8769a1ce827186198e9677402ea.jpg', '', '', 0, 0, 0),
(72, '::1', 'Avni Belure', '$2y$08$53fjG97mk.ntGPRKpgjtMOBE7QXRV3rtGOmtzhHfACL4ucmnjqD4a', NULL, 'avni@gmail.com', NULL, NULL, NULL, NULL, 1652783576, 1663334184, 1, 'Avni', 'Belure', '+917766889956', '08b42f6f18f62e063b5521bb28bda007.jpg', '', '', 0, 0, 0),
(73, '::1', 'Bhargavi Belure', '$2y$08$tcetsWdm2OIL93jCPV96VOqIMXp.qaHbjaXfgJkL5ILjW3yP6dJnW', NULL, 'bhargi@gmail.com', NULL, NULL, NULL, NULL, 1652786936, 1652786936, 1, 'Bhargavi', 'Belure', '+919888099770', '6ac718caae2b9bc73bd1e63047e5a638.jpg', '', '', 0, 0, 0),
(74, '::1', 'Bhargavi belure', '$2y$08$0GfMJYK.VtqgKHn9fz0n1eylFW9IaH2N9OqQOYi0wEojF2i1jfP0y', NULL, 'bhargavi@gmail.com', NULL, NULL, NULL, NULL, 1652787289, 1663326792, 1, 'Bhargavi', 'belure', '+919988990083', 'e625f4e1f0d0c984b7d8c3c9f0e42904.jpg', '', '', 0, 0, 0),
(75, '::1', 'vinod mule', '$2y$08$x9A.ImRWGnsmtpLcssS2UuAupy038beEu4Ml9gqMUfutHvZvTI6Wm', NULL, 'vvv@gmail.com', NULL, NULL, NULL, NULL, 1661764060, 1661764060, 1, 'vinod', 'mule', '+919856278956', '', '', '', 0, 0, 0),
(76, '::1', 'pushpa choudhary', '$2y$08$UQMn836mG8HchRkAlNbgO.hJE.e6uSjhIiS30P4IiXwldoqeRkyRe', NULL, 'ppp@gmail.com', NULL, NULL, NULL, NULL, 1661765071, 1661765071, 1, 'pushpa', 'choudhary', '+919988856789', '', '', '', 0, 0, 0),
(77, '::1', 'sonakshi bhosale', '$2y$08$Et289EPIun13Dxj2BG6WZeSFvB2ye66ILMnupBN1wKv.tPTf1JLMW', NULL, 'sona@gmail.com', NULL, NULL, NULL, NULL, 1661767476, 1669274621, 1, 'sonakshi', 'bhosale', '+919895668956', '15237c7c8da82e51ac5115c6c62d39c8.png', '', '', 0, 0, 0),
(78, '::1', 'testswati sss', '$2y$08$c0dEL4UUlgn8HEQCkcwl4eRJI/onpOiB8K445OzDGsb7blZOhqT6O', NULL, 'sm@gmail.com', NULL, NULL, NULL, NULL, 1668159041, 1668159041, 1, 'testswati', 'sss', '', '', '', '', 0, 0, 0),
(79, '::1', 'testswati bbb', '$2y$08$pWK4MFWcBcsC3LeH.gft..hBN4bSC0ToMFUawIiy4RWCDBSGQq7jK', NULL, 'sss@gmail.com', NULL, NULL, NULL, NULL, 1668159178, 1668176249, 1, 'testswati', 'bbb', '', '', '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(2, 2, 6),
(4, 4, 3),
(6, 6, 7),
(7, 7, 8),
(8, 8, 4),
(9, 9, 4),
(10, 10, 4),
(12, 12, 3),
(13, 13, 3),
(14, 14, 5),
(15, 15, 5),
(16, 16, 3),
(17, 17, 3),
(20, 20, 3),
(21, 21, 3),
(22, 22, 3),
(23, 23, 3),
(24, 24, 3),
(25, 25, 3),
(26, 26, 3),
(27, 27, 3),
(28, 28, 3),
(29, 29, 3),
(30, 30, 3),
(31, 31, 3),
(32, 32, 3),
(33, 33, 3),
(34, 34, 3),
(35, 35, 3),
(36, 36, 3),
(37, 37, 3),
(38, 38, 3),
(39, 39, 3),
(40, 40, 3),
(41, 41, 3),
(42, 42, 3),
(44, 44, 3),
(45, 45, 3),
(46, 46, 3),
(47, 47, 3),
(48, 48, 3),
(49, 49, 3),
(50, 50, 3),
(51, 51, 3),
(52, 52, 3),
(53, 53, 3),
(54, 54, 3),
(55, 55, 3),
(56, 56, 3),
(57, 57, 3),
(58, 58, 3),
(59, 59, 3),
(60, 60, 5),
(61, 61, 3),
(62, 62, 5),
(63, 63, 5),
(64, 64, 5),
(65, 65, 3),
(66, 66, 3),
(67, 67, 3),
(69, 69, 3),
(70, 70, 3),
(71, 71, 3),
(72, 72, 3),
(73, 73, 3),
(74, 74, 3),
(75, 75, 3),
(76, 76, 3),
(77, 77, 3),
(78, 78, 3),
(79, 79, 3);

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `company_phone` varchar(15) NOT NULL,
  `company_email` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `cp_name` varchar(150) NOT NULL,
  `cp_address` varchar(200) NOT NULL,
  `cp_phone` varchar(15) NOT NULL,
  `bank_name` varchar(150) NOT NULL,
  `branch_name` varchar(15) NOT NULL,
  `account_no` varchar(30) NOT NULL,
  `ifsc_code` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `company_name`, `company_phone`, `company_email`, `country`, `state`, `city`, `cp_name`, `cp_address`, `cp_phone`, `bank_name`, `branch_name`, `account_no`, `ifsc_code`) VALUES
(1, 'Roize Ltd.', '01245367', 'test@gmail.com', 'Bangladesh', '', 'Dhaka', 'Roize Uddin', 'Contact,person,test address', '11223344556', 'DBBL LTD.', 'Gulshan1', '123456789425214', ''),
(2, 'Moin Group Ltd.', '01245367', 'moin@gmail.com', 'Bangladesh', '', 'Dhaka', 'Moin Mia', 'Contact,person,test address', '11223344556', 'DBBL LTD.', 'Gulshan1', '123456789425214', ''),
(3, 'Abu Daout', '01245367', 'daout@gmail.com', 'Bangladesh', '', 'Dhaka', 'Moin Mia', 'Contact,person,test address', '11223344556', 'DBBL LTD.', 'Gulshan1', '123456789425214', ''),
(4, 'Bablu Furniture Ltd.', '012453671425', 'bablu@gmail.com', 'Bangladesh', '', 'Dhaka', 'Babul', 'Contact,person,test address', '11223344556', 'DBBL LTD.', 'Gulshan1', '123456789425214', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_title`
--
ALTER TABLE `account_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_exam`
--
ALTER TABLE `add_exam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_details`
--
ALTER TABLE `bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books_category`
--
ALTER TABLE `books_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calender_events`
--
ALTER TABLE `calender_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_routine`
--
ALTER TABLE `class_routine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_students`
--
ALTER TABLE `class_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_subject`
--
ALTER TABLE `class_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config_week_day`
--
ALTER TABLE `config_week_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_attendance`
--
ALTER TABLE `daily_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dormitory`
--
ALTER TABLE `dormitory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dormitory_bed`
--
ALTER TABLE `dormitory_bed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dormitory_room`
--
ALTER TABLE `dormitory_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiry_tbl`
--
ALTER TABLE `enquiry_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_attendanc`
--
ALTER TABLE `exam_attendanc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_grade`
--
ALTER TABLE `exam_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_routine`
--
ALTER TABLE `exam_routine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fees_type_master`
--
ALTER TABLE `fees_type_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_item`
--
ALTER TABLE `fee_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_payment_record`
--
ALTER TABLE `fee_payment_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_scheme`
--
ALTER TABLE `fee_scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_scheme_installment`
--
ALTER TABLE `fee_scheme_installment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `final_result`
--
ALTER TABLE `final_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `installment_master`
--
ALTER TABLE `installment_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inven_category`
--
ALTER TABLE `inven_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inve_item`
--
ALTER TABLE `inve_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issu_item`
--
ALTER TABLE `issu_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead`
--
ALTER TABLE `lead`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_activity`
--
ALTER TABLE `lead_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_history`
--
ALTER TABLE `lead_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_location`
--
ALTER TABLE `lead_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_source`
--
ALTER TABLE `lead_source`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_stage`
--
ALTER TABLE `lead_stage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_application`
--
ALTER TABLE `leave_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `library_member`
--
ALTER TABLE `library_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `massage`
--
ALTER TABLE `massage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nack_table`
--
ALTER TABLE `nack_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice_board`
--
ALTER TABLE `notice_board`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parents_info`
--
ALTER TABLE `parents_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration_history`
--
ALTER TABLE `registration_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration_student`
--
ALTER TABLE `registration_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result_action`
--
ALTER TABLE `result_action`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result_shit`
--
ALTER TABLE `result_shit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result_submition_info`
--
ALTER TABLE `result_submition_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_based_access`
--
ALTER TABLE `role_based_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institute`
--
ALTER TABLE `institute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `set_salary`
--
ALTER TABLE `set_salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slip`
--
ALTER TABLE `slip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_info`
--
ALTER TABLE `student_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects_mark`
--
ALTER TABLE `subjects_mark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suggestion`
--
ALTER TABLE `suggestion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_master`
--
ALTER TABLE `tax_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers_info`
--
ALTER TABLE `teachers_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_attendance`
--
ALTER TABLE `teacher_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transection`
--
ALTER TABLE `transection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transport`
--
ALTER TABLE `transport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_title`
--
ALTER TABLE `account_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `add_exam`
--
ALTER TABLE `add_exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bank_details`
--
ALTER TABLE `bank_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `books_category`
--
ALTER TABLE `books_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `calender_events`
--
ALTER TABLE `calender_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `class_routine`
--
ALTER TABLE `class_routine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `class_students`
--
ALTER TABLE `class_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `class_subject`
--
ALTER TABLE `class_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `config_week_day`
--
ALTER TABLE `config_week_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `daily_attendance`
--
ALTER TABLE `daily_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `dormitory`
--
ALTER TABLE `dormitory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `dormitory_bed`
--
ALTER TABLE `dormitory_bed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `dormitory_room`
--
ALTER TABLE `dormitory_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1241;

--
-- AUTO_INCREMENT for table `employe`
--
ALTER TABLE `employe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enquiry_tbl`
--
ALTER TABLE `enquiry_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `exam_attendanc`
--
ALTER TABLE `exam_attendanc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `exam_grade`
--
ALTER TABLE `exam_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `exam_routine`
--
ALTER TABLE `exam_routine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fees_type_master`
--
ALTER TABLE `fees_type_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `fee_item`
--
ALTER TABLE `fee_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fee_payment_record`
--
ALTER TABLE `fee_payment_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `fee_scheme`
--
ALTER TABLE `fee_scheme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `fee_scheme_installment`
--
ALTER TABLE `fee_scheme_installment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `final_result`
--
ALTER TABLE `final_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `installment_master`
--
ALTER TABLE `installment_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `inven_category`
--
ALTER TABLE `inven_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inve_item`
--
ALTER TABLE `inve_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `issu_item`
--
ALTER TABLE `issu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lead`
--
ALTER TABLE `lead`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lead_activity`
--
ALTER TABLE `lead_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lead_history`
--
ALTER TABLE `lead_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `lead_location`
--
ALTER TABLE `lead_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `lead_source`
--
ALTER TABLE `lead_source`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `lead_stage`
--
ALTER TABLE `lead_stage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `leave_application`
--
ALTER TABLE `leave_application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `library_member`
--
ALTER TABLE `library_member`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `massage`
--
ALTER TABLE `massage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `nack_table`
--
ALTER TABLE `nack_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `notice_board`
--
ALTER TABLE `notice_board`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `parents_info`
--
ALTER TABLE `parents_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `registration_history`
--
ALTER TABLE `registration_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `registration_student`
--
ALTER TABLE `registration_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `result_action`
--
ALTER TABLE `result_action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `result_shit`
--
ALTER TABLE `result_shit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `result_submition_info`
--
ALTER TABLE `result_submition_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `role_based_access`
--
ALTER TABLE `role_based_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `institute`
--
ALTER TABLE `institute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `set_salary`
--
ALTER TABLE `set_salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slip`
--
ALTER TABLE `slip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `student_info`
--
ALTER TABLE `student_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `subjects_mark`
--
ALTER TABLE `subjects_mark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suggestion`
--
ALTER TABLE `suggestion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tax_master`
--
ALTER TABLE `tax_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `teachers_info`
--
ALTER TABLE `teachers_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `teacher_attendance`
--
ALTER TABLE `teacher_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `transection`
--
ALTER TABLE `transection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transport`
--
ALTER TABLE `transport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
