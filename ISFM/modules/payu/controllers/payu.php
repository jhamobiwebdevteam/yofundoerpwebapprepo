<?php 
    if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payu extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/home
     * 	- or -  
     * 		http://example.com/index.php/home/index
     */
    function __construct() {
        parent::__construct();
        $this->load->model('payumodel');
        $this->load->model('payment/paymentmodel');
        // $this->load->model('homeModel');
        $this->load->helper('file');
        $this->load->helper('form');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    //This function will show the users dashboard
    
    public function transaction() {   
        $merchant_key ='';$merchant_salt='';  
        $id = $this->session->userdata('user_id');
        $instArr = $this->common->getSingleField('college_id','registration_student','user_id',$id);
        if($instArr){  
            $paygateway = $this->common->getWhere('payment_gateway','institute_id',$instArr[0]->college_id);
            $merchant_key = $paygateway[0]['merchant_key'];
            $merchant_salt = $paygateway[0]['merchant_salt'];
        }else{
            $merchant_key = MERCHANT_KEY;
            $merchant_salt = SALT;
        }
        // die('--'.$id);
        $data['txnid'] = substr(hash('sha256', mt_rand() . microtime()), 0, 10);
        $data['email'] = $this->input->post('email');
        $data['mobile'] = $this->input->post('mobile');
        $data['firstName'] = $this->input->post('fname');
        $data['lastName'] = $this->input->post('lname');
        $data['totalCost'] = $this->input->post('amount');
        $data['productinfo'] = $this->input->post('pinfo');
        $data['hash'] = '';           
        $data['hash'] = strtolower(hash('sha512',  $merchant_key."|".$data['txnid']."|".$data['totalCost']."|".$data['productinfo']."|".$data['firstName']."|".$data['email']."|||||||||||".$merchant_salt));
        $data['action'] = PAYU_BASE_URL . '/_payment';
        $json_str = json_encode($data,JSON_UNESCAPED_SLASHES);
        $log = date("F j, Y, g:i a")." ".$json_str.PHP_EOL;
        $this->payumodel->save_log($log);

        $insert_data = array( 
            'amount'=>  $this->input->post('amount'),
            'date'=> $this->input->post('date'),
            'student_id'=>$this->input->post('student_id'),
            'mobile' =>$this->input->post('mobile'),
            'email_id'=>$this->input->post('email'),
            'payment_mode'=> "online",
            'online_payment_status'=> "processing",
            'transaction_no'=> $data['txnid'],
        );            
        $this->db->insert('fee_payment_record', $insert_data);
        // print_r($data);
        // die('---');
        $this->load->view('pum_index', $data);
    }


    public function success() {
        $status = $this->input->post("status");
        $firstname = $this->input->post("firstname");
        $lastname = $this->input->post("lastname");
        $phone = $this->input->post("phone");
        $amount = $this->input->post("amount");
        $txnid = $this->input->post("txnid");
        $posted_hash = $this->input->post("hash");
        $key = $this->input->post("key");
        $productinfo = $this->input->post("productinfo");
        $email = $this->input->post("email");
        $mihpayid = $this->input->post("mihpayid");
        $mode = $this->input->post("mode");
        $bankcode = $this->input->post("bankcode");
        $unmappedstatus = $this->input->post("unmappedstatus");
        $error = $this->input->post("error");
        $bank_ref_num = $this->input->post("bank_ref_num");
        $PG_TYPE = $this->input->post("PG_TYPE");

        // $status = "success";
        // $firstname = "sarvdip";
        // $lastname = "pol";
        // $phone = "9730246928";
        // $amount = "1.00";
        // $txnid = "c3439d1952c5a5173165";
        // $posted_hash = "38c7ec4b21298dbcb674b3c4fb7d22498dde46507db515423653f10a5ff47742d8320f3a3b8932243373f596f781c672252280d250a1c9296f31efb0e379b126";
        // $key = "6SDYByYH";
        // $productinfo = "testing";
        // $email = "admin@jhamobi.com";
        // $mihpayid = "15940331929";
        // $mode = "UPI";
        // $bankcode = "UPI";
        // $unmappedstatus = "captured";
        // $error = "E000";
        // $bank_ref_num = "227060131802";
        // $PG_TYPE = "ICICIU";

        // $salt = SALT;
        $id = $this->session->userdata('user_id');
        $instArr = $this->common->getSingleField('college_id','registration_student','user_id',$id);
        if($instArr){  
            $paygateway = $this->common->getWhere('payment_gateway','institute_id',$instArr[0]->college_id);
            $salt = $paygateway[0]['merchant_salt'];
        }else{
            $salt = SALT;
        }
        $getData = $this->payumodel->get_details_from_txnid($txnid);
        // print_r($getData); die('--'.$getData[0]['transaction_no']);
        $data = "REQUEST --- txnid : ".$txnid.", firstname : ".$firstname.", lastname : ".$lastname.", status : ".$status.", amount : ".$amount.", mihpayid : ".$mihpayid.", productinfo : ".$productinfo.", email : ".$email .", PG_TYPE : ".$PG_TYPE.", phone : ".$phone.", mode : ".$mode.", bankcode : ".$bankcode.", unmappedstatus : ".$unmappedstatus.", error : ".$error.", bank_ref_num : ".$bank_ref_num.", posted_hash : ".$posted_hash.", key : ".$key;

        $log = date("F j, Y, g:i a")." ".$data.PHP_EOL;
        // echo $log;
        $this->payumodel->save_log($log);
        If ($this->input->post("additionalCharges")) {
            $additionalCharges = $this->input->post("additionalCharges");
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        } else {
            $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        }
        // $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        $hash = hash("sha512", $retHashSeq);
        // echo "<br><br><br>".$hash;
        // echo "<br>".$posted_hash;
        if(($hash == $posted_hash) && ($txnid == $getData[0]['transaction_no']) && (round($amount, 0) == $getData[0]['amount']) && ($status == 'success')) {
            $update_data = array(
                'online_payment_status' => 'success',
                'payu_transaction_id' => $mihpayid
            );
            $this->db->where('transaction_no', $txnid);
            $this->db->update('fee_payment_record', $update_data);
            $dta['msg'] = 'Payment Done Successfully..';
            $dta['msg1'] = "Your Payment Id is : <b> $mihpayid </b>";

            $this->load->view('temp/header');
            $this->load->view('modal',$dta);
            $this->load->view('temp/footer');     
        } else {            
            $dta['msg'] = 'Invalid Transaction.';
            $dta['msg1'] = "Please try again..";

            $this->load->view('temp/header');
            $this->load->view('modal',$dta);
            $this->load->view('temp/footer');     
        }
    }

    public function fail() {
        $status = $this->input->post("status");
        $firstname = $this->input->post("firstname");
        $amount = $this->input->post("amount");
        $txnid = $this->input->post("txnid");
        $posted_hash = $this->input->post("hash");
        $key = $this->input->post("key");
        $productinfo = $this->input->post("productinfo");
        $email = $this->input->post("email");
        $salt = SALT;
        $data = "txnid : ".$txnid.", firstname : ".$firstname.", status : ".$status.", amount : ".$amount.", productinfo : ".$productinfo.", email : ".$email .", posted_hash : ".$posted_hash.", key : ".$key;

        $log = date("F j, Y, g:i a")." ".$data.PHP_EOL;
        echo $log;
        $this->payumodel->save_log($log);
        $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        $hash = hash("sha512", $retHashSeq);
        if(($hash == $posted_hash) && ($status == 'failure')) {
            $update_data = array(
                'online_payment_status' => 'failure'
            );
            $this->db->where('transaction_no', $txnid);
            $this->db->update('fee_payment_record', $update_data);
            $dta['msg'] = 'Sorry Payment Failed..';
            $dta['msg1'] = "Please try again";

            $this->load->view('temp/header');
            $this->load->view('modal',$dta);
            $this->load->view('temp/footer');     
        } else {            
            $dta['msg'] = 'Invalid Transaction.';
            $dta['msg1'] = "Please try again.";

            $this->load->view('temp/header');
            $this->load->view('modal',$dta);
            $this->load->view('temp/footer');     
        }
    }

}