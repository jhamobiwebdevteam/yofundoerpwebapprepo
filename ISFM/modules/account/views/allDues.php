<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('dues'); ?>
                        </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="sample_1" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>Sr. No</th>
                                    <th>Enrollment No/Student Id</th>
                                    <th>Student Name</th>
                                    <th>Department</th>
                                    <th>Fee type</th>
                                    <th>Expected Amount</th>
                                    <th>Paid Amount</th>
                                    <th>Due Amount</th>
                                    <th>Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php //foreach ($slips as $row) {  ?>
                                    <tr>
                                        <td>1</td>
                                        <td>20220</td>
                                        <td>testing </td>
                                        <td>computer</td>
                                        <td>admission fees</td>
                                        <td>1000</td>
                                        <td>500</td>
                                        <td>500</td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn default" type="button">Action</button>
                                                <button data-toggle="dropdown" class="btn default dropdown-toggle" type="button"><i class="fa fa-angle-down"></i></button>
                                                <ul role="menu" class="dropdown-menu ac_bo_sh">
                                                    <li class="ac_in_sty">
                                                        <a href="index.php/account/view_dues">View </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>202222</td>
                                        <td>testing1 </td>
                                        <td>computer</td>
                                        <td>admission fees</td>
                                        <td>1000</td>
                                        <td>500</td>
                                        <td>500</td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn default" type="button">Action</button>
                                                <button data-toggle="dropdown" class="btn default dropdown-toggle" type="button"><i class="fa fa-angle-down"></i></button>
                                                <ul role="menu" class="dropdown-menu ac_bo_sh">
                                                    <li class="ac_in_sty">
                                                        <a href="index.php/account/view_dues">View </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php //} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script>
    jQuery(document).ready(function() {
        //here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>