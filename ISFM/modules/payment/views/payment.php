<?php

if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
	//Request hash
	$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';	
	if(strcasecmp($contentType, 'application/json') == 0){
		$data = json_decode(file_get_contents('php://input'));
		$hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
		$json=array();
		$json['success'] = $hash;
    	echo json_encode($json);
	
	}
	exit(0);
}
 
function getCallbackUrl()
{
	$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	return $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . 'response.php';
}

?>
<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-
color="e34524" bolt-logo="<?=base_url('assets/images/jm.jpeg')?>"></script>

<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datepicker/css/datepicker3.css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('header_payment'); ?>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i><?php echo lang('home'); ?>
                    </li>
                    <li><?php echo lang('header_payment'); ?>

                    </li>

                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i><?php echo lang('header_payment'); ?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php $form_attributs = array('name' => 'myForm', 'id'=>'payment_form', 'class' => 'form-horizontal', 'role' => 'form', 'onsubmit' => 'return validateForm()');
                        echo form_open('', $form_attributs);

                        ?>
                        <input type="hidden" id="udf5" name="udf5" value="BOLT_KIT_PHP7" />
                        <input type="hidden" id="surl" name="surl" value="<?php echo getCallbackUrl(); ?>" />
                        <input type="hidden" id="key" name="key" placeholder="Merchant Key" value="6SDYByYH" />
                        <input type="hidden" id="salt" name="salt" placeholder="Merchant Salt" value="oP7RqS0X5B" />
                                <input type="hidden" id="txnid"  class="form-control"  name="txnid" placeholder="Transaction ID" value="<?php echo  "Txn" . rand(10000,99999999)?>" />
                        <div class="form-body">

                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label"> Transaction ID <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-md-3 control-label"> Amount <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="amount"  class="form-control"  name="amount" placeholder="Amount" value="6.00" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> Information <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="pinfo" class="form-control" name="pinfo" placeholder="Product Info" value="P01,P02" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> Name <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="fname" class="form-control" name="fname" placeholder="First Name" value="" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> Email <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="email" class="form-control" name="email" placeholder="Email ID" value="" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> Mobile <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="mobile" class="form-control" name="mobile" placeholder="Mobile/Cell Number" value="" />
                                </div>
                            </div>

                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label"> Hash <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                </div>
                            </div> -->
                                <input type="hidden" id="hash" name="hash" placeholder="Hash" value="" />
                            
                        </div>
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                            <input type="submit" value="Pay" class="btn green" onclick="launchBOLT(); return false;" />
                                <!-- <button type="submit" name="submit" class="btn green" value="Add Class"><?php echo lang('clas_add_butt'); ?></button> -->
                                <!-- <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button> -->
                            </div>
                        </div>
                        <?php
                        echo form_close(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

<script type="text/javascript">
$('#payment_form').bind('keyup blur', function(){
	$.ajax({
          url: 'index.php/payment/index',
          type: 'post',
          data: JSON.stringify({ 
            key: $('#key').val(),
			salt: $('#salt').val(),
			txnid: $('#txnid').val(),
			amount: $('#amount').val(),
		    pinfo: $('#pinfo').val(),
            fname: $('#fname').val(),
			email: $('#email').val(),
			mobile: $('#mobile').val(),
			udf5: $('#udf5').val()
          }),
		  contentType: "application/json",
          dataType: 'json',
          success: function(json) {
            if (json['error']) {
			 $('#alertinfo').html('<i class="fa fa-info-circle"></i>'+json['error']);
            }
			else if (json['success']) {	
				$('#hash').val(json['success']);
            }
          }
        }); 
});

</script>
<script type="text/javascript">
function launchBOLT()
{
	bolt.launch({
	key: $('#key').val(),
	txnid: $('#txnid').val(), 
	hash: $('#hash').val(),
	amount: $('#amount').val(),
	firstname: $('#fname').val(),
	email: $('#email').val(),
	phone: $('#mobile').val(),
	productinfo: $('#pinfo').val(),
	udf5: $('#udf5').val(),
	surl : $('#surl').val(),
	furl: $('#surl').val(),
	mode: 'dropout'	
},{ responseHandler: function(BOLT){
	console.log( BOLT.response.txnStatus );		
	if(BOLT.response.txnStatus != 'CANCEL')
	{
		//Salt is passd here for demo purpose only. For practical use keep salt at server side only.
		var fr = '<form action=\"'+$('#surl').val()+'\" method=\"post\">' +
		'<input type=\"hidden\" name=\"key\" value=\"'+BOLT.response.key+'\" />' +
		'<input type=\"hidden\" name=\"salt\" value=\"'+$('#salt').val()+'\" />' +
		'<input type=\"hidden\" name=\"txnid\" value=\"'+BOLT.response.txnid+'\" />' +
		'<input type=\"hidden\" name=\"amount\" value=\"'+BOLT.response.amount+'\" />' +
		'<input type=\"hidden\" name=\"productinfo\" value=\"'+BOLT.response.productinfo+'\" />' +
		'<input type=\"hidden\" name=\"firstname\" value=\"'+BOLT.response.firstname+'\" />' +
		'<input type=\"hidden\" name=\"email\" value=\"'+BOLT.response.email+'\" />' +
		'<input type=\"hidden\" name=\"udf5\" value=\"'+BOLT.response.udf5+'\" />' +
		'<input type=\"hidden\" name=\"mihpayid\" value=\"'+BOLT.response.mihpayid+'\" />' +
		'<input type=\"hidden\" name=\"status\" value=\"'+BOLT.response.status+'\" />' +
		'<input type=\"hidden\" name=\"hash\" value=\"'+BOLT.response.hash+'\" />' +
		'</form>';
		var form = jQuery(fr);
		jQuery('body').append(form);								
		form.submit();
	}
},
	catchException: function(BOLT){
 		alert( BOLT.message );
	}
});
}

</script>