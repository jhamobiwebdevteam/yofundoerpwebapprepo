<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class certificate extends MX_Controller {

    function __construct() {
        parent::__construct();        
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    public function get_marksheet()
    {
         $postData = '';
        $data['stud_info'] ='';
        $urldata = $this->config->item('api_url').'get_schools.php';
        $data['data'] = $this->common->schoolname_data($urldata,$postData);
        if ($this->input->post('submit', TRUE)){   
            $cids = $this->input->post('course_ids');  
            $urldata = $this->config->item('api_url').'get_enrolled_student_by_course.php'; 
            $postData = '{  "course_ids": "'.$cids.'"}';
            $res = $this->common->schoolname_data($urldata,$postData);
            // print_r($res['data']); die('---');
            $data['stud_info'] = $res['data'];
            $data['cname'] = $res['cname'];
             $this->load->view('temp/header');
             $this->load->view('studentList', $data);
             $this->load->view('temp/footer');
           
        }else{
            $data['cname'] = '';
             $this->load->view('temp/header');
             $this->load->view('studentList', $data);
             $this->load->view('temp/footer');
            
        } 
    }

    public function viewMarksheet()
    {
        $this->load->view('marksheet');
    }

}