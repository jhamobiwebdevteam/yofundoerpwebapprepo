<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('add_inst'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('inst'); ?>                        
                    </li>
                    <li>
                        <?php echo lang('add_inst'); ?>                        
                    </li>
                   
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->

        <div class="row">
            <div class="col-md-12 ">
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bars"></i> <?php echo lang('add_inst'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body form">

                         <?php $form_attributs = array('class' => 'form-horizontal', 'role' => 'form', 'name' => 'myForm', 'onsubmit' => 'return validateForm()');
                        echo form_open_multipart('schoolManagement/addschoolInfo', $form_attributs);
                        ?>
                        <div class="form-body">
                            <?php
                            if (!empty($success)) {
                                echo $success;
                            }
                            ?>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo lang('header_sname'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-4">
                                    <!-- <input type="text" class="form-control" name="school_name" required="required"> -->
                                    <select class="form-control" name="school_name" id="school_name">
                                        <option value="">Select School Name</option>
                                        <?php foreach($data['data'] as $key) { ?>
                                            <option value="<?php echo $key['id'].'-'.$key['name'];?>"><?php echo $key['name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <label class="col-md-2 control-label"><?php echo lang('header_sid');?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="school_id" required="required">
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo lang('header_sgrname');?><span class="requiredStar"> * </span></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="school_grpname" required="required">

                                </div>

                                <label class="col-md-2 control-label"><?php echo lang('header_saddress');?><span class="requiredStar"> * </span></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="address" required="required">

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo lang('header_city');?></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="city">
                                </div>

                                <label class="col-md-2 control-label"><?php echo lang('header_pin');?></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="pincode">
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo lang('header_admin');?></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="admin_name">
                                </div>

                                <label class="col-md-2 control-label"><?php echo lang('header_adminNo');?></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="admin_no">
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo lang('header_description');?></label>
                                <div class="col-md-4">
                                    <textarea  class="form-control" name="description"></textarea>
                                </div>

                                <label class="col-md-2 control-label"><?php echo lang('header_registrationdt');?></label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="reg_date">
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo lang('header_foundyear');?></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="found_year">
                                </div>

                                <label class="col-md-2 control-label"><?php echo lang('header_schoolemail');?></label>
                                <div class="col-md-4">
                                    <input type="email" class="form-control" name="school_email">
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo lang('header_regno');?></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="reg_no">
                                </div>
                            </div>

                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn green" name="submit" value="submit"><?php echo lang('save');?></button>
                                    <button type="reset" class="btn default"><?php echo lang('refresh');?></button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                        </div>

                    </div>

                </div>
            </div>

            
        </div>

    </div>
</div>
<!-- END CONTENT -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL script -->
<script type="text/javascript" src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/components-form-tools.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>

<script type="text/javascript">
    

    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>