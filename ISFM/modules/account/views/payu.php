

<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i><?php echo lang('header_payment'); ?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php $form_attributs = array('name' => 'myForm', 'id'=>'payment_form', 'class' => 'form-horizontal', 'role' => 'form', 'onsubmit' => 'return validateForm()');
                        echo form_open('payu/transaction', $form_attributs);
                        // print_r($userInfo); die($userInfo[0]['username']);
                        ?>
                        <div class="form-body">

                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label"> Transaction ID <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                </div>
                            </div> -->
                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label">Date<span class="requiredStar"> </span></label>
                                <div class="col-md-6">
                                    <input type="date" class="form-control" placeholder=" " name="date" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Name<span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <select class="form-control name" id="name" name="name">
                                        <option><?php echo lang('select'); ?></option>
                                        <?php foreach ($studentinfo as $row) { ?>
                                            <option value="<?php echo $row['id'];?>"><?php echo $row['first_name'].' '.$row['last_name'];?></option>
                                        <?php }?>

                                    </select>
                                </div>                                
                            </div> -->
                            <input type="hidden" id="fname" class="form-control" name="fname"  value="<?php echo $userInfo[0]['first_name']; ?>" />
                            <input type="hidden" id="stud_id" class="form-control" name="student_id"  value="<?php echo $studId; ?>" />
                            <input type="hidden" id="class_id" class="form-control" name="class_id"  value="<?php echo $classId; ?>" />
                            <input type="hidden" id="lname" class="form-control" name="lname"  value="<?php echo $userInfo[0]['last_name']; ?>" />
                            <input type="hidden" id="email" class="form-control" name="email"  value="<?php echo $userInfo[0]['email']; ?>" />
                            <input type="hidden" id="eml" class="form-control" name="eml"  value="<?php echo $userInfo[0]['email']; ?>" />
                            <input type="hidden" id="mobile" class="form-control" name="mobile"  value="<?php echo $userInfo[0]['phone']; ?>" />
                            <input type="hidden" id="mbl" class="form-control" name="mbl"  value="<?php echo $userInfo[0]['phone']; ?>" />
                            <input type="hidden" id="pinfo" class="form-control" name="pinfo" placeholder="Product Info" value="E-Payment" />
                            <input type="hidden" class="form-control" name="date" value="<?php echo date('Y-m-d'); ?>">
                            
                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label">First Name <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="fname" class="form-control" name="fname" placeholder="First Name" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Last Name <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="lname" class="form-control" name="lname" placeholder="Last Name" value="" />
                                </div>
                            </div> -->

                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label"> Email <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input readonly type="text" id="eml" class="form-control" name="eml" placeholder="Email ID" value=""  />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> Mobile <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input readonly type="text" id="mbl" class="form-control" name="mbl" placeholder="Mobile/Cell Number"  value=""  />
                                </div>
                            </div> -->
                             <?php //print_r($studId);?>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Select Fee Type<span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <select class="form-control name" id="fee_type" name="fee_type">
                                        <option><?php echo lang('select'); ?></option>
                                        <?php foreach ($feesType as $row) { ?>
                                            <option value="<?php echo $row['id'];?>"><?php echo $row['fee_category'];?></option>
                                        <?php }?>
                                    </select>
                                </div>                                
                            </div> 

                            <div class="form-group">
                                <label class="col-md-3 control-label"> Amount <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="amount"  class="form-control"  name="amount" placeholder="Amount" value="" readonly/>
                                </div>
                            </div>

                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label"> Information <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="pinfo" class="form-control" name="pinfo" placeholder="Product Info" value="" />
                                </div>
                            </div> -->
                            <?php if(!empty($paygateway)){ 
                                if($paygateway[0]['payment_option'] == 'payu'){ ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"> Payment option</label>
                                    <div class="col-md-6">
                                        <label for="payment_option_payu"><input type="radio" checked name="payment_option" value="payu/transaction" id="payment_option_payu"> PayU</label>
                                    </div>
                                </div>      
                            <?php }else{ ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"> Payment option</label>
                                    <div class="col-md-6">
                                        <label for="payment_option_razorpay"><input type="radio" checked name="payment_option" value="razorpay" id="payment_option_razorpay"> RazorPay</label>
                                    </div>
                                </div>  
                           <?php }
                            }else{ ?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"> Payment option</label>
                                        <div class="col-md-6">
                                            <label for="payment_option_payu"><input type="radio" checked name="payment_option" value="payu/transaction" id="payment_option_payu"> PayU</label> &nbsp;&nbsp; 
                                            <label for="payment_option_razorpay"><input type="radio"  name="payment_option" value="razorpay" id="payment_option_razorpay"> RazorPay</label>
                                        </div>
                                    </div>  
                           <?php } ?>
                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label"> Hash <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                </div>
                            </div> -->
                                <input type="hidden" id="hash" name="hash" placeholder="Hash" value="" />
                            
                        </div>
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                            <input type="submit" value="Pay" class="btn green" />
                                <!-- <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button> -->
                            </div>
                        </div>
                        <?php
                        echo form_close(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#fee_type').on('change', function() {
            var id = $(this).val();        
            if(id) {
                $.ajax({
                    url: '<?php echo base_url();?>account/getAmount',
                    data: {'id': id },
                    type: "POST",
                    dataType: "json",
                    success:function(data) {
                       $("#amount").val(data[0]['amount']);                      
                    }
                });
            }
        });
            
        $('input[name="payment_option"]').on('change',function(){
            var payment_url = '<?php echo base_url();?>'+$(this).val();
            $('#payment_form').attr('action',payment_url);
        });
    });
</script>