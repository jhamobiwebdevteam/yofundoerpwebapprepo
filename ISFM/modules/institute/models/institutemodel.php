<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class institutemodel extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function getSchoolInfo() 
    {
        $data = array();
        $query = $this->db->query("SELECT * FROM institute");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    public function get_schoolName_unique($schoolname)
    {
       // $data = array();
        $this->db->select('id');
        $this->db->where('school_name',$schoolname);
        $this->db->from('institute');
        $query = $this->db->get();
        return $query->result();
    }

    
}
