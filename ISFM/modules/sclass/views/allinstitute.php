<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <div class="row">
            <div class="col-md-12 ">
            <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <div class="portlet box green ">
                    <!-- <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bars"></i> <?php echo lang('header_add_school'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div> -->

                   
                <!-- BEGIN All account list-->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <?php echo lang('header_allinstitute'); ?> 
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                <thead>
                                    <tr>
                                        <th><?php echo lang('tax_id'); ?></th>
                                        <th><?php echo lang('header_sname'); ?></th>
                                        <th><?php echo lang('institute_id'); ?></th>
                                        <th><?php echo lang('email'); ?></th>
                                        <th><?php echo lang('institute_grpname'); ?></th>
                                        <th><?php echo lang('header_city'); ?></th>
                                        <th><?php echo lang('header_pin'); ?></th>
                                        <th><?php echo lang('header_admin'); ?></th>
                                        <th><?php echo lang('header_adminNo'); ?></th>
                                        <th><?php echo lang('header_description'); ?></th>
                                        <th><?php echo lang('header_registrationdt'); ?></th>
                                        <th><?php echo lang('header_foundyear'); ?></th>
                                        <th><?php echo "Action"; ?></th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; foreach ($school_info as $row) { ?>
                                        <tr style="">
                                            
                                            <td>
                                                <?php echo $i; ?>
                                            </td>
                                            <td><?php echo $row['name'];?></td>
                                            <td><?php echo $row['lms_id'];?></td>
                                            <td><?php echo $row['email'];?></td>
                                            <td><?php echo $row['grpname'];?></td>
                                            <td><?php echo $row['city'];?></td>
                                            <td><?php echo $row['pincode'];?></td>
                                            <td><?php echo $row['admin_name'];?></td>
                                            <td><?php echo $row['admin_num'];?></td>
                                            <td><?php echo $row['description'];?></td>
                                            <td><?php echo $row['reg_date'];?></td>
                                            <td><?php echo $row['foundation_year'];?></td>
                                            <td><a class="btn btn-xs default" href="index.php/sclass/editInstitute?id=<?php echo $row['id']; ?>"> <i class="fa fa-pencil-square-o"></i> <?php echo lang('edit'); ?> </a></td>

                                        </tr>
                                    <?php $i++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>
</div>
