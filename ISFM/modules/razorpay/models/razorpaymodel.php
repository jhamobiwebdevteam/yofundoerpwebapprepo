<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Razorpaymodel extends CI_Model {
    /**
     * This model is using into the students controller
     * Load : $this->load->model('studentmodel');
     */
    function __construct() {
        parent::__construct();
        $this->load->dbforge();
    }

    public function save_log($msg){
        $log_filename = "payu_logs/".date('m')."_".date('Y');
        if (!file_exists($log_filename)) 
        {
            // create directory/folder uploads.
            mkdir($log_filename, 0777, true);
        }
        $log_file_data = $log_filename.'/logs_payu_' . date('d-M-Y') . '.logs';
        file_put_contents($log_file_data, $msg . "\n", FILE_APPEND);
    }
    public function get_details_from_txnid($txnid)
    {
        $data = array();
        $query = $this->db->query("SELECT * FROM fee_payment_record where transaction_no='$txnid'");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }
    // public function fetch_data($get_selected_id)
    // {
    //     $data = array();
    //     $query = $this->db->query("SELECT fathers_mobile,email FROM registration_student where id=$get_selected_id");
    //     foreach ($query->result_array() as $row) {
    //         $data[] = $row;
    //     }
    //     return $data;
    // }
    // public function get_bank_detail()
    // {
    //     $data = array();
    //     $query = $this->db->query("SELECT * FROM bank_details");
    //     foreach ($query->result_array() as $row) {
    //         $data[] = $row;
    //     }
    //     return $data;
    // }
    
}