<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Students extends MX_Controller {

    /**
     * This controller is using for controlling to students
     *
     * Maps to the following URL
     * 		http://example.com/index.php/users
     * 	- or -  
     * 		http://example.com/index.php/users/<method_name>
     */
    function __construct() {
        parent::__construct();
        $this->load->model('studentmodel');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }
    //This function is used for get all students in this system.
    public function allStudent() {
        if ($this->input->post('submit', TRUE)) {
            $data['class_id'] = $this->input->post('class', TRUE);
            $data['section'] = $this->input->post('section', TRUE);
            //If "class" and "section" fild is not empty the run this condition
            if ($this->input->post('class', TRUE) && $this->input->post('section', TRUE)) {
                //Search student by class,section.
                $class = $this->input->post('class', TRUE);
                $section = $this->input->post('section', TRUE);
                $data['section'] = $section;
                if ($section == 'all') {
                    $data['studentInfo'] = $this->studentmodel->getStudentByClassSection($class, $section);
                    if (!empty($data)) {
                        //If the class have student then run here.
                        $this->load->view('temp/header');
                        $this->load->view('studentclass', $data);
                        $this->load->view('temp/footer');
                    } else {
                        //If the class have no any student then print the massage in the view.
                        $data['message'] = 'This class is no student.';
                        $this->load->view('temp/header');
                        $this->load->view('studentclass', $data);
                        $this->load->view('temp/footer');
                    }
                } else {
                    $data['studentInfo'] = $this->studentmodel->getStudentByClassSection($class, $section);
                    if (!empty($data)) {
                        //If the class have student then run here.
                        $this->load->view('temp/header');
                        $this->load->view('studentclass', $data);
                        $this->load->view('temp/footer');
                    } else {
                        //If the class have no any student then print the massage in the view.
                        $data['message'] = lang('stuc_1');
                        $this->load->view('temp/header');
                        $this->load->view('studentclass', $data);
                        $this->load->view('temp/footer');
                    }
                }
            } elseif ($this->input->post('class', TRUE)) { 
                //onley search student by class or all student the class.
                $class_id = $this->input->post('class', TRUE);
                $data['studentInfo'] = $this->studentmodel->getAllStudent($class_id);
                if (!empty($data)) {
                    //If the class have student then run here.
                    $this->load->view('temp/header');
                    $this->load->view('studentclass', $data);
                    $this->load->view('temp/footer');
                } else {
                    //If the class have no any student then print the massage in the view.
                    $data['message'] = lang('stuc_1');
                    $this->load->view('temp/header');
                    $this->load->view('studentclass', $data);
                    $this->load->view('temp/footer');
                }
            }
        } else {
            //First of all this method run here and load class selecting view.
            $data['s_class'] = $this->common->selectClass();
            $this->load->view('temp/header');
            $this->load->view('slectStudent', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function is used for filtering to get students information
    //Whene class and section gave in the frontend, if the class have section he cane select the section and get student information in the viwe.
    public function ajaxClassSection() {
        $classTitle = $this->input->get('classTitle');
        $query = $this->common->getWhere('department', 'class_title', $classTitle);
        foreach ($query as $row) {
            $data = $row['section'];
        }
        if (!empty($data)) {
            $sectionArray = explode(",", $data);
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-4">
                            <select name="section" class="form-control">
                                <option value="all">' . lang('stu_sel_cla_velue_all') . '</option>';
            foreach ($sectionArray as $sec) {
                echo '<option value="' . $sec . '">' . $sec . '</option>';
            }
            echo '</select></div>
                    </div>';
        } else {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-warning">
                                <strong>' . lang('stu_sel_cla_no_Info') . '</strong> ' . lang('stu_sel_cla_no_section') . '
                        </div></div></div>';
        }
    }
    //This function is giving a student's the full information. 
    public function students_details() {
        $id = $this->input->get('id');
        $studentId = $this->input->get('sid');
        $data['studentInfo'] = $this->studentmodel->studentDetails($id);
        $data['photo'] = $this->studentmodel->studentPhoto($studentId);
        $this->load->view('temp/header');
        $this->load->view('studentsDetails', $data);
        $this->load->view('temp/footer');
    }
    //This function is use for edit student's informations.
    public function editStudent() {
        $userId = $this->input->get('userId');
        $studentInfoId = $this->input->get('sid');
        $studentClass = $this->input->get('di');
        $class_id = $this->input->get('class_id');
        if ($this->input->post('submit', TRUE)) {
            $username = $this->input->post('first_name', TRUE) . ' ' . $this->input->post('last_name', TRUE);
            $additional_data = array(
                'first_name' => $this->db->escape_like_str($this->input->post('first_name', TRUE)),
                'last_name' => $this->db->escape_like_str($this->input->post('last_name', TRUE)),
                'username' => $this->db->escape_like_str($username),
                'phone' => $this->db->escape_like_str($this->input->post('phone', TRUE)),
                'email' => $this->db->escape_like_str($this->input->post('email', TRUE))
            );
            $this->db->where('id', $userId);
            $this->db->update('users', $additional_data);
            $studentsInfo = array(
                'student_id' => $this->db->escape_like_str($this->input->post('student_id', TRUE)),
                'class_id' => $this->db->escape_like_str($this->input->post('class', TRUE)),
                'farther_name' => $this->db->escape_like_str($this->input->post('father_name', TRUE)),
                'mother_name' => $this->db->escape_like_str($this->input->post('mother_name', TRUE)),
                'birth_date' => $this->db->escape_like_str($this->input->post('birthdate', TRUE)),
                'sex' => $this->db->escape_like_str($this->input->post('sex', TRUE)),
                'present_address' => $this->db->escape_like_str($this->input->post('present_address', TRUE)),
                'permanent_address' => $this->db->escape_like_str($this->input->post('permanent_address', TRUE)),
                'father_occupation' => $this->db->escape_like_str($this->input->post('father_occupation', TRUE)),
                'father_incom_range' => $this->db->escape_like_str($this->input->post('father_incom_range', TRUE)),
                'mother_occupation' => $this->db->escape_like_str($this->input->post('mother_occupation', TRUE)),
                'last_class_certificate' => $this->db->escape_like_str($this->input->post('previous_certificate', TRUE)),
                't_c' => $this->db->escape_like_str($this->input->post('tc', TRUE)),
                'academic_transcription' => $this->db->escape_like_str($this->input->post('at', TRUE)),
                'national_birth_certificate' => $this->db->escape_like_str($this->input->post('nbc', TRUE)),
                'testimonial' => $this->db->escape_like_str($this->input->post('testmonial', TRUE)),
                'documents_info' => $this->db->escape_like_str($this->input->post('submit_file_information', TRUE)),
                'blood' => $this->db->escape_like_str($this->input->post('blood', TRUE)),
            );
            $this->db->where('student_id', $studentInfoId);
            $this->db->update('student_info', $studentsInfo);
            $additionalData3 = array(
                'class_title' => $this->db->escape_like_str($this->input->post('class')),
                'student_title' => $this->db->escape_like_str($username),
                'section' => $this->db->escape_like_str($this->input->post('section'))
            );
            $this->db->where('id', $studentClass);
            $this->db->update('class_students', $additionalData3);
            $data['success'] = '<div class="alert alert-info alert-dismissable admisionSucceassMessageFont">
                                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                    <strong>' . lang('success') . '</strong> ' . lang('stuc_2') . '
                                            </div>';
            $data['classStudents'] = $this->common->getWhere('class_students', 'id', $studentClass);
            $data['studentInfo'] = $this->common->getWhere('student_info', 'student_id', $studentInfoId);
            $data['users'] = $this->common->getWhere('users', 'id', $userId);
            $data['s_class'] = $this->common->getAllData('department');
            $data['section'] = $this->studentmodel->section($class_id);
            $this->load->view('temp/header');
            $this->load->view('editStudentInfo', $data);
            $this->load->view('temp/footer');
        } else {
            //first here load the edit student view with student's previous value.
            $data['classStudents'] = $this->common->getWhere('class_students', 'id', $studentClass);
            $data['studentInfo'] = $this->common->getWhere('student_info', 'student_id', $studentInfoId);
            $data['users'] = $this->common->getWhere('users', 'id', $userId);
            $data['s_class'] = $this->common->getAllData('department');
            $data['sectiond'] = $this->studentmodel->section($class_id);
            $this->load->view('temp/header');
            $this->load->view('editStudentInfo', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function is use for delete a student.
    public function studentDelete() {
        $id = $this->input->get('di');
        $studentInfoId = $this->input->get('sid');
        $userId = $this->input->get('userId');
        if ($this->db->delete('class_students', array('id' => $id)) && $this->db->delete('student_info', array('id' => $studentInfoId)) && $this->db->delete('users', array('id' => $userId))) {
            redirect('students/allStudent');
        }
    }
    //This function is use for activate a student in LMS.
    public function lmsActive() {   
        $lms = $this->load->database('lms', TRUE);  
        $uname = $this->input->get('uname');  
        $class_id = $this->input->get('class_id');  
        $section = $this->input->get('section');  
        $data = array(
            'confirmed' => 1,
            'mnethostid' => 1
        );
        $lms->where('username', $uname);
        $lms->update('mdl_user', $data);
        header('Location: '.$_SERVER['PHP_SELF']);  
        redirect('students/allStudent');
    }
    //This function is use for De-activate a student in LMS.
    public function lmsDeActive() {
        $lms = $this->load->database('lms', TRUE);
        $uname = $this->input->get('uname');  
        $class_id = $this->input->get('class_id');  
        $section = $this->input->get('section');  
        $data = array(
            'confirmed' => 0,
            'mnethostid' => 0
        );
        $lms->where('username', $uname);
        $lms->update('mdl_user', $data);
        redirect('students/allStudent');
    }
    //This function will return only logedin students information
    public function studentsInfo() 
    {
        $uid = $this->input->get('uisd');
        $data['studentInfo'] = $this->studentmodel->ownStudentDetails($uid);
        $data['student_photo'] = $this->studentmodel->ownStudentPhoto($uid);
        $this->load->view('temp/stud_header');
        $this->load->view('studentsDetails', $data);
        $this->load->view('temp/footer');
    }
    public function studentAdmission()
    {
       /* if(($this->input->post('submit', TRUE))||($this->input->post('update', TRUE))) 
        {     
        //die('1');       
            $this->load->view('temp/stud_header');
            $this->load->view('student_admissionForm');
            $this->load->view('temp/footer');

        }
        else
        {*/                       
            $uid = $this->input->get('uisd');
            $data1['studadmissionInfo'] = $this->studentmodel->getadmissiondetail($uid);
            /*$query = $this->common->countryPhoneCode();
            $data1['countryPhoneCode'] = $query->countryPhonCode;
            $data1['s_class'] = $this->common->getAllData('department');
            $postData = '';
            $urldata = 'https://yofundo.in/lms/clients/local/erpApi/get_schools.php';
            $data1['data'] = $this->common->schoolname_data($urldata,$postData);*/
            $this->load->view('temp/stud_header');
            $this->load->view('student_admissionForm',$data1);
            $this->load->view('temp/footer');
           
    }

    public function updateStud_Admission()
    {
        $data = $this->input->post('rdid');
        $update_dataStudAdmission = array(
            'fathers_lastname' => $this->db->escape_like_str($this->input->post('fathers_lastname', TRUE)),
            'fathers_mobile' => $this->db->escape_like_str($this->input->post('phone', TRUE)),
            'fathers_occup' => $this->db->escape_like_str($this->input->post('father_occupation', TRUE)),
            'anuual_income' => $this->db->escape_like_str($this->input->post('father_incom_range', TRUE)),
            'mothers_firstname' => $this->db->escape_like_str($this->input->post('mother_name', TRUE)),
            'mothers_occup' => $this->db->escape_like_str($this->input->post('mother_occupation', TRUE)),
            'mothers_midname' => $this->db->escape_like_str($this->input->post('mothers_midname', TRUE)),
            'mothers_lastname' => $this->db->escape_like_str($this->input->post('mothers_lastname', TRUE)),
            'mothers_mobile' => $this->db->escape_like_str($this->input->post('mothers_mob', TRUE)),
            'fathers_qualification' => $this->db->escape_like_str($this->input->post('fathers_education', TRUE)),
            'mothers_qualification' => $this->db->escape_like_str($this->input->post('mothers_education', TRUE)),
            'mother_toung' => $this->db->escape_like_str($this->input->post('mother_tounge', TRUE)),
            'updated_at' => date("Y-m-d H:i:s"),
        );
        $this->db->where('user_id', $data);
        if($this->db->update('registration_student', $update_dataStudAdmission))        
        {
            
            $data['success1'] = '<div class="alert alert-info alert-dismissable admisionSucceassMessageFont">
                                <button type="button" aria-hidden="true" data-dismiss="alert" class="close" ></button>
                                    <strong>Success!</strong> The student form updated successfully.
                                </div>';            
        }
        else
        {
            
            $data['success1'] = '<div class="col-md-12"><div class="alert alert-info alert-dismissable admisionSucceassMessageFont">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                        <strong>Fail!</strong>While updating form of admission</div></div>';
            
        }
        
    }

    public function updateStud_reqdocument()
    {
        $updateid = $this->input->post('rdid');
        $update_req_doc = array(
            'isthr_medical_condition'=> $this->db->escape_like_str($this->input->post('medical_condition', TRUE)),
            'isthr_undergoing_treatment'=> $this->db->escape_like_str($this->input->post('undergoing_treatment', TRUE)),
            'is_child_taking_medication'=> $this->db->escape_like_str($this->input->post('medication', TRUE)),
            'food_allergy' => $this->db->escape_like_str($this->input->post('food_allergy', TRUE)),
            'original_tc' => $this->db->escape_like_str($this->input->post('tc', TRUE)),
            'original_birth_cert' => $this->db->escape_like_str($this->input->post('original_birth_cert', TRUE)),
            'adhar_copy_parent' => $this->db->escape_like_str($this->input->post('adhar_copy_parent', TRUE)),
            'medical_fitness_cert' => $this->db->escape_like_str($this->input->post('medical_fitness_cert', TRUE)),
            'adoption_document' => $this->db->escape_like_str($this->input->post('adoption_document', TRUE)),
            'passport_photos' => $this->db->escape_like_str($this->input->post('passport_photos', TRUE)),
            'passport_pic_both_parent'=> $this->db->escape_like_str($this->input->post('passport_pic_both_parent', TRUE)),
            'original_bonafied' => $this->db->escape_like_str($this->input->post('original_bonafied', TRUE)),
            'pan_card_copy_parent' => $this->db->escape_like_str($this->input->post('pan_card_copy_parent', TRUE)),
            'caste_certificate' => $this->db->escape_like_str($this->input->post('caste_certificate', TRUE)),
            'residence_proof' => $this->db->escape_like_str($this->input->post('residence_proof', TRUE)),
            'if_foreign_nationals_copy'=> $this->db->escape_like_str($this->input->post('if_foreign_nationals_copy', TRUE)),
        ); 

        $this->db->where('user_id',$updateid);
        if($this->db->update('registration_student',$update_req_doc))        
        {
            $data['success1'] = '<div class="alert alert-info alert-dismissable admisionSucceassMessageFont">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                    <strong>Success!</strong> The student form updated successfully.
                                </div>';
            /*$this->load->view('temp/stud_header');
            $this->load->view('student_admissionForm',$data);
            $this->load->view('temp/footer');*/
            redirect($_SERVER['HTTP_HOST']);
        }
        else
        {
            $data['success1'] = '<div class="col-md-12"><div class="alert alert-info alert-dismissable admisionSucceassMessageFont">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                        <strong>Fail!</strong>While updating form of admission</div></div>';
            /*$this->load->view('temp/stud_header');
            $this->load->view('student_admissionForm',$data);
            $this->load->view('temp/footer');*/
            redirect($_SERVER['HTTP_HOST']);
        }





    }
}
