<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>

<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL STYLES -->
<style>
    .show{
        display: block;
        margin-left: auto;
        margin-right: 0;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        
        
        <div class="row">
            <?php //print_r($grade_info); die($id.'--'.$email.'--'.$name.'--'.$phone2);?>
            <div class="col-md-12">
                <div class="portlet box green row">
                    <div class="portlet-title">
                        <div class="caption col-md-10">
                            <?php echo 'Grades of '.$name; ?>
                        </div>  
                    </div>
                    <?php ?>
                    <div class="portlet-body">
                        <table class="table" id="sample_1">
                            <thead>
                                <tr>
                                    <th><?php echo lang('tax_id'); ?></th>
                                    <th> <?php echo lang('email'); ?> </th>  
                                    <th> <?php echo lang('admi_crs_list');?></th>  
                                    <th> <?php echo lang('obtmarks');?></th>  
                                    <th> <?php echo lang('outof');?></th>  
                                    <th> <?php echo lang('rgister_action');?></th>  
                                                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                // print_r($grade_info);
                                $i=1;$m=0;$o=0; foreach ($grade_info as $row) { 
                                    $m = $m+intval($row['marks']);
                                    $o = $o+intval($row['outof']);
                                    ?>
                                    <tr>
                                        <td> <?php echo $i; ?></td>
                                        <td> <?php echo $email; ?></td>
                                        <td> <?php echo $row['fullname']; ?></td> 
                                        <td><?php echo intval($row['marks']); ?>
                                            <!-- <a type="button" class="btn" data-toggle="modal" data-target="#myModal" data-id=" <?php echo  $row['courseid'] .",". $row['userid']; ?>"><?php echo intval($row['marks']); ?></a>  -->
                                            
                                            <!-- <a type="button" class="btn" href="index.php/crm/fetch_record?mystr=<?php echo  $row['courseid'] .",". $row['userid']; ?>"><?php echo intval($row['marks']); ?></a>  -->
                                            
                                        </td> 
                                        <td><?php echo intval($row['outof']); ?> </td> 
                                        <td><a type="button" class="btn btn-xs default" href="index.php/crm/fetch_record?mystr=<?php echo  $row['courseid'] .",". $row['userid']; ?>">View</a>  </td> 
                                    </tr>
                                <?php $i++;} 
                                    
                                ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><b>TOTAL</b></td> 
                                    <td><b><?php echo $m; ?> </b></td> 
                                    <td><b><?php echo $o; ?> </b></td> 
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        

<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT -->


        
    </div>
</div>


<div class="page-content-wrapper">
    <div class="page-content">
       
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" >Mark Details of <strong><span id="titl"></span></strong></h4>
                    </div>
                    <div class="modal-body">
                        <!-- <center>
                        <h4><?php //echo $row['courseid'];?></h4>
                        </center> -->
                        <table class="table" >
                            <thead>
                            <tr>
                                <th>Activity Name</th>
                                <!-- <th>Item Type</th> -->
                                <th>Activity Type</th>
                                <th>Obtained Marks</th>
                                <th>Out Of</th>
                            </tr>
                            </thead>
                            <tbody id="myTable"></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo site_url('crm/viewGrades'); ?>" class="btn blue" data-dismiss="modal">Close</a>
                    </div>
                </div>
                
            </div>
        </div>

    </div>
</div>
<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->


<script type="text/javascript">
    $('select[name="school_name"]').on('change', function() {
        var school_id = $(this).val();
        var sc = $("#school_name option:selected").text();  
        $('#cname').val(sc);     
        $.ajax({
            url: '<?php echo base_url().'index.php/users/get_department'?>',
            data: {'school_id': school_id },
            type: "POST",
            dataType: "json",
            success:function(data) 
            {
                $('select[name="dep_list"]').empty();
                $('select[name="dep_list"]').append('<option>Select Department</option>');
                $.each(data.data, function(key, value) {
                    $('select[name="dep_list"]').append('<option value="'+value.id+'">'+ value.name +'</option>');     
                });                 
            }
        });
    });

    $('select[name="dep_list"]').on('change', function() {
        var acdemic_yrid = $(this).val();
        $.ajax({
            url: '<?php echo base_url().'index.php/users/getyears'?>',
            data: {'acdemic_yrid': acdemic_yrid },
            type: "POST",
            dataType: "json",
            success:function(data) 
            {              
                $('select[name="acadmicyear"]').empty();
                $('select[name="acadmicyear"]').append('<option selected>Select Year</option>');
                $.each(data.data, function(key, value) {
                    $('select[name="acadmicyear"]').append('<option data-id="'+value.path+'#'+value.id+'#'+value.name+'" value="'+ value.id +'">'+ value.name +'</option>');                    
                });
            }
        });
    });
     

    $('select[name="acadmicyear"]').on('change', function() {
        var all_dep_data = $(this).find(':selected').attr('data-id'); 
        var details = all_dep_data.split('#');         
        $.ajax({
            url: '<?php echo base_url().'index.php/users/get_semester'?>',
            data: {'departmnt_id': details[1]},
            type: "POST",
            dataType: "json",
            success:function(data) 
            {  
                $('select[name="semester"]').empty();
                $('select[name="semester"]').append('<option selected>Select Semester</option>');
                $.each(data.data, function(key, value) {
                    $('select[name="semester"]').append('<option data-id="'+value.path+'#'+value.id+'#'+value.name+'" value="'+ value.id +'">'+ value.name +'</option>');                    
                });
            }
        });
    });

    $('select[name="semester"]').on('change', function() {
        var sem_data = $(this).find(':selected').attr('data-id'); 
        var details = sem_data.split('#');
        $.ajax({
            url: '<?php echo base_url().'index.php/users/get_course'?>',
            data: {'semester_id': details[1]},
            type: "POST",
            dataType: "json",
            success:function(data){
                var cid ='';
                $.each(data.data, function(key, value) {
                    cid += value.id+',' ;                    
                });
                var cids =  cid.slice(0, -1);
                $("#course_ids").val(cids);
           }
        });
    });

    $('#myModal').on('show.bs.modal', function (e) {
        var mystr = $(e.relatedTarget).data('id');
        var myarr = mystr.split(",");
        $('#titl').empty();
        $("#myTable").empty();
        $.ajax({
            type : 'POST',
            url : '<?php echo base_url().'crm/fetch_record' ?>', //Here you will fetch records 
            data :  'mystr='+ mystr, //Pass $id
            dataType: "json",
            success : function(data){
                $('#titl').append(data.data[0].fullname);
                $.each(data.data, function(key, value) {                    
                    $('#myTable').append('<tr><td>' + value.itemname+ '</td><td>' + value.itemmodule + '</td><td>' + Math.floor(value.marks) + '</td><td>' +  Math.floor(value.outof) + '</td></tr>');     
                });
            }
        });
     });
</script>