<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class feesManagement extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('feesmanagementmodel');
        $this->load->model('common');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    

    function feeScheme_detail()
    {
        if ($this->input->post('submit', TRUE)) 
        {
            //echo "<pre>";print_r($_POST);echo "</pre>";
            if($this->input->post('yesYearly')=='yesFeeYearly')
            {
                $feescheme_info = array(
                        'school_name' => $this->db->escape_like_str($this->input->post('school_name', TRUE)),
                        'class_title' => $this->db->escape_like_str($this->input->post('class_title', TRUE)),
                        'fee_name' => $this->db->escape_like_str($this->input->post('feeCategory', TRUE)),
                        'is_installment' => $this->db->escape_like_str($this->input->post('yesYearly', TRUE)),
                        'no_of_installment' => $this->db->escape_like_str($this->input->post('no_of_installment', TRUE)),
                        'amount' => $this->db->escape_like_str($this->input->post('totAmt', TRUE)), );
                    

                    $installmnt_number = $this->input->post('no_of_installment');                   

                    $this->db->insert('fee_scheme',$feescheme_info);
                    $inserted_id = $this->db->insert_id();
                   
                    for($f=0;$f<$installmnt_number;$f++)
                    { 
                        $dateofInstallment =  $this->input->post('getinstmntodat_'.$f);
                        $endtInstallment =  $this->input->post('enddate_'.$f);
                      
                        $amountfee = $this->input->post('instalamt_'.$f);
                     
                        $feeinsertedid = $inserted_id;                         

                        $sqlquery =  "INSERT INTO fee_scheme_installment(no_of_installment,fee_scheme_id,installment_start_dt,installment_end_dt)VALUES('$installmnt_number','$feeinsertedid','$dateofInstallment','$endtInstallment')";                           
                         
                         $resul = $this->db->query($sqlquery);                         
                        
                    }  
                   
                    $data['success'] = '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <strong>Success ! </strong> Fee Scheme Detail Added successfully. 
                            </div>';
                    $this->load->view('temp/header');
                    $this->load->view('feeScheme',$data);
                    $this->load->view('temp/footer');
                
            }
            /*else
            {
                $feescheme_info_monthly = array(
                        'school_name' => $this->db->escape_like_str($this->input->post('school_name', TRUE)),
                        'class_title' => $this->db->escape_like_str($this->input->post('class_title', TRUE)),
                        'fee_name' => $this->db->escape_like_str($this->input->post('feeCategory', TRUE)),
                        'is_installment' => $this->db->escape_like_str($this->input->post('yesMonthly', TRUE)),
                        'amount' => $this->db->escape_like_str($this->input->post('totAmt', TRUE)), );
                $monthlyamt = $this->input->post('totAmt');
                $months = $this->input->post('month');
               // print_r($months);
                for($m=0;$m<$months;$m++)
                {
                    $monthlyamt = $this->input->post('month'.$m);
                    print_r($monthlyamt); 
                }
                die();
            }*/
        }
    }

    

    function status_update()
    {
        $status = $this->input->post('status');
        $user_id = $this->input->post('id');
        //echo $status;
        $upd_status = $this->feesmanagementmodel->update_status($user_id,$status);
        if($upd_status)
        {
            //redirect('feesManagement/addTaxDetail');
            //die('hi');
            //echo "<script type='text/javascript'>alert('Successfully Updated!');</script>";
            
            redirect('feesManagement/addTaxDetail');
        }
        else
        {
            //die('hello');
            //echo "<script type='text/javascript'>alert('First you need to Inactivate the Active one!');</script>";
            $data['message'] = '<div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <strong>Failed! </strong> First you need to Inactivate the Active one 
                            </div>';
            redirect('feesManagement/addTaxDetail',$data);
        }

    }
    

    

   
    function feeDetails()
    {
       /* if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }*/
        if ($this->input->post('submit', TRUE)) 
        {
           // echo "<pre>";print_r($_POST);echo "</pre>";
            if($this->input->post('feeLumpsum')!=='yesLumpsum')
            {
                //print_r($this->input->post('feeTaxable'));die();
                $feeConfig = array(
                     'fee_plan_name' => $this->db->escape_like_str($this->input->post('feePlaname', TRUE)),
                    'fee_category' => $this->db->escape_like_str($this->input->post('feeCategory', TRUE)),
                    
                    'amount'=> $this->db->escape_like_str($this->input->post('feeamt', TRUE)),
                    'is_tax'=> $this->db->escape_like_str($this->input->post('taxHideId', TRUE)),
                    'feetax_calculated_amt'=> $this->db->escape_like_str($this->input->post('totAmt', TRUE)),
                    'downpayment'=> $this->db->escape_like_str($this->input->post('total_down', TRUE)),
                    'final_amount'=>$this->db->escape_like_str($this->input->post('payableWithGST', TRUE))
                );

                //print_r($feeConfig);
                    $this->db->insert('fees_type_master', $feeConfig);
                    $insert_id = $this->db->insert_id();
                    //print_r($insert_id);die();
                
                    $install_num = $this->input->post('feeInstallment_period');
                
                    for($i=0;$i<$install_num;$i++)
                    { 
                        if($i==0)
                        {
                            $date = date("Y-m-d");
                        }
                        else
                        { 
                            $date =  $this->input->post('nextdate_'.$i);
                        }
                            
                            $amount = $this->input->post('concatval_'.$i);
                            
                            $intallment = 'yes';
                            
                            $tax_id = $this->input->post('taxHideId');  

                            $fee_plan_name_id = $insert_id;                         

                          $sql =  "INSERT INTO installment_master(installment,amount,installment_date,tax_id,fee_plan_name_id)VALUES('$intallment','$amount','$date','$tax_id','$fee_plan_name_id')";                           
                          //$get_feeplan_id = "SELECT fee_plan_name_id from installment_master";
                         $result = $this->db->query($sql);
                         
                        
                    }  
                        if($result)
                        {    
                            $data['message'] = '<div class="alert alert-success alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                    <strong>Success ! </strong> Fee Detail with tax added successfully. 
                                </div>';
                            
                        }
                        else
                        {
                            $data['message'] = '<div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <strong>Failed! </strong> Something went wrong! 
                            </div>';                            

                        } 
                        $data['getTaxdetail'] = $this->feesmanagementmodel->get_active_tax();
                        $data['fee_configDetail'] = $this->feesmanagementmodel->AllLupmsum();
                        $this->load->view('temp/header');
                        $this->load->view('feeConfiguration',$data);
                        $this->load->view('temp/footer');

                
            }
            else
            {
                $feeConfig = array(
                    'fee_plan_name' => $this->db->escape_like_str($this->input->post('feePlaname', TRUE)),
                    'fee_category' => $this->db->escape_like_str($this->input->post('feeCategory', TRUE)),
                    'amount'=> $this->db->escape_like_str($this->input->post('feeamt', TRUE)),
                    'is_lumpsum' =>$this->db->escape_like_str($this->input->post('feeLumpsum', TRUE)),                 
                    'amount'=> $this->db->escape_like_str($this->input->post('feeamt', TRUE)),
                    'is_tax'=> $this->db->escape_like_str($this->input->post('taxHideId', TRUE)),
                    'feetax_calculated_amt'=> $this->db->escape_like_str($this->input->post('totAmt', TRUE)),
                    'downpayment'=> $this->db->escape_like_str($this->input->post('total_down', TRUE)),
                    'final_amount'=>$this->db->escape_like_str($this->input->post('payableWithGST', TRUE)),
                    
                );
                if($this->db->insert('fees_type_master', $feeConfig))
                {
                     $data['message'] = '<div class="alert alert-success alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                    <strong>Success ! </strong> Fee Detail Lumpsum amount added successfully. 
                                </div>';
                    $data['getTaxdetail'] = $this->feesmanagementmodel->get_active_tax();
                    $data['fee_configDetail'] = $this->feesmanagementmodel->AllLupmsum();
                    $this->load->view('temp/header');
                    $this->load->view('feeConfiguration',$data);
                    $this->load->view('temp/footer');
                }
            }
        }
    }

    function feesStructure(){
    //    $data['getTaxdetail'] = $this->feesmanagementmodel->get_active_tax();
    //    $data['fee_configDetail'] = $this->feesmanagementmodel->AllfeeType();
        $this->load->view('temp/header');
        $this->load->view('feesStructure');
        $this->load->view('temp/footer');
    }
    function feesReceipt(){
        // $this->load->view('temp/header');
        $this->load->view('feesReceipt');
        // $this->load->view('temp/footer');
    }
    function feesReceipt1(){
        $data['fees_receipt_data'] = $this->feesmanagementmodel->allFeesReceiptData();
        $this->load->view('temp/header');
        $this->load->view('feesReceipt1',$data);
        $this->load->view('temp/footer');
    }
}