<?php 

	class Sendemail extends CI_Controller
	{
			public function __construct()
			{
				parent::__construct();
				$this->load->library('email');
				$this->load->model('common');
				
			}

			public function index()
			{
				//$this->load->view('emailresult');
				 $data['s_class'] = $this->common->getAllData('department');
            	$this->load->view('auth/registration_form',$data);
			}

			public function registration_instruction()
	        {
	            $this->load->view('auth/registration_instruction');
	        }
			

		/*public function send()
		{
	    	$to =  $this->input->post('from');  // User email pass here
	    	$subject = 'Welcome To Yofundo ERP';

	    	$from = 'swati.belure@jhamobi.com'; // Pass here your mail id

	    	$emailContent = '<!DOCTYPE><html><head></head><body><table width="600px" style="border:1px solid #cccccc;margin: auto;border-spacing:0;"><tr><td style="background:#000000;padding-left:3%"><img src="https://yofundo.in/erp/assets/admin/layout/img/smlogo.png" width="300px" vspace=10 /></td></tr>';
	    	$emailContent .='<tr><td style="height:20px"></td></tr>';


	    	//$emailContent .= $this->input->post('message');  //   Post message available here
	    	$user_rand = rand();
            $emailContent .= '<p class="form-control">
                             Thank you for Registration, for verification purpose please click on given link <a href="'.base_url('index.php/Sendemail/verify').'/'.$to.'/'.$user_rand.'">Click Here</a></p>';  


	    	$emailContent .='<tr><td style="height:20px"></td></tr>';
	    	$emailContent .= "<tr><td style='background:#000000;color: #999999;padding: 2%;text-align: center;font-size: 13px;'><p style='margin-top:1px;'><a href='https://yofundo.in/' target='_blank' style='text-decoration:none;color: #60d2ff;'>www.yofundo.in</a></p></td></tr></table></body></html>";
	                


	    	$config['protocol']    = 'smtp';
	    	$config['smtp_host']    = 'ssl://smtp.gmail.com';
	    	$config['smtp_port']    = '465';
	   

	    	$config['smtp_user']    = 'swati.belure@jhamobi.com';    //Important
	    	$config['smtp_pass']    = 'jh@mobi123';  //Important

	    	$config['charset']    = 'utf-8';
	    	$config['newline']    = "\r\n";
	    	$config['mailtype'] = 'html'; // or html
	    	$config['validation'] = TRUE; // bool whether to validate email or not 

	     

		    $this->email->initialize($config);
		    $this->email->set_mailtype("html");
		    $this->email->from($from);
		    $this->email->to($to);
		    $this->email->subject($subject);
		    $this->email->message($emailContent);
		    $this->email->send();
	    
		    if($this->email->send())
		    {
		    	$this->session->set_flashdata('msg',"Mail has been sent successfully");
		    	return redirect('Sendemail');
		    }
		    else
		    {
		    	//$this->session->set_flashdata('msg_class','alert-success');
		    	echo $this->email->print_debugger();
		    	//return redirect('Sendemail');
		    }
	}

	public function verify($to)
	{
		$validate_email = $this->common->validate_email($to);
		if($validate_email == true)
		{
			$this->load->view('validate_email',array('email'=>$to));
		}
		else
		{
			echo "Error, giving email activated confirmation";
		}
	}*/

	public function save_data()
        {

            $class_id = $this->db->escape_like_str($this->input->post('class', TRUE));

            $class_title = $this->common->class_title($class_id);
            if ($this->input->post('submit', TRUE)) 
            {
                 //Here is uploading the student's photo.
                $config['upload_path'] = './assets/uploads/registration/';
               
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '10000';
                $config['max_width'] = '10240';
                $config['max_height'] = '7680';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
               // $this->upload->initialize($config);
                $this->upload->do_upload('userfile');
                $uploadFileInfo = $this->upload->data();
                //print_r($uploadFileInfo['file_name']);die();
                //redirect('auth/registration_form', 'refresh');
                
                $username = $this->input->post('first_name') . ' ' . $this->input->post('last_name');
               // $phone =  $this->input->post('fathers_mob', TRUE);
               // $class_id = $this->db->escape_like_str($this->input->post('class', TRUE));
                //$userid = $this->common->usersId();
                $studentsInfo = array(
                    'academic_year' => $this->db->escape_like_str($this->input->post('academic_year', TRUE)),
                    'class' => $this->db->escape_like_str($class_id, TRUE),
                    'first_name'=>$this->db->escape_like_str($this->input->post('first_name', TRUE)),
                    'student_id' => $this->db->escape_like_str($this->input->post('student_id', TRUE)),
                    'middle_name'=> $this->db->escape_like_str($this->input->post('mid_name', TRUE)),
                    'last_name'=> $this->db->escape_like_str($this->input->post('last_name', TRUE)),
                    'dob' => $this->db->escape_like_str($this->input->post('dob', TRUE)),
                    'email' => $this->db->escape_like_str($this->input->post('email', TRUE)),
                    'nationality' => $this->db->escape_like_str($this->input->post('nationality', TRUE)),
                    'gender' => $this->db->escape_like_str($this->input->post('gender', TRUE)),
                    'caste' => $this->db->escape_like_str($this->input->post('caste_cat', TRUE)),
                    'religion' => $this->db->escape_like_str($this->input->post('religion', TRUE)),
                    'student_photo' => $uploadFileInfo['file_name'],
                    'birth_place' => $this->db->escape_like_str($this->input->post('birth_place', TRUE)),
                    'address' => $this->db->escape_like_str($this->input->post('address', TRUE)),
                    'pin_code' => $this->db->escape_like_str($this->input->post('pin', TRUE)),
                    'fathers_firstname' => $this->db->escape_like_str($this->input->post('fathers_firstname', TRUE)),
                    'fathers_midname' => $this->db->escape_like_str($this->input->post('faters_midname', TRUE)),
                    'fathers_lastname' => $this->db->escape_like_str($this->input->post('fathers_lastname', TRUE)),
                    'fathers_mobile' => $this->db->escape_like_str($this->input->post('fathers_mob', TRUE)),
                    'fathers_occup' => $this->db->escape_like_str($this->input->post('fathers_occ', TRUE)),
                    'mothers_occup' => $this->db->escape_like_str($this->input->post('mothers_occ', TRUE)),
                    'mothers_firstname' => $this->db->escape_like_str($this->input->post('mothers_firstname', TRUE)),
                    'mothers_midname' => $this->db->escape_like_str($this->input->post('mothers_midname', TRUE)),
                    'mothers_lastname' => $this->db->escape_like_str($this->input->post('mothers_lastname', TRUE)),
                    'mothers_mobile' => $this->db->escape_like_str($this->input->post('mothers_mob', TRUE)),
                    'mothers_qualification' => $this->db->escape_like_str($this->input->post('mothers_education', TRUE)),
                    'fathers_qualification' => $this->db->escape_like_str($this->input->post('fathers_education', TRUE)),
                    'mother_toung' => $this->db->escape_like_str($this->input->post('mother_tounge', TRUE)),
                    'anuual_income' => $this->db->escape_like_str($this->input->post('father_incom_range', TRUE)),
                    'isthr_medical_condition' => $this->db->escape_like_str($this->input->post('medical_condition', TRUE)),
                    'isthr_undergoing_treatment' => $this->db->escape_like_str($this->input->post('undergoing_treatment', TRUE)),
                    'is_child_taking_medication' =>  $this->db->escape_like_str($this->input->post('medication', TRUE)),
                    'food_allergy' =>  $this->db->escape_like_str($this->input->post('food_allergy', TRUE)),
                    'medical_fitness_cert' => $this->db->escape_like_str($this->input->post('medical_fitness_cert', TRUE)),
                    'adoption_document' => $this->db->escape_like_str($this->input->post('adoption_document', TRUE)),
                    'passport_photos' => $this->db->escape_like_str($this->input->post('passport_photos', TRUE)),
                    'passport_pic_both_parent' =>  $this->db->escape_like_str($this->input->post('passport_pic_both_parent', TRUE)),
                    'original_birth_cert' => $this->db->escape_like_str($this->input->post('original_birth_cert', TRUE)),
                    'pan_card_copy_parent' => $this->db->escape_like_str($this->input->post('pan_card_copy_parent', TRUE)),
                    'adhar_copy_parent'=> $this->db->escape_like_str($this->input->post('adhar_copy_parent', TRUE)),
                    'caste_certificate' => $this->db->escape_like_str($this->input->post('caste_certificate', TRUE)),
                    'original_bonafied' => $this->db->escape_like_str($this->input->post('original_bonafied', TRUE)),
                    'original_tc' => $this->db->escape_like_str($this->input->post('tc', TRUE)),
                    'residence_proof' => $this->db->escape_like_str($this->input->post('residence_proof', TRUE)),
                    'if_foreign_nationals_copy' => $this->db->escape_like_str($this->input->post('if_foreign_nationals_copy', TRUE)),
                    'copy_report_card' => $this->db->escape_like_str($this->input->post('copy_report_card', TRUE)),
                    
                );
                
                if ($this->db->insert('registration_student', $studentsInfo)) 
                {
                    $student_info_id = $this->db->insert_id();
                    $additionalData = array(
                        'year' => $this->db->escape_like_str(date('Y')),
                        'user_id' => $this->db->escape_like_str($student_info_id),
                        'roll_number' => $this->db->escape_like_str($this->input->post('roll_number', TRUE)),
                        'student_id' => $this->db->escape_like_str($this->input->post('student_id', TRUE)),
                        'class_title' => $this->db->escape_like_str($class_title),
                        'class_id' => $this->db->escape_like_str($class_id),
                        'section' => $this->db->escape_like_str($this->input->post('section', TRUE)),
                        'student_title' => $this->db->escape_like_str($username),
                    );

                    if ($this->db->insert('class_students', $additionalData)) 
                    {
                        $studentAmount = $this->common->classStudentAmount($class_id);
                        $clas_info = array(
                            'student_amount' => $this->db->escape_like_str($studentAmount)
                        );
                        $this->db->where('id', $class_id);
                        if ($this->db->update('class', $clas_info)) 
                        {
                            $student_access = array(
                                'user_id' => $this->db->escape_like_str($student_info_id),
                                'group_id' => $this->db->escape_like_str(3),
                                'das_top_info' => $this->db->escape_like_str(0),
                                'das_grab_chart' => $this->db->escape_like_str(0),
                                'das_class_info' => $this->db->escape_like_str(0),
                                'das_message' => $this->db->escape_like_str(1),
                                'das_employ_attend' => $this->db->escape_like_str(0),
                                'das_notice' => $this->db->escape_like_str(1),
                                'das_calender' => $this->db->escape_like_str(1),
                                'admission' => $this->db->escape_like_str(0),
                                'all_student_info' => $this->db->escape_like_str(0),
                                'stud_edit_delete' => $this->db->escape_like_str(0),
                                'stu_own_info' => $this->db->escape_like_str(1),
                                'teacher_info' => $this->db->escape_like_str(1),
                                'add_teacher' => $this->db->escape_like_str(0),
                                'teacher_details' => $this->db->escape_like_str(0),
                                'teacher_edit_delete' => $this->db->escape_like_str(0),
                                'all_parents_info' => $this->db->escape_like_str(0),
                                'own_parents_info' => $this->db->escape_like_str(1),
                                'make_parents_id' => $this->db->escape_like_str(0),
                                'parents_edit_dlete' => $this->db->escape_like_str(0),
                                'add_new_class' => $this->db->escape_like_str(0),
                                'all_class_info' => $this->db->escape_like_str(0),
                                'class_details' => $this->db->escape_like_str(0),
                                'class_delete' => $this->db->escape_like_str(0),
                                'class_promotion' => $this->db->escape_like_str(0),
                                'assin_optio_sub' => $this->db->escape_like_str(0),
                                'add_class_routine' => $this->db->escape_like_str(0),
                                'own_class_routine' => $this->db->escape_like_str(1),
                                'all_class_routine' => $this->db->escape_like_str(0),
                                'rutin_edit_delete' => $this->db->escape_like_str(0),
                                'attendance_preview' => $this->db->escape_like_str(0),
                                'take_studence_atten' => $this->db->escape_like_str(0),
                                'edit_student_atten' => $this->db->escape_like_str(0),
                                'add_employee' => $this->db->escape_like_str(0),
                                'employee_list' => $this->db->escape_like_str(0),
                                'employ_attendance' => $this->db->escape_like_str(0),
                                'empl_atte_view' => $this->db->escape_like_str(0),
                                'add_subject' => $this->db->escape_like_str(0),
                                'all_subject' => $this->db->escape_like_str(0),
                                'make_suggestion' => $this->db->escape_like_str(0),
                                'all_suggestion' => $this->db->escape_like_str(0),
                                'own_suggestion' => $this->db->escape_like_str(1),
                                'add_exam_gread' => $this->db->escape_like_str(0),
                                'exam_gread' => $this->db->escape_like_str(0),
                                'add_exam_routin' => $this->db->escape_like_str(0),
                                'all_exam_routine' => $this->db->escape_like_str(0),
                                'own_exam_routine' => $this->db->escape_like_str(1),
                                'exam_attend_preview' => $this->db->escape_like_str(0),
                                'approve_result' => $this->db->escape_like_str(0),
                                'view_result' => $this->db->escape_like_str(1),
                                'all_mark_sheet' => $this->db->escape_like_str(0),
                                'own_mark_sheet' => $this->db->escape_like_str(1),
                                'take_exam_attend' => $this->db->escape_like_str(0),
                                'change_exam_attendance' => $this->db->escape_like_str(0),
                                'make_result' => $this->db->escape_like_str(0),
                                'add_category' => $this->db->escape_like_str(0),
                                'all_category' => $this->db->escape_like_str(1),
                                'edit_delete_category' => $this->db->escape_like_str(0),
                                'add_books' => $this->db->escape_like_str(0),
                                'all_books' => $this->db->escape_like_str(1),
                                'edit_delete_books' => $this->db->escape_like_str(0),
                                'add_library_mem' => $this->db->escape_like_str(0),
                                'memb_list' => $this->db->escape_like_str(0),
                                'issu_return' => $this->db->escape_like_str(0),
                                'add_dormitories' => $this->db->escape_like_str(0),
                                'add_set_dormi' => $this->db->escape_like_str(0),
                                'set_member_bed' => $this->db->escape_like_str(0),
                                'dormi_report' => $this->db->escape_like_str(1),
                                'add_transport' => $this->db->escape_like_str(0),
                                'all_transport' => $this->db->escape_like_str(1),
                                'transport_edit_dele' => $this->db->escape_like_str(0),
                                'add_account_title' => $this->db->escape_like_str(0),
                                'edit_dele_acco' => $this->db->escape_like_str(0),
                                'trensection' => $this->db->escape_like_str(0),
                                'fee_collection' => $this->db->escape_like_str(0),
                                'all_slips' => $this->db->escape_like_str(0),
                                'own_slip' => $this->db->escape_like_str(1),
                                'slip_edit_delete' => $this->db->escape_like_str(0),
                                'pay_salary' => $this->db->escape_like_str(0),
                                'creat_notice' => $this->db->escape_like_str(0),
                                'send_message' => $this->db->escape_like_str(0),
                                'vendor' => $this->db->escape_like_str(0),
                                'delet_vendor' => $this->db->escape_like_str(0),
                                'add_inv_cat' => $this->db->escape_like_str(0),
                                'inve_item' => $this->db->escape_like_str(0),
                                'delete_inve_ite' => $this->db->escape_like_str(0),
                                'delete_inv_cat' => $this->db->escape_like_str(0),
                                'inve_issu' => $this->db->escape_like_str(0),
                                'delete_inven_issu' => $this->db->escape_like_str(0),
                                'check_leav_appli' => $this->db->escape_like_str(0),
                                'setting_manage_user' => $this->db->escape_like_str(0),
                                'setting_accounts' => $this->db->escape_like_str(0),
                                'other_setting' => $this->db->escape_like_str(0),
                                'front_setings' => $this->db->escape_like_str(0),
                            );
                            if ($this->db->insert('role_based_access', $student_access)) 
                            {
                                $to =  $this->input->post('email');  // User email pass here
                                $subject = 'Welcome To Yofundo ERP';

                                $from = 'swati.belure@jhamobi.com'; // Pass here your mail id

                                $emailContent = '<!DOCTYPE><html><head></head><body><table width="100%" style="border:1px solid #cccccc;margin: auto;border-spacing:0;"><tr><td style="background:#000000;padding-left:3%"><img src="https://yofundo.in/erp/assets/admin/layout/img/smlogo.png" width="300px" vspace=10 /></td></tr>';
                                $emailContent .='<tr><td style="height:20px"></td></tr>';

                                $user_rand = rand();
                                $emailContent .= '<p class="form-control">
                                Thank you for Registration, Please <a href="'.base_url('Sendemail/verify').'/'.$to.'/'.$user_rand.'">Click Here</a> to activate your account</p>';  
                            
                                $emailContent .= '<p>Please use these details for Login</p>';
                                $emailContent .= '<p>Username - student@student.com</p>';
                                $emailContent .= '<p>Password - Password01$</p>';                     
                                $emailContent .='<tr><td style="height:20px"></td></tr>';
                                $emailContent .= "<tr><td style='background:#000000;color: #999999;padding: 2%;text-align: center;font-size: 13px;'><p style='margin-top:1px;'><a href='https://yofundo.in/' target='_blank' style='text-decoration:none;color: #60d2ff;'>www.yofundo.in</a></p></td></tr></table></body></html>";                 


	                           $config['protocol']    = 'smtp';
	                           $config['smtp_host']    = 'ssl://smtp.gmail.com';
	                           $config['smtp_port']    = '465';       

	                            $config['smtp_user']    = 'swati.belure@jhamobi.com';    //Important
	                           $config['smtp_pass']    = 'jh@mobi123';  //Important

	                           $config['charset']    = 'utf-8';
	                           $config['newline']    = "\r\n";
	                           $config['mailtype'] = 'html'; // or html
	                           $config['validation'] = TRUE; // bool whether to validate email or not          

	                           $this->email->initialize($config);
	                           $this->email->set_mailtype("html");
	                           $this->email->from($from);
	                           $this->email->to($to);
	                           $this->email->subject($subject);
	                           $this->email->message($emailContent);
	                           $this->email->send();
        
    	                        if($this->email->send())
    	                        {
    	                            $data['success'] = 
    	                                        '<div class="alert alert-success alert-dismissable admisionSucceassMessageFont">
    	                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
    	                                                <strong>Success!</strong> The Student Registration done Successfully, Please check your email.
    	                                        </div>';      
    	                        }
    	                        else
    	                        {
    	                            $data['success'] = 
    	                                        '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
    	                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
    	                                                <strong>Failed!</strong> While doing Registration.
    	                                        </div>';
    	                            //echo $this->email->print_debugger(); 	                           
    	                        }                                                          
                                           
                                $this->load->view('auth/registration_form', $data);

                            }
                        }
                    }
                }
            }
       } 

       public function verify($to)
       {
            $validate_email = $this->common->validate_email($to);
            if($validate_email)
            {
                $this->load->view('auth/validate_email',array('email'=>$to));
            }
            else
            {
                echo "Error, giving email activated confirmation";
            }
       }


}


	
	
	
	
	

