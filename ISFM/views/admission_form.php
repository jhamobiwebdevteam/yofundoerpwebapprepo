<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!--Start page level style-->

<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
       <!--  <div class="row">
            <div class="col-md-12">
                
                <h3 class="page-title">
                    <?php echo lang('header_fee_type'); ?> <small></small>
                </h3>
                 <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_fees'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_admission'); ?>
                    </li>
                   
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12">
               
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('header_admission'); ?>
                        </div>
                        
                    </div>
                </div>
                    <div class="tabbable tabbable-custom tabbable-full-width">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-1">Basic Details</a>
                            </li>
                            <li class="disabled" style="pointer-events: none;cursor: default;opacity 0.6;">
                                <a class="nav-link" data-toggle="tab" href="#tabs-2">Family Details</a>
                            </li>
                            <li class="disabled" style="pointer-events: none;cursor: default;opacity 0.6;">
                                <a class="nav-link" data-toggle="tab" href="#tabs-3">Required Documents</a>
                            </li>
                        </ul><!-- Tab panes -->
                       
                            <?php if (!empty($success)) 
                            {
                                echo $success;
                            }
                            ?>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-1">

                                 <?php $form_attributs = array('class' => 'form-horizontal', 'role' => 'form', 'name' => 'myForm', 'onsubmit' => 'return validateForm()');
                                echo form_open_multipart('users/admission', $form_attributs);?>
                                <div class="row margin-left">
                                    <div class="col-md-12">

                                            <div class="form-group col-md-12">
                                                    <label class="col-md-3 control-label"><?php echo lang('admi_Class'); ?> <span class="requiredStar"> * </span></label>
                                                    <div class="col-md-6">
                                                        <select name="class" onchange="classInfo(this.value)" class="form-control" data-validation="required" data-validation-error-msg="<?php echo lang('admi_Class_error_msg');?>">
                                                            <option value=""><?php echo lang('admi_select_class');?></option>
                                                            <?php foreach ($s_class as $row) { ?>
                                                                <option value="<?php echo $row['id']; ?>"><?php echo $row['class_title']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="txtHint">

                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><?php echo lang('admi_students_photo');?> <span class="requiredStar">  </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail">
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail uploadImagePreview">
                                                            </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> <?php echo lang('admi_select_photo'); ?> </span>
                                                                    <span class="fileinput-exists">
                                                                        <?php echo lang('admi_stu_photo_change'); ?> </span>
                                                                    <input type="file" name="userfile" >
                                                                </span>
                                                                <a href="#" class="btn red fileinput-exists" data-dismiss="fileinput">
                                                                    <?php echo lang('admi_stu_photo_remove'); ?> </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        
                                            <div class="form-group col-md-12">
                                                <label class="col-md-2 control-label"><?php echo lang('header_sname');?> <span class="requiredStar"> * </span></label>
                                                <div class="col-md-6">
                                                <!-- <input type="text" class="form-control" name="school_name"> -->                                    
                                                    <select class="form-control" name="school_name" id="school_name">
                                                        <option value="">Select School Name</option>
                                                        <?php foreach($data['data'] as $key) { ?>
                                                            <option value="<?php echo $key['id'];?>"><?php echo $key['name'];?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label class="col-md-2 control-label"><?php echo lang('admi_academic_yr'); ?> <span class="requiredStar"> * </span></label>
                                                <div class="col-md-2">
                                                    
                                                    <select class="form-control" name="academic_year" id="academic_year">
                                                        
                                                    </select>
                                                </div>
                                            
                                                <label class="col-md-2 control-label"><?php echo lang('admi_FirstName'); ?> <span class="requiredStar">  </span></label>
                                                <div class="col-md-2">
                                                    <input type="text" class="form-control" placeholder="" name="first_name" >
                                                </div>
                                                <label class="col-md-2 control-label"><?php echo lang('admi_MidName'); ?> <span class="requiredStar"> * </span></label>
                                                <div class="col-md-2">
                                                    <input type="text" class="form-control" placeholder="" name="mid_name" data-validation="required" data-validation-error-msg="<?php echo lang('Middle name is required field.'); ?>">
                                                </div>
                                            </div>

                                            <div class="form-group col-md-12">
                                                           
                                                <label class="col-md-2 control-label"><?php echo lang('admi_LastName'); ?> <span class="requiredStar">  </span></label>
                                                    <div class="col-md-2">
                                                        <input type="text" class="form-control" placeholder="" name="last_name" >
                                                    </div>
                           
                                                <label class="control-label col-md-2"><?php echo lang('admi_DateOfBirth'); ?> <span class="requiredStar"> * </span></label>
                                                    <div class="col-md-2">
                                                        <input class="form-control" name="birthdate" id="mask_date2" type="text" data-validation="required" data-validation-error-msg="<?php echo lang('admi_DateOfBirth_error_msg'); ?>">
                                                        <span class="help-block">
                                                                Date Type  DD/MM/YYYY </span>
                                                    </div>

                                                 <label class="col-md-2 control-label"><?php echo lang('admi_Email'); ?><span class="requiredStar"> * </span></label>
                                                    <div class="col-md-2">
                                                        <input type="email" class="form-control" onkeyup="checkEmail(this.value)" placeholder="demo@demo.com" name="email" data-validation="email required" data-validation-error-msg="<?php echo lang('admi_Email_error_msg'); ?>">
                                                            <span class="help-block">This student can login his profile by this Email and Password </span>
                                                            <div id="checkEmail" class="col-md-12"></div>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-12">
                                                           
                                                    <label class="col-md-2 control-label"><?php echo lang('admi_Nationality'); ?></label>
                                                    <div class="col-md-2">
                                                       <!--  <input type="text" class="form-control" placeholder="" name="last_name" > -->
                                                       <select class="form-control" name="nationality">
                                                            <option>Select</option>
                                                            <option value="Indian">Indian</option>
                                                            <option value="American">American</option>
                                                            <option value="British">British</option>
                                                            <option value="Australian">Australian</option>
                                                            <option value="Canadian">Canadian</option>
                                                        </select>
                                                    </div>

                                                    <label class="col-md-2 control-label"><?php echo lang('admi_Sex'); ?> <span class="requiredStar"> * </span></label>
                                                    <div class="col-md-2">
                                                        <select class="form-control" name="gender">
                                                            <option>Select</option>
                                                            <option value="male">Male</option>
                                                            <option value="female">Female</option>
                                                            <option value="other">Other</option>
                                                        </select>                                     
                                                    </div>

                                                    <label class="col-md-2 control-label"><?php echo lang('admi_casteCat'); ?> <span class="requiredStar"> * </span></label>
                                                    <div class="col-md-2">
                                                        <select class="form-control" name="caste_cat">
                                                            <option>Select</option>
                                                            <option value="Open">Open</option>
                                                            <option value="OBC">OBC</option>
                                                            <option value="NT-B">NT-B</option>
                                                            <option value="NT-C">NT-C</option>
                                                            <option value="NT-D">NT-D</option>
                                                            <option value="Other">Other</option>
                                                            <option value="SC">SC</option>
                                                            <option value="ST">ST</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-12">
                                                    <label class="col-md-2 control-label"><?php echo lang('admi_Religion'); ?> <span class="requiredStar"> * </span></label>
                                                    <div class="col-md-2">
                                                        <select class="form-control" name="religion">
                                                            <option>Select</option>
                                                            <option value="Hindu">Hindu</option>
                                                            <option value="Muslim">Muslim</option>
                                                            <option value="Buddhism">Buddhism</option>
                                                            <option value="Christian">Christian</option>
                                                            <option value="Jainism">Jainism</option>
                                                            <option value="Jew">Jew</option>
                                                            <option value="Navbauddha">Navbauddha</option>
                                                            <option value="Parsi">Parsi</option>
                                                            <option value="Sikh">Sikh</option>
                                                            <option value="Other">Other</option>
                                                        </select>
                                                    </div>

                                                    <label class="col-md-2 control-label"><?php echo lang('admi_birthplace'); ?> <span class="requiredStar"> * </span></label>
                                                    <div class="col-md-2">
                                                        <input type="text" class="form-control" name="birth_place" required="required">
                                                    </div>
                                                    <label class="col-md-2 control-label"> <?php echo lang('admi_PresentAddress'); ?> <span class="requiredStar"> * </span></label>
                                                    <div class="col-md-2">
                                                        <textarea class="form-control" name="present_address" rows="3" data-validation="required" data-validation-error-msg="<?php echo lang('admi_PresentAddress_error_msg'); ?>"> </textarea>
                                                    </div>

                                                </div>

                                                <div class="form-group col-md-12">
                                                    <label class="col-md-2 control-label"><?php echo lang('admi_pin'); ?> <span class="requiredStar"> * </span></label>
                                                     <div class="col-md-2">
                                                        <input type="text" class="form-control" name="pin" required="required">
                                                    </div>

                                                    <label class="col-md-2 control-label"><?php echo lang('admi_FatherName'); ?> <span class="requiredStar"> * </span></label>
                                                    <div class="col-md-2">
                                                        <input type="text" class="form-control" name="father_name" placeholder="" data-validation="required" data-validation-error-msg="<?php echo lang('admi_FatherName_error'); ?>">
                                                    </div>

                                                    <label class="col-md-2 control-label"><?php echo lang('admi_FathrsMidname'); ?> <span class="requiredStar"> * </span></label>
                                                    <div class="col-md-2">
                                                        <input type="text" class="form-control" name="faters_midname" required="required">
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn green" name="submit" value="submit"><?php echo lang('save');?></button>
                                    <a  href="#tabs-2" data-toggle="tab" class="btn default">Next</a>
                                </div>

                                <?php echo form_close(); ?>

                            </div>
                            
                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                <div class="row margin-left">
                                    <div class="col-md-12">
                                <div class="form-group col-md-12">
                                    <label class="col-md-2 control-label"><?php echo lang('admi_FathrsLastname'); ?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="fathers_lastname" required="required">
                                    </div>
                                    <label class="col-md-2 control-label"><?php echo lang('admi_FathrsMob'); ?> <span class="requiredStar"> * </span></label>
                                
                                    <!-- <input type="text" class="form-control" name="fathers_mob" required="required"> -->
                                    <div class="col-md-1">
                                        <input type="text" class="form-control" name="phoneCode" placeholder="+880"  data-validation="required" data-validation-error-msg="" value="<?php if(!empty($countryPhoneCode)){echo $countryPhoneCode;}?>">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="phone" placeholder=""  data-validation="required" data-validation-error-msg="">
                                        <span class="help-block">
                                            1600-000000</span>
                                    </div>
                               
                                </div>

                            <div class="form-group">
                                 <label class="col-md-2 control-label"><?php echo lang('admi_FatherOccupation'); ?> <span class="requiredStar">  </span></label>
                                <div class="col-md-2">
                                    <select name="father_occupation" class="form-control">
                                        <option value=""><?php echo lang('admi_father_occupation_op1'); ?></option>
                                        <option value="Business"><?php echo lang('admi_father_occupation_op2'); ?></option>
                                        <option value="Employer"><?php echo lang('admi_father_occupation_op3'); ?></option>
                                        <option value="Banker"><?php echo lang('admi_father_occupation_op4'); ?></option>
                                        <option value="Teachers"><?php echo lang('admi_father_occupation_op5'); ?></option>
                                        <option value="Farmer"><?php echo lang('admi_father_occupation_op6'); ?></option>
                                        <option value="Car Driver"><?php echo lang('admi_father_occupation_op7'); ?></option>
                                        <option value="Other"><?php echo lang('admi_father_occupation_op8'); ?></option>
                                    </select>
                                </div>

                                <label class="col-md-2 control-label"><?php echo lang('admi_father_Income_range'); ?> <span class="requiredStar">  </span></label>
                                <div class="col-md-5">
                                    <input type="text" name="father_incom_range" class="form-control" placeholder="" >
                                    <span class="help-block"><?php echo lang('admi_father_Income_range_demo_text');?></span>
                                </div>
                                
                            </div>

                            <div class="form-group col-md-12">
                                <label class="col-md-2 control-label"><?php echo lang('admi_MotherName'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="mother_name" placeholder="" data-validation="required" data-validation-error-msg="<?php echo lang('admi_MotherName_error_msg'); ?>">
                                </div>
                                <label class="col-md-2 control-label"><?php echo lang('admi_Mother_occup'); ?></label>
                                <div class="col-md-2">
                                    <select name="mother_occupation" class="form-control" >
                                        <option value=""><?php echo lang('admi_Mother_occup_valu_1'); ?></option>
                                        <option value="Housewife"><?php echo lang('admi_Mother_occup_valu_2'); ?></option>
                                        <option value="Business"><?php echo lang('admi_Mother_occup_valu_3'); ?></option>
                                        <option value="Employer"><?php echo lang('admi_Mother_occup_valu_4'); ?></option>
                                        <option value="Banker"><?php echo lang('admi_Mother_occup_valu_5'); ?></option>
                                        <option value="Teachers"><?php echo lang('admi_Mother_occup_valu_6'); ?></option>
                                        <option value="Farmer"><?php echo lang('admi_Mother_occup_valu_7'); ?></option>
                                        <option value="Car Driver"><?php echo lang('admi_Mother_occup_valu_8'); ?></option>
                                        <option value="Other"><?php echo lang('admi_Mother_occup_valu_9'); ?></option>
                                    </select>
                                </div>
                                <label class="col-md-2 control-label"><?php echo lang('admi_MotherMidname'); ?></label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="mothers_midname">
                                </div>
                            </div>

                             <div class="form-group col-md-12">
                               <label class="col-md-2 control-label"><?php echo lang('admi_MotherLastname'); ?></label>
                               <div class="col-md-2">
                                    <input type="text" class="form-control" name="mothers_lastname">
                                </div>
                                <label class="col-md-2 control-label"><?php echo lang('admi_MotherMob'); ?></label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="mothers_mob" >
                                </div>
                                <label class="col-md-2 control-label"><?php echo lang('admi_FathersQalif'); ?></label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="fathers_education" >
                                </div>   

                            </div>

                            <div class="form-group col-md-12">
                                <label class="col-md-2 control-label"><?php echo lang('admi_MothersQualif'); ?></label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="mothers_education">
                                </div>


                                <label class="col-md-2 control-label"><?php echo lang('admi_MotherToung'); ?></label>
                                <div class="col-md-2">
                                    <select class="form-control" name="mother_tounge">
                                        <option>Select</option>
                                        <option value="English">English</option>
                                        <option value="Marathi">Marathi</option>
                                        <option value="Hindi">Hindi</option>
                                        <option value="Gujrati">Gujrati</option>
                                    </select>
                                </div>
                                 
                                
                            </div>
                        </div>
                    </div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="" class="btn green" name="submit" value="submit">Update</button>
                                    <a  href="#tabs-3" data-toggle="tab" class="btn default">Next</a>
                                </div>
                            </div>
                                
                            </div>
                            <div class="tab-pane" id="tabs-3" role="tabpanel">
                                <div class="row margin-left">
                                    <div class="col-md-12">
                                        <div class="form-group col-md-12">
                                <label class="col-md-4 control-label"><?php echo lang('admi_childMedicalCond'); ?> <span class="requiredStar"></span></label>
                                <div class="col-md-1">
                                     <input type="radio" name="medical_condition" value="yes">                         
                                 </div>
                                <label class="col-md-1 control-label radio-inline">Yes</label>
                                 <div class="col-md-1">
                                    <input type="radio" name="medical_condition" value="no">                         
                                </div>
                                <label class="col-md-1 control-label radio-inline">No</label>
                            </div>

                            <div class="form-group col-md-12">
                                 <label class="col-md-4 control-label"><?php echo lang('admi_undergoingTreatment'); ?> <span class="requiredStar"></span></label>
                                 <div class="col-md-1">
                                     <input type="radio" name="undergoing_treatment" value="yes">                         
                                 </div>
                                 <label class="col-md-1 control-label radio-inline">Yes</label>
                                 <div class="col-md-1">
                                     <input type="radio" name="undergoing_treatment" value="no">                         
                                 </div>
                                 <label class="col-md-1 control-label radio-inline">No</label>
                            </div>

                            <div class="form-group col-md-12">
                                 <label class="col-md-4 control-label"><?php echo lang('admi_IschildTakingMedication'); ?> <span class="requiredStar"></span></label>
                                <div class="col-md-1">
                                     <input type="radio" name="medication" value="yes">                         
                                 </div>
                                 <label class="col-md-1 control-label">Yes</label>
                                 <div class="col-md-1">
                                     <input type="radio" name="medication" value="no">                         
                                 </div>
                                 <label class="col-md-1 control-label">No</label>
                            </div>

                            <div class="form-group col-md-12">
                                 <label class="col-md-4 control-label"><?php echo lang('admi_foodallergy'); ?> <span class="requiredStar"></span></label>
                                 <div class="col-md-1">
                                     <input type="radio" name="food_allergy" value="yes">                         
                                 </div>
                                 <label class="col-md-1 control-label">Yes</label>
                                 <div class="col-md-1">
                                     <input type="radio" name="food_allergy" value="no">                         
                                 </div>
                                 <label class="col-md-1 control-label">No</label>
                            </div>

                            <div class="checkbox-list">
                                <div class="form-group col-md-12">
                                    <label class="col-md-3 control-label">
                                        <input type="checkbox" name="previous_certificate" value="submited"> <?php echo lang('admi_Pre_Class_Cer'); ?> </label>
                                    <label class="col-md-3 control-label">
                                        <input type="checkbox" name="tc" value="submited"> <?php echo lang('admi_TC'); ?> </label>
                                   <!--  <label class="col-md-3 control-label">
                                    <input type="checkbox" name="at" value="submited"> <?php //echo lang('admi_Academic_Transcript');?> </label> -->
                                </div>

                                <div class="form-group col-md-12">

                                    <label class="col-md-3 control-label">
                                    <input type="checkbox" name="original_birth_cert" value="submited"> <?php echo lang('admi_NBC'); ?> </label>
                                   <!--  <label class="col-md-3 control-label">
                                    <input type="checkbox" name="testmonial" value="submited"> <?php //echo lang('admi_Testimonial'); ?>  </label> -->
                                     <label class="col-md-3 control-label"><input type="checkbox" name="adhar_copy_parent" class="col-md-2" value="submited">
                                        Adhar Card Copy</label>


                                </div>

                            </div>

                            <div class="form-group col-md-12">
                                <label class="col-md-3 control-label"><input type="checkbox" name="medical_fitness_cert" class="col-md-2" value="submited">
                                Medical Fitness Certificate</label>

                                <label class="col-md-3 control-label"><input type="checkbox" name="adoption_document" class="col-md-2" value="submited">
                                Adoption Document</label>

                                 <label class="col-md-3 control-label">
                                    <input type="checkbox" name="passport_photos" class="col-md-2" value="submited">3 Passport Size Photos</label>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="col-md-3 control-label"><input type="checkbox" name="passport_pic_both_parent" class="col-md-2" value="submited">
                                2 Passport Size Photos Of Both Parents</label>

                                <label class="col-md-3 control-label"><input type="checkbox" name="original_bonafied" class="col-md-2" value="submited">
                                Original Bonafide Certficate</label>

                                <label class="col-md-3 control-label"><input type="checkbox" name="pan_card_copy_parent" class="col-md-2" value="submited">
                                Copy Of Pan Card Of Parent</label>

                            </div>
                
                                <div class="form-group col-md-12">
                                    <label class="col-md-3 control-label"><input type="checkbox" name="caste_certificate" class="col-md-2" value="submited">
                                    Caste Certificate(If Applicable)</label>

                                    <label class="col-md-2 control-label"><input type="checkbox" name="residence_proof" class="col-md-2" value="submited">
                                    Residence Proof</label>

                                    <label class="col-md-4 control-label"><input type="checkbox" name="if_foreign_nationals_copy" class="col-md-2" value="submited">
                                    For Foreign Nationals-Passport Copy</label>
                                </div>
                        
                            </div>

                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="" class="btn green" name="submit" value="submit">Update</button>
                                    <button type="reset" class="btn default"><?php echo lang('refresh');?></button>
                                </div>
                            
                        </div>

                            </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL script -->
<script type="text/javascript" src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/components-form-tools.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate(); </script>
<script>
    jQuery(document).ready(function() {
        ComponentsFormTools.init();
    });
</script>
<script type="text/javascript">
    var RecaptchaOptions = {
        theme: 'custom',
        custom_theme_widget: 'recaptcha_widget'
    };
</script>
<script>
    function classInfo(str) 
    {
        var xmlhttp;
        if (str.length === 0) {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "index.php/users/student_info?q=" + str, true);
        xmlhttp.send();
    }
    function checkEmail(str) {
        var xmlhttp;
        if (str.length === 0) {
            document.getElementById("checkEmail").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                document.getElementById("checkEmail").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "index.php/commonController/checkEmail?val=" + str, true);
        xmlhttp.send();
    }
</script>
<script>
    /*jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });*/

    /*function getyear()
    {
        var getid = document.getElementById("school_name").value;

        document.getElementById("academic_year") = 'AY22-23';
        
    }*/

        $('select[name="school_name"]').on('change', function() {
        var school_id = $(this).val();
        //alert(school_id);
       
        $.ajax({
            url: '<?php echo base_url().'users/getyears'?>',
            data: {'school_id': school_id },
            type: "POST",
            dataType: "json",
            success:function(data) 
            {
              //console.log(data.data);
              // var objJSON = JSON.parse(data);
              //   console.log(objJSON);
                $('select[name="academic_year"]').empty();
                $.each(data.data, function(key, value) {
                    $('select[name="academic_year"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');
                });
            }
        });
    });

     /*$(document).ready(function() {
        $('select[name="company_name"]').on('change', function() {
            var comId = $(this).val();
            //alert(comId);
            if(comId) {
                $.ajax({
                    url: '<?php //echo base_url();?>Inward/getval',
                    data: {'comId': comId },
                    type: "POST",
                    dataType: "json",
                    success:function(data) {
                      //console.log(data);
                        $('select[name="folio_no"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="folio_no"]').append('<option value="'+ value.folio_no +'">'+ value.folio_no +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="folio_no"]').empty();
            }
        });
    });*/
</script>


<!-- END PAGE LEVEL script