<!--Start page level style-->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!--Start page level style-->

<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
       <!--  <div class="row">
            <div class="col-md-12">
               
                <h3 class="page-title">
                    <?php echo lang('clas_info'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('clas_info'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('header_cor_clas'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('header_class_info'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
               
            </div>
        </div> -->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <?php
                if (!empty($success)) {
                    echo $success;
                }
                ?>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title row">
                        <div class="caption col-md-10">
                            <?php echo lang('clas_info_here'); ?>
                        </div>
                        <div class="caption col-md-2">
                            <a class="btn red" href="index.php/sclass/syncDepartment">Sync LMS data in ERP</a>
                        </div>

                        <?php
                            $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                            echo form_open('sclass/displayDepartment', $form_attributs);
                            ?>
                            <div class="form-group col-md-4">
                                <select class="form-control" name="school_name" id="school_name" required>
                                    <option value="">Select Institute Name</option>
                                    <?php foreach($institute['data'] as $key) { ?>
                                        <option value="<?php echo $key['id'];?>"><?php echo $key['name'];?></option>
                                    <?php } ?>
                                </select>
                            </div>       
                            <div class="form-group col-md-1"></div>
                            <div class="form-group col-md-2">
                                <button class="btn green" name="submit" type="submit" value="Submit"><?php echo lang('submit'); ?></button>
                            </div>  
                        <?php echo form_close(); ?>
                    </div>                                  
                    <?php if(!empty($classInfo)){ ?>           
                    <div class="portlet-body">   
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th><?php echo lang("header_add_school");?></th>
                                    <th>
                                        <?php echo lang('header_cor_clas'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('clas_code'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('hod_mail'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('hod_phon'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('hod_landlin'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('faculty_cnts'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('clas_stu_amount'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('ict_labs'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('labs'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('rgister_action'); ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                        <?php foreach ($classInfo as $row) { ?>
                                    <tr>
                                        <td><?php echo $institute_name;?></td>
                                        <td>
                                            <?php echo $row['name']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['idnumber']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['hod_email']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['hod_phone']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['hod_landline']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['total_faculty']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['student_amount']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['ict_labs']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['labs']; ?>
                                        </td>
                                        <!-- <td>
                                            <?php //echo $row['attendance_percentices_daily'] . ' %'; ?>
                                        </td> -->
                                        <td>
                                            <!-- <a class="btn btn-xs green" href="index.php/sclass/classDetails?c_id=<?php echo $row['id']; ?>"> <i class="fa fa-send-o"></i> <?php echo lang('details'); ?> </a> -->
                                            <?php if($this->common->user_access('class_delete',$userId)){ ?>
                                            <!-- <a class="btn btn-xs red" href="index.php/sclass/deleteClass?id=<?php echo $row['id']; ?>" onClick="javascript:return confirm('Are you sure you want to delete this Class?')"> <i class="fa fa-trash-o"></i> <?php echo lang('delete'); ?> </a> -->
                                            <?php }?>
                                            <a class="btn btn-xs default" href="index.php/sclass/editDepartment?id=<?php echo $row['id']; ?>"> <i class="fa fa-pencil-square-o"></i> <?php echo lang('edit'); ?> </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>                        
                    </div>
                    <?php } ?>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<!-- <script>
    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script> -->
