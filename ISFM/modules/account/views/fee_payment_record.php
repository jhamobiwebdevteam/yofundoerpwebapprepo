<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-
color="e34524" bolt-logo="<?=base_url('assets/images/jm.jpeg')?>"></script>

<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datepicker/css/datepicker3.css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- <div class="row">
            <div class="col-md-12">
               
                <h3 class="page-title">
                    <?php echo lang('header_payment_record'); ?>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i><?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_account');?>
                    </li>
                    <li><?php echo lang('header_payment_record'); ?>

                    </li>

                    <li id="result" class="pull-right topClock"></li>
                </ul>
                
            </div>
        </div> -->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i><?php echo lang('header_payment_record'); ?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php $form_attribut = array('class' => 'form-horizontal', 'role' => 'form');
                        echo form_open_multipart("account/feePaymentInsertData", $form_attribut);
                        ?>
                        <div class="form-body">
                            <?php
                            if (!empty($success)) {
                                echo $success;
                            }
                            ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Receipt *<span class="requiredStar">  </span></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder=" " name="receipt" required>
                                </div>
                                <label class="col-md-3 control-label">Date<span class="requiredStar"> </span></label>
                                <div class="col-md-3">
                                    <input type="date" class="form-control" placeholder=" " name="date" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Name<span class="requiredStar">  </span></label>
                                <div class="col-md-3">
                                    <select class="form-control name" id="name" name="name">
                                        <option><?php echo lang('select'); ?></option>
                                        <?php foreach ($studentinfo as $row) { ?>
                                            
                                            <option value="<?php echo $row['id'];?>"><?php echo $row['first_name'].' '.$row['last_name'];?></option>
                                        <?php }?>

                                    </select>
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Mobile<span class="requiredStar"> </span></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="mobile" name="mobile" >
                                </div>
                                <label class="col-md-3 control-label">E-Mail ID<span class="requiredStar"> </span></label>
                                <div class="col-md-3">
                                    <input type="text" id="email" class="form-control" placeholder=" " name="email">
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Payment Mode<span class="requiredStar"> </span></label>
                                <div class="col-md-3">
                                    <select class="form-control" data-validation="required" data-validation-error-msg="You have to select anyone." name="pay_mode">
                                    <option><?php echo lang('select'); ?></option>
                                    <option value="indian_rupee">Indian Rupee - INR</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control" name="deposit">
                                        <option value="direct_deposit">Direct Deposit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Bank Account<span class="requiredStar"> </span></label>
                                <div class="col-md-3">
                                    <!-- <select class="form-control">
                                        <option></option>
                                    </select> -->
                                    <input type="text" class="form-control" name="bank_account" >
                                    
                                </div>
                                
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Transaction No<span class="requiredStar"> </span></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder=" " name="transaction">
                                </div>

                            </div>

                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" id="Button" class="btn green" name="submit" value="submit"><?php echo lang('tea_si'); ?></button>
                                    <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                                </div>
                            </div>

                            <?php echo form_close(); ?>
                        </div>
                    </div>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

<script type="text/javascript">

    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });


$(document).ready(function() {
        $('select[name="name"]').on('change', function() {
            var id = $(this).val();
            
            if(id) {
                $.ajax({
                    url: '<?php echo base_url();?>index.php/account/studentsdata',
                    data: {'id': id },
                    type: "POST",
                    dataType: "json",
                    success:function(data) {
                      //console.log(data);
                       $("#mobile").val(data[0]['fathers_mobile']);
                       $("#email").val(data[0]['email']);
                       
                    }
                });
            }
        });
    });
</script>