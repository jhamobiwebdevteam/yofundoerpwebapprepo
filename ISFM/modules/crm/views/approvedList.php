<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- <div class="row">
            <div class="col-md-12">
                
                <h3 class="page-title">
                    <?php echo lang('approve_students'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_crm'); ?>
                    </li>
                    <li>
                        <?php echo lang('approve_students'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>

                </ul>
                
            </div>
        </div> -->
        <!-- END PAGE HEADER-->
        
        
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('approve_students'); ?>
                        </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-bordered" id="sample_1">
                        <div class="row smsemail">
                            <div class="col-md-6 margin-bottom-10">
                                <button type="submit" id="sms" class="btn red" name="submit" value="submit"><?php echo lang('sms'); ?></button> OR
                                <button type="submit" id="email" class="btn red"><?php echo lang('eml'); ?></button>
                            </div>
                        </div>
                            <thead>
                                <tr>
                                   
                                    <th><?php echo lang('register_idno');?></th>
                                    <th>
                                        <?php echo lang('register_fn'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('register_ln'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('register_class'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('register_gender'); ?>
                                    </th>
                                    <th><?php echo lang('approve_datetime');?> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($approvedStudent as $row) { ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['first_name'];?></td>
                                        <td><?php echo $row['last_name'];?></td>
                                        <td><?php echo $row['class_id'];?></td>
                                        <td><?php echo $row['gender'];?></td>
                                        <td><?php echo $row['created_at']?></td>
                                    </tr>
                                <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

