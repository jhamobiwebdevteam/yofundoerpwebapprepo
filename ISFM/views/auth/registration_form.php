
<!DOCTYPE html>
<html lang="en" >
    <!-- BEGIN HEAD -->
    <head>
        <style type="text/css">
            .login .content2 
            {
                background-color: #f5f5f5;
                width: 70%;                
                margin-bottom: 0px;
                padding: 30px;
                padding-top: 20px;
                /*padding-bottom: 15px;*/
            }
            .firstrwlbl
            {
                margin-right: 20px;
            }
            
        </style>
        <meta charset="utf-8"/>
        <title><?php echo lang('system_title'); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <base href="<?php echo $this->config->base_url(); ?>">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->


        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
         <link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
        
        <link href="assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="assets/global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/formValidation.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="assets/admin/layout/img/feviconlogo.jpeg"/>
    </head>
    <!-- BEGIN BODY -->
    <body class="login">

       <!--  <div class="logo">
            <a href="index.html">
                <img src="assets/admin/layout/img/smlogo.png" alt="logo" class="logo-default"/>
            </a>
        </div> -->
        <div class="logo">
            <div class="copyright">
                <center><h1>Educational ERP</h1></center>
                <!-- <img src="assets/admin/layout/img/smlogo.png" alt="logo" class="logo-default"/> -->
            </div>
        </div>

        <div class="content2">
            <div class="portlet-body form-horizontal">
             <!-- BEGIN LOGIN FORM -->
            <?php
           /*$a = array('class' => 'registration-form','onsubmit' => 'return validateForm()','name' => 'myForm', 'id' => 'myfrm');
            echo form_open_multipart('/Sendemail/save_data', $a);*/
            ?>
            <form action="<?php echo base_url();?>index.php/admissionRegistration/save_data" method="post" enctype="multipart/form-data">

                <?php
                    if (!empty($success)) 
                    {
                        echo $success;
                    }
                ?>
                <center><h3 class="form-title"><?php echo lang('registration_heading'); ?></h3></center>

                <div class="form-group">
                    <label class="col-md-4 control-label">Registration For Academic Year</label>
                     <div class="col-md-4">
                       <select class="form-control" name="academic_year">
                           <option>Select</option>
                           <option value="2022-2023">2022-2023</option>
                           <option value="2021-2022">2021-2022</option>
                       </select>                                
                    </div>
                    <label class="col-md-2 control-label">Class</label>
                    <div class="col-md-2">
                        <!-- <select class="form-control" name="class" onchange="classInfo(this.value)" class="form-control" data-validation="required" data-validation-error-msg="<?php echo lang('admi_Class_error_msg');?>">
                            <option>Select</option>
                            <option value="NR">NR</option>
                            <option value="JR.KG">JR KG</option>
                            <option value="SR KG">SR KG</option>
                            <option value="I">I</option>
                            <option value="II">II</option>
                            <option value="III">III</option>
                            <option value="IV">IV</option>
                            <option value="V">V</option>
                            <option value="VI">VI</option>
                            <option value="VII">VII</option>
                            <option value="VIII">VIII</option>
                            <option value="IX">IX</option>
                            <option value="X">X</option>                            
                        </select> -->

                       
                            <select name="class" onchange="classInfo(this.value)" class="form-control" data-validation="required" data-validation-error-msg="<?php echo lang('admi_Class_error_msg');?>">
                                <option value=""><?php echo lang('admi_select_class');?></option>
                            <?php foreach ($s_class as $row) { ?>
                                <option value="<?php echo $row['id']; ?>"><?php echo $row['class_title']; ?></option>
                            <?php } ?>
                            </select>
                                
                    </div>
                </div>

                 <div class="form-group" id="txtHint">

                 </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">First Name</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="first_name" required="required">                                
                    </div>
                
                    <label class="col-md-2 control-label">Middle Name</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="mid_name" required="required"> 
                    </div>
                    <label class="col-md-2 control-label">Last Name</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="last_name" required="required"> 
                    </div>
                    
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Date of Birth</label>
                    <div class="col-md-2">
                        <input type="date" class="form-control" name="dob" required="required">
                    </div>

                    <label class="col-md-1 control-label">Email</label>
                    <div class="col-md-3">
                        <input type="email" class="form-control" name="email" id="email" required="required" onchange="validateemail()">
                         <div id="email_error"  style="color:red"></div>
                    </div>

                    <label class="col-md-1 control-label">Nationality</label>
                    <div class="col-md-3">
                        <select class="form-control" name="nationality">
                            <option>Select</option>
                            <option value="Indian">Indian</option>
                            <option value="American">American</option>
                            <option value="British">British</option>
                            <option value="Australian">Australian</option>
                            <option value="Canadian">Canadian</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Gender</label>
                    <div class="col-md-2">
                        <select class="form-control" name="gender">
                            <option>Select</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                    <label class="col-md-2 control-label">Caste Category</label>
                    <div class="col-md-2">
                        <select class="form-control" name="caste_cat">
                            <option>Select</option>
                            <option value="Open">Open</option>
                            <option value="OBC">OBC</option>
                            <option value="NT-B">NT-B</option>
                            <option value="NT-C">NT-C</option>
                            <option value="NT-D">NT-D</option>
                            <option value="Other">Other</option>
                            <option value="SC">SC</option>
                            <option value="ST">ST</option>
                        </select>
                    </div>
                    <label class="col-md-2 control-label">Religion</label>
                    <div class="col-md-2">
                        <select class="form-control" name="religion">
                            <option>Select</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Muslim">Muslim</option>
                            <option value="Buddhism">Buddhism</option>
                            <option value="Christian">Christian</option>
                            <option value="Jainism">Jainism</option>
                            <option value="Jew">Jew</option>
                            <option value="Navbauddha">Navbauddha</option>
                            <option value="Parsi">Parsi</option>
                            <option value="Sikh">Sikh</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Upload photo</label>
                    <div class="col-md-4">
                        <input type="file" class="form-control" name="userfile" required="required"> 
                        
                    </div> 
                   
                    <label class="col-md-2 control-label">Place of Birth</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="birth_place" required="required">
                    </div>
                </div>



                <div class="form-group">
                    <label class="col-md-2 control-label">Address</label>
                    <div class="col-md-2">
                        <textarea class="form-control" name="address"></textarea>
                    </div>
                    <label class="col-md-2 control-label">Pin Code</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="pin" required="required">
                    </div>
                    
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Father's First Name</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="fathers_firstname" required="required">
                    </div>

                    <label class="col-md-2 control-label">Father's Middle Name</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="faters_midname" required="required">
                    </div>

                    <label class="col-md-2 control-label">Father's Last Name</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="fathers_lastname" required="required">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Father's Mobile</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="fathers_mob" id="fathers_mob" required="required" onchange="Validate()">
                        <div id="mobile_error"  style="color:red"></div>
                    </div>

                    <label class="col-md-2 control-label">Father's Occupation</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="fathers_occ" required="required">
                    </div>

                     <label class="col-md-2 control-label">Mother's Occupation</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="mothers_occ" required="required">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Mother's First Name</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="mothers_firstname" required="required">
                    </div>

                    <label class="col-md-2 control-label">Mother's Middle Name</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="mothers_midname" required="required">
                    </div>

                    <label class="col-md-2 control-label">Mother's Last Name</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="mothers_lastname" required="required">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Mother's Mobile</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="mothers_mob" required="required">
                    </div>

                    <label class="col-md-2 control-label">Father's Educational Qualification</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="fathers_education" required="required">
                    </div>                   

                    <label class="col-md-2 control-label">Mother's Educational Qualification</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="mothers_education" required="required">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Mother Tounge</label>
                    <div class="col-md-2">
                        <select class="form-control" name="mother_tounge">
                            <option>Select</option>
                            <option value="English">English</option>
                            <option value="Marathi">Marathi</option>
                            <option value="Hindi">Hindi</option>
                            <option value="Gujrati">Gujrati</option>
                        </select>
                    </div>
                    <label class="col-md-2 control-label">Total Annual Income</label>
                    <div class="col-md-2">
                        <select class="form-control" name="father_incom_range">
                            <option>Select</option>
                            <option value="below_1_lack">Below 1 LACK</option>
                            <option value="1_2_lack">1-2 LACK</option>
                            <option value="2-5_lack">2-5 LACK</option>
                            <option value="above_5_lack">Above 5 LACK </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    
                     <label class="col-md-4 control-label">Does your child have a medical condition that we should know?</label>

                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input"  name="medical_condition" value="yes">
                            <label class="form-check-label">Yes</label>
                        
                            <input type="radio" name="medical_condition" value="no"> 
                            <label class="form-check-label">No</label>                 
                        </div>
                     
                </div>

                <div class="form-group">
                     <label class="col-md-4 control-label">Is your undergoing and short / long term treatment?</label>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" name="undergoing_treatment" value="yes"> 
                            <label class="form-check-label">Yes</label>

                            <input type="radio" class="form-check-input" name="undergoing_treatment" value="no">
                            <label class="form-check-label">No</label>                        
                        </div>
                </div>

                <div class="form-group">
                     <label class="col-md-4 control-label">Is your child taking any medication?</label>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" name="medication" value="yes">   
                            <label class="form-check-label">Yes</label> 

                            <input type="radio" class="form-check-input" name="medication" value="no">
                            <label class="form-check-label">No</label>                     
                        </div>
                </div>

                <div class="form-group">
                     <label class="col-md-4 control-label">Any food allergy?</label>
                     <div class="form-check form-check-inline">
                         <input type="radio" class="form-check-input" name="food_allergy" value="yes">
                         <label class="form-check-label">Yes</label>                       
                     
                            <input type="radio" class="form-check-input" name="food_allergy" value="no">
                            <label class="form-check-label">No</label>                         
                        </div>
                     
                </div>
                
                <div class="form-group">
                    
                        <div class="form-group"><label><blockquote>Certificates</blockquote></label></div>

                        <div class="form-check form-check-inline">
                           <input type="checkbox" name="medical_fitness_cert" class="form-check-input" value="submited">
                            <label class="form-check-label firstrwlbl">Medical Fitness Certificate</label> 
                        
                            <input type="checkbox" name="adoption_document" class="form-check-input"  value="submited">
                            <label class="form-check-label firstrwlbl">Adoption Document</label>
                       
                            <input type="checkbox" name="passport_photos" class="form-check-input" value="submited" >
                            <label class="form-check-label">3 Passport Size Photos</label>
                        </div>
                    
                </div>
                 <div class="form-group">
                    <div class="form-check form-check-inline">
                        <input type="checkbox" name="passport_pic_both_parent" class="form-check-input" value="submited">
                        <label class="form-check-label firstrwlbl"> 2 Passport Size Photos Of Both Parents</label>

                        <input type="checkbox" name="original_birth_cert" style="margin-top: 15px;" class="form-check-input" value="submited">
                        <label class="form-check-label firstrwlbl">Original Birth Certificate</label>

                        <input type="checkbox" name="original_bonafied" class="form-check-input" value="submited">
                        <label class="form-check-label">Original Bonafide Certficate</label>
                    </div>
                </div>
                 <div class="form-group">
                    <div class="form-check form-check-inline">
                        <input type="checkbox" name="tc" class="form-check-input" value="submited">
                        <label class="form-check-label firstrwlbl">Original Leaving Or Transfer Certificate</label>

                        <input type="checkbox" name="pan_card_copy_parent" class="form-check-input" value="submited">
                        <label class="form-check-label firstrwlbl">Copy Of Pan Card Of Parent</label>

                        <input type="checkbox" name="adhar_copy_parent" class="form-check-input" value="submited">
                        <label class="form-check-label">Adhar Card Copy</label>
                    </div>

                </div>
                 <div class="form-group">
                    <div class="form-check form-check-inline">
                        <input type="checkbox" name="caste_certificate" class="form-check-input" value="submited">
                        <label class="form-check-label firstrwlbl">Caste Certificate(If Applicable)</label>

                        <input type="checkbox" name="residence_proof" class="form-check-input" value="submited">
                        <label class="form-check-label firstrwlbl">Residence Proof</label>

                        <input type="checkbox" name="if_foreign_nationals_copy" class="form-check-input" value="submited">
                        <label class="form-check-label">For Foreign Nationals-Passport Copy</label>
                    </div>
                </div>
                 <div class="form-group">
                    <div class="form-check form-check-inline">
                        <input type="checkbox" name="copy_report_card" class="form-check-input" value="submited">
                        <label class="form-check-label firstrwlbl">Copy Of Report Card</label>
                    </div>
                </div>

                <div class="form-actions fluid">
                    <div class="col-md-6 col-sm-6">
                       <!--  <button type="submit" id="Button" class="btn green" name="submit" value="submit"><?php echo lang('tea_si'); ?></button> -->
                       <center><button type="submit" class="btn green" name="submit" value="submit"><?php echo lang('save');?></button></center>
                       <!-- <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                         <button type="button" class="btn green" onclick="window.print()" > Print</button> -->               
                    </div>
                </div>
            </form>
            <?php //echo form_close(); ?>
            </div>
        </div>

    <script>
    function classInfo(str) {
        var xmlhttp;
        if (str.length === 0) {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "index.php/admissionRegistration/student_info?q=" + str, true);
        xmlhttp.send();
    }

        
    </script>

    <script type="text/javascript">
         function Validate()
          {
            var mobile = document.getElementById('fathers_mob').value; 
              if (isNaN(mobile)){  
                document.getElementById("mobile_error").innerHTML="Enter Numeric values only";  
                return false;  
              }  
              if(mobile.value == "") {
                document.getElementById("mobile_error").innerHTML="Mobile no.should not be null";
                return false;
              }

              if(mobile.length != 10) {
                document.getElementById("mobile_error").innerHTML="Mobile no.should be 10 digits";
                return false;
              }
            }

            function validateemail()  
            {  
                var x = document.getElementById('email').value;  
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
             
                if(!x.match(mailformat))
                {  
                    document.getElementById("email_error").innerHTML = "Please enter a valid e-mail";
                   
                    return false;  
                }  
            }  
    </script>
    
    <script type="text/javascript" src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>


    </body>

    </html>