

<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- <div class="row">
            <div class="col-md-12">
               
                <h3 class="page-title">
                    <?php echo lang('header_payment'); ?>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i><?php echo lang('home'); ?>
                    </li>
                    <li><?php echo lang('header_payment'); ?>

                    </li>

                    <li id="result" class="pull-right topClock"></li>
                </ul>
                
            </div>
        </div> -->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i><?php echo lang('header_payment'); ?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php $form_attributs = array('name' => 'myForm', 'id'=>'payment_form', 'class' => 'form-horizontal', 'role' => 'form', 'onsubmit' => 'return validateForm()');
                        echo form_open('payu/transaction', $form_attributs);

                        ?>
                        <div class="form-body">

                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label"> Transaction ID <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-md-3 control-label">Date<span class="requiredStar"> </span></label>
                                <div class="col-md-6">
                                    <input type="date" class="form-control" placeholder=" " name="date" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Name<span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <select class="form-control name" id="name" name="name">
                                        <option><?php echo lang('select'); ?></option>
                                        <?php foreach ($studentinfo as $row) { ?>
                                            <option value="<?php echo $row['id'];?>"><?php echo $row['first_name'].' '.$row['last_name'];?></option>
                                        <?php }?>

                                    </select>
                                </div>                                
                            </div>
                            <input type="hidden" id="fname" class="form-control" name="fname"  value="" />
                            <input type="hidden" id="lname" class="form-control" name="lname"  value="" />
                            <input type="hidden" id="email" class="form-control" name="email"  value="" />
                            <input type="hidden" id="mobile" class="form-control" name="mobile"  value="" />
                            
                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label">First Name <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="fname" class="form-control" name="fname" placeholder="First Name" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Last Name <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="lname" class="form-control" name="lname" placeholder="Last Name" value="" />
                                </div>
                            </div> -->

                            <div class="form-group">
                                <label class="col-md-3 control-label"> Email <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input readonly type="text" id="eml" class="form-control" name="eml" placeholder="Email ID" value=""  />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> Mobile <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input readonly type="text" id="mbl" class="form-control" name="mbl" placeholder="Mobile/Cell Number"  value=""  />
                                </div>
                            </div>
                             
                            <div class="form-group">
                                <label class="col-md-3 control-label"> Amount <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="amount"  class="form-control"  name="amount" placeholder="Amount" value="" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> Information <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                <input type="text" id="pinfo" class="form-control" name="pinfo" placeholder="Product Info" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"> Payment option</label>
                                <div class="col-md-6">
                                    <label for="payment_option_payu"><input type="radio" checked name="payment_option" value="payu/transaction" id="payment_option_payu"> PayU</label> &nbsp;&nbsp; 
                                    <label for="payment_option_razorpay"><input type="radio" name="payment_option" value="razorpay" id="payment_option_razorpay"> RazorPay</label>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label"> Hash <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                </div>
                            </div> -->
                                <input type="hidden" id="hash" name="hash" placeholder="Hash" value="" />
                        </div>
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                            <input type="submit" value="Pay" class="btn green" />
                                <!-- <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button> -->
                            </div>
                        </div>
                        <?php
                        echo form_close(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script type="text/javascript">
    $(document).ready(function() {

        $('select[name="name"]').on('change', function() {
            var id = $(this).val();  
            var name = $("#name option:selected").text(); 
            var nm =name.split(' ');
            $("#fname").val(nm[0]);             
            $("#lname").val(nm[1]); 
            console.log(name+'--'+nm);          
            if(id) {
                $.ajax({
                    url: base_url+'payment/studentsdata',
                    data: {'id': id },
                    type: "POST",
                    dataType: "json",
                    success:function(data) {
                       $("#mobile").val(data[0]['fathers_mobile']);
                       $("#mbl").val(data[0]['fathers_mobile']);
                        $("#email").val(data[0]['email']);                        
                        $("#eml").val(data[0]['email']);                        
                    }
                });
            }
        });
        
        $('input[name="payment_option"]').on('change',function(){
            var payment_url = base_url+$(this).val();
            $('#payment_form').attr('action',payment_url);
        });
    });
</script>