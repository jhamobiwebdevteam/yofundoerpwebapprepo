<?php 

	class admissionEnquiryForm extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			// $this->load->library('email');
			// $this->load->model('common');
			
		}

		public function index()
		{
        	$this->load->view('auth/enquiryForm');
		}
		public function enquiry_save()
		{
			if ($this->input->post('submit', TRUE)) 
            {
            	$stud_enquiry = array(
            		'name'=>$this->db->escape_like_str($this->input->post('name', TRUE)),
            		'class_id'=>$this->db->escape_like_str($this->input->post('class', TRUE)),
            		'email'=>$this->db->escape_like_str($this->input->post('email', TRUE)),
            		'mobile'=>$this->db->escape_like_str($this->input->post('mobile', TRUE)),
            		'city'=>$this->db->escape_like_str($this->input->post('city', TRUE)));
            	if ($this->db->insert('enquiry_tbl', $stud_enquiry)) 
                {
                	$data['success'] = '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <strong>Success ! </strong> Enquiry data added successfully. 
                            </div>';
                            $this->load->view('auth/enquiryForm',$data);
                }
                else
                {
                	$data['success'] = '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <strong>Failed !! </strong>While adding data. 
                            </div>';
                            $this->load->view('auth/enquiryForm',$data);
                }

               
            }

		}
	}