 <!DOCTYPE html>
<html lang="en" >
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo lang('system_title'); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <base href="<?php echo $this->config->base_url(); ?>">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="assets/global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/formValidation.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="assets/admin/layout/img/feviconlogo.jpeg"/>
    </head>
    <!-- BEGIN BODY -->
    <body class="login">

        <!-- <div class="logo">
            <a href="index.html">
                <img src="assets/admin/layout/img/smlogo.png" alt="logo" class="logo-default"/>
            </a>
        </div> -->
         <div class="logo">
            <div class="copyright">
               <!--  <center><h1>Educational ERP</h1></center> -->
                <!-- <img src="assets/admin/layout/img/smlogo.png" alt="logo" class="logo-default"/> -->
            </div>
        </div>
        <div class="content" style="width:50%;">
            <h3>Welcome to Online Admission System</h3>

            <p> The online process for admission is as follows: </p>

            <ol> 
                <li>
                     Apply : Fill up the Registration Form and pay the admissions processing fee of Rs. 1000/- (a payment link for the same will be shared through
                    e-mail after receiving the form).
                    Please upload JPEG photo less than 200KB of the child.
                </li>

                <li>
                    Family Interaction : A virtual interaction will be scheduled with the Section Incharge / Coordinator. Parents are requested to attend the meeting along with the child as per allotted slot.
                </li>
                <li>
                     Provisional Admission : The school will issue Admission Form to the selected students and grant the provisional admission after filled up Form is submitted and required fees paid.
                </li>
            
                <li> Confirmation : Admission gets confirmed after all the necessary documents are submitted.
                </li>
            </ol>

            <p>  *Kindly check Instructions for list required of documents.</p>
            
            <?php
            $a = array('class' => 'registration-form');
           echo form_open("admissionRegistration", $a);
            ?> 
            <!-- <form action="<?php //echo base_url(); ?>index.php/Sendemail" method="post"> -->
                <button class="btn green pull-left" type="submit">Registration Form</button>
            <?php echo form_close(); ?>
            <!-- </form> -->
        </div>

    </body>
</html>