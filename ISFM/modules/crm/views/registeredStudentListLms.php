<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<style>
    .show{
        display: block;
        margin-left: auto;
        margin-right: 0;
    }
</style>
<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        
        
        <div class="row">
            <div class="col-md-12">
                 <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('header_register_stu'); ?>
                        </div>   
                    </div>
                    <div class="portlet-body">
                         <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form', 'method'=>'post');
                        echo form_open_multipart('crm/registeredStudentListLms', $form_attributs);
                        ?>
                          <div class="form-body">   
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo lang('header_sname');?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-4">                                                         
                                    <select class="form-control" name="school_name" id="school_name">
                                        <option value="">Select Institute Name</option>
                                        <?php foreach($data['data'] as $key) { ?>
                                            <option value="<?php echo $key['id'];?>"><?php echo $key['name'];?></option>
                                        <?php } ?>
                                    </select>
                                    <input type="hidden" name="cname" id="cname">
                                </div>
                            
                                <label class="col-md-2 control-label"><?php echo lang('admi_dep_list'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-4">                                    
                                    <select class="form-control" name="dep_list" id="dep_list"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo lang('admi_academic_yr'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-4">                                 
                                    <select class="form-control" name="acadmicyear" id="acadmicyear"></select>
                                </div>                           
                            
                                <label class="col-md-2 control-label"><?php echo lang('admi_sem'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-4">
                                    <select class="form-control" name="semester" id="semester"></select>
                                </div>                                                   
                            </div>
                            <input type="hidden" name="course_ids" id="course_ids">
                            <!-- <div class="form-group">
                                <label class="col-md-2 control-label"><?php //echo lang('admi_crs_list'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-4">                                 
                                    <select class="form-control" name="course" id="course"></select>
                                </div>                                                   
                            </div> -->
                            <div class="form-group">
                                <div class="col-md-3">
                                    <button type="submit" id="Button" class="btn green show" name="submit" value="submit"><?php echo lang('show'); ?></button>
                                </div>
                            </div>
                        
                        </div>
                        <?php echo form_close(); ?>
                       
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-12">
                <div class="portlet box green row">
                    <div class="portlet-title">
                        <div class="caption col-md-10">
                            <?php echo lang('header_register_stu'); ?>
                        </div>  
                        <?php //if($cname != ''){ ?>                                           
                        <div class="caption col-md-2">
                            <?php if(!empty($stud_info)){ 
                                $arr = json_encode($stud_info); $str =urlencode($arr); 
                                // print_r($stud_info); die($str.'---');
                             ?>
                                <a class="btn red" href="index.php/crm/syncStudents?studArr=<?php echo $str; ?>">Sync LMS data in ERP</a>
                                <?php
                            }
                            ?>
                        </div>
                        <?php //} ?>
                    </div>
                    <?php ?>
                    <div class="portlet-body">
                        <?php if(!empty($stud_info)){ ?>
                        <table class="table table-striped table-bordered table-hover">   
                            <thead>
                            <tr>                                
                                <th> <?php echo lang('header_add_school'); ?> </th>  
                                <th> <?php echo lang('header_cor_clas');?></th>  
                                <th> <?php echo lang('admi_academic_yr');?></th>
                                <th> <?php echo lang('admi_sem');?></th>   
                            </tr>
                            </thead>
                            <tbody>
                                <tr>                                
                                    <td> <?php echo $stud_info[0]['schoolname']; ?></td> 
                                    <td><?php echo $stud_info[0]['grades']; ?> </td> 
                                    <td><?php echo $stud_info[0]['years']; ?> </td> 
                                    <td><?php echo $stud_info[0]['sections']; ?> </td> 
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-striped table-bordered table-hover" id="sample_1">   
                            <thead>
                                <tr>
                                    <th><?php echo lang('tax_id'); ?></th>
                                    <th> <?php echo lang('admi_crs_list');?></th>   
                                    <th> <?php echo lang('sub_7');?></th>   
                                    <th> <?php echo lang('email');?></th>   
                                    <th> <?php echo lang('header_mob');?></th>   
                                    <th> <?php echo lang('exa_actd');?></th>   
                                                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                
                                $i=1; foreach ($stud_info as $row) { 
                                    $str = implode(',', $row) ?>
                                    <tr>
                                        <td> <?php echo $i; ?></td>
                                        <td><?php echo rtrim($row['cname'], ','); ?> </td> 
                                        <td><?php echo $row['firstname'].' '.$row['lastname']; ?> </td> 
                                        <td><?php echo $row['email']; ?> </td> 
                                        <td><?php echo $row['phone2']; ?> </td> 
                                        <td>
                                        <a class="btn btn-xs default" href="index.php/crm/viewGrades?id=<?php echo $str; ?>"> <i class="fa fa-eye" aria-hidden="true"></i> <?php echo lang('view'); ?> </a> </td>
                                    </tr>
                                <?php $i++;} ?>
                            </tbody>
                        </table>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>


<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script type="text/javascript">
    $('select[name="school_name"]').on('change', function() {
        var school_id = $(this).val();
        var sc = $("#school_name option:selected").text();  
        $('#cname').val(sc);     
        $.ajax({
            url: '<?php echo base_url().'index.php/users/get_department'?>',
            data: {'school_id': school_id },
            type: "POST",
            dataType: "json",
            success:function(data) 
            {
                $('select[name="dep_list"]').empty();
                $('select[name="dep_list"]').append('<option>Select Department</option>');
                $.each(data.data, function(key, value) {
                    $('select[name="dep_list"]').append('<option value="'+value.id+'">'+ value.name +'</option>');     
                });                 
            }
        });
    });

    $('select[name="dep_list"]').on('change', function() {
        var acdemic_yrid = $(this).val();
        $.ajax({
            url: '<?php echo base_url().'index.php/users/getyears'?>',
            data: {'acdemic_yrid': acdemic_yrid },
            type: "POST",
            dataType: "json",
            success:function(data) 
            {              
                $('select[name="acadmicyear"]').empty();
                $('select[name="acadmicyear"]').append('<option selected>Select Year</option>');
                $.each(data.data, function(key, value) {
                    $('select[name="acadmicyear"]').append('<option data-id="'+value.path+'#'+value.id+'#'+value.name+'" value="'+ value.id +'">'+ value.name +'</option>');                    
                });
            }
        });
    });
     

    $('select[name="acadmicyear"]').on('change', function() {
        var all_dep_data = $(this).find(':selected').attr('data-id'); 
        var details = all_dep_data.split('#');         
        $.ajax({
            url: '<?php echo base_url().'index.php/users/get_semester'?>',
            data: {'departmnt_id': details[1]},
            type: "POST",
            dataType: "json",
            success:function(data) 
            {  
                $('select[name="semester"]').empty();
                $('select[name="semester"]').append('<option selected>Select Semester</option>');
                $.each(data.data, function(key, value) {
                    $('select[name="semester"]').append('<option data-id="'+value.path+'#'+value.id+'#'+value.name+'" value="'+ value.id +'">'+ value.name +'</option>');                    
                });
            }
        });
    });

    $('select[name="semester"]').on('change', function() {
        var sem_data = $(this).find(':selected').attr('data-id'); 
        var details = sem_data.split('#');
        $.ajax({
            url: '<?php echo base_url().'index.php/users/get_course'?>',
            data: {'semester_id': details[1]},
            type: "POST",
            dataType: "json",
            success:function(data){
                var cid ='';
                $.each(data.data, function(key, value) {
                    cid += value.id+',' ;                    
                });
                var cids =  cid.slice(0, -1);
                $("#course_ids").val(cids);
           }
        });
    });

</script>