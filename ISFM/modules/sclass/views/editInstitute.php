<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('edt_institute_title'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        $id = $this->input->get('id');
                        echo form_open("sclass/editInstitute?id=$id", $form_attributs);
                        ?>
                        <?php foreach ($instituteInfo as $row) { ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('header_sname'); ?></label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="inst_name" value="<?php echo $row['name']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('institute_id'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="lms_id" value="<?php echo $row['lms_id']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('email'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="email" value="<?php echo $row['email']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('institute_grpname'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="grpname" value="<?php echo $row['grpname']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('header_city'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="city" value="<?php echo $row['city']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('register_pin'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="pincode" value="<?php echo $row['pincode']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('admin_name'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="admin_name" value="<?php echo $row['admin_name']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('admin_number'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="admin_num" value="<?php echo $row['admin_num']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('header_description'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="description" value="<?php echo $row['description']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('header_registrationdt'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="reg_date" value="<?php echo $row['reg_date']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('header_foundyear'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="foundation_year" value="<?php echo $row['foundation_year']; ?>">
                                    </div>
                                </div>
                                

                            </div>
                        <?php } ?>
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" class="btn green" name="submit" value="Update"><?php echo lang('save'); ?></button>
                                <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>


        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script>
    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
