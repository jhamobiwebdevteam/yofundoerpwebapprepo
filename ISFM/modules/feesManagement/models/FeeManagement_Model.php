<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class FeeManagement_Model extends CI_Model 
{
    function __construct() {
        parent::__construct();
        $this->load->dbforge();
    }

    public function allTax() 
    {
        $data = array();
        $query = $this->db->query("SELECT * FROM tax_master");
        foreach ($query->result_array() as $row) {           
            $data[] = $row;            
        }
        return $data;
    }
    public function allFeesReceiptData() 
    {
        $data = array();
        $query = $this->db->query("SELECT * FROM fee_payment_record");
        foreach ($query->result_array() as $row) {           
            $data[] = $row;            
        }
        return $data;
    }
    public function update_status($user_id,$status)
    {
        $data['status'] = $status;
        //die($status);
        if($status==1)
        {
            $query = $this->db->query("SELECT id FROM tax_master where status = 1");
        
            if($query->num_rows()>0)
            {
                return FALSE;
            }
            else
            {
                //return FALSE;
                 //die("UPDATE tax_master SET status = $status WHERE id = $user_id");
            
                    $sql = $this->db->query("UPDATE tax_master SET status = $status WHERE id = $user_id");
                    return TRUE;
                
            }
        }
        else
        {
            //die("UPDATE tax_master SET status = $status WHERE id = $user_id");
            
                    $sql = $this->db->query("UPDATE tax_master SET status = $status WHERE id = $user_id");
                    return TRUE;
                    
                
               
        }
        /*//$sql = $this->db->query("UPDATE tax_master SET status = $status WHERE id = $user_id");
        $this->db->where('id', $user_id);
        $this->db->update('tax_master',$data);*/
        /*foreach ($query->result_array() as $row) {           
            $data[] = $row;            
        }
        return $data;*/
    }
    public function get_active_tax()
    {
        $query = $this->db->query("SELECT * FROM tax_master where status=1");
        foreach ($query->result_array() as $row) {           
            $data[] = $row;            
        }
        return $data;
    }
     public function AllfeeType() 
     {
        $data = array();
        //$query = $this->db->query("SELECT * FROM fees_type_master");
        $query = $this->db->query("SELECT f.*,i.installment,i.amount,i.installment_date,i.fee_plan_name_id FROM fees_type_master f LEFT JOIN installment_master i ON f.is_tax = i.tax_id where f.id = i.fee_plan_name_id;");
        foreach ($query->result_array() as $row) {           
            $data[] = $row;            
        }
        return $data;
     }
     public function AllLupmsum()
     {
        $data = array();
        $query = $this->db->query("SELECT * FROM fees_type_master");
        
        foreach ($query->result_array() as $row) {           
            $data[] = $row;            
        }
        return $data;
     }
}