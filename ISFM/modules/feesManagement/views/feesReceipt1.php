

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('fees_receipt1'); ?> <small></small>
                </h3>
                 <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_fees'); ?>
                    </li>
                    <li>
                        <?php echo lang('fees_receipt1'); ?>
                    </li>
                   
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="col-md-12">
                <!-- BEGIN All account list-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('fees_receipt1'); ?> 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th><?php echo lang('header_rec_no'); ?> </th> 
                                    <th><?php echo lang('enrollment_no');?></th>
                                    <th><?php echo lang('payment_date');?></th> 
                                    <th><?php echo lang('header_pay_mode');?></th>
                                    <th><?php echo lang('cheque_no');?></th>
                                    <th><?php echo lang('cheque_date');?></th>
                                    <th><?php echo lang('cheque_amount');?></th>
                                    <th><?php echo lang('fees_amount');?></th>
                                    <th><?php echo lang('tax_status'); ?> </th>                                 
                                    <th><?php echo lang('remark'); ?> </th>                                 
                                    <th><?php echo lang('view'); ?> </th>                                          
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($fees_receipt_data as $row) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <?php if($row['payment_mode'] == 'online'){ ?>
                                            <td>
                                                <?php echo $row['transaction_no']; ?>
                                            </td> 
                                        <?php }else{ ?>
                                                <td>
                                                <?php echo $row['receipt_no']; ?>
                                            </td>
                                            <?php } ?>
                                            <td>
                                                <?php echo '-'; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['date']; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['payment_mode']; ?>
                                            </td>
                                            <?php if($row['payment_mode'] == 'online'){ ?>
                                                <td>
                                                    <?php echo '-'; ?>
                                                </td>
                                                <td>
                                                    <?php echo '-'; ?>
                                                </td>
                                                <td>
                                                    <?php echo '-'; ?>
                                                </td> 
                                            <?php }else{ ?>
                                                <td>
                                                    <?php echo $row['bank_account']; ?>
                                                </td>
                                                <td>
                                                    <?php echo '-'; ?>
                                                </td>
                                                <td>
                                                    <?php echo '-'; ?>
                                                </td>
                                            <?php } ?>
                                           
                                               
                                                <td>
                                                    <?php echo $row['amount']; ?>
                                                </td>
                                                                                
                                        <td>
                                            <?php echo $row['online_payment_status']; ?>
                                        </td>
                                        <td>
                                            <?php echo "-"; ?>
                                        </td>                                         
                                        <td>  
                                            <!-- <button type="submit" onClick="window.print()" class="btn blue" name="submit" value="Print"><?php echo "Fees Receipt"; ?></button> -->
                                            <a class="btn blue" href="<?php echo base_url().'index.php/feesManagement/feesReceipt' ?>">feesReceipt </a>
                                        </td>                                  
                                    </tr>
                                <?php $i++;} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END All account list-->
            </div>
        </div>
    </div>
</div>