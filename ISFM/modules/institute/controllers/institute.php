<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Institute extends CI_Controller {

    function __construct() 
    {
        parent::__construct();
        $this->load->model('institutemodel');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    function allInstitute() 
    {        
        $data1['school_info'] = $this->institutemodel->getSchoolInfo();
        $postData = '';
        $urldata = 'https://yofundo.in/lms/clients/local/erpApi/get_schools.php';
        $data1['data'] = $this->common->schoolname_data($urldata,$postData);
        $this->load->view('temp/header');
        $this->load->view('allInstitute',$data1);
        $this->load->view('temp/footer');
        
    }

    function addInstitute()
    {
        $school_insert = array(
                    'school_name' => $this->db->escape_like_str($this->input->post('school_name', TRUE)),
                    'school_id' => $this->db->escape_like_str($this->input->post('school_id', TRUE)),
                    'school_grpname' => $this->db->escape_like_str($this->input->post('school_grpname', TRUE)),
                    'sub_location' => $this->db->escape_like_str($this->input->post('address', TRUE)),
                    'city' => $this->db->escape_like_str($this->input->post('city', TRUE)),
                    'pincode' => $this->db->escape_like_str($this->input->post('pincode', TRUE)),
                    'admin_name' => $this->db->escape_like_str($this->input->post('admin_name', TRUE)),
                    'admin_num' => $this->db->escape_like_str($this->input->post('admin_no', TRUE)),
                    'description' =>$this->db->escape_like_str($this->input->post('description', TRUE)),
                    'reg_date' =>$this->db->escape_like_str($this->input->post('reg_date', TRUE)),
                    'foundation_year' =>$this->db->escape_like_str($this->input->post('found_year', TRUE)),
                    'school_email' =>$this->db->escape_like_str($this->input->post('school_email', TRUE)),
                    'school_reg_no' => $this->db->escape_like_str($this->input->post('reg_no', TRUE))
                );                
                
                $schoolname = $this->input->post('school_name');
                $get_unique_data = $this->institutemodel->get_schoolName_unique($schoolname);
                //print_r($get_unique_data);die();
                if($get_unique_data)
                {

                    $data['success'] = '<div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                        <strong>Failed ! </strong>School name Already existed. 
                        </div>';
                        $this->load->view('temp/header');
                        $this->load->view('addInstutute',$data);
                        $this->load->view('temp/footer');
                    
                }
                else
                {

                    //print_r($get_unique_data);die();
                    $scholID = $this->input->post('school_id');
                    $descrption = $this->input->post('description');

                    $postData = '{
                        "name": "'.$schoolname.'",
                        "schoolid": "'.$scholID.'",
                        "description": "'.$descrption.'",
                        "parent": "0" 
                        }';
                    $urldata = 'https://yofundo.in/lms/clients/local/erpApi/add_school.php';
                    $data1['data'] = $this->common->schoolname_data($urldata,$postData);
                    //print_r($data1);die('--');

                    $insert_schoolInfo = $this->db->insert('institute', $school_insert);
                    if($insert_schoolInfo)
                    {
                        $data1['success'] = '<div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                            <strong>Success ! </strong> School Info Added successfully. 
                            </div>';
                        
                    }
                    else
                    {

                        $data['success'] = '<div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                        <strong>Failed ! </strong>Error occured while adding. 
                        </div>';
                        
                    }
                    $this->load->view('temp/header');
                    $this->load->view('addInstutute',$data1);
                    $this->load->view('temp/footer');

                }
                
               

        

    }

    
}