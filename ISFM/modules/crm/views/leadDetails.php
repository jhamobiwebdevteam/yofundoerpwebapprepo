<link href="assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('header_a_singleLead_info'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_crm'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_a_lead_info'); ?>
                    </li>                    
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row" style="display:none;">
            <div class="col-md-2"> </div>
            <?php
            $stageId = 0;$leadId=0;
            foreach ($lead as $row) { $stageId = $row['lead_stage']; $leadId = $row['id']; }
            foreach ($stages as $row) {  ?>
                    <div class="col-md-1 ldStage" style="<?php if($row['id'] == $stageId){ ?> background:#f5cece; <?php } ?>"> <?php echo  $row['name'];?> </div>
            <?php } ?>
        </div>
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <!-- <div class="col-md-2">
                <?php
                foreach ($lead as $row) {
                ?>
                <ul class="ver-inline-menu tabbable margin-bottom-10">
                    <li class="leadDetails">
                           <?php echo  $row['fname'] .' '.$row['lname']; ?>
                    </li>
                </ul>
                <ul class="ver-inline-menu tabbable margin-bottom-10">
                    <li class="leadDetails">
                            <?php echo $row['class_title']; ?> 
                    </li>
                </ul>               
                <ul class="ver-inline-menu tabbable margin-bottom-10">
                    <li class="leadDetails">
                           <?php echo  $row['lead_sor']; ?>
                    </li>
                </ul>
                <ul class="ver-inline-menu tabbable margin-bottom-10">
                    <li class="leadDetails">
                            <?php echo $row['stage']; ?> 
                    </li>
                </ul>
                <ul class="ver-inline-menu tabbable margin-bottom-10">
                    <li class="leadDetails">
                         <?php echo $row['mobile']; ?>
                    </li>
                </ul>
                <ul class="ver-inline-menu tabbable margin-bottom-10">
                    <li class="leadDetails">
                         <?php echo $row['email']; ?>
                    </li>
                </ul>
                <?php } ?>
            </div> -->
            <div class="col-md-9">
                 <!--BEGIN TABS-->
                 <div class="tabbable tabbable-custom tabbable-full-width">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_0" data-toggle="tab"><?php echo "Enquiry Details";?></a>
                        </li>
                        <li>
                            <a href="#tab_1_1" data-toggle="tab"><?php echo lang('lead_cycle'); ?></a>
                        </li>
                        <li>
                            <a href="#tab_1_3" data-toggle="tab"><?php echo lang('activity'); ?></a>
                        </li>
                        <li>
                            <a href="#tab_1_2" data-toggle="tab"><?php echo lang('action_log');?></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_0">
                            <div class="row margin-left">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <!-- <table class="table table-bordered" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th>Sr.no</th>
                                                    <th>Name</th>
                                                    <th>Class</th>
                                                    <th>Lead Source</th>
                                                    <th>Lead Stage</th>
                                                    <th>Mobile</th>
                                                    <th>Email</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=1; foreach ($lead as $row) {?>
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo  $row['fname'] .' '.$row['lname']; ?></td>
                                                        <td><?php echo $row['class_title']; ?></td>
                                                        <td><?php echo  $row['lead_sor']; ?></td>
                                                        <td><?php echo $row['stage']; ?></td>
                                                        <td><?php echo $row['mobile']; ?></td>
                                                        <td><?php echo $row['email']; ?></td>
                                                    </tr>
                                                     <?php $i++; } ?>
                                            </tbody>
                                        </table> -->

                                        <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('crm_gtifnt'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        foreach ($lead as $row) {
                            $id = $row['id'];
                            $fname = $row['fname'];
                            $lname = $row['lname'];
                            $email = $row['email'];
                            $mobile = $row['mobile'];
                            $lead_source = $row['lead_source'];
                            $class = $row['class'];
                            $location = $row['location'];
                        }
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        echo form_open_multipart("crm/editLead?id=$id", $form_attributs);
                        ?>
                        <div class="form-body">
                            <?php
                            if (!empty($success)) {
                                echo $success;
                            }
                            ?>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_fn'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="First Name" name="first_name" data-validation="required" data-validation-error-msg="" value = <?php echo $fname; ?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_ln'); ?> <span class="requiredStar">  </span></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Last Name" name="last_name" data-validation-error-msg="" value = <?php echo $lname; ?>>
                                </div>
                            </div>                          
                          
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_pn'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="mobile" onchange="checkMobile(this.value)" placeholder=""  data-validation="required" maxlength="10" minlength = "10" data-validation-error-msg="" value = <?php echo $mobile; ?>>
                                    <div id="checkMobile" class="col-md-12"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_eml'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" onchange="checkEmail(this.value)" placeholder="demo@demo.com" name="email" id="name" placeholder=""  data-validation="required" data-validation-error-msg="" value = <?php echo $email; ?>>
                                    <div id="checkEmail" class="col-md-12"></div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_ls'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <select class="form-control" name="lead_sourse" data-validation="required" data-validation-error-msg="You have to select anyone.">
                                        <option value=""><?php echo lang('select'); ?> </option>
                                        <?php foreach ($leadSource as $row) { ?>
                                            <option value="<?php echo $row['id']; ?>" <?php if($row['id'] == $lead_source){?> selected <?php } ?>><?php echo $row['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_cls'); ?> <span class="requiredStar">  </span></label>
                                <div class="col-md-6">
                                    <select name="class" class="form-control" data-validation-error-msg="">
                                        <option value="0"><?php echo lang('select'); ?></option>
                                        <?php foreach ($classTile as $row) { ?>
                                            <option value="<?php echo $row['id']; ?>" <?php if($row['id'] == $class){?> selected <?php } ?> ><?php echo $row['class_title']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('lead_loc'); ?> <span class="requiredStar">  </span></label>
                                <div class="col-md-6">
                                    <select name="location" class="form-control"  data-validation-error-msg="">
                                    <option value=""><?php echo lang('select'); ?> </option>
                                    <?php foreach ($leadLocation as $row) { ?>
                                        <option value="<?php echo $row['id']; ?>" <?php if($row['id'] == $location){?> selected <?php } ?>><?php echo $row['name']; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" id="Button" class="btn green" name="submit" value="submit"><?php echo lang('tea_si'); ?></button>
                                <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_1_1">
                            <div class="row">
                                <div class="col-md-8">
                                <?php foreach ($leadHistory as $row) { ?>
                                    <p class="glyphicon glyphicon-chevron-right"></p>
                                    <ul class="ulLead">
                                        <li>Lead Activity - <?php echo $row['lead_activity_name']?></li>
                                        <li>Lead Stage - <?php echo $row['stage_name']?></li>
                                        <li>Date & Time - <?php echo $row['createdAt']?></li>
                                    </ul>
                                <?php } ?> 
                                </div>
                            </div>
                        </div>
                        <!--tab_1_2-->
                        <div class="tab-pane" id="tab_1_3">
                            <div class="row">
                                <div class="col-md-11">
                                <div class="row margin-left-10">
                                <?php $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                                    echo form_open('crm/activity', $form_attributs);
                                    
                                    ?>
                                    <input type="hidden"  name="leadId" value="<?php echo $leadId ?>">
                                    <input type="hidden"  name="stageId" value="<?php echo $stageId ?>">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="control-label"><?php echo lang('header_master_la'); ?></label>
                                                    <select class="form-control" name="lead_activity" data-validation="required" data-validation-error-msg="You have to select anyone.">
                                                        <option value=""><?php echo lang('select'); ?> </option>
                                                        <?php foreach ($activity as $row) { ?>
                                                            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-9">
                                                    <label class="control-label"><?php echo lang('lead_ar'); ?></label>
                                                    <input type="text" class="form-control" placeholder="Activity Remark" name="act_remark">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label"><?php echo lang('fdate'); ?></label>
                                                    <input type="datetime-local" class="form-control" name="fdate" min="<?= date('Y-m-d'); ?>T00:00">
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="control-label"><?php echo lang('lead_stage'); ?></label>
                                                    <select class="form-control" name="lead_stage">
                                                        <option value=""><?php echo lang('select'); ?> </option>
                                                        <?php foreach ($stages1 as $row) { ?>
                                                            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-9">
                                                    <label class="control-label"><?php echo lang('fremark'); ?></label>
                                                    <input type="text" class="form-control" placeholder="remark" name="fremark">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margiv-top-10">								
                                            <button type="submit" name="submit" class="btn green">Change Now</button>
                                            <button type="reset" class="btn default">Cancel</button>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_1_2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <table class="table table-bordered" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <th>Date & Time</th>
                                                   
                                                    <th>By Whom</th>
                                                    <th>Action Description</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=1; foreach ($leadHistory as $row) { ?>
                                                <tr>
                                                    <td><?php echo $i;?></td>
                                                    <td><?php echo $row['createdAt'];?></td>
                                                    
                                                    <td><?php $user = $this->ion_auth->user()->row(); echo $userId = $user->username;?></td>
                                                    <td><?php echo $row['action_description'];?></td>
                                                </tr>
                                            <?php $i++; } ?>

                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END TABS-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL script -->
<script type="text/javascript" src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/components-form-tools.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate();</script>
<script>
    jQuery(document).ready(function () {
            ComponentsFormTools.init();
        });     
    
        
    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>

<!-- END PAGE LEVEL script -->