<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>

<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        
        
        <div class="row">
            <?php //print_r($grade_info); die($id.'--'.$email.'--'.$name.'--'.$phone2);?>
            <div class="col-md-12">
                <div class="portlet box green row">
                    <div class="portlet-title">
                        <div class="caption col-md-10">
                            <?php echo 'Grades of '.$data[0]['fullname']; ?>
                        </div>  
                    </div>
                    <?php ?>
                    <div class="portlet-body">
                        <table class="table" id="sample_1">
                            <thead>
                                <tr>
                                    <th>Activity Name</th>
                                    <th>Activity Id</th>
                                    <th>Activity Type</th>
                                    <th>Obtained Marks</th>
                                    <th>Out Of</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                // print_r($data); die('---');
                                $i=1;foreach ($data as $row) { 
                                    ?>
                                    <tr>
                                        <td> <?php echo $row['itemname']; ?></td>
                                        <td> <?php echo $row['itemid']; ?></td>
                                        <td> <?php echo $row['itemmodule']; ?></td>
                                        <td> <?php if($row['itemmodule'] == 'quiz'){ ?> <a type="button" class="" href="index.php/examination/fetch_quiz_details?mystr=<?php echo  $row['itemname'] .",". $row['fullname'] .",". $row['iteminstance'] .",". $row['courseid'].",". $row['userid'] .",". number_format((float)$row['marks'], 2, '.', ''); ?>" > <?php echo number_format((float)$row['marks'], 2, '.', '');  ?></a> <?php }else{
                                             echo number_format((float)$row['marks'], 2, '.', '');; 
                                        } ?></td> 
                                        <td><?php echo intval($row['outof']); ?> </td> 
                                    </tr>
                                <?php $i++;} 
                                    
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        

<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT -->


        
    </div>
</div>

<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->


<script type="text/javascript">
    
    $('#myModal').on('show.bs.modal', function (e) {
        var mystr = $(e.relatedTarget).data('id');
        var myarr = mystr.split(",");
        $('#titl').empty();
        $("#myTable").empty();
        $.ajax({
            type : 'POST',
            url : '<?php echo base_url().'crm/fetch_record' ?>', //Here you will fetch records 
            data :  'mystr='+ mystr, //Pass $id
            dataType: "json",
            success : function(data){
                $('#titl').append(data.data[0].fullname);
                $.each(data.data, function(key, value) {                    
                    $('#myTable').append('<tr><td>' + value.itemname+ '</td><td>' + value.itemmodule + '</td><td>' + Math.floor(value.marks) + '</td><td>' +  Math.floor(value.outof) + '</td></tr>');     
                });
            }
        });
     });
</script>