

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('fees_structure'); ?> <small></small>
                </h3>
                 <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_fees'); ?>
                    </li>
                    <li>
                        <?php echo lang('fees_structure'); ?>
                    </li>
                   
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="col-md-12">
                <!-- BEGIN All account list-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('fees_structure'); ?> 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo lang('register_class'); ?> </th> 
                                    <th><?php echo lang('fee_categ');?></th>
                                    <th><?php echo lang('acc_tutionfee');?></th> 
                                    <th><?php echo lang('activity_fees');?></th>
                                    <th><?php echo lang('comp_fees');?></th>
                                    <th><?php echo lang('festival_fees');?></th>
                                    <th><?php echo lang('caution_money');?></th>
                                    <th><?php echo lang('drama_fees');?></th>
                                    <th><?php echo lang('sport_fees'); ?> </th>                                 
                                    <th><?php echo lang('total_fees'); ?> </th>                                 
                                    <th><?php echo lang('rgister_action'); ?> </th>                                 
                                    <th><?php echo lang('rgister_action'); ?> </th>                                 
                                </tr>
                            </thead>
                            <tbody>
                                <?php //$i=1; foreach ($tax_info as $row) { ?>
                                    <tr>
                                        <td>
                                            <?php echo 1; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td> 
                                        <td>
                                            <?php //echo $row['from_date']; ?>
                                            <?php echo "abc"; ?>
                                        </td> 
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>  
                                            <button type="submit" id="Button" class="btn green" name="submit" value="submit"><?php echo lang('edit'); ?></button>
                                        </td>  
                                        <td>  
                                            <!-- <button type="submit" onClick="window.print()" class="btn blue" name="submit" value="Print"><?php echo "Fees Receipt"; ?></button> -->
                                            <a class="btn blue" href="<?php echo base_url().'index.php/feesManagement/feesReceipt' ?>">feesReceipt </a>
                                        </td>                                  
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php echo 2; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td> 
                                        <td>
                                            <?php //echo $row['from_date']; ?>
                                            <?php echo "abc"; ?>
                                        </td> 
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>
                                            <?php echo "abc"; ?>
                                        </td>
                                        <td>  
                                            <button type="submit" id="Button" class="btn green" name="submit" value="submit"><?php echo lang('edit'); ?></button>
                                        </td>  
                                                                           
                                    </tr>
                                <?php //$i++;} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END All account list-->
            </div>
        </div>
    </div>
</div>