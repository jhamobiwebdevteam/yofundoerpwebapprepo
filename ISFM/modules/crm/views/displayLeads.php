<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('header_a_lead_info'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_crm'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_a_lead_info'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        
        
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('tea_tl'); ?>
                        </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        
                        <table class="table table-bordered" id="sample_1">
                        <div class="row smsemail">
                            <div class="col-md-6 margin-bottom-10">
                                <button type="submit" id="sms" class="btn red" name="submit" value="submit"><?php echo lang('sms'); ?></button> OR
                                <button type="submit" id="email" class="btn red"><?php echo lang('eml'); ?></button>
                            </div>
                        </div>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>
                                        <?php echo lang('lead_idno'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('lead_fn'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('lead_ln'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('lead_dphn'); ?>
                                    </th>
                                    <th>
                                       <?php echo lang('lead_eml'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('lead_ls'); ?> 
                                    </th>
                                    <th>
                                        <?php echo lang('lead_cls'); ?>
                                    </th>
                                    <th>
                                       <?php echo lang('lead_loc'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('lead_own'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('lead_stage'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('lead_dte'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('lead_acdte'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('lead_acfdte'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('exa_actd'); ?> 
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($lead as $row) { ?>
                                    <tr style="
                                        <?php if(($row['cdate'] < $row['fdate']) && ($row['stage'] != 'enquiry')){?>
                                        background: orange; <?php 
                                        }if((($row['cdate'] < $row['fdate']) && ($row['fdate'] < date('Y-m-d h:i:s')) && ($row['stage'] != 'enquiry')) || (($row['cdate'] > $row['fdate']) && ($row['fdate'] < date('Y-m-d h:i:s')) && ($row['stage'] != 'enquiry'))){?>
                                        background: #ff9898; <?php 
                                        }else if(($row['cdate'] > $row['fdate'] && ($row['stage'] != 'enquiry'))){?>
                                        background: #35aa47; <?php 
                                        }?>">
                                        <td><input type="checkbox" class="chk" name="chk" value="<?php echo $row['mobile'];?>-<?php echo $row['email']; ?>"></td>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['fname']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['lname']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['mobile']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['email']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['lead_sor']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['class_title']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['locatn']; ?>
                                        </td>
                                        <td>
                                            <?php echo $user->username; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['stage']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['created_at']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['cdate']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['fdate']; ?>
                                        </td>
                                        <td>
                                            <a class="btn btn-xs green" href="index.php/crm/leadDetails?id=<?php echo $row['id']; ?>&stage=<?php echo $row['lead_stage']; ?>"> <i class="fa fa-file-text-o"></i> <?php echo lang('details'); ?> </a>
                                            <a class="btn btn-xs default" href="index.php/crm/editLead?id=<?php echo $row['id']; ?>"> <i class="fa fa-pencil-square"></i> <?php echo lang('edit'); ?> </a>
                                            <a class="btn btn-xs red" href="index.php/crm/teacherDelete?id=<?php echo $row['id']; ?>&uid=<?php echo $row['user_id']; ?>"  onClick="javascript:return confirm('<?php echo lang('tea_tdecon'); ?>')"> <i class="fa fa-trash-o"></i> <?php echo lang('delete'); ?> </a>
                                        
                                        </td>
                                    </tr>
                                <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script>
    jQuery(document).ready(function() {
        //here is auto reload after 1 second for time and date in the top
        // jQuery(setInterval(function() {
        //     jQuery("#result").load("index.php/home/iceTime");
        // }, 1000));
        
        jQuery('.chk').click(function(){
            if (jQuery('input[name="chk"]:checked').length > 0) {
                jQuery(".smsemail").css("display", "block");
            } else {
                jQuery(".smsemail").css("display", "none");
            }
        });
        jQuery("#sms").click(function(event){
            event.preventDefault();
            var searchIDs = jQuery("input:checkbox:checked").map(function(){
                var id = $(this).val();
                var arr = id.split('-');
            return arr[0];
            }).get(); 
            console.log(searchIDs);
        });
    });
</script>