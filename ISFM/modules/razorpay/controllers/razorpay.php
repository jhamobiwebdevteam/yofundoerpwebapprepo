<?php 
    if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Razorpay extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/home
     * 	- or -  
     * 		http://example.com/index.php/home/index
     */
    function __construct() {
        parent::__construct();
        $this->load->model('razorpaymodel');
        // $this->load->model('payment/paymentmodel');
        // $this->load->model('homeModel');
        $this->load->helper('file');
        $this->load->helper('form');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    //This function will show the users dashboard
    public function index() {
		$this->checkout();
	}

	public function checkout() {
        $data['title']              = 'Checkout payment';  
        $data['callback_url']       = base_url().'razorpay/callback';
        $data['surl']               = base_url().'razorpay/success';
        $data['furl']               = base_url().'razorpay/failed';
        $data['currency_code']      = 'INR';
        $data['description']        = isset($_REQUEST['pinfo'])?$_REQUEST['pinfo']:'';
        $data['txnid']              = date("YmdHis");     
        $data['key_id']             = "rzp_test_S2a1GfK3VOga3S";
        $data['currency_code']      = "INR";            
        $data['amount']             = isset($_REQUEST['amount'])?$_REQUEST['amount']:'0';
        $data['total']              = ($data['amount'] * 100); // 100 = 1 indian rupees
        $data['merchant_order_id']  = "RZP".date("YmdHis");
        $data['card_holder_name']   = '';
        $data['email']              = isset($_REQUEST['eml'])?$_REQUEST['eml']:'';
        $data['phone']              = isset($_REQUEST['mbl'])?$_REQUEST['mbl']:'';
        $data['name']               = "RazorPay Jhamobi";


        $insert_data = array( 
            'amount'=>  $this->input->post('amount'),
            'date'=> $this->input->post('date'),
            'student_id'=>$this->input->post('name'),
            'mobile' =>$this->input->post('mbl'),
            'email_id'=>$this->input->post('eml'),
            'payment_mode'=> "online",
            'online_payment_status'=> "processing",
            'transaction_no'=> $data['merchant_order_id'],
        );            
        $this->db->insert('fee_payment_record', $insert_data);


        $this->load->view('temp/stud_header');
        $this->load->view('razorpay/checkout', $data);
        $this->load->view('temp/footer');
    }

    // initialized cURL Request
    private function curl_handler($payment_id, $amount)  {
        $url            = 'https://api.razorpay.com/v1/payments/'.$payment_id.'/capture';
        $key_id         = "rzp_test_S2a1GfK3VOga3S";
        $key_secret     = "UflJEFolVI4H2bROJXqCGmJA";
        $fields_string  = "amount=$amount";
        //cURL Request
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $key_id.':'.$key_secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        return $ch;
    }   
        
    // callback method
    public function callback() {   
        print_r($this->input->post());     
        if (!empty($this->input->post('razorpay_payment_id')) && !empty($this->input->post('merchant_order_id'))) {
            $razorpay_payment_id = $this->input->post('razorpay_payment_id');
            $merchant_order_id = $this->input->post('merchant_order_id');
            
            $this->session->set_flashdata('razorpay_payment_id', $this->input->post('razorpay_payment_id'));
            $this->session->set_flashdata('merchant_order_id', $this->input->post('merchant_order_id'));
            $currency_code = 'INR';
            $amount = $this->input->post('merchant_total');
            $success = false;
            $error = '';
            try {                
                $ch = $this->curl_handler($razorpay_payment_id, $amount);
                //execute post
                $result = curl_exec($ch);
                $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($result === false) {
                    $success = false;
                    $error = 'Curl error: '.curl_error($ch);
                } else {
                    $response_array = json_decode($result, true);
                        //Check success response
                        if ($http_status === 200 and isset($response_array['error']) === false) {
                            $success = true;
                        } else {
                            $success = false;
                            if (!empty($response_array['error']['code'])) {
                                $error = $response_array['error']['code'].':'.$response_array['error']['description'];
                            } else {
                                $error = 'RAZORPAY_ERROR:Invalid Response <br/>'.$result;
                            }
                        }
                }
                //close curl connection
                curl_close($ch);
            } catch (Exception $e) {
                $success = false;
                $error = 'Request to Razorpay Failed';
            }
            
            if ($success === true) {
                if(!empty($this->session->userdata('ci_subscription_keys'))) {
                    $this->session->unset_userdata('ci_subscription_keys');
                }
                if (!$order_info['order_status_id']) {
                    redirect($this->input->post('merchant_surl_id'));
                } else {
                    redirect($this->input->post('merchant_surl_id'));
                }

            } else {
                redirect($this->input->post('merchant_furl_id'));
            }
        } else {
            echo 'An error occured. Contact site administrator, please!';
        }
    } 
    public function success() {

        $razorpay_payment_id = $this->session->flashdata('razorpay_payment_id');
        $merchant_order_id = $this->session->flashdata('merchant_order_id');
        $getData = $this->razorpaymodel->get_details_from_txnid($merchant_order_id);
        $data['title'] = 'Razorpay Success';
      
        if($getData){
            if($merchant_order_id == $getData[0]['transaction_no']){
                $update_data = array(
                    'online_payment_status' => 'success',
                    'payu_transaction_id' => $razorpay_payment_id
                );
                $this->db->where('transaction_no', $merchant_order_id);
                $this->db->update('fee_payment_record', $update_data);
                $dta['msg'] = '<b style="color:green">Payment Done Successfully..<b>';
                $dta['msg1'] = "Your Payment Id is : <b> $merchant_order_id </b>";
            }else{
                $dta['msg'] = '<b style="color:red">Invalid Transaction.</b>';
                $dta['msg1'] = "Please try again..";
            }
        }else{
                $dta['msg'] = '<b style="color:red">Invalid Transaction.</b>';
                $dta['msg1'] = "Please try again..";
        }
        
        

        $this->load->view('temp/header');
        $this->load->view('modal',$dta);
        $this->load->view('temp/footer'); 
        

    }  
    public function failed() {
        $razorpay_payment_id = $this->session->flashdata('razorpay_payment_id');
        $merchant_order_id = $this->session->flashdata('merchant_order_id');
        $getData = $this->razorpaymodel->get_details_from_txnid($merchant_order_id);

        if($getData){
            if($merchant_order_id == $getData[0]['transaction_no']){
                $update_data = array(
                    'online_payment_status' => 'failure',
                    'payu_transaction_id' => $razorpay_payment_id?$razorpay_payment_id:''
                );
                $this->db->where('transaction_no', $merchant_order_id);
                $this->db->update('fee_payment_record', $update_data);
                $dta['msg'] = '<b style="color:red">Sorry Payment Failed..</b>';
                $dta['msg1'] = "Please try again";
            }else{
                $dta['msg'] = '<b style="color:red">Invalid Transaction.</b>';
                $dta['msg1'] = "Please try again..";
            }
        }else{
            $dta['msg'] = '<b style="color:red">Invalid Transaction.</b>';
            $dta['msg1'] = "Please try again..";
        }

        $this->load->view('temp/header');
        $this->load->view('modal',$dta);
        $this->load->view('temp/footer'); 
    }

}