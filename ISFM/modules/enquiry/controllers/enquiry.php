<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Enquiry extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/home
     * 	- or -  
     * 		http://example.com/index.php/home/index
     */
    function __construct() {
        parent::__construct();
        
        $this->load->helper('file');
        $this->load->helper('form');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    //This function will show the users dashboard
    public function index() {
        $this->load->view('temp/header');
        $this->load->view('index');
        $this->load->view('temp/footer');
    }

    function addLeadSource() { 
        /*if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }*/
        if ($this->input->post('submit', TRUE)) {
            $leadInfo = array(
                'name' => $this->db->escape_like_str($this->input->post('name', TRUE))
            );
            if ($this->db->insert('lead_source', $leadInfo)) {
                $data['message'] = '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <strong>Success ! </strong> Lead added successfully. 
                            </div>';
                $data['allRecords'] = $this->common->getAllData('lead_source');
                $this->load->view('temp/header');
                $this->load->view('addLeadSource',$data);
                $this->load->view('temp/footer');
            }
        }else {      
            $data['allRecords'] = $this->common->getAllData('lead_source');      
            $this->load->view('temp/header');
            $this->load->view('addLeadSource',$data);
            $this->load->view('temp/footer');
        }
    }

     function addLocation() { 
        /*if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }*/
        if ($this->input->post('submit', TRUE)) {
            $leadInfo = array(
                'name' => $this->db->escape_like_str($this->input->post('location', TRUE))
            );
            if ($this->db->insert('lead_location', $leadInfo)) {
                $data['message'] = '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <strong>Success ! </strong> Lead added successfully. 
                            </div>';
                $data['allRecords'] = $this->common->getAllData('lead_location');
                $this->load->view('temp/header');
                $this->load->view('addLocation',$data);
                $this->load->view('temp/footer');
            }
        }else {
            $data['allRecords'] = $this->common->getAllData('lead_location');
            $this->load->view('temp/header');
            $this->load->view('addLocation',$data);
            $this->load->view('temp/footer');
        }
    }

    function addLeadStage() { 
       /* if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }*/
        if ($this->input->post('submit', TRUE)) {
            $leadInfo = array(
                'name' => $this->db->escape_like_str($this->input->post('name', TRUE))
            );
            if ($this->db->insert('lead_stage', $leadInfo)) {
                $data['message'] = '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <strong>Success ! </strong> Lead added successfully. 
                            </div>';
                $data['allRecords'] = $this->common->getAllData('lead_stage');
                $this->load->view('temp/header');
                $this->load->view('addLeadStage',$data);
                $this->load->view('temp/footer');
            }
        }else {     
            $data['allRecords'] = $this->common->getAllData('lead_stage');       
            $this->load->view('temp/header');
            $this->load->view('addLeadStage',$data);
            $this->load->view('temp/footer');
        }
    }

    function addActivity() { 
        /*if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }*/
        if ($this->input->post('submit', TRUE)) {
            $leadInfo = array(
                'name' => $this->db->escape_like_str($this->input->post('activity', TRUE))
            );
            if ($this->db->insert('lead_activity', $leadInfo)) {
                $data['message'] = '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <strong>Success ! </strong> Lead added successfully. 
                            </div>';
                $data['allRecords'] = $this->common->getDataExceptId(1,'lead_activity');
                $this->load->view('temp/header');
                $this->load->view('addActivity',$data);
                $this->load->view('temp/footer');
            }
        }else {
            $data['allRecords'] = $this->common->getDataExceptId(1,'lead_activity');
            $this->load->view('temp/header');
            $this->load->view('addActivity',$data);
            $this->load->view('temp/footer');
        }
    }
}