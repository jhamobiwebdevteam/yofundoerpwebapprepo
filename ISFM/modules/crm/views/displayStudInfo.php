<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('displayStudInfo'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_crm'); ?>
                    </li>
                    <li>
                        <?php echo lang('displayStudInfo'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        
        <div class="tabbable tabbable-custom tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab">Students Detail Information</a>
                </li>
               
                <li>
                    <a href="#tab_1_2" data-toggle="tab"><?php echo lang('action_log');?></a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <?php echo lang('displayStudInfo'); ?>
                                </div>
                                <div class="tools">
                                </div>
                            </div>
                            <div class="portlet-body">
                                    <form action="<?php echo base_url();?>index.php/crm/edit_data" class="form-horizontal" method="POST">
                                            <div class="form-group">
                                                <center><blockquote>Students Personal Details</blockquote></center><hr />
                                            </div>
                                            <?php  foreach($stud_info as $row) { ?>
                                            
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Registration For Academic Year</label>
                                                
                                                     <input type="hidden" name="ed_id" id="ed_id" value="<?php echo $row['id'];?>">
                                                <div class="col-md-2">
                                                   <input type="text" class="form-control" id="academic_year" name="academic_year" value="<?php echo $row['academic_year']?>">                        
                                                </div> 
                                                <label class="col-md-2 control-label">First Name</label>
                                                <div class="col-md-2">
                                                    <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $row['first_name']?>">                           
                                                </div>
                                                
                                                    <label class="col-md-2 control-label">Students Photo</label>
                                                    <div class="col-md-2 control-label">
                                                        <img src="assets/uploads/registration/<?php echo $row['student_photo']; ?>"  alt="..." style="width: 100%;">
                                                    </div>                   
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Middle Name</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="mid_name" name="mid_name" value="<?php echo $row['middle_name']?>"> 
                                                </div>
                                                <label class="col-md-2 control-label">Last Name</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $row['last_name']?>"> 
                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Date of Birth</label>
                                                <div class="col-md-3">
                                                    <input type="date" class="form-control" id="dob" name="dob" value="<?php echo $row['dob']?>">
                                                </div>
                                                <label class="col-md-2 control-label">Email</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="email" name="email" value="<?php echo $row['email']?>" disabled>                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Nationality</label>
                                               <!--  <div class="col-md-3">
                                                    <input type="text" class="form-control" id="nationality" name="nationality" value="">
                                                    
                                                </div> -->
                                                 <div class="col-md-3">
                                                    <select class="form-control" name="nationality">
                                                        
                                                        <option value="<?php echo $row['nationality']?>"><?php echo $row['nationality']?></option>
                                                        <option value="Indian">Indian</option>
                                                        <option value="American">American</option>
                                                        <option value="Australian">Australian</option>
                                                        <option value="Canadian">Canadian</option>
                                                       
                                                    </select>
                                                </div>
                                                <label class="col-md-2 control-label">Gender</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="gender" name="gender" value="<?php echo $row['gender']?>">
                                                </div>
                                                
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Caste Category</label>
                                               <!--  <div class="col-md-3">
                                                    <input type="text" class="form-control" id="caste" name="caste" value="">    
                                                </div>  -->
                                                 <div class="col-md-3">
                                                    <select class="form-control" name="caste_cat">
                                                        <option value="<?php echo $row['caste']?>"><?php echo $row['caste']?></option>
                                                        <option value="Open">Open</option>
                                                        <option value="OBC">OBC</option>
                                                        <option value="NT-B">NT-B</option>
                                                        <option value="NT-C">NT-C</option>
                                                        <option value="NT-D">NT-D</option> 
                                                        <option value="SC">SC</option>
                                                        <option value="ST">ST</option>
                                                    </select>
                                                </div>
                                                <label class="col-md-2 control-label">Religion</label>
                                                <!-- <div class="col-md-3">
                                                    <input type="text" class="form-control" id="religion" name="religion" value="">  
                                                </div>      --> 
                                                <div class="col-md-3">
                                                    <select class="form-control" name="religion">
                                                         <option value="<?php echo $row['religion']?>"><?php echo $row['religion']?></option>
                                                        <option value="Hindu">Hindu</option>
                                                        <option value="Muslim">Muslim</option>
                                                        <option value="Buddhism">Buddhism</option>
                                                        <option value="Christian">Christian</option>
                                                        <option value="Jainism">Jainism</option>
                                                        <option value="Navbauddha">Navbauddha</option>
                                                        <option value="Parsi">Parsi</option>
                                                        <option value="Sikh">Sikh</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                </div>              
                                            </div>
                        
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Birth Place</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="birth_place" name="birth_place" value="<?php echo $row['birth_place']?>">  
                                                </div>                    
                                            
                                                <label class="col-md-2 control-label">Address</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="address" name="address" value="<?php echo $row['address']?>">  
                                                </div>                    
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Pincode</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="pin_code" name="pin_code" value="<?php echo $row['pin_code']?>">  
                                                </div>
                                                 <label class="col-md-2 control-label">Mobile</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="fathers_mobile" name="fathers_mobile" value="<?php echo $row['fathers_mobile']?>" disabled>  
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <center><blockquote>Parents Details</blockquote></center><hr />
                                            </div>      
                                            <div class="form-group">
                                                
                                                <label class="col-md-3 control-label">Fathers Name</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="fathers_firstname" name="fathers_firstname" value="<?php echo $row['fathers_firstname']?>">   
                                                </div> 
                                                 <label class="col-md-2 control-label">Fathers Middle Name</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="fathers_midname" name="fathers_midname" value="<?php echo $row['fathers_midname']?>">  
                                                </div>                          
                                            </div>
                                            <div class="form-group">   
                                                <label class="col-md-3 control-label">Last Name</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="fathers_lastname" name="fathers_lastname" value="<?php echo $row['fathers_lastname']?>">  
                                                </div>
                                                <label class="col-md-2 control-label">Fathers Occupation</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="fathers_occup" name="fathers_occup" value="<?php echo $row['fathers_occup']?>">  
                                                </div>   
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Mothers Occupation</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="mothers_occup" name="mothers_occup" value="<?php echo $row['mothers_occup']?>">  
                                                </div>
                                                <label class="col-md-2 control-label">Mothers Name</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="mothers_firstname" name="mothers_firstname" value="<?php echo $row['mothers_firstname']?>">  
                                                </div>                    
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Mothers Middle Name</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="mothers_midname" name="mothers_midname" value="<?php echo $row['mothers_midname']?>">  
                                                </div> 
                                                <label class="col-md-2 control-label">Mothers Last Name</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="mothers_lastname" name="mothers_lastname" value="<?php echo $row['mothers_lastname']?>">  
                                                </div>                 
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Mothers Mobile</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="mothers_mobile" name="mothers_mobile" value="<?php echo $row['mothers_mobile']?>">  
                                                </div>
                                                <label class="col-md-2 control-label">Mothers Qualification</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="mothers_qualification" name="mothers_qualification" value="<?php echo $row['mothers_qualification']?>">  
                                                </div>                     
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Fathers Qualification</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="fathers_qualification" name="fathers_qualification" value="<?php echo $row['fathers_qualification']?>">  
                                                </div>
                                                <label class="col-md-2 control-label">Mother Tounge</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="mother_toung" name="mother_toung" value="<?php echo $row['mother_toung']?>">  
                                                </div>                       
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Annual Income</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="anuual_income" name="anuual_income" value="<?php echo $row['anuual_income']?>">  
                                                </div>                       
                                            </div>
                                            <div class="form-group">
                                                <center><blockquote>Other Details</blockquote></center><hr />
                                            </div>      
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Does your child have a medical condition that we should know? </label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="isthr_medical_condition" name="isthr_medical_condition" value="<?php echo $row['isthr_medical_condition']?>">  
                                                </div>
                                                <label class="col-md-2 control-label">Is your undergoing and short / long term treatment?</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="isthr_undergoing_treatment" name="isthr_undergoing_treatment" value="<?php echo $row['isthr_undergoing_treatment']?>">  
                                                </div>                       
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Does your child have a medical condition that we should know? </label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="is_child_taking_medication" name="is_child_taking_medication" value="<?php echo $row['is_child_taking_medication']?>">  
                                                </div>
                                                <label class="col-md-2 control-label">Any food allergy</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="food_allergy" name="food_allergy" value="<?php echo $row['food_allergy']?>">  
                                                </div>                       
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Medical Fitness Certificate</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="medical_fitness_cert" name="medical_fitness_cert" value="<?php echo $row['medical_fitness_cert']?>">  
                                                </div>
                                                <label class="col-md-2 control-label">Adoption Document</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="adoption_document" name="adoption_document" value="<?php echo $row['adoption_document']?>">  
                                                </div>                       
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">3 Passport Size Photos</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="passport_photos" name="passport_photos" value="<?php echo $row['passport_photos']?>">  
                                                </div>
                                                <label class="col-md-2 control-label">2 Passport Size Photos Of Both Parents</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="adoption_document" name="adoption_document" value="<?php echo $row['adoption_document']?>">  
                                                </div>                       
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Original Birth Certificate</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="original_birth_cert" name="original_birth_cert" value="<?php echo $row['original_birth_cert']?>">  
                                                </div>
                                                <label class="col-md-2 control-label">Original Bonafide Certficate</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="original_bonafied" name="original_bonafied" value="<?php echo $row['original_bonafied']?>">  
                                                </div>                       
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Original Leaving Or Transfer Certificate</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="original_tc" name="original_tc" value="<?php echo $row['original_tc']?>">  
                                                </div>
                                                <label class="col-md-2 control-label">Copy Of Pan Card Of Parent</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="pan_card_copy_parent" name="pan_card_copy_parent" value="<?php echo $row['pan_card_copy_parent']?>">  
                                                </div>                       
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Adhar Card Copy</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="adhar_copy_parent" name="adhar_copy_parent" value="<?php echo $row['adhar_copy_parent']?>">  
                                                </div>
                                                <label class="col-md-2 control-label">Caste Certificate(If Applicable)</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="caste_certificate" name="caste_certificate" value="<?php echo $row['caste_certificate']?>">  
                                                </div>                       
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Residence Proof</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="residence_proof" name="residence_proof" value="<?php echo $row['residence_proof']?>">  
                                                </div>
                                                <label class="col-md-2 control-label">For Foreign Nationals-Passport Copy</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="if_foreign_nationals_copy" name="if_foreign_nationals_copy" value="<?php echo $row['if_foreign_nationals_copy']?>">  
                                                </div>                       
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Copy Of Report Card</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="copy_report_card" name="copy_report_card" value="<?php echo $row['copy_report_card']?>">  
                                                </div>  
                                            </div>
                                                                        <div class="form-actions fluid">
                                        <div class="col-md-offset-3 col-md-6 col-sm-6">
                                          
                                            <button type="submit" onclick="changeStatus()" data-id="<?php echo $row['id'];?>" class="btn green status_checks <?php echo ($row['status'])? 'btn-success': 'btn-danger'?>"><?php echo ($row['status'])? 'Approve' : 'Reject'?>
                                            </button>
                                            <button type="submit" class="btn green" name="update" value="update">
                                                <?php echo lang('displayStudInfo_update');?></button>
                                          
                                           <button type="reset" onclick="javascript:history.back()" class="btn default"><?php echo lang('back'); ?></button>
                                            
                                             
                                        </div>
                                    </div>
                                             
                                             <?php  } ?>
                                    </form>

                            </div>
                        </div>
                    </div>
                </div>
                </div>
            
                <div class="tab-pane" id="tab_1_2">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="col-md-12">
                                <table class="table table-bordered" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Date & Time</th>
                                            <th>Module</th>                                
                                            <th>Student Name</th>                          
                                            <th>By Whom</th>
                                            <th>Action Description</th>
                                        </tr>
                                        <tbody>
                                            <?php $i=1; foreach($student_history as $row) { ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $row['created_at']; ?></td>
                                                <td><?php echo "Display Students Module";?></td>
                                                <td><?php echo  $row['students_name']; ?></td>
                                                <td><?php echo  $row['by_whom']; ?></td>
                                                <td><?php echo  $row['description']; ?> Has been updated</td>

                                                <!-- <span id="dots">...</span>
                                                <p><span id="more" style="display: none;"> 
                                                    Has been updated
                                                </span></p>
                                            <button class="btn btn-xs green" onclick="myFunction()" id="myBtn">Read more</button>
                                                </td> -->
                                            </tr>
                                             
                                             <?php $i++;} ?>

                                             
                                             
                                        </tbody>
                                    </thead>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script type="text/javascript">


function changeStatus()
{
    $(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '0' : '1';
      var msg = (status=='0')? 'Reject' : 'Approve';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this);
        var dataId = $(this).attr("data-id");       
        url = "<?php echo base_url().'crm/update'?>";
       
        $.ajax({
          type:"POST",
          url: url,
          data: {id:dataId,status:status},
         
          success: function(response)
          {   
            location.reload();
           //console.log(response);
          }
        });
      }      
    });
}

function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more";
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less";
    moreText.style.display = "inline";
  }
}
</script>
