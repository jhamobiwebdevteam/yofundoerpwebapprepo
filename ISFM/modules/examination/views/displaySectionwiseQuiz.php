<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>

<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        
        
        <div class="row">
            <?php echo "test2"; //print_r($data); die('--');?>
            <div class="col-md-12">
                <div class="portlet box green row">
                    <div class="portlet-title">
                        <div class="caption col-md-10">
                            <?php echo $coursename; ?>
                        </div>  
                    </div>
                    <div class="portlet-body">
                        <table class="table" id="sample_1">
                            <thead>
                                <tr>
                                    <?php 
                                    for($i=0; $i< count($headings); $i++) { ?>
                                    <th><?php echo $headings[$i]; ?></th>
                                    <?php  } ?>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>              
                                         <?php for($i=0; $i< count($total); $i++) { ?>
                                            <td><?php echo $total[$headings[$i]] ; ?> </td>
                                            <?php } ?>
                                    </tr>
                                    
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        

<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT -->


        
    </div>
</div>

<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->


<script type="text/javascript">
    
    $('#myModal').on('show.bs.modal', function (e) {
        var mystr = $(e.relatedTarget).data('id');
        var myarr = mystr.split(",");
        $('#titl').empty();
        $("#myTable").empty();
        $.ajax({
            type : 'POST',
            url : '<?php echo base_url().'crm/fetch_record' ?>', //Here you will fetch records 
            data :  'mystr='+ mystr, //Pass $id
            dataType: "json",
            success : function(data){
                $('#titl').append(data.data[0].fullname);
                $.each(data.data, function(key, value) {                    
                    $('#myTable').append('<tr><td>' + value.itemname+ '</td><td>' + value.itemmodule + '</td><td>' + Math.floor(value.marks) + '</td><td>' +  Math.floor(value.outof) + '</td></tr>');     
                });
            }
        });
     });
</script>