<!-- BEGIN PAGE LEVEL STYLES -->
<style>
th {
    color: #00bcd4;
}
.ttl {
    font-weight: 900;
    font-size: 15px;
}
</style>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('duesRpt'); ?>
                        </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body"><div class="ttl">Term 1</div>
                        <table id="sample_1" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>FEE TYPE</th>
                                    <th>EXPECTED AMOUNT</th>
                                    <th>PAID FEE AMOUNT </th>
                                    <th>DUE AMOUNT</th>                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php //foreach ($slips as $row) {  ?>
                                    <tr>
                                        <td>TUTION FEE</td>
                                        <td>1000</td>
                                        <td>1000</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>TRANSPORT FEE</td>
                                        <td>2000</td>
                                        <td>2000 </td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td><b>TOTAL</b></td>
                                        <td></td>
                                        <td> </td>
                                        <td><b>0</b></td>
                                    </tr>
                                <?php //} ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="portlet-body"><div class="ttl">Term 2</div>
                        <table id="sample_1" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>FEE TYPE</th>
                                    <th>EXPECTED AMOUNT</th>
                                    <th>PAID FEE AMOUNT </th>
                                    <th>DUE AMOUNT</th>                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php //foreach ($slips as $row) {  ?>
                                    <tr>
                                        <td>TUTION FEE</td>
                                        <td>1000</td>
                                        <td>1000</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>TRANSPORT FEE</td>
                                        <td>2000</td>
                                        <td>2000 </td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td><b>TOTAL</b></td>
                                        <td></td>
                                        <td> </td>
                                        <td><b>0</b></td>
                                    </tr>
                                <?php //} ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="portlet-body"><div class="ttl">Yearly Fees</div>
                        <table id="sample_1" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>FEE TYPE</th>
                                    <th>EXPECTED AMOUNT</th>
                                    <th>PAID FEE AMOUNT </th>
                                    <th>DUE AMOUNT</th>                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php //foreach ($slips as $row) {  ?>
                                    <tr>
                                        <td>School Charges</td>
                                        <td>5000</td>
                                        <td>5000</td>
                                        <td>0</td>
                                    </tr>
                                <?php //} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script>
    jQuery(document).ready(function() {
        //here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>