<!--Start page level style-->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!--Start page level style-->
<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- <div class="row">
            <div class="col-md-12">
                
                <h3 class="page-title">
                    <?php echo lang('header_feeScheme'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_fees'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_feeScheme'); ?>
                    </li>
                   
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12 ">
                <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('fee_info_heading'); ?>
                        </div>
                        
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable tabbable-custom tabbable-full-width">
                            <ul class="nav nav-tabs">
                                <li class="active" id='tab1'>
                                    <a class="nav-link active" data-toggle="tab" href="#tabs-1">Basic Info</a>
                                </li>
                                <li class="disabled" id='tab2' style="pointer-events: none;cursor: default;opacity 0.6;">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-2">Fees Payment Schedule</a>
                                </li>
                            
                            </ul><!-- Tab panes -->
                         <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form', 'method'=>'post');
                        echo form_open_multipart('account/fee_scheme', $form_attributs);
                        ?>
                        <div class="form-body">
                            <div class="tab-content" style="margin:20px">
                                <div class="tab-pane active" id="tabs-1">

                                    <?php
                                    if (!empty($success)) {
                                        echo $success;
                                    }
                                    ?>

                                    <div class="form-group col-md-12">
                                        <label class="col-md-2 control-label"><?php echo lang('header_sname');?> <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                        <!-- <input type="text" class="form-control" name="school_name"> -->                                    
                                            <select class="form-control" name="institute_id" id="institute_id" required>
                                                <option value="">Select Institute Name</option>
                                                <?php foreach($data['data'] as $key) { ?>
                                                    <option value="<?php echo $key['id'];?>"><?php echo $key['name'];?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label class="col-md-2 control-label"><?php echo lang('admi_dep_list'); ?> <span class="requiredStar"> *</span></label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="dep_id" id="dep_id" required>
                                            
                                            </select>
                                            <input type="hidden" name="dep_name" id="dep_name">
                                        </div>
                                    </div>
                                    
                                    <!-- <div class="form-group">
                                        <label class="col-md-2 control-label"> <?php echo lang('clas_class_title'); ?> <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" placeholder="<?php echo lang('clas_title_plash'); ?>" name="class_title" id="class_title" data-validation="required" data-validation-error-msg="<?php echo lang('clas_cls_tit_requi'); ?>" required>
                                        </div>
                                    </div> -->
                                    <div class="form-group  col-md-12">
                                        <label class="col-md-2 control-label"><?php echo lang('fee_category'); ?> 
                                        <span class="requiredStar" required> * </span></label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" placeholder="Fee Category Name eg:Admission, Registraion, Transportation" name="feeCategory" required="required"> -->
                                            <select class="form-control" name="feeCategory" id="feeCategory" required>
                                                <option><?php echo lang('stu_sel_cla_select');?></option>
                                                <?php foreach($feeCategory as $key) { ?>
                                                    <option value="<?php echo $key['id'];?>"><?php echo $key['fee_category'];?></option>
                                                <?php } ?>
                                            </select>
                                           
                                        </div>
                                    </div>

                                    <div class="form-actions fluid">
                                        <div class="col-md-offset-3 col-md-6">                                             
                                            <a  href="#tabs-2" data-toggle="tab" class="btn default" id="next">Next</a>         
                                        </div>
                                    </div>
                                </div>


                                <div class="tab-pane" id="tabs-2">
                                    
                                    
                                    <!-- <div class="form-group">
                                        <label class="col-md-3 control-label">Is fees on Installment basis?</label>

                                        <div class="col-md-1">
                                            <input type="checkbox" class="form-control feesYearly"  name="yesYearly" value="yesFeeYearly" onclick="yearlyFees()">
                                        </div>
                                        <label class="col-md-1 control-label">Yes</label>
                                        
                                        <div class="col-md-1">
                                            <input type="checkbox" class="form-control no_checkbox"  name="yesMonthly" value="noYearly" onclick="noYearlyFees()">                                                           
                                        </div>
                                        <label class="col-md-1 control-label">No</label>
                                    </div> -->

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Total Amount
                                        <span class="requiredStar"> *</span></label>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="totAmt" id="totAmt" required>
                                        </div>                                      
                                    </div>

                                    <!-- <div class="form-group hidediv_ofMonths" style="display:none;">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Jan </label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="month" value="jan" onclick="selectmonth()" id="month">                                
                                            </div>
                                            <label class="col-md-2 control-label">Feb </label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="month" value="feb" onclick="selectmonth()" id="month">                                
                                            </div>
                                            <label class="col-md-2 control-label">Mar </label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="month" value="mar" onclick="selectmonth()" id="month">                                
                                            </div>
                                            <label class="col-md-2 control-label">Apr </label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="apr" value="apr" onclick="selectmonth()" id="month">                                
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            
                                            <label class="col-md-2 control-label">May </label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="may" value="may" onclick="selectmonth()">                                
                                            </div>
                                            <label class="col-md-2 control-label">June </label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="june" value="june" onclick="selectmonth()">                                
                                            </div>
                                            <label class="col-md-2 control-label">July</label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="jule" value="jule" onclick="selectmonth()">                                
                                            </div>
                                            <label class="col-md-2 control-label">Aug</label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="aug" value="aug" onclick="selectmonth()">                                
                                            </div>
                                        </div>
                                    
                                        <div class="form-group">
                                             <label class="col-md-2 control-label">Sept</label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="sept" value="sept" onclick="selectmonth()">                                
                                            </div>
                                            <label class="col-md-2 control-label">Oct</label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="oct" value="oct" onclick="selectmonth()">                                
                                            </div>
                                            <label class="col-md-2 control-label">Nov</label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="nov" value="nov" onclick="selectmonth()">                                
                                            </div>
                                            <label class="col-md-2 control-label">Dec</label>
                                            <div class="col-md-1">
                                                <input type="checkbox" class="form-control month_checkbox"  name="dec" value="dec" onclick="selectmonth()">                                
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="portlet-body show_month_div" >
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                                
                                        </table>
                                    </div>


                                    <!-- <div class="form-group show_div" > -->
                                        <!-- <label class="col-md-2 control-label">Installment Start Date</label>
                                        <div class="col-md-4">
                                            <input type="date" class="form-control" name="installment_startdate" id="installment_startdate">
                                        </div>
                                    
                                        <label class="col-md-2 control-label">Installment End Date</label>
                                        <div class="col-md-4">
                                            <input type="date" class="form-control" name="installment_enddate" id="installment_enddate">
                                        </div> -->

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">No. of Installments<span class="requiredStar"> * </span></label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control inst_period" name="no_of_installment" id="installment_num" onkeyup="press()" required>                               
                                            </div>
                                       
                                        </div>
                                    <!-- </div> -->

                                    <div class="portlet-body show_installment_div" >
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                            
                                        </table>
                                        
                                       
                                    </div> 

                                    <div class="form-actions fluid">
                                        <div class="col-md-offset-3 col-md-6">
                                            <!-- <button type="" id="Button" class="btn green" name="submit" value="submit">Update</button> -->
                                            <button type="submit" class="btn green" name="submit" value="submit"><?php echo lang('save');?></button>     
                                            
                                            <a  href="#tabs-1" data-toggle="tab" id='back' class="btn default">Back</a>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <?php echo form_close(); ?>
                       
                    </div>
                </div>
            </div>
            </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    function yearlyFees()
    {
        var lumpsum = $('.feesYearly').is(":checked", true);
        if(lumpsum)
           
            $(".show_div").show();

    }

    function noYearlyFees()
    {
        var monthly = $('.no_checkbox').is(":checked", true);
        if(monthly)
            $(".hidediv_ofMonths").show();
            $(".show_div").hide();
    }

    function press()
    {
        //var start_date = $("#installment_startdate").val();
        //var end_date = $("#installment_enddate").val();        
       
        //var total2 = "";
        var table = $("#sample_1");
        var rowNum = parseInt($("#installment_num").val(), 10);
       
        var resultHtml = '';
                
       //today = dd + '/' + mm + '/' + yyyy;
        resultHtml += ["<tr>", 

        '<th><?php echo lang('fee_instal_head'); ?></th>',
        
        '<th>Installment Start Date</th>',
        '<th>Installment End Date</th>',
        '<th><?php echo lang('fee_amt'); ?></th>',

        '</tr>'].join("\n");


        
        for(var i = 0 ; i < rowNum ; i++) 
        {       
            if(i==0){
                var day_dt = "<input type='date' class='datetime' name='getinstmntodat_"+i+"' id='getinstmntodat_"+i+"' required onchange='strtDteVal("+i+")' min='<?php echo date("Y-m-d"); ?>'>";
            }else{                
                var day_dt = "<input type='date' class='datetime' name='getinstmntodat_"+i+"' id='getinstmntodat_"+i+"' required onchange='strtDteVal("+i+")'>";
            }

            var enddt = "<input type='date' class='datetime' name='enddate_"+i+"' id='enddate_"+i+"' required onchange='endDteVal("+i+")'>";
            $( "#getinstmntodat_" ).datepicker({ minDate: 0}); 
            resultHtml += ["<tr>", 
                 "<td>", 
                  (i+1),
                 "</td>",
                 
                
                    "<td name='today_dt_strt' id='today_dt_strt'>"+ day_dt + "</td>",
                    "<td name='today_dt_end' id='today_dt_end'>"+ enddt +"</td>",
                    "<td name='installment_amount' id='installment_amount' >"+"<input type='text' name='instalamt_"+i+"' id='instalamtconcat_"+i+"' onkeyup='getinstamount("+i+")' required>"+"</td>", 
                    
                 '</tr>'].join("\n");

           
        }  
         
       
        table.html(resultHtml);
        return false;

    }



    function getinstamount(text_id)
    { //debugger;
        var instalment_amount = 0;
        var res = 0;
        var totalamt = document.getElementById("totAmt").value;
        var instalnumber = document.getElementById("installment_num").value;

       for(var h=0;h<instalnumber;h++)
        {

             instalment_amount = document.getElementById("instalamtconcat_"+h).value;
             if(instalment_amount!='undefined'||instalment_amount!=undefined)
             {
                 res = Number(res) + Number(instalment_amount);
             }

             
        }
        
        if(res>totalamt)
        {
            alert("Installment Amount Should be less than total amount.");
        }
        
           
    }

    function selectmonth()
    {
        var monthlyFee = document.getElementById("totAmt").value;
        var mm = $('.month_checkbox:checked').size();
        
        var totamout = monthlyFee/mm;

        var table2 = $("#sample_2");
        var resultHtmlview = '';
                
       //today = dd + '/' + mm + '/' + yyyy;
        resultHtmlview += ["<tr>", 

        '<th>Srno</th>',
        '<th>Amount</th>',
        '<th>Date</th>',

        '</tr>'].join("\n");

        
        for(var j = 0 ; j < mm ; j++) 
        {            
            var dateofmonthly_installment = "<input type='date' class='datetime' name='tddate_"+j+"' id='tddate_"+j+"' onchange='getmonthlyInstalldt()'>";
            resultHtmlview += ["<tr>", 
                 "<td>", 
                  (j+1),
                 "</td>",
                 "<td name='monthly_inst_amount' id='monthly_inst_amount'>"+totamout+"</td>",
                 "<td name='monthly_installment_dt' id='monthly_installment_dt'>"+ dateofmonthly_installment + "</td>",
                   
                 '</tr>'].join("\n");
        }

        table2.html(resultHtmlview);
        return false;
        

    }

    function getmonthlyInstalldt()
    {
        var modt = document.getElementById("tddate").value;
        //console.log(modt);

    }

     
   $('select[name="institute_id"]').on('change', function() {
        var inst_id = $(this).val();  
        $.ajax({
            url: '<?php echo base_url().'account/getdep'?>',
            data: {'inst_id': inst_id },
            type: "POST",
            dataType: "json",
            success:function(data) 
            {
              //console.log(data.data);
              // var objJSON = JSON.parse(data);
              //   console.log(objJSON);
                $('select[name="dep_id"]').empty();
                $('select[name="dep_id"]').append('<option>Select Department</option>');
                $.each(data.data, function(key, value) {
                    $('select[name="dep_id"]').append('<option value="'+value.id+'">'+ value.name +'</option>');
                    $('#dep_name').val(value.name);
                });                 
            }
        });
    });

    $('#next').on('click', function() {
        $('#tab1').removeClass('active');
        $('#tab2').addClass('active');       
    });
    $('#back').on('click', function() {
        $('#tab2').removeClass('active');
        $('#tab1').addClass('active');       
    });

    function strtDteVal(a){
        var b = $("#getinstmntodat_"+a).val();
        $('#enddate_'+a).val('');
        $('#enddate_'+a).attr('min', b);
        
    }
    function endDteVal(a){
        var b = $("#enddate_"+a).val();
        var c = a+1;
        $('#getinstmntodat_'+c).val('');
        $('#getinstmntodat_'+c).attr('min', b);
        // alert(b+'---'+c);
    }
	
</script>
