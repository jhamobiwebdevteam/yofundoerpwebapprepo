<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('edt_institute_title'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        $id = $this->input->get('id');
                        echo form_open("sclass/editDepartment?id=$id", $form_attributs);
                        ?>
                        <?php foreach ($departmentInfo as $row) { ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('clas_class_title'); ?></label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="dept_name" value="<?php echo $row['name']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('clas_code'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="class_code" value="<?php echo $row['idnumber']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('hod_phone'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="hod_phone" value="<?php echo $row['hod_phone']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('hod_email'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="email" placeholder="" name="hod_email" value="<?php echo $row['hod_email']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('hod_landline'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="hod_landline" value="<?php echo $row['hod_landline']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('faculty_cnt'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="total_faculty" value="<?php echo $row['total_faculty']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('std_cnt'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="student_amount" value="<?php echo $row['student_amount']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('ict_labs'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="ict_labs" value="<?php echo $row['ict_labs']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('labs'); ?> </label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" placeholder="" name="labs" value="<?php echo $row['labs']; ?>">
                                    </div>
                                </div>
                                

                            </div>
                        <?php } ?>
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" class="btn green" name="submit" value="Update"><?php echo lang('save'); ?></button>
                                <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>


        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script>
    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
