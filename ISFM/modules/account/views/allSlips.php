<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<style>
    .pd{
        padding: 3px 3px;
    }
    .pds{
        padding: 3px 7px;
    }
    .act{
        width: 90px;
    }
</style>
<!-- END PAGE LEVEL STYLES -->

<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('acc_stsl'); ?>
                        </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="sample_1" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>
                                        Sr. No
                                    </th>
                                    <th>
                                        Receipt No
                                    </th>
                                    <th>
                                        Student ID
                                    </th>
                                    <th>
                                        Student Name
                                    </th>
                                    <th>
                                        Department
                                    </th> 
                                    <th>
                                        Payment Date
                                    </th>
                                    <th>
                                        Payment Mode
                                    </th>
                                    <th>
                                        Cheque No.
                                    </th>
                                    <th>
                                        Amount
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th class ="act">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($slips as $row) {  ?>
                                    <tr>
                                        <td>
                                            <?php echo $row['id']; ?>
                                        </td>
                                        <td>
                                            <?php echo '123456'; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['student_id']; ?>
                                        </td>
                                        <td>
                                            <?php $studInfo = $this->accountmodel->get_stud_details_by_id($row['student_id']);
                                            if($studInfo){
                                                echo $studInfo[0]['first_name'].' '.$studInfo[0]['middle_name'].' '.$studInfo[0]['last_name'];
                                            } ?>
                                        </td>
                                        <td>
                                            <?php  if($studInfo){ echo $studInfo[0]['name']; } ?>
                                        </td>
                                        <td>
                                            <?php echo $row['created_at']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['payment_mode']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['cheque_no']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['amount']; ?>
                                        </td>
                                        <td>
                                            <?php
                                            if($row['online_payment_status']=='failure'){
                                                echo '<span class="label label-sm label-danger">'. $row['online_payment_status'] .'</span>';
                                            } elseif ($row['online_payment_status']=='processing'){
                                                echo '<span class="label label-sm label-warning">'. $row['online_payment_status'] .'</span>';
                                            } else {
                                                echo '<span class="label label-sm label-success">'. $row['online_payment_status'] .'</span>';
                                            }?>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                    <button class="btn default pd" type="button">Action</button>
                                                    <button data-toggle="dropdown" class="btn default dropdown-toggle pds" type="button"><i class="fa fa-angle-down"></i></button>
                                                    <ul role="menu" class="dropdown-menu ac_bo_sh">
                                                        <?php //if($row['status']=='Unpaid'||$row['status']=='Not Clear'){ ?>
                                                            <!-- <li class="ac_in_sty">
                                                                <a href="index.php/account/fee_pay?sid=<?php echo $row['id']; ?>"> Take Payment </a>
                                                            </li> -->
                                                        <?php //}?>
                                                        <li class="ac_in_sty">
                                                            <a href="index.php/account/view_invoice?sid=<?php echo $row['id']; ?>"> Payment receipts </a>
                                                        </li>
                                                        <!-- <li class="ac_in_sty">
                                                            <a href="index.php/account/edit_fee_pay?sid=<?php echo $row['id']; ?>"> Edit </a>
                                                        </li>
                                                        <li class="ac_in_sty">
                                                            <a href="#"> Delete</a>
                                                        </li> -->
                                                    </ul>
                                                </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script>
    jQuery(document).ready(function() {
        //here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>