<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- <div class="row">
            <div class="col-md-12">
               
                <h3 class="page-title">
                    <?php echo lang('header_register_stu'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_crm'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_register_stu'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>

                </ul>
                
            </div>
        </div> -->
        <!-- END PAGE HEADER-->
        
        
        <div class="row">
            <div class="col-md-12">
                 <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('header_register_stu'); ?>
                        </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        
                        <table class="table table-bordered" id="sample_1">
                        <div class="row smsemail">
                            <div class="col-md-6 margin-bottom-10">
                                <button type="submit" id="sms" class="btn red" name="submit" value="submit"><?php echo lang('sms'); ?></button> OR
                                <button type="submit" id="email" class="btn red"><?php echo lang('eml'); ?></button>
                            </div>
                        </div>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>
                                        <?php echo lang('register_idno'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('admi_dep_list'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('admi_academic_yr'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('register_fn'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('register_ln'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('register_mob'); ?>
                                    </th>
                                    <th>
                                       <?php echo lang('register_eml'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_dob'); ?> 
                                    </th>
                                    <th>
                                        <?php echo lang('register_nationality'); ?>
                                    </th>
                                    <th>
                                       <?php echo lang('register_gender'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_caste'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_religion'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_pic'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_birthplce'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_add'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_pin'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_fathersname'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_fathersocc'); ?> 
                                    </th>

                                    <th>
                                       <?php echo lang('register_mothersocc'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_mothername'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_mothersqualif'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_fathersqualif'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_mothertoung'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_annualIncome'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_medicalcond'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_treatment'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_is_medication_going'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_foodallergy'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_fitnesscert'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_adoptiondoc'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_passportphoto'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_parent_passport'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_birthcert'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_pancard'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_aadharcard'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_castecert'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_bonafide'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_tc'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_residencepro'); ?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_foreign_national');?> 
                                    </th>
                                    <th>
                                       <?php echo lang('register_reportcard'); ?> 
                                    </th>
                                    <th>
                                        <?php echo lang('rgister_action');?>
                                    </th>
                                    <th>
                                        <?php echo lang('register_approve');?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($student_info as $row) { ?>
                                    <tr style="
                                        <?php if(($row['cdate'] < $row['fdate']) && ($row['stage'] != 'enquiry')){?>
                                        background: orange; <?php 
                                        }if((($row['cdate'] < $row['fdate']) && ($row['fdate'] < date('Y-m-d h:i:s')) && ($row['stage'] != 'enquiry')) || (($row['cdate'] > $row['fdate']) && ($row['fdate'] < date('Y-m-d h:i:s')) && ($row['stage'] != 'enquiry'))){?>
                                        background: #ff9898; <?php 
                                        }else if(($row['cdate'] > $row['fdate'] && ($row['stage'] != 'enquiry'))){?>
                                        background: #35aa47; <?php 
                                        }?>">
                                        <td><input type="checkbox" class="chk" name="chk" value="<?php echo $row['fathers_mobile'];?>-<?php echo $row['email']; ?>"></td>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                            <?php $data = $this->common->getSingleField('name','department','lms_id',$row['dept_id']); 
                                            if($data){echo $data[0]->name;}  ?>
                                        </td>
                                        <td>
                                        <?php $data = $this->common->getSingleField('name','department','lms_id',$row['year_id']); 
                                            if($data){echo $data[0]->name;}  ?>
                                        </td>
                                        <td>
                                            <?php echo $row['first_name']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['last_name']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['fathers_mobile']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['email']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['dob']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['nationality']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['gender']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['caste']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['religion']; ?>
                                        </td>
                                        <td>
                                            <div class="tableImage">
                                                <img src="assets/uploads/registration/<?php echo $row['student_photo']; ?>" alt="">
                                            </div>                                            
                                        </td>
                                        <td>
                                            <?php echo $row['birth_place']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['address']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['pin_code']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['fathers_firstname']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['fathers_occup']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['mothers_occup']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['mothers_firstname'];?>
                                        </td>
                                        <td>
                                            <?php echo $row['mothers_qualification']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['fathers_qualification']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['mother_toung']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['anuual_income']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['isthr_medical_condition']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['isthr_undergoing_treatment']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['is_child_taking_medication']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['food_allergy']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['medical_fitness_cert']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['adoption_document']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['passport_photos']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['passport_pic_both_parent']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['original_birth_cert']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['pan_card_copy_parent']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['adhar_copy_parent']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['caste_certificate']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['original_bonafied']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['original_tc']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['residence_proof']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['if_foreign_nationals_copy']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['copy_report_card']; ?>
                                        </td>

                                        <!-- <td>
                                            <a class="btn btn-xs green" href="index.php/crm/leadDetails?id=<?php //echo $row['id']; ?>&stage=<?php //echo $row['lead_stage']; ?>"> <i class="fa fa-file-text-o"></i> <?php //echo lang('details'); ?> </a>
                                            <a class="btn btn-xs default" href="index.php/crm/editLead?id=<?php //echo $row['id']; ?>"> <i class="fa fa-pencil-square"></i> <?php //echo lang('edit'); ?> </a>
                                            <a class="btn btn-xs red" href="index.php/crm/teacherDelete?id=<?php //echo $row['id']; ?>&uid=<?php //echo $row['user_id']; ?>"  onClick="javascript:return confirm('<?php //echo lang('tea_tdecon'); ?>')"> <i class="fa fa-trash-o"></i> <?php // echo lang('delete'); ?> </a>
                                        
                                        </td> -->
                                        <td>
                                            
                                            <a href="index.php/crm/getStudentsDetail?id=<?php echo $row['id']; ?>" class="btn btn-xs green viewdetail"><i class="fa fa-pencil-square"></i>
                                             Details</a>
                                             <a class="btn btn-xs red" href="index.php/crm/StudeInfoDelete?id=<?php echo $row['id']; ?>"  onClick="javascript:return confirm('<?php echo lang('tea_tdecon'); ?>')"> <i class="fa fa-trash-o"></i> <?php echo lang('delete'); ?> </a>
                                            
                                        </td>
                                        <td>
                                            <i data-id="<?php echo $row['id'];?>" class="status_checks btn-xs <?php echo ($row['status'])? 'btn-success': 'btn-danger'?>"><?php echo ($row['status'])? 'Approve' : 'Reject'?>
                                            </i>
                                        </td>
                                    </tr>
                                <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>


<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script type="text/javascript">

    $(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '0' : '1';
      var msg = (status=='0')? 'Reject' : 'Approve';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this);
        var dataId = $(this).attr("data-id");       
        url = "<?php echo base_url().'crm/update'?>";
       
        $.ajax({
          type:"POST",
          url: url,
          data: {id:dataId,status:status},
         
          success: function(response)
          {   
            location.reload();
           //console.log(response);
          }
        });
      }      
    });
   
      /*$(document).on("click", ".viewdetail", function () {
          var registered_id = $(this).data('id');
          //alert(lastid);
          var url = '<?php echo base_url();?>crm/getStudentsDetail'; 
          $.ajax({
                url: url,
                data: {'registered_id': registered_id },
                type: "POST",
                dataType: "json",
                success:function(data) {                 
                  //response=JSON.parse(data);
                  //var json = JSON.stringify(data); 
                  $('#ed_id').val(data[0]['id']);
                  $('#academic_year').val(data[0]['academic_year']);
                  $('#first_name').val(data[0]['first_name']);
                  $('#mid_name').val(data[0]['middle_name']);
                  $('#last_name').val(data[0]['last_name']);                  
                  $('#dob').val(data[0]['dob']);
                  $('#email').val(data[0]['email']);
                  $('#nationality').val(data[0]['nationality']);
                  $('#gender').val(data[0]['gender']);
                  $('#caste_cat').val(data[0]['caste']);
                  $('#religion').val(data[0]['religion']);
                  $('#birth_place').val(data[0]['birth_place']);
                  $('#birth_place').val(data[0]['address']);
                  $('#birth_place').val(data[0]['pin_code']);
                  $('#fathers_firstname').val(data[0]['fathers_firstname']);
                  $('#fathers_midname').val(data[0]['fathers_midname']);
                  $('#fathers_lastname').val(data[0]['fathers_lastname']);
                  $('#fathers_occup').val(data[0]['fathers_occup']);
                  $('#mothers_occup').val(data[0]['mothers_occup']);
                  $('#mothers_firstname').val(data[0]['mothers_firstname']);

                  $('#exampleModal').modal('show');
                  }
          
      });
        });*/
     

    jQuery(document).ready(function() {
        //here is auto reload after 1 second for time and date in the top
        // jQuery(setInterval(function() {
        //     jQuery("#result").load("index.php/home/iceTime");
        // }, 1000));
        
        jQuery('.chk').click(function(){
            if (jQuery('input[name="chk"]:checked').length > 0) {
                jQuery(".smsemail").css("display", "block");
            } else {
                jQuery(".smsemail").css("display", "none");
            }
        });
        jQuery("#sms").click(function(event){
            event.preventDefault();
            var searchIDs = jQuery("input:checkbox:checked").map(function(){
                var id = $(this).val();
                var arr = id.split('-');
            return arr[0];
            }).get(); 
            console.log(searchIDs);
        });
    });
</script>