
<!DOCTYPE html>
<html lang="en" >
    <head>
        <style>
            .form-row>[class*=col-] {
                padding-right: 15px;
                padding-left: 5px;
                margin-left: 20px;
            }
            .margin-15{
                margin: 15px 0px 0px;
            }
            .col-md-5.mb-5 {
                margin-bottom: 0px !important;
            }
        </style>
        <link href="<?php echo base_url();?>assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    </head>
<body>
    <h2><center>Registration Form</center></h1>
    <div class="container content2">
        <form class="needs-validation" novalidate>
            <div class="form-row"><h6>For Admission form Checking Committee</h6></div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Fee Structure applicable of the year</label>
                    <input type="text" class="form-control" name="year" placeholder="Year" required>
                </div>
                <div class="col-md-3 mb-3">
                    <div class="form-row"><label for="validationCustom02">Admission Type:</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="admType" value="grant" required>
                        <label class="form-check-label" for="inlineRadio1">Grant</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="admType"value="nongrant">
                        <label class="form-check-label" for="inlineRadio2">Non Grant</label>
                    </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom03">Class</label>
                    <input type="text" class="form-control" name="class" placeholder="Class" required>
                </div>
                <div class="col-md-2 mb-2">
                    <label for="validationCustom05">Division</label>
                    <input type="text" class="form-control" name="division" placeholder="Division" required>
                </div>                
            </div>

            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom04">Faculty</label>
                    <input type="text" class="form-control" name="faculty" placeholder="Faculty" required>
                </div>                
                <div class="col-md-3 mb-3">
                    <label for="validationCustom06">Roll No</label>
                    <input type="text" class="form-control" name="rollno" placeholder="Roll No" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom07">Merit Form Number:</label>
                    <input type="text" class="form-control" name="meritnumber" placeholder="Merit Form Number:" required>
                </div>
                <div class="col-md-2 mb-2">
                    <label for="validationCustom08">Merit Number:</label>
                    <input type="text" class="form-control" name="meritnumber:" placeholder="Merit Number" required>
                </div>
            </div>
            <div class="form-row ">
                <div class="col-md-12 mb-12">
                    <div class="form-row"><label for="validationCustom04">Document Enclosed</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Mark sheet">
                        <label class="form-check-label">Mark sheet</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="LC/TC">
                        <label class="form-check-label">LC/TC</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Caste Cert">
                        <label class="form-check-label">Caste Cert.</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Eligibility Form">
                        <label class="form-check-label">Eligibility Form</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Bonafide">
                        <label class="form-check-label">Bonafide</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Passing Cert">
                        <label class="form-check-label">Passing Cert.</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Scholarship/Fee Concession Form">
                        <label class="form-check-label">Scholarship/Fee Concession Form</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Migration Cert.">
                        <label class="form-check-label">Migration Cert.</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Nationality/ Domicile Cert.">
                        <label class="form-check-label">Nationality/ Domicile Cert.</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="No Objection Cert.">
                        <label class="form-check-label">No Objection Cert.</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Gap Cert.">
                        <label class="form-check-label">Gap Cert.</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Affidavit">
                        <label class="form-check-label">Affidavit</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Cert. In Performa A to E">
                        <label class="form-check-label">Cert. In Performa A to E</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Cast/Tribe Validity Cert/Form H">
                        <label class="form-check-label">Cast/Tribe Validity Cert/Form H</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Income Certificate">
                        <label class="form-check-label">Income Certificate</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Non Creamy layer cert">
                        <label class="form-check-label">Non Creamy layer cert.</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Birth Cert">
                        <label class="form-check-label">Birth Cert.</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Allotment letter">
                        <label class="form-check-label">Allotment letter</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="MHT-CET Score card">
                        <label class="form-check-label">MHT-CET Score card</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="AIEE Score Card">
                        <label class="form-check-label">AIEE Score Card</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Sport Certificate">
                        <label class="form-check-label">Sport Certificate</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Medical Fitness Cert.">
                        <label class="form-check-label">Medical Fitness Cert.</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="docEnclosed[]" value="Eligibility Form">
                        <label class="form-check-label">Eligibility Form</label>
                    </div>
                </div>      
            </div>
            <div class="form-row margin-15">
                <div class="col-md-12 mb-12">
                    <div class="form-row"><label for="validationCustom04">Fee Category</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="Paying">
                        <label class="form-check-label">Paying</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="EBC">
                        <label class="form-check-label">EBC</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="FEG">
                        <label class="form-check-label">FEG</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="BCS">
                        <label class="form-check-label">BCS</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="NB">
                        <label class="form-check-label">NB</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="FFW">
                        <label class="form-check-label">FFW</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="BC">
                        <label class="form-check-label">BC</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="Reserve">
                        <label class="form-check-label">Reserve</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="BCF">
                        <label class="form-check-label">BCF</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="PTW">
                        <label class="form-check-label">PTW</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="STW">
                        <label class="form-check-label">STW</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="Ex-Ser">
                        <label class="form-check-label">Ex-Ser</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="MSW">
                        <label class="form-check-label">MSW</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="SBC">
                        <label class="form-check-label">SBC</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="Special">
                        <label class="form-check-label">Special</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="feeCategory[]" value="Other">
                        <label class="form-check-label">Other</label>
                    </div>
                </div>      
            </div>
            <div class="form-row margin-15">
                <div class="col-md-12 mb-12">
                    <div class="form-row"><label for="validationCustom04">Caste Category</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory" value="Open">
                        <label class="form-check-label">Open</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory" value="SC">
                        <label class="form-check-label">SC</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory" value="ST">
                        <label class="form-check-label">ST</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory" value="DT (A)">
                        <label class="form-check-label">DT (A)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory" value="NT (B)">
                        <label class="form-check-label">NT (B)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory" value="NT (C)">
                        <label class="form-check-label">NT (C)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory" value="NT (D)">
                        <label class="form-check-label">NT (D)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory" value="OBC">
                        <label class="form-check-label">OBC</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory" value="SBC">
                        <label class="form-check-label">SBC</label>
                    </div>
                </div>      
            </div>
            <hr>
            <div class="form-row"><h6>For Office use only</h6></div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label >Admission Receipt No.</label>
                    <input type="text" class="form-control" name="admrecptno" placeholder="Admission Receipt No." required>
                </div>
                <div class="col-md-3 mb-3">
                    <label>Receipt date</label>
                    <input type="text" class="form-control" name="recptdate" placeholder="Receipt date" required>
                </div>
                <div class="col-md-4 mb-4">
                    <label >Bank name</label>
                    <input type="text" class="form-control" name="bankname" placeholder="Bank name" required>
                </div>                
            </div>
            <div class="form-row">            
                <div class="col-md-3 mb-3">
                    <label>DD No.</label>
                    <input type="text" class="form-control" name="ddno" placeholder="DD No." required>
                </div>   
                <div class="col-md-3 mb-3">
                    <label>DD Amount</label>
                    <input type="text" class="form-control" name="ddamount" placeholder="DD Amount" required>
                </div>                 
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label>Other Receipt No.</label>
                    <input type="text" class="form-control" name="otherrecptno" placeholder="Other Receipt No." required>
                </div>
                <div class="col-md-3 mb-3">
                    <label>Other Receipt date</label>
                    <input type="text" class="form-control" name="otherrecptdate" placeholder="Other Receipt date" required>
                </div>
                <div class="col-md-4 mb-4">
                    <label>Bank name</label>
                    <input type="text" class="form-control" name="bankname1" placeholder="Bank name" required>
                </div>                
            </div>            
            <div class="form-row">            
                <div class="col-md-3 mb-3">
                    <label>DD No.</label>
                    <input type="text" class="form-control" name="ddno1" placeholder="DD No." required>
                </div>   
                <div class="col-md-3 mb-3">
                    <label>DD Amount</label>
                    <input type="text" class="form-control" name="ddamount1" placeholder="DD Amount" required>
                </div>                 
            </div>
            <hr>

            <div class="form-row"><h6>Student Information</h6></div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label >Surname</label>
                    <input type="text" class="form-control" name="surname" placeholder="Surname" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Name" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label >Father/Husband</label>
                    <input type="text" class="form-control" name="father" placeholder="Father/Husband" required>
                </div>                
            </div>
            <div class="form-row">            
                <div class="col-md-3 mb-3">
                    <label>College PRN No./Member ID</label>
                    <input type="text" class="form-control" name="clgprn" placeholder="College PRN No./Member ID" required>
                </div>   
                <div class="col-md-3 mb-3">
                    <label>University/Board PRN</label>
                    <input type="text" class="form-control" name="boardprn" placeholder="University/Board PRN" required>
                </div>  
                <div class="col-md-3 mb-3">
                    <div class="form-row"><label for="validationCustom02">Gender</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" value="Male" required>
                        <label class="form-check-label" for="inlineRadio1">Male</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" value="Female">
                        <label class="form-check-label" for="inlineRadio2">Female</label>
                    </div>
                </div>               
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label>Residence address</label>
                    <textarea class="form-control" name="address" rows="3"></textarea>
                </div>  
                <div class="col-md-3 mb-3">
                    <label >Village</label>
                    <input type="text" class="form-control" name="village" placeholder="Village" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label >Taluka</label>
                    <input type="text" class="form-control" name="taluka" placeholder="Taluka" required>
                </div>     
            </div>  
            <div class="form-row">
                <div class="col-md-2 mb-2">
                    <label >District</label>
                    <input type="text" class="form-control" name="district" placeholder="District" required>
                </div>
                <div class="col-md-2 mb-2">
                    <label>State</label>
                    <input type="text" class="form-control" name="state" placeholder="State" required>
                </div>
                <div class="col-md-2 mb-2">
                    <label >Pin code</label>
                    <input type="text" class="form-control" name="pincode" placeholder="Pin code" required>
                </div>                
                <div class="col-md-2 mb-2">
                    <label >Fax</label>
                    <input type="text" class="form-control" name="fax" placeholder="Fax" required>
                </div>                
                <div class="col-md-2 mb-2">
                    <label >Personal Cell</label>
                    <input type="text" class="form-control" name="personalcell" placeholder="Personal Cell" required>
                </div>                
            </div> 
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label >Parent’s Cell</label>
                    <input type="text" class="form-control" name="parentscell" placeholder="Parent’s Cell" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label>Email Id</label>
                    <input type="text" class="form-control" name="email" placeholder="Email Id" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label >Date of Birth</label>
                    <input type="text" class="form-control" name="dob" placeholder="Date of Birth" required>
                </div>                
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label >Birth Place</label>
                    <input type="text" class="form-control" name="birthplace" placeholder="Birth Place" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label>Nationality</label>
                    <input type="text" class="form-control" name="nationality" placeholder="Nationality" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label >Mother Tongue</label>
                    <input type="text" class="form-control" name="mothertongue" placeholder="Mother Tongue" required>
                </div>                
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <div class="form-row"><label>Marital Status</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="maritalstatus" value="Married" required>
                        <label class="form-check-label">Married</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="maritalstatus"value="Unmarried">
                        <label class="form-check-label">Unmarried</label>
                    </div>
                </div>
                <div class="col-md-3 mb-3">
                    <div class="form-row"><label>Occupational Status</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="occupationalstatus" value="Employed" required>
                        <label class="form-check-label" for="inlineRadio1">Employed</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="occupationalstatus"value="unemployed">
                        <label class="form-check-label" for="inlineRadio2">Unemployed</label>
                    </div>
                </div> 
                <div class="col-md-3 mb-3">
                    <label>Blood group</label>
                    <input type="text" class="form-control" name="bloodgroup" placeholder="Blood group" required>
                </div>                
            </div>
            <div class="form-row">
                <div class="col-md-2 mb-2">
                    <div class="form-row"><label>Handicap</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="handicap" value="Yes" required>
                        <label class="form-check-label">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="handicap"value="No">
                        <label class="form-check-label">No</label>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <label>(If Yes, details of nature of handicap)</label>
                    <input type="text" class="form-control" name="handicapreason" placeholder="Handicap Reason" required>
                </div>                              
            </div>
            <div class="form-row">                
                <div class="col-md-5 mb-5">
                    <div class="form-row"><label>Conveyance use</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="conveyanceuse" value="Bicycle" required>
                        <label class="form-check-label">Bicycle</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="conveyanceuse"value="Bus">
                        <label class="form-check-label">Bus</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="conveyanceuse"value="Two Wheeler">
                        <label class="form-check-label">Two Wheeler</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="conveyanceuse"value="By walks">
                        <label class="form-check-label">By walks</label>
                    </div>
                </div> 
                <div class="col-md-2 mb-2">
                    <label>Other</label>
                    <input type="text" class="form-control" name="cnvther" placeholder="Other" required>
                </div>                             
            </div>

            <div class="form-row">                
                <div class="col-md-5 mb-5">
                    <div class="form-row"><label>Religion</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="religion" value="Hindu" required>
                        <label class="form-check-label">Hindu</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="religion" value="Muslim" required>
                        <label class="form-check-label">Muslim</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="religion" value="Sikh">
                        <label class="form-check-label">Sikh</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="religion" value="Christian">
                        <label class="form-check-label">Christian</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="religion" value="Jain">
                        <label class="form-check-label">Jain</label>
                    </div>
                </div> 
                <div class="col-md-2 mb-2">
                    <label>Other</label>
                    <input type="text" class="form-control" name="religionother" placeholder="Other" required>
                </div>                             
            </div>
            <div class="form-row">    
                <div class="col-md-2 mb-2">
                    <label>Caste</label>
                    <input type="text" class="form-control" name="Caste" placeholder="Caste" required>
                </div>  
                <div class="col-md-2 mb-2">
                    <label>Sub caste</label>
                    <input type="text" class="form-control" name="subcaste" placeholder="Sub caste" required>
                </div>              
                <div class="col-md-7 mb-7">
                    <div class="form-row"><label for="validationCustom04">Caste Category</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory1" value="Open">
                        <label class="form-check-label">Open</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory1" value="SC">
                        <label class="form-check-label">SC</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory1" value="ST">
                        <label class="form-check-label">ST</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory" value="DT (A)">
                        <label class="form-check-label">DT (A)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory1" value="NT (B)">
                        <label class="form-check-label">NT (B)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory1" value="NT (C)">
                        <label class="form-check-label">NT (C)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory1" value="NT (D)">
                        <label class="form-check-label">NT (D)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory1" value="OBC">
                        <label class="form-check-label">OBC</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="casteCategory1" value="SBC">
                        <label class="form-check-label">SBC</label>
                    </div>
                </div>                                             
            </div>
            <div class="form-row">    
                <div class="col-md-4 mb-">
                    <label>Specialization Subject</label>
                    <input type="text" class="form-control" name="splsubj" placeholder="Specialization Subject" required>
                </div>  
                <div class="col-md-4 mb-4">
                    <label>Qualification</label>
                    <input type="text" class="form-control" name="studqualification" placeholder="Qualification" required>
                </div>                                  
            </div>   
            <hr>

            <div class="form-row"><h6>Guardian Information</h6></div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label >Surname</label>
                    <input type="text" class="form-control" name="gsurname" placeholder="Surname" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label>Name</label>
                    <input type="text" class="form-control" name="gname" placeholder="Name" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label >Father/Husband</label>
                    <input type="text" class="form-control" name="gfather" placeholder="Father/Husband" required>
                </div>                
            </div>
            <div class="form-row">           
                <div class="col-md-6 mb-6">
                    <div class="form-row"><label>Relation with Guardian</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="grelation" value="Mother" required>
                        <label class="form-check-label">Mother</label>
                    </div>                    
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="grelation" value="Father" required>
                        <label class="form-check-label">Father</label>
                    </div>                    
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="grelation" value="Sister" required>
                        <label class="form-check-label" >Sister</label>
                    </div>                    
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="grelation" value="Brother" required>
                        <label class="form-check-label">Brother</label>
                    </div>                    
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="grelation" value="Aunty" required>
                        <label class="form-check-label" >Aunty</label>
                    </div>                    
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="grelation" value="Uncle" required>
                        <label class="form-check-label">Uncle</label>
                    </div>                    
                </div>  
                <div class="col-md-4 mb-4">
                    <label>Residence address</label>
                    <textarea class="form-control" name="gaddress" rows="2"></textarea>
                </div>             
            </div>
            <div class="form-row">                 
                <div class="col-md-3 mb-3">
                    <label >City</label>
                    <input type="text" class="form-control" name="gcity" placeholder="City" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label >Village</label>
                    <input type="text" class="form-control" name="gvillage" placeholder="Village" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label >Taluka</label>
                    <input type="text" class="form-control" name="gtaluka" placeholder="Taluka" required>
                </div>     
            </div>  
            <div class="form-row">
                <div class="col-md-2 mb-2">
                    <label >District</label>
                    <input type="text" class="form-control" name="gdistrict" placeholder="District" required>
                </div>
                <div class="col-md-2 mb-2">
                    <label>State</label>
                    <input type="text" class="form-control" name="gstate" placeholder="State" required>
                </div>
                <div class="col-md-2 mb-2">
                    <label >Pin code</label>
                    <input type="text" class="form-control" name="gpincode" placeholder="Pin code" required>
                </div>                
                <div class="col-md-2 mb-2">
                    <label >Phone</label>
                    <input type="text" class="form-control" name="gphone" placeholder="Phone" required>
                </div>                   
            </div> 
            <div class="form-row">           
                <div class="col-md-3 mb-3">
                    <label >Fax</label>
                    <input type="text" class="form-control" name="gfax" placeholder="Fax" required>
                </div>  
                <div class="col-md-3 mb-3">
                    <label >Cell</label>
                    <input type="text" class="form-control" name="gcell" placeholder="Cell" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label>Email Id</label>
                    <input type="text" class="form-control" name="gemail" placeholder="Email Id" required>
                </div>                              
            </div>
            
            <hr>

            <div class="form-row"><h6>Parent Information</h6></div>
            <div class="form-row">
                <div class="col-md-5 mb-5">
                    <label >Father Name</label>
                    <input type="text" class="form-control" name="fathername" placeholder="Father Name" required>
                </div>             
                <div class="col-md-5 mb-5">
                    <label >Mother Name</label>
                    <input type="text" class="form-control" name="mothername" placeholder="Mother Name" required>
                </div>             
            </div>
            <div class="form-row">   
                <div class="col-md-6 mb-6">
                    <label>Residence address</label>
                    <textarea class="form-control" name="gaddress" rows="2"></textarea>
                </div>             
            </div>
            <div class="form-row">                 
                <div class="col-md-3 mb-3">
                    <label >City</label>
                    <input type="text" class="form-control" name="pcity" placeholder="City" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label >Village</label>
                    <input type="text" class="form-control" name="pvillage" placeholder="Village" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label >Taluka</label>
                    <input type="text" class="form-control" name="ptaluka" placeholder="Taluka" required>
                </div>     
            </div>  
            <div class="form-row">
                <div class="col-md-2 mb-2">
                    <label >District</label>
                    <input type="text" class="form-control" name="pdistrict" placeholder="District" required>
                </div>
                <div class="col-md-2 mb-2">
                    <label>State</label>
                    <input type="text" class="form-control" name="pstate" placeholder="State" required>
                </div>
                <div class="col-md-2 mb-2">
                    <label >Pin code</label>
                    <input type="text" class="form-control" name="ppincode" placeholder="Pin code" required>
                </div>                
                <div class="col-md-2 mb-2">
                    <label >Phone</label>
                    <input type="text" class="form-control" name="pphone" placeholder="Phone" required>
                </div>                   
            </div> 
            <div class="form-row">           
                <div class="col-md-3 mb-3">
                    <label >Fax</label>
                    <input type="text" class="form-control" name="pfax" placeholder="Fax" required>
                </div>  
                <div class="col-md-3 mb-3">
                    <label >Cell</label>
                    <input type="text" class="form-control" name="pcell" placeholder="Cell" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label>Email Id</label>
                    <input type="text" class="form-control" name="pemail" placeholder="Email Id" required>
                </div>                              
            </div>
            <div class="form-row">           
                <div class="col-md-3 mb-3">
                    <label >Father Profession</label>
                    <input type="text" class="form-control" name="fatherprofession" placeholder="Father Profession" required>
                </div>  
                <div class="col-md-3 mb-3">
                    <label >Annual Income</label>
                    <input type="text" class="form-control" name="annualincome" placeholder="Annual Income" required>
                </div>  
                <div class="col-md-2 mb-2">
                    <div class="form-row"><label>Is Creamy layer?</label></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="Yes" value="Yes" required>
                        <label class="form-check-label">Yes</label>
                    </div>                    
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="No" value="No">
                        <label class="form-check-label">No</label>                  
                    </div>                    
                </div>   
                <div class="col-md-2 mb-2"><span>(If yes Attached necessary Certificates)</span></div>                         
            </div>
            <hr>

            <div class="form-row"><h6>Parent’s Company Information</h6></div>
            <div class="form-row">
                <div class="col-md-4 mb-4">
                    <label >Parent’s Company</label>
                    <input type="text" class="form-control" name="pcompany" placeholder="Parent’s Company" required>
                </div>  
                <div class="col-md-7 mb-7">
                    <label>address</label>
                    <textarea class="form-control" name="pcaddress" rows="2"></textarea>
                </div>             
            </div>
            <div class="form-row margin-15">                 
                <div class="col-md-3 mb-3">
                    <label >City</label>
                    <input type="text" class="form-control" name="pccity" placeholder="City" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label >Village</label>
                    <input type="text" class="form-control" name="pcvillage" placeholder="Village" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label >Taluka</label>
                    <input type="text" class="form-control" name="pctaluka" placeholder="Taluka" required>
                </div>     
            </div>  
            <div class="form-row">
                <div class="col-md-2 mb-2">
                    <label >District</label>
                    <input type="text" class="form-control" name="pcdistrict" placeholder="District" required>
                </div>
                <div class="col-md-2 mb-2">
                    <label>State</label>
                    <input type="text" class="form-control" name="pcstate" placeholder="State" required>
                </div>
                <div class="col-md-2 mb-2">
                    <label >Pin code</label>
                    <input type="text" class="form-control" name="pcpincode" placeholder="Pin code" required>
                </div>                
                <div class="col-md-2 mb-2">
                    <label >Phone</label>
                    <input type="text" class="form-control" name="pcphone" placeholder="Phone" required>
                </div>                   
            </div> 
            <div class="form-row">           
                <div class="col-md-2 mb-2">
                    <label >Fax</label>
                    <input type="text" class="form-control" name="pcfax" placeholder="Fax" required>
                </div>  
                <div class="col-md-2 mb-2">
                    <label >Cell</label>
                    <input type="text" class="form-control" name="pccell" placeholder="Cell" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label>Email Id</label>
                    <input type="text" class="form-control" name="pcemail" placeholder="Email Id" required>
                </div>  
                <div class="col-md-3 mb-3">
                    <label >Company Website</label>
                    <input type="text" class="form-control" name="pcwebsite" placeholder="Company Website" required>
                </div>                            
            </div>

            <hr>

            <div class="form-row"><h6>Previous Institute Information</h6></div>
            <div class="form-row">
                <div class="col-md-2 mb-2">
                    <label >Last Attended Class</label>
                    <input type="text" class="form-control" name="lastatndclass" placeholder="Last Attended Class" required>
                </div>             
                <div class="col-md-5 mb-5">
                    <label >Last Attended Institute name</label>
                    <input type="text" class="form-control" name="lastatndinst" placeholder="Last Attended Institute name" required>
                </div>             
            </div>
            <div class="form-row">   
                <div class="col-md-10 mb-10">
                    <label>Name of Board or University</label>
                    <input type="text" class="form-control" name="university" placeholder="Name of Board or University" required>
                </div>             
            </div>
            
            <hr>

            
            <div class="form-row"><h6>Eligibility Criteria</h6></div>
                <div class="form-row">
                    <div class="col-md-8 mb-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="criteria" value="Foreigner" required>
                            <label class="form-check-label">Foreigner</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="criteria" value="N.R.I">
                            <label class="form-check-label">N.R.I</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="criteria" value="Other College" >
                            <label class="form-check-label">Other College</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="criteria" value="Other University" >
                            <label class="form-check-label">Other University</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="criteria" value="SAARC" >
                            <label class="form-check-label">SAARC</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="criteria" value="Same College" >
                            <label class="form-check-label">Same College</label>
                        </div>
                    </div>             
                </div>
                <div class="form-row margin-15">
                    <div class="col-md-4 mb-4">
                        <div class="form-row"><label>Resident Type</label></div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="residenttype" value="Maharashtra State" required>
                                <label class="form-check-label">Maharashtra State</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="residenttype" value="Other State" required>
                            <label class="form-check-label">Other State</label>
                        </div>
                    </div>  
                    <div class="col-md-3 mb-3">
                        <label>Last Qualified Examination</label>
                        <input type="text" class="form-control" name="lastquaexam" placeholder="Last Qualified Examination" required>
                    </div>     
                    <div class="col-md-3 mb-3">
                        <label>Passing year</label>
                        <input type="text" class="form-control" name="passingyeat" placeholder="Passing year" required>
                    </div>        
                </div>
                <div class="form-row">               
                    <div class="col-md-4 mb-4">
                        <label>Name of Examination center</label>
                        <input type="text" class="form-control" name="examcenter" placeholder="Name of Examination center" required>
                    </div> 
                    <div class="col-md-3 mb-3">
                        <label>Result %</label>
                        <input type="text" class="form-control" name="result" placeholder="Result" required>
                    </div>             
                    <div class="col-md-3 mb-3">
                        <label>Seat No</label>
                        <input type="text" class="form-control" name="seatno" placeholder="Seat No" required>
                    </div>             
                </div>
                <div class="form-row">               
                    <div class="col-md-4 mb-4">
                        <label>T.C./L.C. No</label>
                        <input type="text" class="form-control" name="tcno" placeholder="T.C./L.C. No" required>
                    </div> 
                    <div class="col-md-3 mb-3">
                        <label>T.C./L.C. Issue Date</label>
                        <input type="text" class="form-control" name="tcdate" placeholder="T.C./L.C. Issue Date" required>
                    </div>    
                </div>

            <hr>
            
            <div class="form-row"><h6>Current Year Information</h6></div>                
                <div class="form-row margin-15">
                    <div class="col-md-4 mb-4">
                        <div class="form-row"><label>Other Activities</label></div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="otheractivity" value="NSS" required>
                                <label class="form-check-label">NSS</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="otheractivity" value="NCC">
                            <label class="form-check-label">NCC</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="otheractivity" value="None">
                            <label class="form-check-label">None</label>
                        </div>
                    </div>         
                </div>

            <hr>
            
            <div class="form-row"><h6>Concession</h6></div>                
                <div class="form-row margin-15">
                    <div class="col-md-6 mb-6">
                        <div class="form-row"><label>Fee Concession</label></div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="feeConcession" value="G.O.I.S." required>
                                <label class="form-check-label">G.O.I.S.</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="feeConcession" value="G.O.I.F.">
                                <label class="form-check-label">G.O.I.F.</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="feeConcession" value="E.B.C." >
                                <label class="form-check-label">E.B.C.</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="feeConcession" value="Ex-serviceman" >
                                <label class="form-check-label">Ex-serviceman</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="feeConcession" value="F.E.G.">
                                <label class="form-check-label">F.E.G.</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="feeConcession" value="P.T.C.">
                                <label class="form-check-label">P.T.C.</label>
                        </div>
                    </div>   
                    <div class="col-md-4 mb-4">
                        <label>Bank Account</label>
                        <input type="text" class="form-control" name="bankacc" placeholder="Bank Account" required>
                    </div>       
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <div class="form-row"><label>Resident Type</label></div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="recdtype" value="D.S." required>
                                <label class="form-check-label">D.S.</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="recdtype" value="G.h.">
                                <label class="form-check-label">G.H.</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="recdtype" value="P.H.">
                                <label class="form-check-label">P.H.</label>
                        </div>
                    </div>   
                    <div class="col-md-3 mb-3">
                        <div class="form-row"><label>Concession Type</label></div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="concessiontype" value="New" required>
                                <label class="form-check-label">New</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="concessiontype" value="Renewed" required>
                                <label class="form-check-label">Renewed</label>
                        </div>
                    </div>   
                    <div class="col-md-2 mb-2">
                        <label>Award No</label>
                        <input type="text" class="form-control" name="awardno" placeholder="Award No" required>
                    </div>       
                    <div class="col-md-3 mb-3">
                        <label>Maintenance Allowance</label>
                        <input type="text" class="form-control" name="maintenanceallowance" placeholder="Maintenance Allowance" required>
                    </div>       
                </div>

            <hr>
            
            <div class="form-row"><h6>Subject Information</h6></div>                
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <div class="form-row"><label>Medium of Learning</label></div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="medium" value="English" required>
                                <label class="form-check-label">English</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="medium" value="Hindi">
                                <label class="form-check-label">Hindi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="medium" value="Marathi">
                                <label class="form-check-label">Marathi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="medium" value="Urdu">
                                <label class="form-check-label">Urdu</label>
                        </div>
                    </div>                          
                </div>
                <div class="form-row margin-15">
                    <div class="col-md-2 mb-2">
                        <label>1-Subject</label>
                        <input type="text" class="form-control" name="sub1" placeholder="1-Subject">
                    </div>           
                    <div class="col-md-2 mb-2">
                        <label>5-Subject</label>
                        <input type="text" class="form-control" name="sub5" placeholder="5-Subject">
                    </div>           
                    <div class="col-md-2 mb-2">
                        <label>9-Subject</label>
                        <input type="text" class="form-control" name="sub9" placeholder="9-Subject">
                    </div>           
                    <div class="col-md-2 mb-2">
                        <label>13-Subject</label>
                        <input type="text" class="form-control" name="sub13" placeholder="13-Subject">
                    </div>           
                    <div class="col-md-2 mb-2">
                        <label>17-Subject</label>
                        <input type="text" class="form-control" name="sub17" placeholder="17-Subject">
                    </div>           
                </div>
                <div class="form-row">
                    <div class="col-md-2 mb-2">
                        <label>2-Subject</label>
                        <input type="text" class="form-control" name="sub2" placeholder="2-Subject">
                    </div>        
                    <div class="col-md-2 mb-2">
                        <label>6-Subject</label>
                        <input type="text" class="form-control" name="sub6" placeholder="6-Subject">
                    </div>    
                    <div class="col-md-2 mb-2">
                        <label>10-Subject</label>
                        <input type="text" class="form-control" name="sub10" placeholder="10-Subject">
                    </div>           
                    <div class="col-md-2 mb-2">
                        <label>14-Subject</label>
                        <input type="text" class="form-control" name="sub14" placeholder="14-Subject">
                    </div>           
                    <div class="col-md-2 mb-2">
                        <label>18-Subject</label>
                        <input type="text" class="form-control" name="sub18" placeholder="18-Subject">
                    </div>           
                </div>
                <div class="form-row">
                    <div class="col-md-2 mb-2">
                        <label>3-Subject</label>
                        <input type="text" class="form-control" name="sub3" placeholder="3-Subject">
                    </div> 
                    <div class="col-md-2 mb-2">
                        <label>7-Subject</label>
                        <input type="text" class="form-control" name="sub7" placeholder="7-Subject">
                    </div>          
                    <div class="col-md-2 mb-2">
                        <label>11-Subject</label>
                        <input type="text" class="form-control" name="sub11" placeholder="11-Subject">
                    </div>              
                    <div class="col-md-2 mb-2">
                        <label>15-Subject</label>
                        <input type="text" class="form-control" name="sub15" placeholder="15-Subject">
                    </div>         
                    <div class="col-md-2 mb-2">
                        <label>19-Subject</label>
                        <input type="text" class="form-control" name="sub19" placeholder="19-Subject">
                    </div>           
                </div>
                <div class="form-row">
                    <div class="col-md-2 mb-2">
                        <label>4-Subject</label>
                        <input type="text" class="form-control" name="sub4" placeholder="4-Subject">
                    </div>      
                    <div class="col-md-2 mb-2">
                        <label>8-Subject</label>
                        <input type="text" class="form-control" name="sub8" placeholder="8-Subject">
                    </div>      
                    <div class="col-md-2 mb-2">
                        <label>12-Subject</label>
                        <input type="text" class="form-control" name="sub12" placeholder="12-Subject">
                    </div>           
                    <div class="col-md-2 mb-2">
                        <label>16-Subject</label>
                        <input type="text" class="form-control" name="sub16" placeholder="16-Subject">
                    </div>           
                    <div class="col-md-2 mb-2">
                        <label>20-Subject</label>
                        <input type="text" class="form-control" name="sub20" placeholder="20-Subject">
                    </div>           
                </div>

            <hr>
            
            <div class="form-row"><h6>Passport Information (If applicable)</h6></div>                
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <div class="form-row"><label>Passport Type</label></div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="passporttype" value="Permanent" required>
                                <label class="form-check-label">Permanent</label>
                        </div>                        
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="passporttype" value="Temporary">
                                <label class="form-check-label">Temporary</label>
                        </div>                        
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="passporttype" value="Tourist" >
                                <label class="form-check-label">Tourist</label>
                        </div>                        
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="passporttype" value="Educational">
                                <label class="form-check-label">Educational</label>
                        </div>                        
                    </div>         
                </div>
                <div class="form-row margin-15">
                    <div class="col-md-3 mb-3">
                        <label>Country Code</label>
                        <input type="text" class="form-control" name="countrycode" placeholder="Country Code">
                    </div>      
                    <div class="col-md-3 mb-3">
                        <label>Passport Number</label>
                        <input type="text" class="form-control" name="passportnumber" placeholder="Passport Number">
                    </div>      
                    <div class="col-md-3 mb-3">
                        <label>Expiry Date</label>
                        <input type="text" class="form-control" name="expirydate" placeholder="Expiry Date">
                    </div>      
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label>Place of issue</label>
                        <input type="text" class="form-control" name="issueplace" placeholder="Place of issue">
                    </div>      
                    <div class="col-md-3 mb-3">
                        <label>Issue Date</label>
                        <input type="text" class="form-control" name="issuedate" placeholder="Issue Date">
                    </div>      
                    <div class="col-md-3 mb-3">
                        <label>Visa Number</label>
                        <input type="text" class="form-control" name="visanumber" placeholder="Visa Number">
                    </div>      
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label>Nature of Visa</label>
                        <input type="text" class="form-control" name="visanature" placeholder="Nature of Visa:">
                    </div>      
                    <div class="col-md-3 mb-3">
                        <label>Visa Issue Date</label>
                        <input type="text" class="form-control" name="visaissuedate" placeholder="Visa Issue Date">
                    </div>      
                    <div class="col-md-3 mb-3">
                        <label>Expiry Date</label>
                        <input type="text" class="form-control" name="expirydate" placeholder="Expiry Date">
                    </div>      
                </div>

            <hr>

            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                    <label class="form-check-label" for="invalidCheck">
                    Agree to terms and conditions
                    </label>
                    <div class="invalid-feedback">
                        You must agree before submitting.
                    </div>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Submit form</button>
        </form>
    </div>

</body>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

    </html>