<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<style>
    .radio{
             padding-top: 0px !important;
    }
    .hide{
        display:none;
    }
</style>
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('addpgateway'); ?> 
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        echo form_open('account/paymentGateway', $form_attributs);
                        ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('header_sname');?> <span class="requiredStar"> * </span></label>
                            <div class="col-md-6">                          
                                <select class="form-control" name="institute_id" id="institute_id" required>
                                    <option value="">Select Institute Name</option>
                                    <?php foreach($institutes['data'] as $key) { ?>
                                        <option value="<?php echo $key['id'];?>"><?php echo $key['name'];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('pOption');?><span class="requiredStar"> *</span></label>
                            <div class="col-md-6">
                            
                                <label for="payu" class=" control-label">
                                    <input type="radio" class="" name="payment_option" value="payu" id="payu" required>PayU
                                </label> &nbsp;&nbsp;&nbsp;
                                <label for="razorpay">
                                    <input type="radio" name="payment_option" value="razorpay" id="razorpay" required> RazorPay
                                </label>
                            </div>
                        </div>
                        <div class="hide" id="rpay">               
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('mid'); ?> <span class="requiredStar"> *</span></label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="mid" id="mid" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('kid'); ?>  <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" placeholder="" name="kid" id="kid" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('ksecret'); ?>  <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" placeholder="" name="secret" id="secret" >
                                </div>
                            </div>
                        </div>
                        <div class="hide" id="payu1">               
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('mkey'); ?> <span class="requiredStar"> *</span></label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="mkey" id="mkey" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('msalt'); ?>  <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" id="msalt" name="msalt" >
                                </div>
                            </div>
                        </div>

                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" class="btn green" name="submit" value="Submit"><?php echo lang('acc_add'); ?></button>
                                <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>

            <div class="col-md-12">
                <!-- BEGIN All account list-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('listpg'); ?> 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <?php echo lang('srno'); ?>
                                    </th>
                                    <th><?php echo lang('header_add_school');?></th>
                                    <th><?php echo lang('pOption');?></th>
                                    <th>
                                        <?php echo lang('mkey'); ?> 
                                    </th>
                                    <th>
                                        <?php echo lang('msalt'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('mid'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('kid'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('ksecret'); ?>
                                    </th>
                                    <?php if($this->common->user_access('edit_dele_acco',$userId)){ ?>
                                        <th>

                                        </th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($allData as $row) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td><?php $data = $this->common->getSingleField('name','institute','lms_id',$row['institute_id']); 
                                            if($data){echo $data[0]->name;}  ?>
                                        <td><?php echo $row['payment_option']; ?></td>
                                        <td><?php echo $row['merchant_key']; ?></td>
                                        <td><?php echo $row['merchant_salt']; ?></td>
                                        <td><?php echo $row['merchant_id']; ?></td>
                                        <td><?php echo $row['key_id']; ?></td>
                                        <td><?php echo $row['key_secret']; ?></td>
                                        <?php if($this->common->user_access('edit_dele_acco',$userId)){ ?>
                                            <td>
                                                <a class="btn btn-xs default" href="index.php/account/editPaymentGateway?id=<?php echo $row['id']; ?>"> <i class="fa fa-pencil-square-o"></i> <?php echo lang('edit'); ?> </a>
                                                <a class="btn btn-xs red" href="index.php/account/deletepGateway?id=<?php echo $row['id']; ?>" onclick="javascript:return confirm('<?php echo lang('pgdelete'); ?>')"> <i class="fa fa-trash-o"></i> <?php echo lang('delete'); ?> </a>                                            </td>
                                            <?php } ?>
                                    </tr>
                                <?php $i++;} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END All account list-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script>
   $('input[name="payment_option"]').on('change',function(){
            var pOption = $(this).val();
            console.log(pOption);
            if(pOption == "payu"){  
                $('#payu1').removeClass('hide');
                $('#rpay').addClass('hide');
                $('#kid').removeAttr('required');
                $('#mid').removeAttr('required');
                $('#secret').removeAttr('required');
                $("#mkey").prop('required',true);
                $("#msalt").prop('required',true);
            }else{
                $('#payu1').addClass('hide');
                $('#rpay').removeClass('hide');
                $('#mkey').removeAttr('required');
                $('#msalt').removeAttr('required');
                $("#kid").prop('required',true);
                $("#mid").prop('required',true);
                $("#secret").prop('required',true);
            }
        });
</script>

